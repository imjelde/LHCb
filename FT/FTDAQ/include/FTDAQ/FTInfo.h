/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/SystemOfUnits.h"
#include <array>
#include <map>
#include <string>

/** Constant information of the detector
 *  @author Sebastien Ponce
 *  @date   2016-09-30
 */

namespace LHCb::Pr::FT {
  namespace Info {

    enum Numbers { NFTZones = 24, NFTXLayers = 6, NFTUVLayers = 6, NFTLayers = 12, NFTStations = 3 };

    /**
     * the maximum number of possible hits in the SciFiTracker is a hardware limit determined by the
     * output bandwidth of the TELL40. The max number is 45568, round it up to 50k.
     * (for more details ask Sevda Esen and Olivier Le Dortz)
     */
    constexpr inline std::size_t maxNumberHits{50000};

    // max number of mats is motivated by: 2 sides * 12 layers * 12 modules * 8 mats
    constexpr inline std::size_t maxNumberMats  = 2304;
    constexpr inline float       triangleHeight = 22.7f * float{Gaudi::Units::mm};

    constexpr unsigned int nbZones() { return Numbers::NFTZones; }

    const inline std::string FTHitsLocation    = "FT/FTHits";
    const inline std::string SciFiHitsLocation = "FT/SciFiHits";
    const inline std::string FTCondLocation    = "Conditions/FT";
    const inline std::string FTZonesLocation   = "Conditions/FT/FTZones";

    // layer structure of the FT det
    constexpr inline auto xZonesUpper = std::array{1, 7, 9, 15, 17, 23};
    constexpr inline auto xZonesLower = std::array{0, 6, 8, 14, 16, 22};

    constexpr inline auto uvZonesUpper = std::array{3, 5, 11, 13, 19, 21};
    constexpr inline auto uvZonesLower = std::array{2, 4, 10, 12, 18, 20};

    constexpr inline auto ZonesLower = std::array{0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20, 22};

    constexpr inline auto stereoZones = std::array{2, 3, 4, 5, 10, 11, 12, 13, 18, 19, 20, 21};

    constexpr inline auto xLayers       = std::array{0, 3, 4, 7, 8, 11};
    constexpr inline auto stereoLayers  = std::array{1, 2, 5, 6, 9, 10};
    constexpr inline auto xStereoLayers = std::array{0, 3, 4, 7, 8, 11, 1, 2, 5, 6, 9, 10};
    constexpr inline auto isXLayer =
        std::array{true, false, false, true, true, false, false, true, true, false, false, true};

    enum UpperZones {
      T1X1 = xZonesUpper[0],
      T1U  = uvZonesUpper[0],
      T1V  = uvZonesUpper[1],
      T1X2 = xZonesUpper[1],
      T2X1 = xZonesUpper[2],
      T2U  = uvZonesUpper[2],
      T2V  = uvZonesUpper[3],
      T2X2 = xZonesUpper[3],
      T3X1 = xZonesUpper[4],
      T3U  = uvZonesUpper[4],
      T3V  = uvZonesUpper[5],
      T3X2 = xZonesUpper[5],
    };
  } // namespace Info
} // namespace LHCb::Pr::FT

namespace PrFTInfo = LHCb::Pr::FT::Info;
