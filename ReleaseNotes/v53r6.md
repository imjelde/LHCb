2022-01-27 LHCb v53r6
===

This version uses
Gaudi [v36r4](../../../../Gaudi/-/tags/v36r4) and
LCG [101](http://lcginfo.cern.ch/release/101/) with ROOT 6.24.06.

This version is released on `master` branch.
Built relative to LHCb [v53r5](/../../tags/v53r5), with the following changes:

### New features ~"new feature"

- ~Configuration | Add monitoring_file option (to dump data in json format), !3219 (@ziyiw)
- ~FT ~Conditions | DD4Hep implementation of FT, !3264 (@lohenry) :star:
- ~"Event model" ~Utilities | Hierarchical SOACollections, !3353 (@ahennequ)
- ~Persistency | Added support for compressed MDF in IOAlg, !3347 (@sponce)
- Newly Created Sink algorithm (Phoenix), !3241 (@anpappas)


### Fixes ~"bug fix" ~workaround

- ~Configuration | Fix output location UnpackMC, !3352 (@dfazzini)
- ~Decoding ~RICH | RichDAQ - Decoding fixes when inactive links are present, !3300 (@jonrob)
- ~FT ~Monitoring | Fix monitoringIDs in FTChannelID, !3325 (@shollitt)
- ~Persistency | Implemented support for large streams in ZSTD compression, !3360 (@sponce)
- ~Conditions | IConditionDerivationMgr - Extend type checks to handle CopyWrapper<OUT>, !3346 (@jonrob)
- ~Build | Improve unused sources detection and fix warnings, !3356 (@clemenci) [#171]


### Enhancements ~enhancement

- ~Configuration | Adding set_hltAnn_svc function in GaudiConf.reading, !3377 (@dfazzini)
- ~Tracking ~"Event model" | Track v3 modifications, !3232 (@decianm)
- ~Calo | Handling of missing fibers, !3342 (@jmarchan)
- ~Calo ~"Event model" | Change doubles to floats in calo event model, !3348 (@cmarinbe)
- ~Functors | Add pid accessor to Particle v1 for ADL, !3374 (@poluekt)
- ~"Event model" | Use CUDA- or HIP-specific memcpy when compiling for GPU, !3358 (@lmeyerga)
- ~"Event model" | Add endVertexPos to RecVertex, !3354 (@pkoppenb)
- ~"Event model" | Augment Particle v1 to make it forward compatible with v2, !3338 (@graven) [Rec#223]
- ~"Event model" | SOACollection's proxy features, !3333 (@ahennequ)
- ~"Event model" ~Utilities | MCParticle opt-in to (subset of) ThOr functors, use SIMDWrapper types as early as possible, !3367 (@graven) [Rec#223]
- ~Persistency | Use rawbank views for sprucing, !3303 (@nskidmor)
- ~Conditions | Extended DeVP to be able to retrieve the module of a given sensor, !3339 (@sponce)
- ~Utilities | Add SIMD versions of abs(int) and operator-(int), !3362 (@poluekt)
- ~Utilities | Move TMV Utils To LHCbMath Kernel, !3350 (@gunther)
- DD4hepSvc Option to print DD4hep conditions in message stream after loading a slice, !3373 (@bcouturi)
- Add support for parsing vectors of enums in TrackEnums, !3324 (@graven)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Tracking | Drop SOA_ACCESSOR macro from PrSciFiHits, !3372 (@gunther)
- ~UT | Removal of UT common base classes, !3369 (@sponce)
- ~Muon | Make Muon code ready for DD4hep integration, !3309 (@sponce)
- ~RICH | RICH Reco Preparations for DD4Hep support, !3355 (@jonrob)
- ~"Event model" | Rec#223: Unify accessors using ADL, !3359 (@graven) [Rec#223]
- Fix DD4hep cond DB access tests to set version for new cond DB only, !3382 (@bcouturi)
- Move away from using CERN-SWTEST, !3375 (@rmatev)
- Refactor: SOACollection tidy up, !3370 (@chasse)
- Fix clang warning (follow up !3355), !3361 (@rmatev)


### Documentation ~Documentation

- Add complete documentation for the Phoenix Sink, !3371 (@anpappas)
