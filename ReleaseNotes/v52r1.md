2021-05-27 LHCb v52r1
===

This version uses
Gaudi [v35r4](../../../../Gaudi/-/tags/v35r4) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to LHCb [v52r0](/../../tags/v52r0), with the following changes:

### New features ~"new feature"

- ~Configuration | Add minor quality-of-life enhancements to LHCbApp, !2963 (@apearce) [#3]
- ~"Event model" | Add a Generic Track SOA Container, !2920 (@agilman) [#97] :star:
- ~"Event model" ~Persistency | Add RawBank types for Run 3: CaloSpecial, Plume, PlumeSpecial and PlumeError, !3006 (@gvouters)
- ~Utilities | Add integer XOR, NOT, elementwise popcount and to_array to SIMDWrapper, !2970 (@gunther)
- ~Utilities | MatVec operations for BPVLTIME functor, !2910 (@vrenaudi)
- Add functional raw event combiner, !3043 (@dovombru) [#101]


### Fixes ~"bug fix" ~workaround

- ~Configuration | Only look for depth specifiers at string ends, !3066 (@apearce) [#134]
- ~Configuration | Add tools to Algorithm inputs and use caching, !3003 (@apearce) [#126]
- ~Tracking ~UT | Unify treatment of sector search for UT-based pattern reco, !2990 (@decianm)
- ~Persistency | Remove default type for RawBank::range, !3021 (@graven) [#132]
- ~Build | Randomise default File Catalog name, !3004 (@cattanem)


### Enhancements ~enhancement

- ~Configuration | PyConf compatibility with Multi Event Scheduler, !2964 (@dcampora)
- ~Configuration | Add possibility to lock an option slot of ApplicationOptions, !2961 (@chasse)
- ~Calo ~"Event model" | Implement remove_if in CaloClusters_v2, !2971 (@cmarinbe)
- ~Persistency | MDF writing functionallity for Moore, !2978 (@sesen) [Moore#228]
- ~Persistency | Configurable ANNSvc and expose PR descriptor names, !2965 (@nskidmor)
- ~Core | Add new platforms strings to PlatformInfo, !3062 (@clemenci)
- ~Conditions | Small fixes for DD4hep support of alignment, !2998 (@sponce)
- ~Conditions | Fixes usage of handle with DDhep, !2949 (@sponce)


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Configuration | Make comparison in warning printout more robust, !3033 (@cattanem)
- ~Decoding | Tweak UTDecoder, !2997 (@graven)
- ~Decoding | Unify UT decoders + strong enum UTDAQ::version, !2986 (@graven)
- ~Decoding ~UT | Tweak UTDecoder (follow up !2997), !3050 (@graven)
- ~FT | Remove unused DeFTMat::calculateChannels, !2958 (@graven)
- ~Calo | Calo{Cluster,Hypo}_v2: streamline range and iterator implementations, !2987 (@graven)
- ~"Event model" | Modernize old GOD-generated event headers, !3058 (@graven)
- ~Persistency | Prefer addBank over createBank/adoptBank, !2994 (@graven)
- ~Persistency | Remove unsafe Warning(...) counter to avoid race condition in BankKiller, !2989 (@chasse)
- ~Persistency | Prefer `Rawbank View` over explicit `span<RawBank const* const>`, !2975 (@graven)
- ~Core | Fix to known platforms and clang 11 warning, !3073 (@rmatev)
- ~Core | Adapt HLTControlFlowManager to range v3 0.11, !2981 (@graven) [LBCORE-2001]
- ~Core | Change the XMLSummary ref files to work for Python 3.8, !2917 (@clemenci)
- ~Conditions | Remove remnant from COOL to GitCondDB migration, !2982 (@cattanem)
- ~Utilities | Add constraint to template arguments of {stable,}partitionPosition, !2999 (@graven)
- ~Utilities | Tweak SIMDWrappers, !2988 (@graven)
- ~Utilities | Adapt MultiIndexedContainer to range-v3 0.11, !2985 (@graven) [LBCORE-2001]
- Add a RawBank::types() which returns a range of RawBank::BankTypes, !3059 (@graven)
- Modernize UT, !3054 (@graven)
- Remove algorithm correlations, !2960 (@pkoppenb)


### Documentation ~Documentation

- Update CONTRIBUTING.md to update supported platforms, !3020 (@cattanem)
- Add some readme for SOACollection, !2992 (@decianm)

### Other

- ~Configuration ~Persistency | Support specifying barrier algorithms in PyConf, !2954 (@apearce) [Moore#145]
- ~Decoding ~UT | Getters for UT decoding in Allen, !2924 (@mstahl)
- ~Calo ~Conditions | Get zero suppresison parameters from DB, !2945 (@jmarchan)
- Add check for UT rawbank size consistency, !3063 (@graven)
- Extend the unknown-attributes warning workaround to ROOT 6.24, !3048 (@clemenci)
- Revert "Merge branch 'unify-utdecoders' into 'master'", !3037 (@rmatev)
- Flexible UT hits container, !3035 (@decianm)
- Add RawBank::View type, and make it the output type of UnpackRawEvent, !2969 (@graven)
