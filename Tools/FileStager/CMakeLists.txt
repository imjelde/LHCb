###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Tools/FileStager
----------------
#]=======================================================================]

gaudi_add_library(FileStagerFunctions
    SOURCES
        src/Lib/FileStagerFunctions.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
        PRIVATE
            Boost::headers
            Boost::regex
)

gaudi_add_module(FileStager
    SOURCES
        src/Component/File.cpp
        src/Component/FileStagerSvc.cpp
        src/Component/StagedIODataManager.cpp
        src/Component/StagedStreamTool.cpp
    LINK
        Boost::date_time
        Boost::filesystem
        Boost::headers
        Boost::iostreams
        Boost::regex
        Boost::thread
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::FileStagerFunctions
)

gaudi_add_dictionary(FileStagerDict
    HEADERFILES dict/FileStagerDict.h
    SELECTION dict/FileStagerDict.xml
    LINK LHCb::FileStagerFunctions
)

gaudi_add_executable(garbage
    SOURCES
        src/app/garbage.cpp
    LINK
        Boost::filesystem
        Boost::date_time
        Boost::program_options
        Boost::thread
)

gaudi_add_executable(hash_filename
    SOURCES
        src/app/hash_filename.cpp
    LINK
        Boost::filesystem
        Boost::headers
        Boost::program_options
        Boost::regex
        LHCb::FileStagerFunctions
)

gaudi_install(PYTHON)

gaudi_add_tests(QMTest)
