/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files
// Gaudi
#include <GaudiKernel/GaudiException.h>
#include <GaudiKernel/IIncidentListener.h>
#include <GaudiKernel/Service.h>
#include <GaudiKernel/StateMachine.h>

// ZeroMQ
#include <ZeroMQ/IZeroMQSvc.h>
#include <zmq/zmq.hpp>

/** @class ZeroMQSvc ZeroMQSvc.h
 *
 *
 *  @author Roel Aaij
 *  @date   2015-06-22
 */
class ZeroMQSvc : public extends1<Service, IZeroMQSvc> {
public:
  /// Standard constructor
  ZeroMQSvc( const std::string& name, ISvcLocator* sl ) : base_class{name, sl} {}

  // Service pure virtual member functions
  StatusCode initialize() override;

  zmq::context_t& context() const override;

  zmq::socket_t socket( int type ) const override {
    auto socket = zmq::socket_t{context(), type};
    int  period = 0;
    socket.setsockopt( zmq::LINGER, &period, sizeof( period ) );
    return socket;
  }

  Encoding encoding() const override { return m_enc; }

private:
  Gaudi::Property<int>         m_nThreads{this, "NThreads", 1};
  Gaudi::Property<std::string> m_encoding{this, "Encoding", "text"};
  Encoding                     m_enc;

  mutable std::unique_ptr<zmq::context_t> m_context;
};

// Factory for instantiation of service objects
DECLARE_COMPONENT( ZeroMQSvc )

//=============================================================================
StatusCode ZeroMQSvc::initialize() {
  auto sc = Service::initialize();
  if ( !sc.isSuccess() ) return sc;

  if ( m_encoding == "text" ) {
    m_enc = IZeroMQSvc::Text;
  } else if ( m_encoding == "binary" ) {
    m_enc = IZeroMQSvc::Binary;
  } else {
    error() << "Bad encoding specified: " << m_encoding << ". Must be text or binary." << endmsg;
    return StatusCode::FAILURE;
  }
  return sc;
}

//=============================================================================
zmq::context_t& ZeroMQSvc::context() const {
  if ( !m_context ) { m_context.reset( new zmq::context_t{m_nThreads} ); }
  return *m_context;
}
