###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from .dataflow import DataHandle
from .components import Algorithm, Tool, _json_dump
from .control_flow import CompositeNode
from . import tonic


@tonic.add_cache_serializer
def _serializer(obj):
    if isinstance(obj, (Algorithm, Tool, DataHandle, CompositeNode)):
        # only hash objects where the hash depends on the contents
        return type(obj).__name__ + '#' + str(hash(obj))
    return _json_dump(obj)
