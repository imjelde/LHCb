###############################################################################
# (c) Copyright 2019-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from __future__ import absolute_import, division, print_function
import datetime
import logging
import math
import os
import re
from collections import OrderedDict
from six import string_types
import pydot
import six
import sys

from Gaudi.Configuration import (appendPostConfigAction, ConfigurableUser,
                                 Configurable, INFO)
from Configurables import AuditorSvc, ChronoStatSvc
import GaudiKernel.ProcessJobOptions
from GaudiConf import IOHelper
from RawEventFormat import Raw_location_db

from . import ConfigurationError
from .components import Algorithm, setup_component, is_algorithm, force_location
from .control_flow import CompositeNode
from .dataflow import dataflow_config, ensure_event_prefix
from .tonic import configurable

from .Algorithms import (
    createODIN,
    LHCb__MDFWriter,
    OutputStream,
    CopyInputStream,
    CallgrindProfile,
)
from Configurables import (
    # FIXME(NN): We shouldn't need to refer to IOV explicitly in our framework
    LHCb__DetDesc__ReserveDetDescForEvent as reserveIOV,
    LHCb__MDF__IOAlg,
    LHCb__Tests__FakeEventTimeProducer as DummyEventTime,
    Gaudi__Hive__FetchDataFromFile,
    LHCb__UnpackRawEvent,
    ApplicationMgr,
    NTupleSvc,
    DataOnDemandSvc,
    Gaudi__Monitoring__MessageSvcSink as MessageSvcSink,
    Gaudi__Monitoring__JSONSink as JSONSink)
from DDDB.CheckDD4Hep import UseDD4Hep
if UseDD4Hep:
    from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

log = logging.getLogger(__name__)
MDF_KEY = 'MDF'
ROOT_KEY = 'ROOT'
#: Valid input/output filetypes
FILE_TYPES = {MDF_KEY, ROOT_KEY}


def mdf_writer(path, location, compression=0):
    """Return an MDF writer which writes a single  TES `location` to `path`.

    By default no compression is used, since `IOSvc` does not currently support
    compressed inputs and also because there is already compression internally
    to the "DstData" raw banks (which dominate when no detector raw banks are
    written out). See https://gitlab.cern.ch/lhcb/LHCb/-/issues/163.
    """

    return LHCb__MDFWriter(
        BankLocation=location,
        Compress=compression,
        Connection="file://{}".format(path),
    )


def online_writer(location):
    """Return an OnlineAlg writer which writes a single  TES `location`."""
    from PyConf.Algorithms import Online__OutputAlg, Online__RawEventToBanks

    # Grab the RawGuard output of the Online__InputAlg component
    # TODO find a less ugly/fragile way to do that
    guard = default_raw_event(
        bank_types=["VP"], maker=make_raw_event_with_Online
    ).producer.inputs["RawData"].producer.RawGuard

    raw_data = Online__RawEventToBanks(RawEvent=location).RawData
    return Online__OutputAlg(
        name="EventOutput",
        RawData=raw_data,
        # outputs={'RawGuard': force_location(location)},
        RawGuard=guard,  # TODO this should also work
    )


def format_output_location(l, add_depth=True):
    """Return the TES location `l` formatted for an output writer.

    Args:
        l (str or DataHandle)
    """
    try:
        l = l.location
    except AttributeError:
        pass
    # Ensure the location ends with the depth specification `#N` or `#*`
    if add_depth and not re.match(r".*#(\d+|\*)$", l):
        l = "{}#1".format(l)
    return l


def root_writer(path, locations):
    """Return a ROOT/DST writer which writes TES `locations` to `path`."""
    locations = [format_output_location(l) for l in locations]
    return OutputStream(
        OptItemList=locations,
        Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
            path),
    )


def root_copy_input_writer(path,
                           copy_input_leaves,
                           locations=None,
                           tes_veto_locations=None):
    """Return algorithm for writing ROOT/DST files.

    The writer will write not only the locations specified in `locations`,
    but also the locations collected by `Gaudi::Hive::FetchLeavesFromFile`.
    By scheduling the latter as the very first producer, one can copy all
    input file locations to the output.

    Args:
        path (str): Path the output file should be written to.
        copy_input_leaves (DataHandle): Output of `FetchLeavesFromFile`
            (the input locations to copy).
        locations (list of str): TES locations to write.
        tes_veto_locations (list of str): TES locations that should not
            be propagated from the input.

    Returns:
        The writer algorithm to typically be scheduled last.

    """
    locations = [format_output_location(l) for l in locations or []]
    tes_veto_locations = [
        format_output_location(l, add_depth=False)
        for l in tes_veto_locations or []
    ]
    writer = CopyInputStream(
        InputFileLeavesLocation=copy_input_leaves,
        OptItemList=locations,
        TESVetoList=tes_veto_locations,
        Output="DATAFILE='{}' SVC='Gaudi::RootCnvSvc' OPT='RECREATE'".format(
            path),
    )
    return writer


_ApplicationOptions_locked = False
_ApplicationOptions_lockedSlots = {}


class ApplicationOptions(ConfigurableUser):
    """Holder for application configuration.

    Configuration can be mutated until `.finalize()` is called. At that
    point any dynamic defaults based on other properties are resolved.
    """

    # FIXME(RM) we can improve upon the base class ConfigurableUser by creating
    # a new base clase that
    # - it more pythonic (e.g. no __slots__, __doc__)
    # - does better type checking of the properties
    # - makes `c.prop` work if prop hasn't been set
    # - supports namespaced/grouped options
    # - forbids a subsequent instantiation of ApplicationOptions such that one
    #   has to do `from ... import app` rather than
    #   `from ... import ApplicationOptions; ApplicationOptions().xxx`
    # - nicer printout of the options
    # - by default do not overwrite, i.e.
    #   opts.evt_max = 1; opts.evt_max = 2; assert opts.evt_max == 1;
    #   opts.evt_max.force(3); assert opts.evt_max == 3
    #   OR
    #   opts.evt_max.set_if_not_set(x) OR opts.evt_max.default(x)
    # - ...

    def __setattr__(self, attr, val):
        if attr in _ApplicationOptions_lockedSlots:
            raise ConfigurationError(
                "Can't change property {}! It was locked in: {}".format(
                    attr, _ApplicationOptions_lockedSlots[attr]))
        super(ApplicationOptions, self).__setattr__(attr, val)

    def lockOption(self, option):
        """make a specific option entry immutable

        Args:
            option (str): name of option entry to lock
        """
        if option not in self.__slots__:
            raise ConfigurationError(
                "Trying to lock unkown option {}".format(option))

        global _ApplicationOptions_lockedSlots
        if option in _ApplicationOptions_lockedSlots:
            raise ConfigurationError(
                "Property {} was already locked in: {}".format(
                    option, _ApplicationOptions_lockedSlots[option]))

        try:
            import inspect
            doc = inspect.stack()[1][0].f_code.co_filename
        except:
            doc = "callsite couldn't be determined"

        _ApplicationOptions_lockedSlots[option] = doc

    __slots__ = {
        # input related
        'input_files': [],  # type is list of str
        'input_type': '',  # TODO use enum
        'input_raw_format': 0.5,
        'use_iosvc': False,
        'evt_max': -1,
        'first_evt': 0,
        'print_freq': 10000,
        # number of events to pre-fetch if use_iosvc=True, the default value
        # is reasonable for most machines; it might need to be increased for
        # more modern/powerful machines
        'buffer_events': 20000,
        # conditions for processing
        'data_type': 'Upgrade',
        'dddb_tag': '',
        'conddb_tag': '',
        'simulation': True,
        # output related
        'output_file': '',
        'output_type': '',
        'histo_file': '',
        'ntuple_file': '',
        'output_level': INFO,
        'python_logging_level': logging.INFO,
        # - dump monitoring entities (counters, histograms, etc.)
        'monitoring_file': '',
        # multithreaded processing
        'n_threads': 1,
        'event_store': 'HiveWhiteBoard',  # one of EvtStoreSvc, HiveWhiteBoard
        'n_event_slots': -1,  # defaults to 1.2 * n_threads
        # if false scheduler calls Algorithm::execute instead of Algorithm::sysExecute,
        # which breaks some non-functional algorithms
        'scheduler_legacy_mode': True,
        # estimated size of the per-event memory pool. set to zero to disable the pool.
        'memory_pool_size': 10 * 1024 * 1024,
        # TODO such defaults can be expressed here explicitly
        # debugging
        # Control flow file name; not generated if empty
        'control_flow_file': '',
        # Data flow file name; not generated if empty
        'data_flow_file': '',
        # Enables callgrind profiling
        'callgrind_profile': False,
        'msg_svc_format': '% F%35W%S %7W%R%T %0W%M',
        'msg_svc_time_format': '%Y-%m-%d %H:%M:%S UTC',
        # Moore-specific options
        # TODO move to a Moore-specific ApplicationOptions class
        'tck': 0x0,
        # TODO move to a Moore-specific ApplicationOptions class
        'lines_maker': None,
        # Optional Auditors. Disabled (empty) by default.
        'auditors': [],
        # phoenix file name; not generated if empty
        'phoenix_filename': ''
    }

    _propertyDocDct = {
        'input_raw_format':
        ('sets the expected raw input format (splitting)'
         'See definitions at '
         'https://gitlab.cern.ch/lhcb-datapkg/RawEventFormat/blob/master/python/RawEventFormat/__init__.py'
         ),
        'use_iosvc':
        'Use an alternative, faster IIOSvc implementation for MDFs.',
        'event_store':
        'Event store implementation: HiveWhiteBoard (default) or EvtStoreSvc (faster).',
        'auditors':
        'Define list of auditors to run. Possible common choices include "NameAuditor", "MemoryAuditor" or "ChronoAuditor". For a full list see Gaudi documentation.',
        'phoenix_filename':
        ' define the file where phoenix event data are writen. Defaults to none = no phoenix output'
    }

    def _validate(self):
        """Raise an exception if the options are not consistent."""

        # TODO validate separately the case of input_type == "NONE"
        #      since now tags are not used in that case.

        # Configuration is ill-defined without these properties
        required = ["input_type", "dddb_tag", "conddb_tag"]
        not_set = [attr for attr in required if not getattr(self, attr)]
        if not_set:
            raise ConfigurationError(
                "Required options not set: {}".format(not_set))

        if self.input_files:
            if self.input_type not in ('MDF',
                                       'Online') and self.n_event_slots > 1:
                # TODO use a different exception here
                raise ConfigurationError(
                    "Only MDF files can run in multithreaded mode. Please "
                    "change the number of eventslots to 1")

        # TODO check if input raw_format makes sense for Online

        if self.output_file:
            assert self.n_event_slots == 1, 'Cannot write output multithreaded'
            assert self.n_threads == 1, 'Cannot write output multithreaded'

            assert self.output_type in FILE_TYPES, (
                'Output filetype not supported: {}'.format(self.output_type))

    def _set_defaults(self):
        if self.n_event_slots <= 0:
            self.n_event_slots = (math.ceil(1.2 * self.n_threads)
                                  if self.n_threads > 1 else 1)

    def finalize(self):
        """Finalize configuration and prevent further changes."""
        global _ApplicationOptions_locked
        if _ApplicationOptions_locked:
            return
        _ApplicationOptions_locked = True

        # Never filter messages in the root logger's handler.
        # A non-root logger can thus have a lower level if set explicitly.
        GaudiKernel.ProcessJobOptions.GetConsoleHandler().enable(0)
        # applyConfigurableUsers() is too loud with info(), so silence it
        if '--debug' not in sys.argv:
            GaudiKernel.Configurable.log.setLevel(logging.WARNING)
        # If python_logging_level is set, pass it to the root logger.
        # Otherwise, gaudirun.py would set INFO or DEBUG (with --debug).
        if self.isPropertySet('python_logging_level'):
            logging.getLogger().setLevel(self.getProp('python_logging_level'))

        # Workaround ConfigurableUser limitation
        for name, default in self.getDefaultProperties().items():
            if not self.isPropertySet(name):
                self.setProp(name, default)

        self._set_defaults()
        self._validate()
        if self.python_logging_level <= logging.INFO:
            print(self, flush=True)

    def set_conds_from_testfiledb(self, key):
        """Set DDDB and CondDB tags, data type and simulation flag
        from a TestFileDB entry.

        Args:
          key (str): Key in the TestFileDB.
        """
        from PRConfig.TestFileDB import test_file_db

        qualifiers = test_file_db[key].qualifiers
        self.data_type = qualifiers['DataType']
        self.simulation = qualifiers['Simulation']
        self.dddb_tag = qualifiers['DDDB']
        self.conddb_tag = qualifiers['CondDB']

    def set_input_from_testfiledb(self, key):
        """Set input file paths and file type from a TestFileDB entry.

        Args:
          key (str): Key in the TestFileDB.
        """
        from PRConfig.TestFileDB import test_file_db
        file_format = test_file_db[key].qualifiers['Format']
        self.input_files = test_file_db[key].filenames
        self.input_type = 'ROOT' if file_format != 'MDF' else file_format

    def set_input_and_conds_from_testfiledb(self, key):
        """Set input and conditions according to a TestFileDB entry.

        Args:
          key (str): Key in the TestFileDB.
        """
        self.set_input_from_testfiledb(key)
        self.set_conds_from_testfiledb(key)

    def applyConf(self):
        # we should not do anything here
        raise NotImplementedError(
            'The {} configurable should not be called'.format(
                self.__class__.__name__))

    def getProperties(self):
        """Stable-order override of Configurable.getProperties."""
        props = super(ApplicationOptions, self).getProperties()
        return OrderedDict(sorted(props.items()))


def all_nodes_and_algs(node, recurse_algs=False):
    """Return the list of all reachable nodes and algorithms."""
    if not isinstance(node, CompositeNode):
        raise TypeError('{} is not of type CompositeNode'.format(node))
    # use OrderedDict as there is no OrderedSet is the standard library
    nodes = OrderedDict()
    algs = OrderedDict()
    _all_nodes_and_algs(node, recurse_algs, nodes, algs)
    return list(nodes), list(algs)


def _all_nodes_and_algs(node, recurse_algs, nodes, algs):
    if node in nodes:
        return
    nodes[node] = None
    for c in node.children:
        if isinstance(c, CompositeNode):
            _all_nodes_and_algs(c, recurse_algs, nodes, algs)
        elif is_algorithm(c):
            algs[c] = None
            if recurse_algs:
                algs.update({a: None for a in c.all_producers()})
        else:
            raise TypeError(
                'Child {!r} of {!r} is neither a CompositeNode nor an '
                'Algorithm'.format(c, node))


class ComponentConfig(dict):
    """Object holding the configuration of Gaudi Configurables."""

    def add(self, component):
        self[component.getFullName()] = component
        return component

    def update(self, other):
        for c in other.values():
            self.add(c)


def make_data_with_FetchDataFromFile(location, bank_type=None):
    """Return input data read using `FetchDataFromFile`.

    `FetchDataFromFile` preloads data on the transient event store for
    a given list of locations that must be present in the input file.
    This maker configures an instance to read a single given location
    and returns the `DataHandle` corresponding to the output.

    Args:
        location (str): Location to be fetched from input data.
    """
    # Since we return the location directly from output_transform, we
    # need to ensure it's consistent (same as DataHandle.location does).
    location = ensure_event_prefix(location)
    return Algorithm(
        Gaudi__Hive__FetchDataFromFile,
        outputs={'Output': force_location(location)},
        output_transform=lambda Output: {"DataKeys": [location]},
        require_IOVLock=False).Output


def make_raw_event_with_IOAlg(location,
                              bank_type=None):  #TODO What this means exactly
    # TODO what if we're called twice with a different location?
    return Algorithm(
        LHCb__MDF__IOAlg,
        outputs={
            "RawEventLocation": force_location(location),
            "RawBanksBufferLocation": None,
        },
        IOSvc="LHCb::MDF::IOSvcMM/LHCb__MDF__IOSvcMM").RawEventLocation


def make_raw_event_with_Online(location, bank_type=""):
    from PyConf.Algorithms import Online__InputAlg, Online__BanksToRawEvent
    # TODO what if we're called twice with a different location?
    alg = Online__InputAlg(
        outputs={
            "RawData": force_location(location),
            "RawGuard": None,
            "DAQErrors": None,
        },
        DeclareData=True,  # TODO what does this do?
        DeclareEvent=True,  # TODO what does this do?
        DeclareErrors=False,
    )
    return Online__BanksToRawEvent(
        name="Online__BanksToRawEvent" + bank_type,
        RawData=alg.RawData,
        BankType=bank_type).RawEvent


@configurable
def default_raw_event(bank_types=[],
                      raw_event_format=None,
                      maker=make_data_with_FetchDataFromFile):
    """Return a raw event that contains a given set of banks.

    Args:
        bank_types (list): Required raw bank types.
            Defaults to all types (empty list).
        raw_event_format: RawEventFormat key in `Raw_location_db`.
        maker: Maker of input data.
            Defaults to `make_data_with_FetchDataFromFile`.

    Returns:
        DataHandle: Raw event containing banks of the requested types.
    """

    if len(bank_types) != 1:
        raise ValueError("bank_types needs to hold exactly one item")
    if isinstance(bank_types, six.string_types):
        raise TypeError('bank_types must be an iterable of str')
    bank_types = list(bank_types)

    if raw_event_format is None:
        raise ValueError('raw_event_format is required (must be bound)')

    raw_bank_locations = Raw_location_db[raw_event_format]
    # Raw_location_db entries might be lists => make everything a tuple
    raw_bank_locations = {
        k: (v, ) if isinstance(v, string_types) else tuple(v)
        for k, v in raw_bank_locations.items()
    }

    if bank_types:
        locations = set(
            raw_bank_locations.get(bt, ("DAQ/RawEvent", ))
            for bt in bank_types)
    else:
        locations = set(raw_bank_locations.values())

    if len(locations) > 1:
        raise ValueError(
            'Requested raw banks ({}) are split among multiple locations ({}) '
            'for raw event format {}. Restrict raw bank types or merge raw '
            'events manually.'.format(bank_types, locations, raw_event_format))
    location = locations.pop()
    if len(location) > 1:
        raise NotImplementedError(
            'Alternatives are not supported ({} -> {})'.format(
                bank_types, location))
    return maker(location[0], bank_type=bank_types[0])


@configurable
def default_raw_banks(bank_type, make_raw=default_raw_event):
    return Algorithm( LHCb__UnpackRawEvent,
                      RawEventLocation = make_raw( [ bank_type ] ),
                      BankTypes = [bank_type],
                      output_transform = lambda Output: { "RawBankLocations":[Output] },
                      require_IOVLock = False
                    ).Output


@configurable
def make_odin(make_raw=default_raw_banks):
    return createODIN(RawBanks=make_raw('ODIN')).ODIN


@configurable
def make_callgrind_profile(start=10,
                           stop=90,
                           dump=90,
                           dumpName='CALLGRIND-OUT'):
    """Return algorithm that allows to use callgrind profiling.

    For more info see CodeAnalysisTools_.

    .. _CodeAnalysisTools: https://twiki.cern.ch/twiki/bin/view/LHCb/CodeAnalysisTools

    Args:
        start: Event count at which callgrind starts profiling.
            Defaults to 10 events.
        stop: Event count at which callgrind stops profiling.
            Defaults to 90 events.
        dump: Event count at which callgrind dumps results.
            Defaults to 90 events.
        dumpName: Name of dump.
            Defaults to 'CALLGRIND-OUT'.
    """
    return CallgrindProfile(
        StartFromEventN=start,
        StopAtEventN=stop,
        DumpAtEventN=dump,
        DumpName=dumpName)


def configure_input(options):
    """Configure all aspects related to application inputs.

    Args:
        options (ApplicationOptions): Application options instance.

    Returns:
        ComponentConfig: Dict of configured Gaudi Configurable instances.
    """

    options.finalize()
    config = ComponentConfig()

    event_selector = "EventSelector"
    if options.use_iosvc or options.input_type == "NONE":
        event_selector = "NONE"

    config.add(ApplicationMgr(EvtSel=event_selector, EvtMax=options.evt_max))

    if options.input_type == "NONE":
        return config

    # TODO split out conditions from configure_input
    config.add(
        setup_component(
            'DDDBConf',
            Simulation=options.simulation,
            DataType=options.data_type))
    config.add(
        setup_component(
            'CondDB',
            Upgrade=True,
            Tags={
                'DDDB': options.dddb_tag,
                'SIMCOND': options.conddb_tag,
            }))

    if options.input_type == 'Online':
        if options.input_files:
            raise ConfigurationError("input_files must be empty for Online")
        default_raw_event.global_bind(
            raw_event_format=options.input_raw_format,
            maker=make_raw_event_with_Online)
    elif options.use_iosvc:
        if not options.input_files:
            raise ConfigurationError(
                "Cannot set use_iosvc with an empty input_files list")
        config.add(
            setup_component(
                'LHCb__MDF__IOSvcMM',
                Input=options.input_files,
                BufferNbEvents=options.buffer_events,
                NSkip=options.first_evt))

        default_raw_event.global_bind(
            raw_event_format=options.input_raw_format,
            maker=make_raw_event_with_IOAlg)
    else:
        # TODO do not use IOHelper here but setup components we need directly
        input_iohelper = IOHelper(options.input_type, options.output_type
                                  or None)
        # setupServices may create (the wrong) EventDataSvc, so do it first
        setup_component(options.event_store, instance_name='EventDataSvc')
        input_iohelper.setupServices()
        evtSel = input_iohelper.inputFiles(options.input_files, clear=True)
        evtSel.FirstEvent = options.first_evt
        evtSel.Input = [
            # The checksum computation is expensive, so skip it
            inp + " IgnoreChecksum='YES'" for inp in evtSel.Input
        ]
        evtSel.PrintFreq = options.print_freq
        config.add(evtSel)
        config.add(setup_component('IODataManager', DisablePFNWarning=True))

        default_raw_event.global_bind(
            raw_event_format=options.input_raw_format)

    return config


def assert_empty_dataondemand_service():
    assert DataOnDemandSvc().AlgMap == {}, DataOnDemandSvc().AlgMap
    assert DataOnDemandSvc().NodeMap == {}, DataOnDemandSvc().NodeMap


def configure(options,
              control_flow_node,
              public_tools=[],
              barrier_algorithms=[]):
    # TODO get rid of magic initial time (must go to configure_input)
    INITIAL_TIME = 1433509200

    options.finalize()
    config = ComponentConfig()
    if options.callgrind_profile:
        control_flow_node = CompositeNode(
            'profile_' + control_flow_node.name,
            [make_callgrind_profile(), control_flow_node])
    nodes, algs = all_nodes_and_algs(control_flow_node)
    configuration = dataflow_config()
    for alg in algs:
        configuration.update(alg.configuration())
    for tool in public_tools:
        configuration.update(tool.configuration())
    configurable_algs, configurable_tools = configuration.apply()

    # TODO what is this dummy stuff?
    odin_loc = '/Event/DAQ/DummyODIN'
    configurable_algs += [
        setup_component(
            DummyEventTime,
            "DummyEventTime",
            Start=INITIAL_TIME,
            Step=0,
            ODIN=odin_loc,
            require_IOVLock=False),
        setup_component(
            reserveIOV, "reserveIOV", require_IOVLock=False, ODIN=odin_loc)
    ]
    if UseDD4Hep:
        configurable_algs += [
            setup_component(
                IOVProducer,
                "ReserveIOVDD4hep",
                require_IOVLock=False,
                SliceLocation="IOVLockDD4hep",
                ODIN=odin_loc)
        ]

    whiteboard = config.add(
        setup_component(
            options.event_store,
            instance_name='EventDataSvc',
            EventSlots=options.n_event_slots,
            ForceLeaves=True))
    scheduler = config.add(
        setup_component(
            'HLTControlFlowMgr',
            CompositeCFNodes=[node.represent() for node in nodes],
            MemoryPoolSize=options.memory_pool_size,
            ThreadPoolSize=options.n_threads,
            EnableLegacyMode=options.scheduler_legacy_mode,
            BarrierAlgNames=[alg.name for alg in barrier_algorithms]))
    appMgr = config.add(
        ApplicationMgr(OutputLevel=options.output_level, EventLoop=scheduler))
    appMgr.ExtSvc.insert(
        0, whiteboard
    )  # FIXME this cannot work when configurables are not singletons
    appMgr.ExtSvc.insert(0, NTupleSvc())

    if options.auditors:
        config.add(setup_component('AuditorSvc', Auditors=options.auditors))
        appMgr.ExtSvc += [AuditorSvc()]
        appMgr.AuditAlgorithms = True

    if not any(a for a in options.auditors if a == 'ChronoAuditor' or (
            isinstance(a, Configurable) and a.getType() == 'ChronoAuditor')):
        # Turn off most output from ChronoStatSvc
        # Even if it is not explicitly configured, some components will
        # instantiate it (like DetailedMaterialLocator) causing some output.
        ChronoStatSvc().ChronoPrintOutTable = False
        ChronoStatSvc().PrintUserTime = False

    if not UseDD4Hep:
        config.add(setup_component('UpdateManagerSvc', WithoutBeginEvent=True))
    config.add(
        setup_component('HiveDataBrokerSvc', DataProducers=configurable_algs))
    # for LoKi
    config.add(setup_component('AlgContextSvc', BypassIncidents=True))
    config.add(setup_component('LoKiSvc', Welcome=False))

    # configure message service
    config.add(
        setup_component(
            'MessageSvc',
            Format=options.msg_svc_format,
            timeFormat=options.msg_svc_time_format))
    appMgr.ExtSvc += [MessageSvcSink()]
    if not UseDD4Hep:
        config.add(
            setup_component(
                'EventClockSvc', InitialTime=int(INITIAL_TIME * 1e9)))

    # configure the monitoring sink that writes to a file
    if options.monitoring_file:
        sink = setup_component(
            JSONSink, FileName=options.monitoring_file, DumpFullInfo=True)
        config.add(sink)
        appMgr.ExtSvc += [sink]

    # Instantiate the histogram service if we're creating histograms or ntuples,
    # preferring the histo_name if set, otherwise the ntuple_name
    histo_name = options.histo_file or options.ntuple_file
    if histo_name:
        config.add(
            setup_component('HistogramPersistencySvc', OutputFile=histo_name))
        config.add(ApplicationMgr(HistogramPersistency="ROOT"))
        # New-style histograms a written to a separate file
        new_histo_name = '{}_new{}'.format(*os.path.splitext(histo_name))
        root_sink = setup_component(
            'Gaudi__Histograming__Sink__Root', FileName=new_histo_name)
        config.add(root_sink)
        appMgr.ExtSvc.append(root_sink)

    if options.ntuple_file:
        config.add(
            setup_component(
                'NTupleSvc',
                Output=[
                    "FILE1 DATAFILE='{}' TYPE='ROOT' OPT='NEW'".format(
                        options.ntuple_file)
                ]))
    # set the Phoenix Sink output filename (same as histo_name above)
    if options.phoenix_filename:
        phoenix_sink = setup_component(
            'LHCb__Phoenix__Sink', FileName=options.phoenix_filename)
        config.add(phoenix_sink)
        appMgr.ExtSvc.append(phoenix_sink)

    if options.control_flow_file:
        fn_root, fn_ext = os.path.splitext(options.control_flow_file)
        plot_control_flow(
            control_flow_node, filename=fn_root, extensions=[fn_ext[1:]])
    if options.data_flow_file:
        fn_root, fn_ext = os.path.splitext(options.data_flow_file)
        plot_data_flow(algs, filename=fn_root, extensions=[fn_ext[1:]])

    # Make sure nothing's configuring the DoD behind our back, e.g. LHCbApp
    appendPostConfigAction(assert_empty_dataondemand_service)

    # TODO add configurable_tools and configurable_algs to config
    return config


def _gaudi_datetime_format(dt):
    """Return the datetime object formatted as Gaudi does it.

    As seen in the application "Welcome" banner.
    """
    return dt.strftime('%a %h %d %H:%M:%S %Y')


def plot_data_flow(algs, filename='data_flow', extensions=('gv', )):
    """Save a visualisation of the current data flow.

    Args:
      algs (list): List of Algorithm instances.
      filename (str): Basename of the file to create.
      extensions (list): List of file extensions to create.
          One file is created per extensions.
          Possible values include `'gv'` for saving the raw graphviz representation,
          and `'png'` and `'pdf'` for saving graphics.

    Note:
      The `dot` binary must be present on the system for saving
      files with graphical extensions. The raw `.gv` format can be
      convert be hand like::

        dot -Tpdf data_flow.gv > data_flow.pdf
    """
    now = _gaudi_datetime_format(datetime.datetime.now())
    label = 'Data flow generated at {}'.format(now)
    top = pydot.Dot(
        graph_name='Data flow', label=label, strict=True, rankdir='LR')
    top.set_node_defaults(shape='box')
    for alg in algs:
        alg._graph(top)
    for ext in extensions:
        format = 'raw' if ext == 'gv' else ext
        top.write('{}.{}'.format(filename, ext), format=format)


def plot_control_flow(top_node, filename='control_flow', extensions=('gv', )):
    """Save a visualisation of the current control flow.

    Args:
      filename (str): Basename of the file to create.
      extensions (list): List of file extensions to create.
          One file is created per extensions.
          Possible values include `'gv'` for saving the raw graphviz representation,
          and `'png'` and `'pdf'` for saving graphics.

    Note:
      The `dot` binary must be present on the system for saving
      files with graphical extensions. The raw `.gv` format can be
      convert be hand like::

        dot -Tpdf data_flow.gv > data_flow.pdf
    """
    now = _gaudi_datetime_format(datetime.datetime.now())
    label = 'Control flow generated at {}'.format(now)
    graph = pydot.Dot(
        graph_name='control_flow', label=label, strict=True, compound=True)
    top_node._graph(graph)

    for ext in extensions:
        format = 'raw' if ext == 'gv' else ext
        graph.write('{}.{}'.format(filename, ext), format=format)
