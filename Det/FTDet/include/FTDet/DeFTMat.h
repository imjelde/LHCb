/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTMat.h"
using DeFTMat = LHCb::Detector::DeFTMat;
#else

#  include "DetDesc/DetectorElement.h"
#  include "DetDesc/IPVolume.h"
#  include "GaudiKernel/Plane3DTypes.h"
#  include "Kernel/DetectorSegment.h"
#  include "Kernel/FTChannelID.h"
#  include "Kernel/LineTraj.h"

/** @class DeFTMat DeFTMat.h "FTDet/DeFTMat.h"
 *
 *  This is the detector element class of the Fibre Tracker (FT) mat.
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-10-21
 */

static const CLID CLID_DeFTMat = 8606;

class DeFTMat final : public DetectorElement {

public:
  /// Standard constructor
  using DetectorElement::DetectorElement;

  /** Initialization method */
  StatusCode initialize() override;

  /** Retrieves reference to class identifier
   *  @return The class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   *  @return The class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTMat; }

  /** @return matID */
  [[nodiscard]] LHCb::FTChannelID::MatID matID() const { return m_elementID.mat(); }

  /** @return moduleID */
  [[nodiscard]] LHCb::FTChannelID::ModuleID moduleID() const { return m_elementID.module(); }

  /** @return quarterID */
  [[nodiscard]] LHCb::FTChannelID::QuarterID quarterID() const { return m_elementID.quarter(); }

  /** @return  layerID */
  [[nodiscard]] LHCb::FTChannelID::LayerID layerID() const { return m_elementID.layer(); }

  /** @return stationID */
  [[nodiscard]] LHCb::FTChannelID::StationID stationID() const { return m_elementID.station(); }

  /** @return unique matID */
  [[nodiscard]] unsigned int uniqueMatID() const { return m_elementID.uniqueMat(); }

  /** @return unique moduleID */
  [[nodiscard]] unsigned int uniqueModuleID() const { return m_elementID.uniqueModule(); }

  /** @return unique quarterID */
  [[nodiscard]] unsigned int uniqueQuarterID() const { return m_elementID.uniqueQuarter(); }

  /** @return  unique layerID */
  [[nodiscard]] unsigned int uniqueLayerID() const { return m_elementID.uniqueLayer(); }

  /** @return unique stationID */
  [[nodiscard]] unsigned int uniqueStationID() const;

  /** Element id */
  [[nodiscard]] LHCb::FTChannelID elementID() const { return m_elementID; }

  /** Set element id */
  DeFTMat& setElementID( LHCb::FTChannelID chanID ) {
    m_elementID = chanID;
    return *this;
  }

  /** @return flag true if this quarter is bottom half */
  [[nodiscard]] bool isBottom() const { return m_elementID.isBottom(); }

  /** @return flag true if this quarter is top half */
  [[nodiscard]] bool isTop() const { return m_elementID.isTop(); }

  /**
   * Return a sensitive volume identifier for a given point in the
   * global reference frame. This function is vital for Gauss.
   */
  [[nodiscard]] int sensitiveVolumeID( const Gaudi::XYZPoint& ) const override { return m_elementID; }

  /** Returns the pitch between two channels (250 micron) */
  [[nodiscard]] float channelPitch() const { return m_channelPitch; }

  /** Returns the width of the fibre mat */
  [[nodiscard]] float fibreMatWidth() const { return m_sizeX; }

  /** Get the length of the fibre in this mat */
  [[nodiscard]] float fibreLength() const { return m_sizeY; }

  /** Returns the thickness of the fibre mat (1.3 mm) */
  [[nodiscard]] float fibreMatThickness() const { return m_sizeZ; }

  /** Returns the global z position of the module */
  [[nodiscard]] float globalZ() const { return m_globalZ; }

  /** Returns the xy-plane at z-middle the layer */
  [[nodiscard]] const Gaudi::Plane3D& plane() const { return m_plane; }

  /** Main method to determine which channel was hit
   *  @param localX is the input x coordinate in the local frame.
   *  @param frac returns the corresponding fraction in the range 0-1.
   *  @return channel returns the corresponding readout channel
   */
  [[nodiscard]] LHCb::FTChannelID calculateChannelAndFrac( float localX, float& frac ) const;

  /** Get the list of SiPM channels between two channels
   *  @param provide first and last channels
   *  Fills a vector of FTChannelIDs, and a vector of the
   *  corresponding left edges (along x) in the local frame.
   */
  [[nodiscard]] std::vector<std::pair<LHCb::FTChannelID, float>>
  calculateChannels( LHCb::FTChannelID thisChannel, LHCb::FTChannelID endChannel ) const;

  /** Get the list of SiPM channels traversed by the hit.
   *  The particle trajectory is a straight line defined by:
   *  @param provide local entry and exit point
   *  @param provide the number of additional channels to add
   *  Fills a vector of FTChannelIDs, and a vector of the
   *  corresponding left edges (along x) in the local frame.
   */
  [[nodiscard]] std::vector<std::pair<LHCb::FTChannelID, float>>
  calculateChannels( const float localEntry, const float localExit, const unsigned int numOfAdditionalChannels ) const;

  /** Get the distance from the hit to the SiPM
   *  @param localPoint is the position of the half module in local coordinates
   *  @return the distance to the SiPM
   */
  [[nodiscard]] float distanceToSiPM( const Gaudi::XYZPoint& localPoint ) const {
    return 0.5 * m_sizeY - localPoint.y();
  };

  /** Get the local x from a channelID and its fraction */
  [[nodiscard]] float localXfromChannel( const LHCb::FTChannelID channelID, const float frac ) const;

  /** Get the local x from a channelID and its fraction */
  [[nodiscard]] float localXfromChannel( const LHCb::FTChannelID channelID, const int frac ) const {
    float uFromChannel = m_uBegin + ( 2 * channelID.channel() + 1 + frac ) * m_halfChannelPitch;
    if ( channelID.die() ) uFromChannel += m_dieGap;
    uFromChannel += channelID.sipm() * m_sipmPitch;
    return uFromChannel;
  }

  /** Calculate the distance between a global point and FTChannelID + fraction
   *  @param channelID is the FTChannelID of the cluster
   *  @param frac is the fraction of the cluster centre of gravity
   *  @return distance in mm
   */
  [[nodiscard]] float distancePointToChannel( const Gaudi::XYZPoint& globalPoint, const LHCb::FTChannelID channelID,
                                              const float frac ) const;

  /** Get the trajectory defined by the begin and end positions of a hit
   *   (channelID + fraction)
   *  @param channelID input FTChannelID
   *  @param frac input fraction
   */
  [[nodiscard]] LHCb::LineTraj<double> trajectory( const LHCb::FTChannelID channelID, const float frac ) const;

  /** Get the line defined by the begin and end positions of a hit
   *   (channelID + fraction)
   *  @param channelID input FTChannelID
   *  @param frac input fraction
   */
  [[nodiscard]] std::pair<Gaudi::XYZPointF, Gaudi::XYZPointF> endPoints( const LHCb::FTChannelID channelID,
                                                                         const float             frac ) const;

  /** Get begin positions of a hit (channelID + integer fraction)
   *  @param channelID input FTChannelID
   *  @param frac input fraction
   */
  [[nodiscard]] Gaudi::XYZPointF endPoint( const LHCb::FTChannelID channelID, const int frac ) const {
    return Gaudi::XYZPointF( m_mirrorPoint + m_ddx * localXfromChannel( channelID, frac ) );
  }

  /** Get the global slope of the mat in dx/dy */
  [[nodiscard]] float dxdy() const { return m_dxdy; }

  /** Get the global slope of the mat in dz/dy */
  [[nodiscard]] float dzdy() const { return m_dzdy; }

  /** Get the global height of the mat in y */
  [[nodiscard]] float globaldy() const { return m_globaldy; }

  /** Get the Location of end of fibres at x=z=0 */
  [[nodiscard]] const Gaudi::XYZPointF& mirrorPoint() const { return m_mirrorPoint; }

  /** Get the global direction vector for a local displacement in unit x */
  [[nodiscard]] const Gaudi::XYZVectorF& ddx() const { return m_ddx; }

  /** Get the local u-coordinate of sensitive SiPM */
  [[nodiscard]] float uBegin() const { return m_uBegin; }

  /** Get the half of the readout channel pitch */
  [[nodiscard]] float halfChannelPitch() const { return m_halfChannelPitch; }

  /** Get the gap between channel 63 and 64 */
  [[nodiscard]] float dieGap() const { return m_dieGap; }

  /** Get the pitch between SiPMs in mat */
  [[nodiscard]] float sipmPitch() const { return m_sipmPitch; }

  /** Flag if there is a gap on the left of this channel */
  [[nodiscard]] bool hasGapLeft( const LHCb::FTChannelID thisChannel ) const {
    return ( thisChannel.channel() == 0u || int( thisChannel.channel() ) == m_nChannelsInDie );
  }

  /** Flag if there is a gap on the right of this channel */
  [[nodiscard]] bool hasGapRight( const LHCb::FTChannelID thisChannel ) const {
    return ( int( thisChannel.channel() ) == m_nChannelsInSiPM - 1 ||
             int( thisChannel.channel() ) == m_nChannelsInDie - 1 );
  }

  Gaudi::XYZPoint  toLocal( const Gaudi::XYZPoint& p ) const { return this->geometry()->toLocal( p ); }
  Gaudi::XYZPoint  toGlobal( const Gaudi::XYZPoint& p ) const { return this->geometry()->toGlobal( p ); }
  Gaudi::XYZVector toLocal( const Gaudi::XYZVector& v ) const { return this->geometry()->toLocal( v ); }
  Gaudi::XYZVector toGlobal( const Gaudi::XYZVector& v ) const { return this->geometry()->toGlobal( v ); }

private:
  StatusCode updateCache();

private:
  LHCb::FTChannelID m_elementID; ///< element ID

  int m_nChannelsInSiPM; ///< number of channels per SiPM
  int m_nChannelsInDie;  ///< number of channels per die
  int m_nSiPMsInMat;     ///< number of SiPM arrays per mat
  int m_nDiesInSiPM;     ///< number of dies per SiPM

  Gaudi::Plane3D   m_plane;        ///< xy-plane in the z-middle of the module
  Gaudi::XYZPointF m_sipmPoint;    ///< Location of end of fibres at x=z=0
  float            m_globalZ;      ///< Global z position of module closest to y-axis
  float            m_airGap;       ///< air gap
  float            m_deadRegion;   ///< dead region
  float            m_channelPitch; ///< readout channel pitch (250 micron)
  float            m_diePitch;     ///< pitch between dies in SiPM
  float            m_sizeX;        ///< Width in x of the mat
  float            m_sizeY;        ///< Length in y of the fibre in the mat
  float            m_sizeZ;        ///< Thickness of the fibre mat (nominal: 1.3 mm)

  // Parameters needed for decoding
  Gaudi::XYZPointF  m_mirrorPoint;      ///< Location of end of fibres at x=z=0
  Gaudi::XYZVectorF m_ddx;              ///< Global direction vector for a local displacement in unit x
  float             m_uBegin;           ///< start in local u-coordinate of sensitive SiPM
  float             m_halfChannelPitch; ///< half of the readout channel pitch (125 micron)
  float             m_dieGap;           ///< gap between channel 63 and 64
  float             m_sipmPitch;        ///< pitch between SiPMs in mat
  float             m_dxdy;             ///< Global slope dx/dy for a fibre mat
  float             m_dzdy;             ///< Global slope dz/dy for a fibre mat
  float             m_globaldy;         ///< Length of a fibre projected along global y

}; // end of class
#endif
