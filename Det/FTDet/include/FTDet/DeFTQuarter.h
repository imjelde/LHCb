/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP
#  include "Detector/FT/DeFTQuarter.h"
using DeFTQuarter = LHCb::Detector::DeFTQuarter;
#else
#  include "DetDesc/DetectorElement.h"
#  include "FTDet/DeFTModule.h"
#  include "Kernel/FTChannelID.h"

/** @class DeFTQuarter DeFTQuarter.h "FTDet/DeFTQuarter.h"
 *
 *  This is the detector element class for a Fibre Tracker Quarter.
 *
 *  @author Jeroen van Tilburg
 *  @date   2016-07-18
 *
 */

static const CLID CLID_DeFTQuarter = 8603;

class DeFTQuarter final : public DetectorElement {

public:
  using ID = LHCb::FTChannelID::QuarterID;

  /// Standard constructor
  using DetectorElement::DetectorElement;

  /** Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** Another reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeFTQuarter; };

  /** Initialization method */
  StatusCode initialize() override;

  /** Const method to return the module for a given channel id
   * @param  aChannel  an FT channel id
   * @return pointer to detector element
   */
  [[nodiscard]] const DeFTModule* findModule( const LHCb::FTChannelID aChannel ) const {
    auto m = to_unsigned( aChannel.module() );
    return m < m_modules.size() ? m_modules[m] : nullptr;
  }

  /** Const method to return the module for a given XYZ point
   * @param  aPoint the given point
   * @return const pointer to detector element
   */
  [[nodiscard]] const DeFTModule* findModule( const Gaudi::XYZPoint& aPoint ) const;

  /** Flat vector of all FT modules
   * @return vector of modules
   */
  [[nodiscard]] const auto& modules() const { return m_modules; }

  /** @return quarterID */
  [[nodiscard]] ID quarterID() const { return m_quarterID; }

  /** @return flag true if this quarter is bottom half */
  [[nodiscard]] bool isBottom() const { return m_quarterID == ID{0} || m_quarterID == ID{1}; }

  /** @return flag true if this quarter is top half */
  [[nodiscard]] bool isTop() const { return m_quarterID == ID{2} || m_quarterID == ID{3}; }

private:
  ID                                                    m_quarterID; ///< quarter ID number
  boost::container::static_vector<const DeFTModule*, 6> m_modules;   ///< vector of modules

}; // end of class
#endif
