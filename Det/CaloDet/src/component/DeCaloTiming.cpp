/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiTupleAlg.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/StdArrayAsProperty.h"

namespace LHCb {

  /**
   * Simple algorithm to add time shift  (user-defined or Gaussian or flat randomly
   * distributed) at the DeCalorimeter initialisation level
   * Useful to produce time misalignment (in Gauss) and check/develop alignment procedure
   *
   * @author Olivier DESCHAMPS
   * @date   2007-08-22
   */
  class DeCaloTiming : public Algorithm::Consumer<void( const DeCalorimeter& ),
                                                  DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {
  public:
    DeCaloTiming( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator, {KeyValue{"DetectorName", ""}} ) {}
    StatusCode initialize() override;
    void       operator()( const DeCalorimeter& ) const override;

  private:
    double delta( long id ) const {
      auto delta = m_deltas.find( std::to_string( id ) );
      return delta != m_deltas.end() ? delta->second : m_deltas.value().at( "Default" );
    }

    // Note that thread safety depends on the thread safety of the underlying random generator
    mutable Rndm::Numbers m_shoot;

    ServiceHandle<IRndmGenSvc> m_rndmSvc{this, "RndmGenSvc", "RndmGenSvc"};

    Gaudi::Property<std::string>                   m_method{this, "Method", "Flat", "Flat/Gauss/User"};
    Gaudi::Property<std::array<double, 2>>         m_params{this, "Params", {0, 0}};
    Gaudi::Property<std::map<std::string, double>> m_deltas{this, "deltaTime", {{"Default", 0.0 * Gaudi::Units::ns}}};
    Gaudi::Property<std::string>                   m_key{this, "Key", "CellID"};

    // used to run only one first event
    mutable std::once_flag m_onlyFirstEvent;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::DeCaloTiming, "DeCaloTiming" )

StatusCode LHCb::DeCaloTiming::initialize() {
  return GaudiTupleAlg::initialize().andThen( [&]() -> StatusCode {
    info() << " ======= SIMULATING THE MISALIGNMENT of " << inputLocation<DeCalorimeter>()
           << " TIMING  ======= " << endmsg;
    if ( m_method == "Gauss" ) {
      // Gaussian random (mean, rms)
      info() << "---- Method : gaussian random timing values " << endmsg;
      double a = m_params[0];
      double b = m_params[1];
      info() << " mean/sigma = " << a << "/" << b << endmsg;
      auto sc = m_shoot.initialize( m_rndmSvc.get(), Rndm::Gauss( a, b ) );
      if ( !sc.isSuccess() ) return sc;
    } else if ( m_method == "Flat" ) {
      // Flat random (min, max)
      info() << "---- Method : flat random timing values " << endmsg;
      double a = m_params[0];
      double b = m_params[1];
      info() << " min/max = " << a << "/" << b << endmsg;
      auto sc = m_shoot.initialize( m_rndmSvc.get(), Rndm::Flat( a, b ) );
      if ( !sc.isSuccess() ) return sc;
    } else if ( m_method == "User" ) {
      info() << "---- Method : user-defined timing values " << endmsg;
      info() << "Timing value have been defined for " << m_deltas.size() << " cells " << endmsg;
      info() << "Default value [" << m_deltas["Default"] << "] will be applied to other cells." << endmsg;
      if ( m_key == "CellID" ) {
        info() << "The timing values are mapped with KEY = CellID " << endmsg;
      } else if ( m_key == "Index" ) {
        info() << "The timing values are are mapped with KEY = Index" << endmsg;
      } else {
        error() << "undefined deltaKey : must be either 'CellID' or 'Index' " << endmsg;
        return StatusCode::FAILURE;
      }
    } else {
      error() << "Method " << m_method << " unknown - should be 'Flat', 'Gauss' or 'User'" << endmsg;
      return StatusCode::FAILURE;
    }
    return StatusCode::SUCCESS;
  } );
}

void LHCb::DeCaloTiming::operator()( const DeCalorimeter& deCalo ) const {
  std::call_once( m_onlyFirstEvent, [&]() {
    // update cellParams
    auto&               cells = const_cast<CaloVector<CellParam>&>( deCalo.cellParams() ); // no-const conversion
    std::vector<int>    cellids, cellind;
    std::vector<double> times, dtimes;
    for ( auto& icell : cells ) {
      LHCb::Calo::CellID id = icell.cellID();
      if ( !deCalo.valid( id ) || deCalo.isPinId( id ) ) continue;
      long   num = deCalo.cellIndex( id );
      double dt  = ( m_method != "User" ? m_shoot() : delta( m_key != "Index" ? id.index() : num ) );
      if ( msgLevel( MSG::DEBUG ) ) debug() << num << " Delta time for cellID " << id << " : " << dt << endmsg;
      icell.setDeltaTime( dt ); // add delta time (ns)
      cellids.push_back( id.index() );
      cellind.push_back( num );
      times.push_back( icell.time() );
      dtimes.push_back( icell.deltaTime() );
    }

    // Ntupling
    Tuple ntp = nTuple( 500, inputLocation<DeCalorimeter>() + "DeTiming", CLID_ColumnWiseTuple );
    auto  max = deCalo.numberOfCells();
    ntp->farray( "cellID", cellids, "Nchannels", max ).ignore();
    ntp->farray( "index", cellind, "Nchannels", max ).ignore();
    ntp->farray( "time", times, "Nchannels", max ).ignore();
    ntp->farray( "dtime", dtimes, "Nchannels", max ).ignore();
    ntp->write().ignore();
  } );
}
