/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "LHCbAlgs/Consumer.h"

#include <mutex>
#include <string>

namespace LHCb {

  /**
   *  Simple Test         Algorithm
   *
   *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
   *  @date   14/12/2001
   */
  struct CaloDetTestAlgorithm
      : public Algorithm::Consumer<void( const DeCalorimeter& ), DetDesc::usesConditions<DeCalorimeter>> {

    CaloDetTestAlgorithm( const std::string& name, ISvcLocator* svcloc )
        : Consumer( name, svcloc, {KeyValue{"DetDataLocation", ""}} ) {}

    void operator()( const DeCalorimeter& ) const override;

    // used to run only one first event
    mutable std::once_flag m_onlyFirstEvent;
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::CaloDetTestAlgorithm, "CaloDetTestAlgorithm" )

void LHCb::CaloDetTestAlgorithm::operator()( const DeCalorimeter& calo ) const {
  std::call_once( m_onlyFirstEvent, [&]() {
    const CaloVector<CellParam>& cells = calo.cellParams();
    for ( const auto& cell : cells ) {
      LHCb::Calo::CellID id   = ( cell ).cellID();
      int                card = ( cell ).cardNumber();
      info() << " | " << id << " | " << id.all() << " | " << format( "0x%04X", id.all() ) << " | "
             << ( cell ).cardColumn() << "/" << ( cell ).cardRow() << " | " << ( cell ).cardNumber() << " ( "
             << calo.cardCode( card ) << ") | " << calo.cardCrate( card ) << " | " << calo.cardSlot( card ) << " | "
             << calo.cardToTell1( card ) << " | " << endmsg;
    }
  } );
}
