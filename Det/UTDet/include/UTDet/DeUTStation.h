/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/UTChannelID.h"

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUTStation.h"
using DeUTStation = LHCb::Detector::UT::DeUTSide;

#else

#  include "GaudiKernel/MsgStream.h"
#  include "UTDet/DeUTBaseElement.h"
#  include <string>
#  include <vector>

class DeUTLayer;
class DeUTDetector;

/** @class DeUTStation DeUTStation.h UTDet/DeUTStation.h
 *
 *  UT Station detector element
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

static const CLID CLID_DeUTStation = 9302;

class DeUTStation : public DeUTBaseElement {

public:
  /** child type **/
  using child_type = const DeUTLayer;

  /** children **/
  using Children = std::vector<child_type*>;

  /** Constructor **/
  using DeUTBaseElement::DeUTBaseElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   **/
  static const CLID& classID() { return CLID_DeUTStation; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   **/
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   **/
  StatusCode initialize() override;

  /** station identifier
   *  @return identifier
   **/
  [[nodiscard]] unsigned int id() const { return m_id; }

  /** print to stream */
  std::ostream& printOut( std::ostream& os ) const override;

  /** print to stream */
  MsgStream& printOut( MsgStream& os ) const override;

  /** check contains channel
   *  @param  aChannel channel
   *  @return bool
   **/
  [[nodiscard]] bool contains( const LHCb::UTChannelID aChannel ) const override {
    return elementID().station() == aChannel.station();
  }

  /**
   * Nickname for the station
   **/
  [[nodiscard]] const std::string& nickname() const { return m_nickname; }

  /**  locate the layer based on a channel id
   *   @return  layer
   **/
  [[nodiscard]] const DeUTLayer* findLayer( const LHCb::UTChannelID aChannel ) const;

  /** locate layer based on a point
   *  @return layer
   **/
  [[nodiscard]] const DeUTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** vector of children */
  [[nodiscard]] const Children& layers() const { return m_layers; }

  /**
   * fraction active channels
   * @return bool fraction active
   **/
  [[nodiscard]] double fractionActive() const;

  /** ouput operator for class DeUTStation
   *  @see DeUTStation
   *  @see MsgStream
   *  @param os       reference to STL output stream
   *  @param aStation reference to DeUTStation object
   **/
  friend std::ostream& operator<<( std::ostream& os, const DeUTStation& aStation ) { return aStation.printOut( os ); }

  /** ouput operator for class DeUTStation
   *  @see DeUTStation
   *  @see MsgStream
   *  @param os       reference to MsgStream output stream
   *  @param aStation reference to DeUTStation object
   **/
  friend MsgStream& operator<<( MsgStream& os, const DeUTStation& aStation ) { return aStation.printOut( os ); }

private:
  std::string  m_nickname;
  unsigned int m_id = 0u;
  Children     m_layers;
};

[[deprecated( "please deref first" )]] std::ostream& operator<<( std::ostream&      os,
                                                                 const DeUTStation* aStation )                = delete;
[[deprecated( "please deref first" )]] MsgStream&    operator<<( MsgStream& os, const DeUTStation* aStation ) = delete;

#endif
