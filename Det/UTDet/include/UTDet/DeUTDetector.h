/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/UT/DeUT.h"

using DeUTDetector        = LHCb::Detector::UT::DeUT;
using DeUTLayer           = LHCb::Detector::UT::DeUTLayer;
namespace DeUTDetLocation = LHCb::Detector::UT;

#else

#  include "DetDesc/DetectorElement.h"
#  include "GaudiKernel/VectorMap.h"
#  include "Kernel/LHCbID.h"
#  include "Kernel/LineTraj.h"
#  include "Kernel/UTChannelID.h"
#  include "UTDet/DeUTBaseElement.h"
#  include "UTDet/DeUTLayer.h"
#  include "UTDet/DeUTSector.h"
#  include "UTDet/DeUTStation.h"

#  include "fmt/format.h"

#  include <cassert>
#  include <memory>
#  include <string>
#  include <vector>

namespace DeUTDetLocation {

  /// UT location in transient detector store
  inline const std::string UT = "/dd/Structure/LHCb/BeforeMagnetRegion/UT";

  inline const std::string& location() { return DeUTDetLocation::UT; }

  inline unsigned int detType() { return LHCb::UTChannelID::detType::typeUT; }

} // namespace DeUTDetLocation

/** @class DeUTDetector DeUTDetector.h UTDet/DeUTDetector.h
 *
 *  UT Detector Element class
 *
 *  All UT elements are modification from TT element classes
 *  that were originally written by Matt Needham)
 *
 *  @author Andy Beiter (based on code by Jianchun Wang, Matt Needham)
 *  @date   2018-09-04
 *
 */

constexpr CLID CLID_DeUTDetector = 9301;

namespace {
  constexpr int NBSTATION = 2;
  constexpr int NBLAYER   = 2;  // nbr layers per station
  constexpr int NBREGION  = 3;  // nbr regions per layer
  constexpr int NBSECTOR  = 98; // nbr sectors per region
} // namespace

class DeUTDetector : public DetectorElement {

public:
  using Sectors = std::vector<const DeUTSector*>;

  /** Constructor */
  using DetectorElement::DetectorElement;

  /**
   * Retrieves reference to class identifier
   * @return the class identifier for this class
   */
  static const CLID& classID() { return CLID_DeUTDetector; }

  /**
   * another reference to class identifier
   * @return the class identifier for this class
   */
  const CLID& clID() const override;

  /** initialization method
   * @return Status of initialisation
   */
  StatusCode initialize() override;

  /** @return number of first station */
  [[nodiscard]] unsigned int firstStation() const { return m_firstStation; }

  /** @return number of last station */
  [[nodiscard]] unsigned int lastStation() const { return m_firstStation + m_stations.size() - 1u; }

  /** @return number of stations */
  [[nodiscard]] unsigned int nStation() const { return m_stations.size(); }

  /** Implementation of sensitive volume identifier for a given point in the
      global reference frame. This is the sensor number defined in the xml.
  */
  [[nodiscard]] int sensitiveVolumeID( const Gaudi::XYZPoint& globalPos ) const override;

  /**
   *  short cut to pick up the station corresponding to a given nickname
   * @param nickname
   * @return layer
   */
  [[nodiscard]] const DeUTLayer* findLayer( std::string_view nickname ) const;

  /**  locate the layer based on a channel id
  @return  layer */
  [[nodiscard]] const DeUTLayer* findLayer( LHCb::UTChannelID aChannel ) const;

  /** locate layer based on a point
   *return layer */
  [[nodiscard]] const DeUTLayer* findLayer( const Gaudi::XYZPoint& point ) const;

  /** check contains channel
   *  @param  aChannel channel
   *  @return bool
   */
  [[nodiscard]] bool contains( LHCb::UTChannelID aChannel ) const;

  /// Workaround to prevent hidden base class function
  [[nodiscard]] bool isValid() const override { return ValidDataObject::isValid(); }

  /// Workaround to prevent hidden base class function
  [[nodiscard]] bool isValid( const Gaudi::Time& t ) const override { return ValidDataObject::isValid( t ); }

  /** check channel number is valid */
  [[nodiscard]] bool isValid( LHCb::UTChannelID aChannel ) const;

  /**
   * access to a given station
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTStation& station( unsigned int index ) const { return *m_stations[index]; }

  /**
   * access to a given sector
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTSector& sector( unsigned int index ) const { return *m_sectors[index]; }

  /// @return number of sectors
  unsigned int nSectors() const { return m_sectors.size(); }

  /// apply given callable to all layers
  void applyToAllSectors( const std::function<void( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectors ) { func( *sector ); }
  }

  /// check no sector returns true for the given callable
  bool none_of_sectors( const std::function<bool( DeUTSector const& )>& func ) const {
    for ( auto* sector : m_sectors ) {
      if ( func( *sector ) ) return false;
    }
    return true;
  }

  /**
   * access to a given layer
   * Note that there is no checks on the validity of the index.
   * use at your own risks
   */
  [[nodiscard]] const DeUTLayer& layer( unsigned int index ) const { return *m_layers[index]; }

  /// apply given callable to all layers
  void applyToAllLayers( const std::function<void( DeUTLayer const& )>& func ) const {
    for ( auto* layer : m_layers ) { func( *layer ); }
  }

  /**
   *  short cut to pick up the wafer corresponding to x,y,z
   * @param  aPoint point in global frame
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( const Gaudi::XYZPoint& aPoint ) const;

  /**
   *  short cut to pick up the wafer corresponding to a channel
   * @param  aChannel channel
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( LHCb::UTChannelID aChannel ) const;

  /**
   *  get the sector corresponding to the input channel
   * @param  aChannel channel
   * @return sector
   */
  [[nodiscard]] const DeUTSector& getSector( LHCb::UTChannelID chan ) const {
    auto* sec = getSector( chan.station(), chan.layer(), chan.detRegion(), chan.sector(), chan.uniqueSector() );
    if ( !sec ) throw std::runtime_error{"DeUTDetector::getSector returned nullptr"};
    return *sec;
  }

  /**
   *  short cut to pick up the wafer corresponding to a given nickname
   * @param nickname
   * @return sector
   */
  [[nodiscard]] const DeUTSector* findSector( std::string_view nickname ) const;

  /** get the next channel left */
  [[nodiscard]] LHCb::UTChannelID nextLeft( LHCb::UTChannelID testChan ) const;

  /** get the next channel right */
  [[nodiscard]] LHCb::UTChannelID nextRight( LHCb::UTChannelID testChan ) const;

  /** get the trajectory
   @return trajectory
  */
  [[nodiscard]] LHCb::LineTraj<double> trajectory( LHCb::LHCbID id, double offset ) const;

  /** get the number of strips in detector*/
  [[nodiscard]] unsigned int nStrip() const { return m_nStrip; }

  /** get the number of layers **/
  [[nodiscard]] unsigned int nLayer() const { return m_layers.size(); }

  /** get the number of readout sectors **/
  [[nodiscard]] unsigned int nReadoutSector() const { return m_sectors.size(); }

  /** number of layers per station **/
  [[nodiscard]] unsigned int nLayersPerStation() const { return nLayer() / nStation(); }

  /**
   * fraction active channels
   * @return bool fraction active
   */
  [[nodiscard]] double fractionActive() const;

  [[nodiscard]] const DeUTSector* getSector( unsigned int station, //
                                             unsigned int layer,   //
                                             unsigned int region,  //
                                             unsigned int sector,  //
                                             unsigned int uniqueSector ) const;

  const DeUTDetector& setOffset();

  /// apply function to given sectors
  void applyToSectors( const std::vector<LHCb::UTChannelID>&           vec,
                       const std::function<void( DeUTSector const& )>& func ) const;

  /** get list of all disabled sectors */
  Sectors disabledSectors() const;

  /** get list of disabled beetles */
  [[nodiscard]] std::vector<LHCb::UTChannelID> disabledBeetles() const;

  /** beetle as a string */
  std::string uniqueBeetle( const LHCb::UTChannelID& chan ) const {
    auto const sector = findSector( chan );
    return fmt::format( "{}Beetle{}", sector->nickname(), sector->beetle( chan ) );
  };

  /** port */
  std::string uniquePort( const LHCb::UTChannelID& chan ) const {
    const unsigned int port = ( ( chan.strip() - 1u ) / LHCbConstants::nStripsInPort ) + 1u;
    return fmt::format( "{}Port{}", uniqueBeetle( chan ), port );
  };

  /**  locate the station based on a channel id
  @return  station */
  [[nodiscard]] const DeUTStation* findStation( LHCb::UTChannelID aChannel ) const;

private:
  /** locate station based on a point
  @return station */
  [[nodiscard]] const DeUTStation* findStation( const Gaudi::XYZPoint& point ) const;

  /**
   *  short cut to pick up the station corresponding to a given nickname
   * @param nickname
   * @return station
   */
  [[nodiscard]] const DeUTStation* findStation( std::string_view nickname ) const;

  /** set the first Station number */
  void setFirstStation( unsigned int iStation );

  /** set the strip number  */
  DeUTDetector& setNstrip( unsigned int nStrip ) {
    m_nStrip = nStrip;
    return *this;
  }

  std::vector<const DeUTStation*>                        m_stations;
  Sectors                                                m_sectors;
  std::vector<const DeUTLayer*>                          m_layers;
  GaudiUtils::VectorMap<unsigned int, const DeUTSector*> m_sMap;

private:
  std::size_t getOffset( std::size_t index ) const noexcept {
    assert( index < m_offset.size() );
    return m_offset[index];
  }

private:
  unsigned int m_firstStation = 0u;
  unsigned int m_nStrip       = 0u;

  /** make flat list of lowest descendents  and also layers */
  void flatten();
  /** offsets on the "flatten" list of sectors in order to have quicker access */
  std::array<std::size_t, NBSTATION * NBLAYER * NBREGION> m_offset;
};

inline bool DeUTDetector::contains( const LHCb::UTChannelID aChannel ) const {
  return ( ( aChannel.station() >= firstStation() ) && ( aChannel.station() < lastStation() ) );
}

inline LHCb::UTChannelID DeUTDetector::nextLeft( const LHCb::UTChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->nextLeft( aChannel ) : LHCb::UTChannelID( 0u ) );
}

inline LHCb::UTChannelID DeUTDetector::nextRight( const LHCb::UTChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->nextRight( aChannel ) : LHCb::UTChannelID( 0u ) );
}

inline bool DeUTDetector::isValid( const LHCb::UTChannelID aChannel ) const {
  const auto aSector = findSector( aChannel );
  return ( aSector ? aSector->isStrip( aChannel.strip() ) : false );
}

inline const DeUTSector* DeUTDetector::findSector( const LHCb::UTChannelID aChannel ) const {
  auto iter = m_sMap.find( aChannel.uniqueSector() );
  return ( iter != m_sMap.end() ? iter->second : nullptr );
}

#endif
