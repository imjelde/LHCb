/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <Gaudi/Interfaces/IOptionsSvc.h>
#include <Gaudi/Parsers/CommonParsers.h>
#include <GaudiKernel/Bootstrap.h>
#include <GaudiKernel/IMessageSvc.h>
#include <GaudiKernel/ISvcLocator.h>
#include <GaudiKernel/StatusCode.h>
#include <string>

namespace {
  /// Helper to initialize OutputLevel for components not deriving from CommonMessaging
  StatusCode initOutputLevel( IMessageSvc* msgSvc, const std::string& name ) {
    if ( !msgSvc ) throw std::invalid_argument( "invalid IMessageSvc (nullptr)" );
    const auto& optsSvc = Gaudi::svcLocator()->getOptsSvc();
    int         lvl     = 0;

    using Gaudi::Parsers::parse;
    return parse( lvl, optsSvc.get( name + ".OutputLevel", "0" ) ).andThen( [&]() {
      if ( lvl > 0 ) msgSvc->setOutputLevel( name, lvl );
    } );
  }
} // namespace
