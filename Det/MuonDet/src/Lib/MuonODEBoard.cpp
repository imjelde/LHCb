/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "MuonDet/MuonODEBoard.h"
#include <iostream>

//-----------------------------------------------------------------------------
// Implementation file for class : MuonODEBoard
//
// 2004-01-12 : Alessia Satta
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
MuonODEBoard::MuonODEBoard() {
  for ( int i = 0; i < 4; i++ ) { m_quadrant[i] = 0; }
}
//=============================================================================
// Destructor
//=============================================================================
MuonODEBoard::~MuonODEBoard() {}

//=============================================================================
StatusCode MuonODEBoard::update( long tslayx, long tslayy, long tsnumb, std::vector<long> gridx,
                                 std::vector<long> gridy, std::vector<long> quadrant ) {
  m_TSLayoutX  = tslayx;
  m_TSLayoutY  = tslayy;
  m_TSNumber   = tsnumb;
  m_TSGridX    = gridx;
  m_TSGridY    = gridy;
  m_TSQuadrant = quadrant;

  return StatusCode::SUCCESS;
}

bool MuonODEBoard::addTSName( std::string name ) {
  if ( (int)m_TSName.size() <= m_TSNumber ) {
    m_TSName.push_back( name );
    return true;
  } else {
    return false;
  }
}

void MuonODEBoard::initialize( long number, long region ) {
  m_region    = region;
  m_ODENumber = number;
}

void MuonODEBoard::setQuadrants() {
  for ( int i = 0; i < m_TSNumber; i++ ) {
    unsigned int a = m_TSQuadrant[i];
    m_quadrant[a]  = 1;
  }
}

bool MuonODEBoard::isQuadrantContained( long quadrant ) { return m_quadrant[quadrant] > 0; }

bool MuonODEBoard::isTSContained( LHCb::MuonTileID TSTile ) {
  long quadrant = TSTile.quarter();
  long gridx    = TSTile.nX();
  long gridy    = TSTile.nY();
  for ( int i = 0; i < m_TSNumber; i++ ) {
    if ( m_TSQuadrant[i] == quadrant && m_TSGridX[i] == gridx && m_TSGridY[i] == gridy ) { return true; }
  }
  return false;
}
