/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "GaudiKernel/IAlgTool.h"
#include "Kernel/MuonTileID.h"
#include "MuonDet/DeMuonDetector.h"

#include <optional>

/**
 *  Interface for the tools to convert MuonTileID to coordinates
 *
 *  @author David Hutchcroft
 *  @date   11/03/2002
 */

class IMuonFastPosTool : public extend_interfaces<IAlgTool> {
public:
  DeclareInterfaceID( IMuonFastPosTool, 5, 0 );

  /** Calculate the x,y,z and dx,dy,dz of a MuonTileID in mm
   *  this ignores gaps: these can never be read out independently
   */
  virtual std::optional<DeMuonDetector::TilePosition> calcTilePos( const LHCb::MuonTileID& tile ) const   = 0;
  virtual std::optional<DeMuonDetector::TilePosition> calcStripXPos( const LHCb::MuonTileID& tile ) const = 0;
  virtual std::optional<DeMuonDetector::TilePosition> calcStripYPos( const LHCb::MuonTileID& tile ) const = 0;
};
