/*****************************************************************************\
* (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "LbDD4hep/ConditionAccessorHolder.h"

namespace LHCb::DetDesc {
  using LHCb::Det::LbDD4hep::ConditionAccessor;
  using LHCb::Det::LbDD4hep::ConditionAccessorHolder;
  using LHCb::Det::LbDD4hep::usesBaseAndConditions;
  using LHCb::Det::LbDD4hep::usesConditions;
} // namespace LHCb::DetDesc

#else

#  include "DetDesc/ConditionAccessorHolder.h"

#endif
