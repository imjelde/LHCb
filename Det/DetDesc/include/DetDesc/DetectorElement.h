/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include Files
#include <map>
#include <mutex>
#include <string>
#include <vector>

#include "DetDesc/CLIDDetectorElement.h"
#include "DetDesc/Condition.h"
#include "DetDesc/IDetectorElement.h"
#include "DetDesc/IGeometryInfo.h"
#include "DetDesc/ILVolume.h"
#include "DetDesc/ParamValidDataObject.h"
#include "DetDesc/Services.h"

#include "GaudiKernel/MsgStream.h"
#include "GaudiKernel/SmartRef.h"
#include "GaudiKernel/Transform3DTypes.h"

#include "Math/Point3D.h"
#include "Math/Transform3D.h"
#include "Math/Vector3D.h"

// Forward declarations
class IDataProviderSvc;
class IMessageSvc;
template <class T>
class DataObjectFactory;
class IUpdateManagerSvc;
#include "GaudiKernel/IRegistry.h"

/** @class DetectorElement DetectorElement.h DetDesc/DetectorElement.h
 *
 *  A "basic" implementation of Detector Description - Detector Element
 *
 *  @author Sebastien Ponce
 *  @author Rado Chytracek
 *  @author Ivan Belyaev
 *  @author Marco Clemencic
 *  @author Juan Palacios
 */

class DetectorElement : public ParamValidDataObject, virtual public IDetectorElement {

  friend class DataObjectFactory<DetectorElement>;

public:
  /// Type of the internal list of used conditions
  typedef std::map<std::string, SmartRef<Condition>> ConditionMap;

  DetectorElement( const std::string& name = "NotYetDefined" );
  ~DetectorElement() override;

  /**
   * This method initializes the detector element. It should be overridden
   * and used for computation purposes. This is a kind of hook for adding
   * user code easily in the initialization of a detector element.
   */
  StatusCode initialize() override;

  /// Obtain class ID
  const CLID&        clID() const override { return classID(); }
  static const CLID& classID() { return CLID_DetectorElement; }

  const std::string& name() const override;
  const std::string& lVolumeName() const override;

  IMessageSvc* msgSvc() const { return m_services->msgSvc(); }

  /// Check if the condition called 'name' is in the list of conditionrefs.
  bool hasCondition( const std::string& name ) const override;

  /// Return the SmartRef for the condition called 'name'.
  SmartRef<Condition> condition( const std::string& name ) const override;

  /// Iterator to the first element of the list of conditions.
  inline ConditionMap::const_iterator conditionBegin() const { return m_de_conditions.begin(); }

  /// Iterator to the last+1 element of the list of conditions.
  inline ConditionMap::const_iterator conditionEnd() const { return m_de_conditions.end(); }

  /// delegation for geometry
  inline IGeometryInfo* geometry() override;

  ROOT::Math::Transform3D toLocalMatrix() const { return geometry()->toLocalMatrix(); }
  ROOT::Math::Transform3D toLocalMatrixNominal() const { return geometry()->toLocalMatrixNominal(); }
  ROOT::Math::XYZPoint    toLocal( const ROOT::Math::XYZPoint& globalPoint ) const {
    return geometry()->toLocal( globalPoint );
  }
  ROOT::Math::XYZVector toLocal( const ROOT::Math::XYZVector& globalPoint ) const {
    return geometry()->toLocal( globalPoint );
  }
  ROOT::Math::Transform3D toLocal( const ROOT::Math::Transform3D& transform ) const {
    return geometry()->toLocalMatrix() * transform;
  }
  ROOT::Math::Transform3D toGlobalMatrix() const { return geometry()->toGlobalMatrix(); }
  ROOT::Math::Transform3D toGlobalMatrixNominal() const { return geometry()->toGlobalMatrixNominal(); }
  ROOT::Math::XYZPoint    toGlobal( const ROOT::Math::XYZPoint& globalPoint ) const {
    return geometry()->toGlobal( globalPoint );
  }
  ROOT::Math::XYZVector toGlobal( const ROOT::Math::XYZVector& globalPoint ) const {
    return geometry()->toGlobal( globalPoint );
  }
  ROOT::Math::Transform3D toGlobal( const ROOT::Math::Transform3D& transform ) const {
    return geometry()->toGlobalMatrix() * transform;
  }
  ROOT::Math::Transform3D ownToOffNominalMatrix() const { return geometry()->ownToOffNominalMatrix(); }
  /// return true is succesful, false otherwise
  bool ownToOffNominalMatrix( const ROOT::Math::Transform3D& newDelta ) const {
    auto sc = const_cast<IGeometryInfo*>( geometry() )->ownToOffNominalMatrix( newDelta );
    return sc.isSuccess();
  }

  std::vector<double> elemDeltaTranslations() const {
    return geometry()->alignmentCondition()->paramVect<double>( "dPosXYZ" );
  }
  std::vector<double> elemDeltaRotations() const {
    return geometry()->alignmentCondition()->paramVect<double>( "dRotXYZ" );
  }

  /// returns motionSystemTransform if we are dealing with the Velo, false otherwise
  virtual std::optional<ROOT::Math::Transform3D> motionSystemTransform() const { return {}; }

  void getGlobalMatrixDecomposition( ROOT::Math::Rotation3D& ltg_rot, ROOT::Math::XYZVector& ltg_trans ) const {
    geometry()->toGlobalMatrix().GetDecomposition( ltg_rot, ltg_trans );
  }

  /// delegation for geometry  (const version)
  inline const IGeometryInfo* geometry() const override;

  /// helper member using IGeometryInfo::isInside
  bool isInside( const ROOT::Math::XYZPoint& globalPoint ) const;

  const IDetectorElement* childDEWithPoint( const ROOT::Math::XYZPoint& globalPoint ) const;

  /// delegation for alignnment
  inline IAlignment* alignment() override;

  /// delegation for alignment (const version)
  inline const IAlignment* alignment() const override;

  // delegation for calibration
  inline ICalibration* calibration() override;

  // delegation for calibration (const version)
  inline const ICalibration* calibration() const override;

  // delegation for readout
  inline IReadOut* readOut() override;

  // delegation for readout (const version)
  inline const IReadOut* readOut() const override;

  // delegation for slow control
  inline ISlowControl* slowControl() override;

  // delegation for slow control (const version)
  inline const ISlowControl* slowControl() const override;

  // delegation for fast control
  inline IFastControl* fastControl() override;

  // delegation for fast control (const version)
  inline const IFastControl* fastControl() const override;

  // another way to access: "pseudo-conversion"
  // cast to         IGeometryInfo*
  operator IGeometryInfo*() override { return m_de_iGeometry.get(); }

  // cast to   const IGeometryInfo*
  operator const IGeometryInfo*() const override { return m_de_iGeometry.get(); }

  // cast to         IAligment*
  operator IAlignment*() override { return m_de_iAlignment.get(); }

  // cast to   const IAlignment*
  operator const IAlignment*() const override { return m_de_iAlignment.get(); }

  // cast to         ICalibration*
  operator ICalibration*() override { return m_de_iCalibration.get(); }

  // cast to   const ICalibration*
  operator const ICalibration*() const override { return m_de_iCalibration.get(); }

  // cast to         IReadOut*
  operator IReadOut*() override { return m_de_iReadOut.get(); }

  // cast to   const IReadOut*
  operator const IReadOut*() const override { return m_de_iReadOut.get(); }

  // cast to         ISlowControl*
  operator ISlowControl*() override { return m_de_iSlowControl.get(); }

  // cast to   const ISlowControl*
  operator const ISlowControl*() const override { return m_de_iSlowControl.get(); }

  // cast to         IFastControl*
  inline operator IFastControl*() override { return m_de_iFastControl.get(); }

  // cast to   const IFastControl*
  inline operator const IFastControl*() const override { return m_de_iFastControl.get(); }

  // cast to         IGeometryInfo&
  // (potentially could throw DetectorElementException)
  inline operator IGeometryInfo&() override;

  // cast to   const IGeometryInfo&
  // (potentially could throw DetectorElementException)
  inline operator const IGeometryInfo&() const override;

  // cast to         IAlignment&
  // (potentially could throw DetectorElementException)
  inline operator IAlignment&() override;

  // cast to   const IAlignment&
  // (potentially could throw DetectorElementException)
  inline operator const IAlignment&() const override;

  // cast to         ICalibration&
  // (potentially could throw DetectorElementException)
  inline operator ICalibration&() override;

  // cast to   const ICalibration&
  // (potentially could throw DetectorElementException)
  inline operator const ICalibration&() const override;

  // cast to         IReadOut&
  // (potentially could throw DetectorElementException)
  inline operator IReadOut&() override;

  // cast to   const IReadOut&
  // (potentially could throw DetectorElementException)
  inline operator const IReadOut&() const override;

  // cast to         ISlowControl&
  // (potentially could throw DetectorElementException)
  inline operator ISlowControl&() override;

  // cast to   const ISlowControl&
  // (potentially could throw DetectorElementException)
  inline operator const ISlowControl&() const override;

  // cast to         IFastControl&
  // (potentially could throw DetectorElementException)
  inline operator IFastControl&() override;

  // cast to   const IFastControl&
  // (potentially could throw DetectorElementException)
  inline operator const IFastControl&() const override;

  //  printout (overloaded)
  // (potentially could throw DetectorElementException)
  std::ostream& printOut( std::ostream& ) const override;

  /// reset to the initial state
  IDetectorElement* reset() override;

  virtual MsgStream& printOut( MsgStream& ) const;

  // pointer to parent IDetectorElement (const version)
  IDetectorElement* parentIDetectorElement() const override;

  // (reference to) container of pointers to child detector elements
  IDetectorElement::IDEContainer& childIDetectorElements() const override;

  /**
   * Method used to access the ParamValidDataObject methods from IDetectorElement
   * interface.
   */
  const ParamValidDataObject* params() const override;

  /**
   * Return a sensitive volume identifier for a given point in the
   * global reference frame.
   */

  int sensitiveVolumeID( const ROOT::Math::XYZPoint& globalPos ) const override;

  /// Used to create a link with a given name to the condition at 'path' in the detector data store.
  void createCondition( const std::string& name, const std::string& path );

  /// Returns list of existing parameter vectors as a vector of their names
  virtual std::vector<std::string> conditionNames() const;

  ///
  /// specific
  // create "ghost"
  const IGeometryInfo* createGeometryInfo();

  // create "orphan"
  const IGeometryInfo* createGeometryInfo( const std::string& logVol );

  // create "regular"
  const IGeometryInfo* createGeometryInfo( const std::string& logVol, const std::string& support,
                                           const ILVolume::ReplicaPath& replicaPath );

  const IGeometryInfo* createGeometryInfo( const std::string& logVol, const std::string& support,
                                           const ILVolume::ReplicaPath& replicaPath, const std::string& alignmentPath );

  const IGeometryInfo* createGeometryInfo( const std::string& logVol, const std::string& support,
                                           const std::string& namePath );

  const IGeometryInfo* createGeometryInfo( const std::string& logVol, const std::string& support,
                                           const std::string& namePath, const std::string& alignmentPath );

  const IAlignment* createAlignment( const std::string& condition );

  const ICalibration* createCalibration( const std::string& condition );

  const IReadOut* createReadOut( const std::string& condition );

  const ISlowControl* createSlowControl( const std::string& condition );

  const IFastControl* createFastControl( const std::string& condition );

public:
  // Implementation of IInterface
  unsigned long addRef() override;
  unsigned long release() override;
  StatusCode    queryInterface( const InterfaceID& riid, void** ppvInterface ) override;

protected:
  /// specific
  void setGeometry( IGeometryInfo* geoInfo ) { m_de_iGeometry.reset( geoInfo ); }

  IDataProviderSvc*  dataSvc() const;
  IUpdateManagerSvc* updMgrSvc() const;

  // technicalities

  void Assert( bool assertion, const std::string& message = "DetectorElement Unknown Exception" ) const;

  void Assert( bool assertion, const char* message ) const;

private:
  // for IDetectorElement implementation
  std::unique_ptr<IGeometryInfo> m_de_iGeometry;
  std::unique_ptr<IAlignment>    m_de_iAlignment;
  std::unique_ptr<ICalibration>  m_de_iCalibration;
  std::unique_ptr<IReadOut>      m_de_iReadOut;
  std::unique_ptr<ISlowControl>  m_de_iSlowControl;
  std::unique_ptr<IFastControl>  m_de_iFastControl;

  /// Container of the SmartRefs for the conditions.
  ConditionMap m_de_conditions;

  mutable std::atomic<bool>              m_de_childrensLoaded{false};
  mutable IDetectorElement::IDEContainer m_de_childrens;
  mutable std::mutex                     m_de_childrens_lock;

  /// This defines the type of a userParameter
  enum userParamKind { DOUBLE, INT, OTHER };

  /// reference to services
  DetDesc::ServicesPtr m_services;
};

// implementation of the inlines functions
///  output operators to MsgStream ///////////////////////////////////////////
inline MsgStream& operator<<( MsgStream& os, const DetectorElement& de ) { return de.printOut( os ); }

inline MsgStream& operator<<( MsgStream& os, const DetectorElement* de ) {
  if ( !de ) {
    return os << "DetectorElement* points to NULL" << endmsg;
  } else {
    return os << *de;
  }
}

inline const IGeometryInfo* DetectorElement::geometry() const { return m_de_iGeometry.get(); }
inline IGeometryInfo*       DetectorElement::geometry() { return m_de_iGeometry.get(); }
inline const IAlignment*    DetectorElement::alignment() const { return m_de_iAlignment.get(); }
inline IAlignment*          DetectorElement::alignment() { return m_de_iAlignment.get(); }
inline const ICalibration*  DetectorElement::calibration() const { return m_de_iCalibration.get(); }
inline ICalibration*        DetectorElement::calibration() { return m_de_iCalibration.get(); }
inline const IReadOut*      DetectorElement::readOut() const { return m_de_iReadOut.get(); }
inline IReadOut*            DetectorElement::readOut() { return m_de_iReadOut.get(); }
inline const ISlowControl*  DetectorElement::slowControl() const { return m_de_iSlowControl.get(); }
inline ISlowControl*        DetectorElement::slowControl() { return m_de_iSlowControl.get(); }
inline const IFastControl*  DetectorElement::fastControl() const { return m_de_iFastControl.get(); }
inline IFastControl*        DetectorElement::fastControl() { return m_de_iFastControl.get(); }

// "pseudo-casting"
inline DetectorElement::operator const IGeometryInfo&() const {
  Assert( bool( m_de_iGeometry ), "DetectorElement::geometry is not available!" );
  return *m_de_iGeometry;
}

inline DetectorElement::operator IGeometryInfo&() {
  Assert( bool( m_de_iGeometry ), "DetectorElement::geometry is not available!" );
  return *m_de_iGeometry;
}

inline DetectorElement::operator const IAlignment&() const {
  Assert( bool( m_de_iAlignment ), "DetectorElement::alignment is not available!" );
  return *m_de_iAlignment;
}

inline DetectorElement::operator IAlignment&() {
  Assert( bool( m_de_iAlignment ), "DetectorElement::alignment is not available!" );
  return *m_de_iAlignment;
}

inline DetectorElement::operator const ICalibration&() const {
  Assert( bool( m_de_iCalibration ), "DetectorElement::calibration is not available!" );
  return *m_de_iCalibration;
}

inline DetectorElement::operator ICalibration&() {
  Assert( bool( m_de_iCalibration ), "DetectorElement::calibration is not available!" );
  return *m_de_iCalibration;
}

inline DetectorElement::operator const IReadOut&() const {
  Assert( bool( m_de_iReadOut ), "DetectorElement::readout is not available!" );
  return *m_de_iReadOut;
}

inline DetectorElement::operator IReadOut&() {
  Assert( bool( m_de_iReadOut ), "DetectorElement::readout is not available!" );
  return *m_de_iReadOut;
}

inline DetectorElement::operator const ISlowControl&() const {
  Assert( bool( m_de_iSlowControl ), "DetectorElement::slowcontrol is not available!" );
  return *m_de_iSlowControl;
}

inline DetectorElement::operator ISlowControl&() {
  Assert( bool( m_de_iSlowControl ), "DetectorElement::slowcontrol is not available!" );
  return *m_de_iSlowControl;
}

inline DetectorElement::operator const IFastControl&() const {
  Assert( bool( m_de_iFastControl ), "DetectorElement::fastcontrol is not available!" );
  return *m_de_iFastControl;
}

inline DetectorElement::operator IFastControl&() {
  Assert( bool( m_de_iFastControl ), "DetectorElement::fastcontrol is not available!" );
  return *m_de_iFastControl;
}
