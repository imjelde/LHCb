###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/DetDescCnv
--------------
#]=======================================================================]

gaudi_add_library(DetDescCnvLib
    SOURCES
        src/Lib/AddressTools.cpp
        src/Lib/XmlBaseConditionCnv.cpp
        src/Lib/XmlBaseDetElemCnv.cpp
        src/Lib/XmlGenericCnv.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::DetDescLib
            LHCb::XmlToolsLib
            XercesC::XercesC
            yaml-cpp
)

gaudi_add_module(DetDescCnv
    SOURCES
        src/component/XmlAlignmentConditionCnv.cpp
        src/component/XmlCatalogCnv.cpp
        src/component/XmlConditionCnv.cpp
        src/component/XmlDetectorElementCnv.cpp
        src/component/XmlElementCnv.cpp
        src/component/XmlIsotopeCnv.cpp
        src/component/XmlLVolumeCnv.cpp
        src/component/XmlMixtureCnv.cpp
        src/component/XmlSurfaceCnv.cpp
        src/component/XmlTabulatedPropertyCnv.cpp
    LINK
        LHCb::DetDescCnvLib
)
