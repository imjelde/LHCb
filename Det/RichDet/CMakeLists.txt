###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Det/RichDet
-----------
#]=======================================================================]

gaudi_add_library(RichDetLib
    SOURCES
        src/Lib/DeHorizRich.cpp
        src/Lib/DeRich.cpp
        src/Lib/DeRich1.cpp
        src/Lib/DeRich2.cpp
        src/Lib/DeRichAerogelRadiator.cpp
        src/Lib/DeRichBase.cpp
        src/Lib/DeRichBeamPipe.cpp
        src/Lib/DeRichGasRadiator.cpp
        src/Lib/DeRichHPD.cpp
        src/Lib/DeRichHPDPanel.cpp
        src/Lib/DeRichLocations.cpp
        src/Lib/DeRichMultiSolidRadiator.cpp
        src/Lib/DeRichPD.cpp
        src/Lib/DeRichPDPanel.cpp
        src/Lib/DeRichPMT.cpp
        src/Lib/DeRichPMTClassic.cpp
        src/Lib/DeRichPMTPanel.cpp
        src/Lib/DeRichPMTPanelClassic.cpp
        src/Lib/DeRichRadiator.cpp
        src/Lib/DeRichSingleSolidRadiator.cpp
        src/Lib/DeRichSphMirror.cpp
        src/Lib/DeRichSystem.cpp
        src/Lib/Rich1DTabFunc.cpp
        src/Lib/Rich1DTabProperty.cpp
        src/Lib/RichDetConfigType.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            GSL::gsl
            LHCb::DetDescLib
            LHCb::LHCbKernel
            LHCb::LHCbMathLib
            LHCb::RichUtils
        PRIVATE
            VDT::vdt
            Boost::headers
)

gaudi_add_module(RichDet
    SOURCES
        src/component/XmlDeHorizRichCnv.cpp
        src/component/XmlDeRich1Cnv.cpp
        src/component/XmlDeRich2Cnv.cpp
        src/component/XmlDeRichAerogelRadiatorCnv.cpp
        src/component/XmlDeRichBeamPipeCnv.cpp
        src/component/XmlDeRichGasRadiatorCnv.cpp
        src/component/XmlDeRichHPDCnv.cpp
        src/component/XmlDeRichHPDPanelCnv.cpp
        src/component/XmlDeRichMultiSolidRadiatorCnv.cpp
        src/component/XmlDeRichPMTClassicCnv.cpp
        src/component/XmlDeRichPMTCnv.cpp
        src/component/XmlDeRichPMTPanelClassicCnv.cpp
        src/component/XmlDeRichPMTPanelCnv.cpp
        src/component/XmlDeRichSingleSolidRadiatorCnv.cpp
        src/component/XmlDeRichSphMirrorCnv.cpp
        src/component/XmlDeRichSystemCnv.cpp
    LINK
        LHCb::DetDescCnvLib
        LHCb::RichDetLib
)

gaudi_add_dictionary(RichDetDict
    HEADERFILES dict/RichDetDict.h
    SELECTION dict/RichDetDict.xml
    LINK LHCb::RichDetLib
)
