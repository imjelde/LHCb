/*****************************************************************************\
* (c) Copyright 2020-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Core/ConditionConverter.h>
#include <Core/ConditionsRepository.h>
#include <Core/yaml_converters.h>

#include "DetDesc/ParamValidDataObject.h"

#include "DD4hep/ConditionDerived.h"
#include "DD4hep/Detector.h"
#include "DD4hep/Factories.h"
#include "DD4hep/GrammarUnparsed.h"
#include "XML/Conversions.h"
#include "XML/XML.h"

#include <memory>
#include <vector>

/// very simple plugin registering an hardcoded condition directly to /world element
static long create_conditions_recipes( dd4hep::Detector& description, xml_h /*e*/ ) {
  auto world    = description.world();
  auto requests = std::make_shared<LHCb::Detector::ConditionsRepository>();

  LHCb::YAMLConverters::set_converter( "!testcond", []( std::string_view name, const YAML::Node& cond_data ) {
    dd4hep::Condition c{std::string{name}, "testcond"};
    auto*             value = &c.bind<ParamValidDataObject>();
    for ( const auto& param : cond_data ) {
      const auto param_name = param.first.as<std::string>();
      if ( param_name != "parv" ) {
        const auto val = param.second.as<double>();
        value->addParam<double>( param_name, val );
      } else {
        const auto val = param.second.as<std::vector<double>>();
        value->addParam<std::vector<double>>( param_name, val );
      }
    }
    return c;
  } );

  // Here we specify the name of the condition and where they can be cound within the repository
  requests->addLocation( world, dd4hep::ConditionKey::itemCode( "TestCondition" ), "Conditions/test/conditions.yml",
                         "TestCondition" );
  requests->addLocation( world, dd4hep::ConditionKey::itemCode( "TestConditionYML" ), "Conditions/test/conditions.yml",
                         "TestConditionYML" );
  using Ext_t = std::shared_ptr<LHCb::Detector::ConditionsRepository>;
  world.addExtension( new dd4hep::detail::DeleteExtension<Ext_t, Ext_t>( new Ext_t( requests ) ) );
  return 1;
}
DECLARE_XML_DOC_READER( Fake_cond, create_conditions_recipes )
