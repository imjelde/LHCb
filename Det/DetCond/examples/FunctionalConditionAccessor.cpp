/*****************************************************************************\
* (c) Copyright 2019-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <DetDesc/Condition.h>
#include <DetDesc/GenericConditionAccessorHolder.h>

#include "LHCbAlgs/Consumer.h"

#include <sstream>
#include <type_traits>
#include <yaml-cpp/yaml.h>

#ifdef USE_DD4HEP
#  include <DD4hep/Grammar.h>
#  include <DD4hep/GrammarUnparsed.h>
static const std::string ConditionPath   = "/world:TestCondition";
static const std::string ConditionPrefix = "/world:";
#else
static const std::string ConditionPath   = "TestCondition";
static const std::string ConditionPrefix = "";
#endif

namespace LHCb::DetCond::Examples::Functional {

  // Example of algorithm accessing conditions
  struct CondAccessExample
      : Algorithm::Consumer<void( const ParamValidDataObject& ), DetDesc::usesConditions<ParamValidDataObject>> {
    // constructor
    CondAccessExample( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"CondPath", ConditionPath}}} {}

    void operator()( const ParamValidDataObject& cond ) const override {
      auto const& [p1, p2] = cond.params<double, double>( "par1", "par2" );
      auto const pv        = cond.paramAsDoubleVect( "parv" );
      info() << "parameter value extracted: " << p1 << " " << p2 << " " << pv[0] << " " << pv[1] << endmsg;
    }
  };

  // Example of algorithm accessing conditions
  struct CondAccessExampleYAML
      : Gaudi::Functional::Consumer<void( const YAML::Node& ), LHCb::DetDesc::usesConditions<YAML::Node>> {
    // constructor
    CondAccessExampleYAML( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"CondPath", ConditionPath}}} {}

    void operator()( const YAML::Node& cond ) const override {
      // tutorial on how to use yaml-cpp can be found at
      // https://github.com/jbeder/yaml-cpp/wiki/Tutorial
      auto const p1 = cond["par1"].as<double>();
      auto const p2 = cond["par2"].as<double>();
      auto const pv = cond["parv"].as<std::vector<double>>();
      info() << "parameter value extracted: " << p1 << " " << p2 << " " << pv[0] << " " << pv[1] << endmsg;
    }
  };

  // Example of algorithm accessing conditions
  struct CondAccessExampleYamlNoProp
      : Gaudi::Functional::Consumer<void( const EventContext& ), DetDesc::usesConditions<>> {
    // constructor
    CondAccessExampleYamlNoProp( const std::string& name, ISvcLocator* loc ) : Consumer{name, loc} {}

    ConditionAccessor<YAML::Node> m_myCond {
      this,
#if USE_DD4HEP
          "/world:TestConditionYML"
#else
          "TestCondition"
#endif
    };

    void operator()( const EventContext& ctx ) const override {
      const auto& cond = m_myCond.get( getConditionContext( ctx ) );
      auto const  p1   = cond["par1"].as<double>();
      auto const  p2   = cond["par2"].as<double>();
      auto const  pv   = cond["parv"].as<std::vector<double>>();
      info() << "parameter value extracted: " << p1 << " " << p2 << " " << pv[0] << " " << pv[1] << endmsg;
    }
  };

  /// Example of a derived condition that does not inherit from Condition base class.
  struct MyData {
    double p1, p2, v;
  };

  /** Example of algorithm that requires caching of condition derivations.
   *  Note that we exercise a number of things at the same time here :
   *  - derived conditions
   *  - derived condition with custom type (not ParamValidDatObject)
   *  - ability to have several derived conditions concurrently
   *  - ability to have 2 derived conditions of the same type
   *  - ability to derive a condition from 2 others, not just one
   *  - deriving a condition from a derived condition (kinf od recursive derivation)
   */
  struct CondAccessExampleWithDerivation
      : Algorithm::Consumer<void( const MyData&, const MyData& ), DetDesc::usesConditions<MyData>> {
    CondAccessExampleWithDerivation( const std::string& name, ISvcLocator* loc )
        : Consumer{name,
                   loc,
                   {KeyValue{"Level1", ConditionPrefix + "DerivedCondition1"},
                    KeyValue{"Level2", ConditionPrefix + "DerivedCondition2"}}} {}

    /// Property used to change the path of the input condition, if needed
    Gaudi::Property<std::string> m_srcPath{this, "Source", ConditionPath};

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&]() {
        addConditionDerivation( {m_srcPath.value()}, inputLocation<0>(),
                                /// Callable which converts raw information into a derived condition.
                                []( const ParamValidDataObject& obj ) {
                                  const auto& [p1, p2] = obj.params<double, double>( "par1", "par2" );
                                  return MyData{p1, p2, std::sqrt( p1 ) + 2.0 * p2 * p2};
                                } );
        addConditionDerivation( {m_srcPath.value(), inputLocation<0>()}, inputLocation<1>(),
                                /// Callable which converts raw information into a derived condition.
                                []( const ParamValidDataObject& obj, const MyData& cond1 ) {
                                  const auto& [p1, p2] = obj.params<double, double>( "par1", "par2" );
                                  return MyData{cond1.p1 + p1, cond1.p2 + p2, 2 * cond1.v};
                                } );
      } );
    }

    void operator()( const MyData& cond1, const MyData& cond2 ) const override {
      info() << "condition value for level1 : {\n  p1: " << cond1.p1 << "\n  p2: " << cond1.p2 << "\n  v:  " << cond1.v
             << "\n}\n"
             << "condition value for level2 : {\n  p1: " << cond2.p1 << "\n  p2: " << cond2.p2 << "\n  v:  " << cond2.v
             << "\n}" << endmsg;
    }
  };

  /** Example of algorithm that requires caching of condition derivations.
   *  Note that we exercise a number of things at the same time here :
   *  - derived conditions
   *  - derived condition with custom type (not ParamValidDatObject)
   *  - ability to have several derived conditions concurrently
   *  - ability to have 2 derived conditions of the same type
   *  - ability to derive a condition from 2 others, not just one
   *  - deriving a condition from a derived condition (kind of recursive derivation)
   */

  namespace {
    MyData callback1( const ParamValidDataObject& obj ) {
      const auto& [p1, p2] = obj.params<double, double>( "par1", "par2" );
      return MyData{p1, p2, std::sqrt( p1 ) + 2.0 * p2 * p2};
    }
    MyData callback2( const ParamValidDataObject& obj, const MyData& cond1 ) {
      const auto& [p1, p2] = obj.params<double, double>( "par1", "par2" );
      return MyData{cond1.p1 + p1, cond1.p2 + p2, 2 * cond1.v};
    }

  } // namespace

  struct CondAccessExampleWithSharedDerivation
      : Algorithm::Consumer<void( const MyData&, const MyData& ), DetDesc::usesConditions<MyData>> {
    CondAccessExampleWithSharedDerivation( const std::string& name, ISvcLocator* loc )
        : Consumer{name,
                   loc,
                   {KeyValue{"Level1", ConditionPrefix + "DerivedCondition1"},
                    KeyValue{"Level2", ConditionPrefix + "DerivedCondition2"}}} {}

    /// Property used to change the path of the input condition, if needed
    Gaudi::Property<std::string> m_srcPath{this, "Source", ConditionPath};

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&]() {
        bool ret1 = addSharedConditionDerivation( {m_srcPath.value()}, inputLocation<0>(),
                                                  /// Callable which converts raw information into a derived condition.
                                                  &callback1 );
        bool ret2 = addSharedConditionDerivation( {m_srcPath.value(), inputLocation<0>()}, inputLocation<1>(),
                                                  /// Callable which converts raw information into a derived condition.
                                                  // Uncomment the lambda to trigger a compilation error.
                                                  // addShared should only take function pointers
                                                  // []( const ParamValidDataObject& obj, const MyData& cond1 ) {
                                                  //   const auto& [p1, p2] = obj.params<double, double>( "par1", "par2"
                                                  //   ); return MyData{cond1.p1 + p1, cond1.p2 + p2, 2 * cond1.v};
                                                  // }
                                                  &callback2 );
        return ret1 && ret2;
      } );
    }

    void operator()( const MyData& cond1, const MyData& cond2 ) const override {
      info() << "condition value for level1 : {\n  p1: " << cond1.p1 << "\n  p2: " << cond1.p2 << "\n  v:  " << cond1.v
             << "\n}\n"
             << "condition value for level2 : {\n  p1: " << cond2.p1 << "\n  p2: " << cond2.p2 << "\n  v:  " << cond2.v
             << "\n}" << endmsg;
    }
  };

  /// Example of algorithm that requires caching of condition derivations and inherits from non default base class
  struct MyAlgorithm : Gaudi::Algorithm {
    using Algorithm::Algorithm;
    static constexpr double m_pi{3.1415};
    double                  pi() { return m_pi; };
  };
  struct CondAccessExampleWithDerivationAndBase
      : Algorithm::Consumer<void( const MyData& ), DetDesc::usesBaseAndConditions<MyAlgorithm, MyData>> {
    CondAccessExampleWithDerivationAndBase( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"Target", ConditionPrefix + "DerivedCondition"}}} {}

    /// Property used to change the path of the input condition, if needed
    Gaudi::Property<std::string> m_srcPath{this, "Source", ConditionPath};

    StatusCode initialize() override {
      const auto sc = Consumer::initialize();
      if ( !sc ) return sc;

      addConditionDerivation( {m_srcPath.value()}, inputLocation<MyData>(),
                              /// Callable which converts raw information into a derived condition.
                              [this]( const ParamValidDataObject& obj ) -> MyData {
                                const auto& [p1, p2] = obj.params<double, double>( "par1", "par2" );
                                return {p1, p2, std::sqrt( p1 ) + pi() * p2 * p2};
                              } );

      return sc;
    }

    void operator()( const MyData& cond ) const override {
      info() << "condition value: {\n  p1: " << cond.p1 << "\n  p2: " << cond.p2 << "\n  v:  " << cond.v << "\n}"
             << endmsg;
    }
  };

} // namespace LHCb::DetCond::Examples::Functional

DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExample )
DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExampleYAML )
DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExampleYamlNoProp )
DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExampleWithDerivation )
DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExampleWithSharedDerivation )
DECLARE_COMPONENT( LHCb::DetCond::Examples::Functional::CondAccessExampleWithDerivationAndBase )
