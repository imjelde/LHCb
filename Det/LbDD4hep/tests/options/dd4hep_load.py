###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
###############################################################################
# Test option to load the DD4hep geometry
###############################################################################
# Syntax is:
###############################################################################
import sys
from Gaudi.Configuration import *
from Configurables import LHCbApp, ApplicationMgr, LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc

dd4hepsvc = DD4hepSvc()
dd4hepsvc.VerboseLevel = 1
dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
dd4hepsvc.GeometryVersion = "v0.1"
dd4hepsvc.GeometryMain = "LHCb.xml"
dd4hepsvc.DetectorList = ["/world"]
app = ApplicationMgr()
app.EvtSel = "NONE"
app.EvtMax = 1
app.ExtSvc += [dd4hepsvc]

LHCbApp().DataType = "Upgrade"
LHCbApp().Simulation = True
