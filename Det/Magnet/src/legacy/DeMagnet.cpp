/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Magnet/DeMagnet.h"

Gaudi::XYZVector DeMagnet::fieldVector( const Gaudi::XYZPoint& pos ) const { return m_magFieldSvc->fieldVector( pos ); }

const LHCb::MagneticFieldGrid* DeMagnet::fieldGrid() const { return m_magFieldSvc->fieldGrid(); }

bool DeMagnet::useRealMap() const { return m_magFieldSvc->useRealMap(); }

bool DeMagnet::isDown() const { return m_magFieldSvc->isDown(); }

double DeMagnet::signedRelativeCurrent() const { return m_magFieldSvc->signedRelativeCurrent(); }
