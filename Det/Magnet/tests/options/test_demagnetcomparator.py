#####################################################################################
# (c) Copyright 1998-2019 CERN for the benefit of the LHCb and ATLAS collaborations #
#                                                                                   #
# This software is distributed under the terms of the Apache version 2 licence,     #
# copied verbatim in the file "LICENSE".                                            #
#                                                                                   #
# In applying this licence, CERN does not waive the privileges and immunities       #
# granted to it by virtue of its status as an Intergovernmental Organization        #
# or submit itself to any jurisdiction.                                             #
#####################################################################################
from Gaudi.Configuration import *

import os
import DDDB
from DDDB.CheckDD4Hep import UseDD4Hep
from Configurables import DeMagnetComparator, LHCbApp

# If no DD4hep, just skip the test
# this is what the rc=77 indicates
if not UseDD4Hep:
    exit(77)

# Prepare detector description
##############################
from Configurables import LHCb__Det__LbDD4hep__DD4hepSvc as DD4hepSvc
dd4hepsvc = DD4hepSvc()
dd4hepsvc.VerboseLevel = 1
dd4hepsvc.GeometryLocation = "${DETECTOR_PROJECT_ROOT}/compact"
dd4hepsvc.GeometryVersion = "trunk"
dd4hepsvc.GeometryMain = "LHCb.xml"
dd4hepsvc.DetectorList = ["/world", "Magnet"]
dd4hepsvc.ConditionsLocation = "/cvmfs/lhcb.cern.ch/lib/lhcb/git-conddb/lhcb-conditions-database.git"

# Main application
##################
app = ApplicationMgr(EvtSel="NONE", EvtMax=1, OutputLevel=INFO)
LHCbApp().DataType = "Upgrade"
LHCbApp().Simulation = True

# Configure fake run number
###########################
from Configurables import LHCb__Tests__FakeRunNumberProducer as FET
# Add the ReserveIOVDD4hep which creates a fake ODIN bank from the location specified
from Configurables import LHCb__Det__LbDD4hep__IOVProducer as IOVProducer

odin_path = '/Event/DummyODIN'
app.TopAlg = [
    FET('FakeRunNumber', ODIN=odin_path, Start=42, Step=20),
    IOVProducer('ReserveIOVDD4hep', ODIN=odin_path),
    DeMagnetComparator('DeMagnetComparator')
]
