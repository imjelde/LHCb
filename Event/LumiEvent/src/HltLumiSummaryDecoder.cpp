/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/HltLumiSummary.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/Transformer.h"

#include "Gaudi/Accumulators.h"

#include <algorithm>
#include <string>

namespace LHCb {

  /**
   *  Decodes the LumiSummary.
   *
   *  @author Jaap Panman
   *  HenryIII Changed to use Transform Algorithm
   *
   *  @date   2008-08-01
   */

  class HltLumiSummaryDecoder : public Algorithm::Transformer<HltLumiSummary( const RawEvent& )> {
  public:
    /// Standard constructor
    HltLumiSummaryDecoder( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives(
                                                         RawEventLocation::Trigger, RawEventLocation::Default )},
                       KeyValue{"OutputContainerName", HltLumiSummaryLocation::Default} ) {}

    HltLumiSummary operator()( const RawEvent& event ) const override {

      HltLumiSummary hltLumiSummary;

      // Get the buffers associated with the HltLumiSummary
      // Now copy the information from all banks (normally there should only be one)
      auto size_buffer = m_totDataSize.buffer();
      for ( const auto& ibank : event.banks( RawBank::HltLumiSummary ) ) {
        // get the raw data
        for ( const unsigned w : ibank->range<unsigned int>() ) {
          // decode the info
          int iKey = ( w >> 16 );
          int iVal = ( w & 0xFFFF );
          if ( msgLevel( MSG::VERBOSE ) ) { verbose() << format( " %8x %11d %11d %11d ", w, w, iKey, iVal ) << endmsg; }
          // add this counter
          hltLumiSummary.addInfo( iKey, iVal );
        }

        // keep statistics
        int totDataSize = ibank->size() / sizeof( unsigned int );
        size_buffer += totDataSize;

        if ( msgLevel( MSG::DEBUG ) ) {
          debug() << "Bank size: " << format( "%4d ", ibank->size() ) << "Total Data bank size " << totDataSize
                  << endmsg;
        }
      }

      return hltLumiSummary;
    }

  private:
    // Statistics, mutable to allow statistics to be kept
    mutable Gaudi::Accumulators::AveragingCounter<> m_totDataSize{this, "Average event size / 32-bit words"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::HltLumiSummaryDecoder, "HltLumiSummaryDecoder" )
