###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/LumiEvent
---------------
#]=======================================================================]

gaudi_add_header_only_library(LumiEventLib
    LINK
        Gaudi::GaudiKernel
)

gaudi_add_module(LumiEvent
    SOURCES
        src/HltLumiSummaryDecoder.cpp
    LINK
        LHCb::LHCbAlgsLib
        LHCb::DAQEventLib
        LHCb::LumiEventLib
)

gaudi_add_dictionary(LumiEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::LumiEventLib
)
