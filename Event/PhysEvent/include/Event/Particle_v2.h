/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/MuonPIDs_v2.h"
#include "Event/SOACollection.h"
#include "Event/Track_v3.h"
#include "Event/UniqueIDGenerator.h"
#include "Kernel/ParticleID.h"
#include "Kernel/STLExtensions.h"
#include "Kernel/Traits.h"
#include "Kernel/Variant.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/SIMDWrapper.h"
#include <array>
#include <limits>
#include <tuple>
#include <type_traits>
#include <vector>

namespace LHCb::Event {
  namespace ChargedBasicsTag {
    struct RichPIDCode : int_field {};
    struct Masse : float_field {};
    struct PID : int_field {};
    struct CombDLLp : float_field {};
    struct CombDLLe : float_field {};
    struct CombDLLpi : float_field {};
    struct CombDLLk : float_field {};
    struct CombDLLmu : float_field {};

    template <typename T>
    using combdlls_t = SOACollection<T, CombDLLp, CombDLLe, CombDLLpi, CombDLLk, CombDLLmu>;
  } // namespace ChargedBasicsTag

  namespace v2 {
    struct RichPIDs : SOACollection<RichPIDs, ChargedBasicsTag::RichPIDCode> {
      using base_t = SOACollection<RichPIDs, ChargedBasicsTag::RichPIDCode>;
      using base_t::base_t;

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      struct PIDProxy : Proxy<simd, behaviour, ContainerType> {
        using Proxy<simd, behaviour, ContainerType>::Proxy;
        [[nodiscard, gnu::always_inline]] auto pid_code() const {
          return this->template get<ChargedBasicsTag::RichPIDCode>();
        }
      };

      template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
      using proxy_type = PIDProxy<simd, behaviour, ContainerType>;
    };
  } // namespace v2

  struct AssignedMasses : SOACollection<AssignedMasses, ChargedBasicsTag::Masse> {
    using base_t = SOACollection<AssignedMasses, ChargedBasicsTag::Masse>;
    using base_t::base_t;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct MasseProxy : Proxy<simd, behaviour, ContainerType> {
      using Proxy<simd, behaviour, ContainerType>::Proxy;
      [[nodiscard, gnu::always_inline]] auto mass() const { return this->template get<ChargedBasicsTag::Masse>(); }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = MasseProxy<simd, behaviour, ContainerType>;
  };

  struct ParticleIDs
      : SOACollection<ParticleIDs, ChargedBasicsTag::PID> { // TODO make this correct an actual particleid
    using base_t = SOACollection<ParticleIDs, ChargedBasicsTag::PID>;
    using base_t::base_t;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PIDProxy : Proxy<simd, behaviour, ContainerType> {
      using Proxy<simd, behaviour, ContainerType>::Proxy;
      [[nodiscard, gnu::always_inline]] auto pid() const { return this->template get<ChargedBasicsTag::PID>(); }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PIDProxy<simd, behaviour, ContainerType>;
  };

  struct CombDLLs : ChargedBasicsTag::combdlls_t<CombDLLs> {
    using base_t = typename ChargedBasicsTag::combdlls_t<CombDLLs>;
    using base_t::base_t;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct CombDLLProxy : Proxy<simd, behaviour, ContainerType> {
      using Proxy<simd, behaviour, ContainerType>::Proxy;
      [[nodiscard, gnu::always_inline]] auto CombDLLp() const {
        return this->template get<ChargedBasicsTag::CombDLLp>();
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLe() const {
        return this->template get<ChargedBasicsTag::CombDLLe>();
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLpi() const {
        return this->template get<ChargedBasicsTag::CombDLLpi>();
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLk() const {
        return this->template get<ChargedBasicsTag::CombDLLk>();
      }
      [[nodiscard, gnu::always_inline]] auto CombDLLmu() const {
        return this->template get<ChargedBasicsTag::CombDLLmu>();
      }
    };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = CombDLLProxy<simd, behaviour, ContainerType>;
  };

  namespace CompositeTags {
    // Constants defining array/matrix members
    inline constexpr std::size_t NumPositionEntries    = 3;
    inline constexpr std::size_t NumPositionCovEntries = NumPositionEntries * ( NumPositionEntries + 1 ) / 2;
    inline constexpr std::size_t NumFourMomEntries     = 4;
    inline constexpr std::size_t NumFourMomCovEntries  = NumFourMomEntries * ( NumFourMomEntries + 1 ) / 2;

    // Tag types
    struct PID : public Event::int_field {};
    struct nDoF : public Event::int_field {};
    struct Chi2 : public Event::float_field {};
    // Child relations -- two int columns per child.
    struct ChildRels : public Event::vector_field<Event::ints_field<2>> {
      enum { IndexOffset = 0, ZipFamilyOffset, NumColumns };
    };
    /** Descendant unique IDs -- one int column per descendant.
     *
     * By descendant we mean any decay product (including intermediate
     * states). e.g. For Bs0 -> J/psi(-> mu+ mu-) K*0(892)(-> K+ pi-) the
     * J/psi has as descendants (mu+, mu-) and the Bs0 has (J/psi, mu+, mu-,
     * K*0(892), K+, pi-).
     *
     * Unique IDs must be created using LHCb::UniqueIDGenerator (unless for test
     * and development scripts).
     */
    struct DescendantUniqueIDs : public Event::vector_field<Event::int_field> {};
    // Position vector (3D)
    struct Position : public Event::floats_field<NumPositionEntries> {};
    // Position covariance (3D symmetric)
    struct PositionCov : public Event::floats_field<NumPositionCovEntries> {};
    // Four-momentum (4D...)
    struct FourMom : public Event::floats_field<NumFourMomEntries> {};
    // Four-momentum covariance (4D symmetric)
    struct FourMomCov : public Event::floats_field<NumFourMomCovEntries> {};
    // Position-four-momentum covariance (4x3)
    struct PositionFourMomCov : public Event::floats_field<NumFourMomEntries, NumPositionEntries> {};

    template <typename T>
    using base_t = Event::SOACollection<T, PID, nDoF, Chi2, Position, PositionCov, FourMom, FourMomCov,
                                        PositionFourMomCov, ChildRels, DescendantUniqueIDs>;
  } // namespace CompositeTags

  struct Composites : public CompositeTags::base_t<Composites> {
    using base_t                                = typename CompositeTags::base_t<Composites>;
    constexpr static auto NumPositionEntries    = CompositeTags::NumPositionEntries;
    constexpr static auto NumPositionCovEntries = CompositeTags::NumPositionCovEntries;
    constexpr static auto NumFourMomEntries     = CompositeTags::NumFourMomEntries;
    constexpr static auto NumFourMomCovEntries  = CompositeTags::NumFourMomCovEntries;
    using base_t::allocator_type;
    using base_t::emplace_back;

    Composites( UniqueIDGenerator const& unique_id_gen,
                Zipping::ZipFamilyNumber zip_identifier = Zipping::generateZipIdentifier(), allocator_type alloc = {} )
        : base_t{std::move( zip_identifier ), std::move( alloc )}, m_unique_id_gen_tag{unique_id_gen.tag()} {}
    // Constructor used by zipping machinery when making a copy of a zip
    Composites( Zipping::ZipFamilyNumber zn, Composites const& old )
        : base_t{std::move( zn ), old}, m_unique_id_gen_tag{old.m_unique_id_gen_tag} {}

    // Define a custom proxy class for composites, allowing generic code to use
    // uniformly-named accessors.
    template <SIMDWrapper::InstructionSet simd, Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct Proxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = LHCb::Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using dType  = simd_t; // for compatibility, TODO remove

    private:
      /** (0
       *   1 2
       *   3 4 5)
       */
      constexpr static std::size_t sym_store_idx( std::size_t x, std::size_t y ) {
        auto col = std::min( x, y );
        auto row = std::max( x, y );
        return row * ( row + 1 ) / 2 + col;
      }

    public:
      // Standard accessors
      [[nodiscard, gnu::always_inline]] inline auto chi2() const { return this->template get<CompositeTags::Chi2>(); }
      [[nodiscard, gnu::always_inline]] inline auto nDoF() const { return this->template get<CompositeTags::nDoF>(); }

      [[nodiscard, gnu::always_inline]] auto pid() const { return this->template get<CompositeTags::PID>(); }

      [[nodiscard, gnu::always_inline]] auto e() const { return this->template get<CompositeTags::FourMom>( 3 ); }
      [[nodiscard, gnu::always_inline]] auto px() const { return this->template get<CompositeTags::FourMom>( 0 ); }
      [[nodiscard, gnu::always_inline]] auto py() const { return this->template get<CompositeTags::FourMom>( 1 ); }
      [[nodiscard, gnu::always_inline]] auto pz() const { return this->template get<CompositeTags::FourMom>( 2 ); }
      [[nodiscard, gnu::always_inline]] auto momentum() const { return LinAlg::Vec{px(), py(), pz(), e()}; }
      [[nodiscard, gnu::always_inline]] auto threeMomentum() const { return LinAlg::Vec3{px(), py(), pz()}; }
      [[nodiscard, gnu::always_inline]] auto slopes() const {
        auto mom = threeMomentum();
        return mom / Z( mom );
      }

      [[nodiscard, gnu::always_inline]] auto x() const { return this->template get<CompositeTags::Position>( 0 ); }
      [[nodiscard, gnu::always_inline]] auto y() const { return this->template get<CompositeTags::Position>( 1 ); }
      [[nodiscard, gnu::always_inline]] auto z() const { return this->template get<CompositeTags::Position>( 2 ); }
      [[nodiscard, gnu::always_inline]] auto endVertex() const {
        return LinAlg::Vec3{x(), y(), z()};
      } // FIXME: it is not guaranteed that the position at which the momentum is given is the 'endVertex' -- so fix
        // names!

      [[nodiscard, gnu::always_inline]] auto referencePoint() const {
        return endVertex();
      } // the point at which the momentum is given

      [[nodiscard, gnu::always_inline]] auto childRelationIndex( std::size_t n_child ) const {
        return this->template field<CompositeTags::ChildRels>()[n_child]
            .field( CompositeTags::ChildRels::IndexOffset )
            .get();
      }
      [[nodiscard, gnu::always_inline]] auto childRelationFamily( std::size_t n_child ) const {
        return this->template field<CompositeTags::ChildRels>()[n_child]
            .field( CompositeTags::ChildRels::ZipFamilyOffset )
            .get();
      }
      [[nodiscard, gnu::always_inline]] auto descendant_unique_id( std::size_t n_child ) const {
        return UniqueIDGenerator::ID<typename simd_t::int_v>{
            this->template field<CompositeTags::DescendantUniqueIDs>()[n_child].get(),
            this->container()->unique_id_gen_tag()};
      }
      [[nodiscard, gnu::always_inline]] auto momCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::FourMomCov>( sym_store_idx( i, j ) );
      }
      [[nodiscard, gnu::always_inline]] auto momPosCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::PositionFourMomCov>( i, j );
      }
      [[nodiscard, gnu::always_inline]] auto numDescendants() const {
        return this->template field<CompositeTags::DescendantUniqueIDs>().maxSize();
      }
      [[nodiscard, gnu::always_inline]] auto posCovElement( std::size_t i, std::size_t j ) const {
        return this->template get<CompositeTags::PositionCov>( sym_store_idx( i, j ) );
      }
      [[nodiscard, gnu::always_inline]] auto posCovMatrix() const {
        using Utils::unwind;
        LinAlg::MatSym<decltype( this->posCovElement( 0, 0 ) ), CompositeTags::NumPositionEntries> out;
        unwind<0, CompositeTags::NumPositionEntries>(
            [&]( auto i ) { unwind<0, i + 1>( [&]( auto j ) { out( i, j ) = this->posCovElement( i, j ); } ); } );
        return out;
      }

      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = CanBeExtrapolatedDownstream::no;

    private:
      // Helper for returning bits of the momentum covariance matrix
      template <std::size_t FirstFourMomElement, std::size_t NumFourMomEntries>
      [[nodiscard]] auto momCovMatrixImpl() const {
        using Utils::unwind;
        LinAlg::MatSym<decltype( this->momCovElement( 0, 0 ) ), NumFourMomEntries> out;
        unwind<FirstFourMomElement, FirstFourMomElement + NumFourMomEntries>( [&]( auto i ) {
          unwind<FirstFourMomElement, i + 1>( [&]( auto j ) { out( i, j ) = this->momCovElement( i, j ); } );
        } );
        return out;
      }
      // Helper for returning bits of the momentum - position covariance matrix
      template <std::size_t FirstFourMomElement, std::size_t FirstPosElement, std::size_t NumFourMomEntries,
                std::size_t NumPositionEntries>
      [[nodiscard]] auto momPosCovMatrixImpl() const {
        LinAlg::Mat<decltype( this->momPosCovElement( 0, 0 ) ), NumFourMomEntries, NumPositionEntries> out;
        // TODO check the ordering is correct
        using Utils::unwind;
        unwind<FirstFourMomElement, FirstFourMomElement + NumFourMomEntries>( [&]( auto i ) {
          unwind<FirstPosElement, FirstPosElement + NumPositionEntries>(
              [&]( auto j ) { out( i, j ) = this->momPosCovElement( i, j ); } );
        } );
        return out;
      }

    public:
      [[nodiscard, gnu::always_inline]] auto momCovMatrix() const {
        return this->momCovMatrixImpl<0, CompositeTags::NumFourMomEntries>();
      }
      // Note tacit assumption that the order is (px, py, pz, pe)
      [[nodiscard, gnu::always_inline]] auto threeMomCovMatrix() const { return this->momCovMatrixImpl<0, 3>(); }
      [[nodiscard, gnu::always_inline]] auto momPosCovMatrix() const {
        // default, {4, 3} shape
        return this->momPosCovMatrixImpl<0, 0, CompositeTags::NumFourMomEntries, CompositeTags::NumPositionEntries>();
      }
      [[nodiscard, gnu::always_inline]] auto threeMomPosCovMatrix() const {
        // ignore the 4th/energy part of the momentum covariance and return a 3x3
        return this
            ->momPosCovMatrixImpl<0, 0, CompositeTags::NumFourMomEntries - 1, CompositeTags::NumPositionEntries>();
      }
      /** Return the full 7x7 covariance matrix.
       *  Order: {x, y, z, px, py, pz, pe}
       */
      [[nodiscard]] auto covMatrix() const {
        LinAlg::MatSym<decltype( this->posCovElement( 0, 0 ) ),
                       CompositeTags::NumPositionEntries + CompositeTags::NumFourMomEntries>
            out;
        out = out.template place_at<0, 0>( this->posCovMatrix() );
        out = out.template place_at<CompositeTags::NumPositionEntries, 0>( this->momPosCovMatrix() );
        return out.template place_at<CompositeTags::NumPositionEntries, CompositeTags::NumPositionEntries>(
            this->momCovMatrix() );
      }

      // Accessors for derived quantities
      [[nodiscard, gnu::always_inline]] auto pt() const {
        auto const px = this->px();
        auto const py = this->py();
        return sqrt( px * px + py * py );
      }
      [[nodiscard, gnu::always_inline]] auto mom2() const {
        auto const px = this->px();
        auto const py = this->py();
        auto const pz = this->pz();
        return px * px + py * py + pz * pz;
      }
      [[nodiscard, gnu::always_inline]] auto p() const { return sqrt( this->mom2() ); }
      [[nodiscard, gnu::always_inline]] auto mass2() const {
        auto const energy = this->e();
        return energy * energy - this->mom2();
      }
      [[nodiscard, gnu::always_inline]] auto mass() const { return sqrt( this->mass2() ); }
      // Accessors for container-level information
      [[nodiscard, gnu::always_inline]] auto numChildren() const {
        return this->template field<CompositeTags::ChildRels>().maxSize();
      }
      [[nodiscard, gnu::always_inline]] auto zipIdentifier() const { return this->container()->zipIdentifier(); }
    };
    template <SIMDWrapper::InstructionSet simd, Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = Proxy<simd, behaviour, ContainerType>;

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best, typename F, typename I,
              typename M                       = std::true_type>
    void emplace_back( LinAlg::Vec<F, NumPositionEntries> const& pos, LinAlg::Vec<F, NumFourMomEntries> const& p4,
                       I const& pid, F const& chi2, I const& ndof, LinAlg::MatSym<F, NumPositionEntries> const& pos_cov,
                       LinAlg::MatSym<F, NumFourMomEntries> const&                  p4_cov,
                       LinAlg::Mat<F, NumFourMomEntries, NumPositionEntries> const& mom_pos_cov,
                       span<std::add_const_t<I>> child_indices, span<std::add_const_t<I>> child_zip_ids,
                       std::vector<UniqueIDGenerator::ID<I>> const& descendant_unique_ids, M&& mask = {} ) {

      auto const& proxy = [&]() {
        if constexpr ( std::is_same_v<std::true_type, std::decay_t<M>> ) {
          return emplace_back<simd>();
        } else {
          return compress_back<simd>( mask );
        }
      }();

      proxy.template field<CompositeTags::PID>().set( pid );
      proxy.template field<CompositeTags::Chi2>().set( chi2 );
      proxy.template field<CompositeTags::nDoF>().set( ndof );
      Utils::unwind<0, NumPositionEntries>(
          [&]( auto i ) { proxy.template field<CompositeTags::Position>( i ).set( pos( i ) ); } );
      Utils::unwind<0, NumPositionCovEntries>(
          [&]( auto i ) { proxy.template field<CompositeTags::PositionCov>( i ).set( pos_cov.m[i] ); } );
      Utils::unwind<0, NumFourMomEntries>(
          [&]( auto i ) { proxy.template field<CompositeTags::FourMom>( i ).set( p4( i ) ); } );
      Utils::unwind<0, NumFourMomCovEntries>(
          [&]( auto i ) { proxy.template field<CompositeTags::FourMomCov>( i ).set( p4_cov.m[i] ); } );
      // This is using the element ordering imposed by MatVec for a generic
      // matrix, we have to be careful to follow the same convention below.
      // That's not so nice...TODO improve it in future.
      Utils::unwind<0, NumFourMomEntries>( [&]( auto i ) {
        Utils::unwind<0, NumPositionEntries>( [&]( auto j ) {
          proxy.template field<CompositeTags::PositionFourMomCov>( i, j ).set(
              mom_pos_cov.m[i * NumPositionEntries + j] );
        } );
      } );

      assert( child_indices.size() == child_zip_ids.size() );
      proxy.template field<CompositeTags::ChildRels>().resize( child_indices.size() );
      for ( auto i = 0u; i < child_indices.size(); ++i ) {
        proxy.template field<CompositeTags::ChildRels>()[i]
            .field( CompositeTags::ChildRels::IndexOffset )
            .set( child_indices[i] );
        proxy.template field<CompositeTags::ChildRels>()[i]
            .field( CompositeTags::ChildRels::ZipFamilyOffset )
            .set( child_zip_ids[i] );
      }

      proxy.template field<CompositeTags::DescendantUniqueIDs>().resize( descendant_unique_ids.size() );
      for ( auto i = 0u; i < descendant_unique_ids.size(); ++i ) {
        assert( descendant_unique_ids[i].generator_tag() == m_unique_id_gen_tag );
        proxy.template field<CompositeTags::DescendantUniqueIDs>()[i].set( descendant_unique_ids[i].value() );
      }
    }

    template <SIMDWrapper::InstructionSet simd = SIMDWrapper::InstructionSet::Best, typename F, typename I,
              std::size_t                 NChildren, typename M = std::true_type>
    void emplace_back( LinAlg::Vec<F, NumPositionEntries> const& pos, LinAlg::Vec<F, NumFourMomEntries> const& p4,
                       I const& pid, F const& chi2, I const& ndof, LinAlg::MatSym<F, NumPositionEntries> const& pos_cov,
                       LinAlg::MatSym<F, NumFourMomEntries> const&                  p4_cov,
                       LinAlg::Mat<F, NumFourMomEntries, NumPositionEntries> const& mom_pos_cov,
                       std::array<I, NChildren> const& child_indices, std::array<I, NChildren> const& child_zip_ids,
                       std::vector<UniqueIDGenerator::ID<I>> const& descendant_unique_ids, M&& mask = {} ) {
      emplace_back<simd>( pos, p4, pid, chi2, ndof, pos_cov, p4_cov, mom_pos_cov, span{child_indices},
                          span{child_zip_ids}, descendant_unique_ids, mask );
    }

    auto const& unique_id_gen_tag() const { return m_unique_id_gen_tag; }

  private:
    /// Keep the identifier of the generator used to build this container
    boost::uuids::uuid m_unique_id_gen_tag;
  };
} // namespace LHCb::Event

REGISTER_HEADER( LHCb::Event::v2::RichPIDs, "Event/Particle_v2.h" );
REGISTER_HEADER( LHCb::Event::AssignedMasses, "Event/Particle_v2.h" );
REGISTER_HEADER( LHCb::Event::ParticleIDs, "Event/Particle_v2.h" );
REGISTER_HEADER( LHCb::Event::CombDLLs, "Event/Particle_v2.h" );
REGISTER_HEADER( LHCb::Event::Composites, "Event/Particle_v2.h" );
template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
struct LHCb::header_map<LHCb::Event::Composites::Proxy<simd, behaviour, ContainerType>> {
  constexpr static auto value = LHCb::header_map_v<LHCb::Event::Composites>;
};

namespace LHCb::Event {
  // TODO add all the other extrainfo things
  using ChargedBasics = zip_t<v3::Tracks, v2::RichPIDs, v2::Muon::PIDs, AssignedMasses, ParticleIDs, CombDLLs>;
  using Particles     = LHCb::variant<ChargedBasics, Zip<SIMDWrapper::Best, Composites const>>;

  template <SIMDWrapper::InstructionSet instruction_set, Pr::ProxyBehaviour behaviour>
  auto fourMomentum(
      typename zip_proxy_types_adapter<ChargedBasics>::const_proxy_type<instruction_set, behaviour> const& proxy ) {

    auto p3   = threeMomentum( proxy );
    auto px   = p3.x;
    auto py   = p3.y;
    auto pz   = p3.z;
    auto mass = proxy.mass();
    return LinAlg::Vec{px, py, pz, sqrt( px * px + py * py + pz * pz + mass * mass )};
  }
} // namespace LHCb::Event

REGISTER_HEADER( LHCb::Event::ChargedBasics, "Event/Particle_v2.h" );
