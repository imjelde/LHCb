/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"

/** @file  MuonPIDs_v2.h
 *  @brief Definition of object for MuonPID.
 */
namespace LHCb::Event::v2::Muon {
  enum StatusMasks { IsMuon = 0x1, InAcceptance = 0x2, PreSelMomentum = 0x4, IsMuonLoose = 0x8, IsMuonTight = 0x10 };

  namespace details {
    template <StatusMasks Mask>
    constexpr int setFlag( int input, bool value ) {
      return input ^ ( ( -value ^ input ) & Mask );
    }
  } // namespace details

  namespace Tag {
    struct Status : Event::int_field {};
    struct Chi2Corr : Event::float_field {};
    struct IsMuonTight : Event::int_field {};
    struct InAcceptance : Event::int_field {};
    struct PreSelMomentum : Event::int_field {};

    template <typename T>
    using pids_t = Event::SOACollection<T, Status, Chi2Corr, IsMuonTight, InAcceptance, PreSelMomentum>;
  } // namespace Tag

  struct PIDs : Tag::pids_t<PIDs> {
    using base_t = typename Tag::pids_t<PIDs>;
    using base_t::base_t;

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct PIDProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using Event::Proxy<simd, behaviour, ContainerType>::Proxy;
      using simd_t = SIMDWrapper::type_map_t<simd>;
      using int_v  = typename simd_t::int_v;

      [[nodiscard]] auto IsMuon() const {
        auto status = this->template get<Tag::Status>();
        return !( ( status & int_v{StatusMasks::IsMuon} ) == int_v{0} );
      }
      [[nodiscard]] auto Chi2Corr() const { return this->template get<Tag::Chi2Corr>(); }
      [[nodiscard]] auto IsMuonTight() const {
        auto status = this->template get<Tag::Status>();
        return !( ( status & int_v{StatusMasks::IsMuonTight} ) == int_v{4} );
      }
      [[nodiscard]] auto InAcceptance() const {
        auto status = this->template get<Tag::Status>();
        return !( ( status & int_v{StatusMasks::InAcceptance} ) == int_v{1} );
      }
      [[nodiscard]] auto PreSelMomentum() const {
        auto status = this->template get<Tag::Status>();
        return !( ( status & int_v{StatusMasks::PreSelMomentum} ) == int_v{2} );
      }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = PIDProxy<simd, behaviour, ContainerType>;
  };
} // namespace LHCb::Event::v2::Muon

REGISTER_HEADER( LHCb::Event::v2::Muon::PIDs, "Event/MuonPIDs_v2.h" );
