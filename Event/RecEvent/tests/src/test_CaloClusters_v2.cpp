/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestCaloClustersV2
#include "Event/CaloClusters_v2.h"
#include <boost/test/unit_test.hpp>
#include <type_traits>

using namespace LHCb::Event::Calo;

using Cluster = Clusters::const_reference;

static_assert( std::is_nothrow_move_constructible_v<Cluster> );
static_assert( std::is_nothrow_copy_constructible_v<Cluster> );
static_assert( std::is_nothrow_move_assignable_v<Cluster> );
static_assert( std::is_nothrow_copy_assignable_v<Cluster> );

static_assert( std::is_trivially_copyable_v<Cluster> );
static_assert( std::is_trivially_destructible_v<Cluster> );

static_assert( std::is_nothrow_move_assignable_v<Clusters> );
static_assert( std::is_nothrow_move_constructible_v<Clusters> );
// note: container should _not_ by copyable, as that would allow wastefull code...
// static_assert( !std::is_copy_constructible_v<Clusters> );
// static_assert( !std::is_copy_assignable_v<Clusters> );
// static_assert( !std::is_assignable_v<Clusters,Clusters> );

constexpr auto cov_isnan = []( const auto& cov ) {
  using Idx = LHCb::CaloPosition::Index;
  return std::isnan( cov( Idx::X, Idx::X ) ) || std::isnan( cov( Idx::X, Idx::Y ) ) ||
         std::isnan( cov( Idx::Y, Idx::Y ) ) || std::isnan( cov( Idx::X, Idx::E ) ) ||
         std::isnan( cov( Idx::Y, Idx::E ) ) || std::isnan( cov( Idx::E, Idx::E ) );
};

void get_nonconst_ref( Gaudi::XYZPointF& ) {}
void get_nonconst_ref( Gaudi::SymMatrix3x3& ) {}
void get_nonconst_ref( Gaudi::SymMatrix2x2& ) {}
void get_nonconst_ref( float& ) {}

void get_const_ref( Gaudi::XYZPointF const& ) {}
void get_const_ref( Gaudi::SymMatrix3x3 const& ) {}
void get_const_ref( Gaudi::SymMatrix2x2 const& ) {}
void get_const_ref( float const& ) {}

// this _should_ compile
void clus_ref( LHCb::Event::Calo::Clusters::reference c ) {
  get_const_ref( c.energy() );
  get_const_ref( c.position() );
  get_const_ref( c.covariance() );
  get_const_ref( c.spread() );

  get_nonconst_ref( c.energy() );
  get_nonconst_ref( c.position() );
  get_nonconst_ref( c.covariance() );
  get_nonconst_ref( c.spread() );
}

void clus_const_ref( LHCb::Event::Calo::Clusters::const_reference c ) {
  get_const_ref( c.energy() );
  get_const_ref( c.position() );
  get_const_ref( c.covariance() );
  get_const_ref( c.spread() );
#if 0
// should not compile!!!
    get_nonconst_ref( c.energy() );
    get_nonconst_ref( c.position() );
    get_nonconst_ref( c.covariance() );
    get_nonconst_ref( c.spread() );
#endif
}

BOOST_AUTO_TEST_CASE( test_caloclusters ) {
  Clusters clusters;
  clusters.reserveForEntries( 100 );

  for ( unsigned c = 0; c < 5; ++c ) {
    LHCb::Calo::CellID seed{LHCb::Calo::CellCode::Index::EcalCalo, 0, 12, 4 + 2 * c};
    float              energy = 1 + c * 5;
    int                o      = clusters.totalNumberOfEntries();
    clusters.emplace_back( seed, energy, LHCb::CaloDigitStatus::Mask::Undefined );
    clusters.emplace_back( {seed.calo(), seed.area(), seed.row() + 1, seed.col()}, energy / 2,
                           LHCb::CaloDigitStatus::Mask::Undefined );
    clusters.emplace_back( {seed.calo(), seed.area(), seed.row(), seed.col() + 1}, energy / 3,
                           LHCb::CaloDigitStatus::Mask::Undefined );
    auto cluster = clusters.emplace_back( seed, Clusters::Type::CellularAutomaton, {o, 3},
                                          energy * ( 1 + 1.0f / 2 + 1.0f / 3 ), {c + 0.f, c + 0.f, 10000.f} );
    BOOST_CHECK( cluster.cellID() == seed );
  }

  for ( const auto& c : clusters ) { BOOST_CHECK( !cov_isnan( c.covariance() ) ); }

  auto clusters2 = clusters;
  BOOST_CHECK( clusters2.size() == clusters.size() );

  for ( const auto& c : clusters2 ) { BOOST_CHECK( !cov_isnan( c.covariance() ) ); }

  auto c2 = clusters2.emplace_back( clusters[0] );
  BOOST_CHECK( !cov_isnan( c2.covariance() ) );

  BOOST_CHECK( ( c2.position() != Gaudi::XYZPointF{0, 0, 0} ) );
  c2.position().SetX( 0 );
  c2.position().SetY( 0 );
  c2.position().SetZ( 0 );
  BOOST_CHECK( ( c2.position() == Gaudi::XYZPointF{0, 0, 0} ) );

  // clusters.sort( );
}
