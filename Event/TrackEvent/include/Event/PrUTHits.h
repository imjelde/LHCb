/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/SOACollection.h"
#include "Kernel/EventLocalAllocator.h"
#include "LHCbMath/SIMDWrapper.h"
#include "PrTracksTag.h"

/** @class PrUTHits PrUTHits.h
 *  SoA Implementation of Upstream tracker hit for pattern recognition
 *  @author Michel De Cian, based on Arthur Hennequin's PrVeloHits
 *  @date   2019-11-07
 */

namespace LHCb::Pr::UT {

  namespace UTHitsTag {

    struct channelID : Event::int_field {};
    struct weight : Event::float_field {};
    struct xAtYEq0 : Event::float_field {};
    struct yBegin : Event::float_field {};
    struct yEnd : Event::float_field {};
    struct zAtYEq0 : Event::float_field {};
    struct dxDy : Event::float_field {};
    struct cos : Event::float_field {};

    template <typename T>
    using uthit_t = Event::SOACollection<T, channelID, weight, xAtYEq0, yBegin, yEnd, zAtYEq0, dxDy, cos>;
  } // namespace UTHitsTag

  struct Hits : UTHitsTag::uthit_t<Hits> {
    using base_t = typename UTHitsTag::uthit_t<Hits>;
    using base_t::base_t;
  };
} // namespace LHCb::Pr::UT
