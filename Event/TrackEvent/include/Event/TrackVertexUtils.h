/***************************************************************************** \
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/State.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/MatVec.h"
#include "LHCbMath/StateVertexUtils.h"

namespace LHCb::TrackVertexUtils {
  using namespace StateVertexUtils;

  /////////////////////////////////////////////////////////////////////////
  /// Add a track to a vertex represented by a position and a cov
  /// matrix. Returns the chi2 increment. This routine calls the
  /// routine below that also has a vertex weight matrix, because
  /// the formalism requires the computation of both. If you add
  /// tracks to vertex one-by-one, then it makes sense to keep the
  /// weight matrix as this is more precise.
  /////////////////////////////////////////////////////////////////////////
  double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Add a track to a vertex represented by a position and a weight
  /// matrix. Both the weight matrix and the covariance matrix are
  /// computed. Returns the chi2
  /////////////////////////////////////////////////////////////////////////
  double addToVertex( const LHCb::State& state, Gaudi::XYZPoint& vertexpos, Gaudi::SymMatrix3x3& vertexweight,
                      Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Computes a vertex from two track states.
  /////////////////////////////////////////////////////////////////////////
  double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                 Gaudi::SymMatrix3x3& vertexweight, Gaudi::SymMatrix3x3& vertexcov );

  /////////////////////////////////////////////////////////////////////////
  /// Computes a vertex from two track states.
  /////////////////////////////////////////////////////////////////////////
  double vertex( const LHCb::State& stateA, const LHCb::State& stateB, Gaudi::XYZPoint& vertexpos,
                 Gaudi::SymMatrix3x3& vertexcov );

} // namespace LHCb::TrackVertexUtils
