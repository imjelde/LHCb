/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once
#include "Event/PrLongTracks.h"
#include "Event/PrProxyHelpers.h"
#include "Event/PrTracksTag.h"
#include "Event/UniqueIDGenerator.h"
#include "Kernel/EventLocalAllocator.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Traits.h"
#include "LHCbMath/SIMDWrapper.h"
#include "LHCbMath/Vec3.h"
#include "TrackEnums.h"

/**
 * Track data after the Kalman fit
 *
 * @author: Arthur Hennequin
 * 2020-08-17 updated to SOACollection by Peilian Li
 */

namespace TracksInfo = LHCb::Pr::TracksInfo;

namespace LHCb::Pr::Fitted::Forward {
  namespace detail {
    /** Helper type for fitted track proxies -- TODO: remove this wrapper, use ADL on top of proxy itself instead... **/
    template <typename TrackProxy>
    struct FittedState {
      FittedState( TrackProxy proxy ) : m_proxy{std::move( proxy )} {}
      decltype( auto ) qOverP() const { return m_proxy.qOverP(); }
      decltype( auto ) momentum() const { return m_proxy.momentum(); }
      decltype( auto ) slopes() const { return m_proxy.closestToBeamStateDir(); }
      decltype( auto ) position() const { return m_proxy.closestToBeamStatePos(); }
      decltype( auto ) covariance() const { return m_proxy.closestToBeamStateCovariance(); }
      decltype( auto ) x() const { return position().X(); }
      decltype( auto ) y() const { return position().Y(); }
      decltype( auto ) z() const { return position().Z(); }
      decltype( auto ) tx() const { return slopes().X(); }
      decltype( auto ) ty() const { return slopes().Y(); }

      [[nodiscard, gnu::always_inline]] friend auto referencePoint( const FittedState& p ) { return p.position(); }
      [[nodiscard, gnu::always_inline]] friend auto threeMomentum( const FittedState& p ) { return p.momentum(); }

      // flag which indicates client code can go for 'threeMomCovMatrix', `momPosCovMatrix` and `posCovMatrix` and not
      // for a track-like stateCov -- possible values: yes (for track-like objects ) no (for neutrals, composites),
      // maybe (particle, check at runtime, calls may return invalid results)
      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;

    private:
      TrackProxy m_proxy;
    };
  } // namespace detail

  enum struct CovXVector { x_x, x_tx, tx_tx };
  enum struct CovYVector { y_y, y_ty, ty_ty };

  struct Tag {
    struct trackSeed : Event::int_field {};
    struct Chi2 : Event::float_field {};
    struct Chi2nDoF : Event::int_field {};

    struct UniqueID : Event::int_field {};

    struct State : Event::state_field {};
    struct StateCovX : Event::floats_field<TracksInfo::NumCovXY> {};
    struct StateCovY : Event::floats_field<TracksInfo::NumCovXY> {};

    template <typename T>
    using fitfwd_t = Event::SOACollection<T, trackSeed, Chi2, Chi2nDoF, UniqueID, State, StateCovX, StateCovY>;
  };

  namespace SP = LHCb::Event::StateParameters;

  struct Tracks : Tag::fitfwd_t<Tracks> {
    using base_t = typename Tag::fitfwd_t<Tracks>;
    using tag_t  = Tag; // Needed for tag_type_t helper defined in PrTracksTag.h

    using base_t::allocator_type;
    using History = Event::Enum::Track::History;

    Tracks( LHCb::Pr::Long::Tracks const* forward_ancestors, LHCb::UniqueIDGenerator const& unique_id_gen,
            History history, Zipping::ZipFamilyNumber zipIdentifier = Zipping::generateZipIdentifier(),
            allocator_type alloc = {} )
        : base_t{std::move( zipIdentifier ), std::move( alloc )}
        , m_forward_ancestors{forward_ancestors}
        , m_unique_id_gen_tag{unique_id_gen.tag()}
        , m_history{history} {}

    // Special constructor for zipping machinery
    Tracks( Zipping::ZipFamilyNumber zipIdentifier, Tracks const& other )
        : base_t{std::move( zipIdentifier ), other}
        , m_forward_ancestors{other.m_forward_ancestors}
        , m_unique_id_gen_tag{other.m_unique_id_gen_tag}
        , m_history{other.m_history} {}

    [[nodiscard, gnu::always_inline]] LHCb::Pr::Long::Tracks const* getForwardAncestors() const noexcept {
      return m_forward_ancestors;
    };

    [[nodiscard, gnu::always_inline]] History history() const { return m_history; };

    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    struct FittedProxy : Event::Proxy<simd, behaviour, ContainerType> {
      using base_t = typename Event::Proxy<simd, behaviour, ContainerType>;
      using base_t::base_t;
      using base_t::loop_mask;
      using base_t::width;
      using simd_t  = SIMDWrapper::type_map_t<simd>;
      using int_v   = typename simd_t::int_v;
      using float_v = typename simd_t::float_v;

      [[nodiscard, gnu::always_inline]] auto trackSeed() const { return this->template get<Tag::trackSeed>(); }
      [[nodiscard, gnu::always_inline]] auto qOverP() const { return this->template get<Tag::State>().qOverP(); }
      [[nodiscard, gnu::always_inline]] auto p() const { return abs( 1.f / qOverP() ); }
      [[nodiscard, gnu::always_inline]] auto chi2() const { return this->template get<Tag::Chi2>(); }
      [[nodiscard, gnu::always_inline]] auto chi2nDoF() const { return this->template get<Tag::Chi2nDoF>(); }
      [[nodiscard, gnu::always_inline]] auto nDoF() const { return chi2nDoF(); }
      [[nodiscard, gnu::always_inline]] auto chi2PerDoF() const { return chi2(); }
      [[nodiscard, gnu::always_inline]] auto pt2() const {
        auto const mom    = p();
        auto const s      = this->template get<Tag::State>();
        auto const tx2ty2 = s.tx() * s.tx() + s.ty() * s.ty();
        return mom * mom * tx2ty2 / ( tx2ty2 + 1 );
      }
      [[nodiscard, gnu::always_inline]] auto pt() const {
        using std::sqrt;
        return sqrt( pt2() );
      }
      [[nodiscard, gnu::always_inline]] auto slopes() const { return closestToBeamStateDir(); }
      [[nodiscard, gnu::always_inline]] auto threeMomentum() const {
        auto const dir = slopes();
        return dir * ( p() / dir.mag() );
      }
      [[nodiscard, gnu::always_inline]] auto closestToBeamStatePos() const {
        auto const state = this->template get<Tag::State>();
        return LinAlg::Vec3<float_v>( state.x(), state.y(), state.z() );
      }
      [[nodiscard, gnu::always_inline]] auto closestToBeamStateDir() const {
        auto const state = this->template get<Tag::State>();
        return LinAlg::Vec3<float_v>( state.tx(), state.ty(), 1.f );
      }
      [[nodiscard, gnu::always_inline]] auto closestToBeamState() const { return detail::FittedState{*this}; }
      [[nodiscard, gnu::always_inline]] auto covX() const {
        return LinAlg::Vec3<float_v>( this->template get<Tag::StateCovX>( 0 ), this->template get<Tag::StateCovX>( 1 ),
                                      this->template get<Tag::StateCovX>( 2 ) );
      }
      [[nodiscard, gnu::always_inline]] auto covY() const {
        return Vec3<float_v>( this->template get<Tag::StateCovY>( 0 ), this->template get<Tag::StateCovY>( 1 ),
                              this->template get<Tag::StateCovY>( 2 ) );
      }
      [[nodiscard, gnu::always_inline]] auto charge() const { return select( qOverP() > 0.f, int_v{+1}, int_v{-1} ); }
      [[nodiscard, gnu::always_inline]] auto closestToBeamStateCovariance() const {
        // This is not zero-initialised
        LHCb::LinAlg::MatSym<float_v, 5> cov;
        auto const                       qop = qOverP();
        cov( 0, 0 )                          = covX().x; // x error
        cov( 0, 2 )                          = covX().y; // x-tx covariance
        cov( 2, 2 )                          = covX().z; // tx error
        cov( 1, 1 )                          = covY().x; // y error
        cov( 1, 3 )                          = covY().y; // y-ty covariance
        cov( 3, 3 )                          = covY().z; // ty error
        cov( 0, 1 )                          = 0.f;      // x-y covariance
        cov( 0, 3 )                          = 0.f;      // x-ty covariance
        cov( 0, 4 )                          = 0.f;      // x-qop covariance
        cov( 1, 2 )                          = 0.f;      // y-tx covariance
        cov( 1, 4 )                          = 0.f;      // y-qop covariance
        cov( 2, 3 )                          = 0.f;      // tx-ty covariance
        cov( 2, 4 )                          = 0.f;      // tx-qop covariance
        cov( 3, 4 )                          = 0.f;      // ty-qop covariance
        // FIXME this hack should not be hardcoded, the factor does significantly
        // impact the p4cov of a particle that is fitted with this daughter.
        // ipchi2 of the mother is basically useless in that case!
        cov( 4, 4 ) = 0.6e-5f * qop * qop; // qop erro
        return cov;
      }

      // @brief Return a sorted vector of unique LHCbID objects on the t'th track.
      [[nodiscard]] std::vector<LHCbID> lhcbIDs() const {
        static_assert( width() == 1, "lhcbIDs() method cannot be used on vector proxies" );
        auto const* forward_tracks = this->container()->getForwardAncestors();
        if ( !forward_tracks ) { return std::vector<LHCbID>{}; }
        const int  forward_track_index    = trackSeed().cast();
        auto const iterable_tracks_scalar = forward_tracks->scalar();
        auto       lhcbids                = iterable_tracks_scalar[forward_track_index].lhcbIDs();

        // Sort and remove duplicates
        std::sort( lhcbids.begin(), lhcbids.end() );
        lhcbids.erase( std::unique( lhcbids.begin(), lhcbids.end() ), lhcbids.end() );

        return lhcbids;
      }

      [[nodiscard, gnu::always_inline]] auto unique_id() const {
        return LHCb::UniqueIDGenerator::ID<int_v>{this->template get<Tag::UniqueID>(), unique_id_gen_tag()};
      }

      [[nodiscard, gnu::always_inline]] auto unique_id_gen_tag() const {
        return this->container()->unique_id_gen_tag();
      }

      [[nodiscard, gnu::always_inline]] auto history() const { return this->container()->history(); };

      auto nHits() const {
        auto const* forward = this->container()->getForwardAncestors();
        if constexpr ( std::is_same_v<simd_t, SIMDWrapper::scalar::types> ) {
          if ( !forward ) return SIMDWrapper::scalar::int_v{0};
          auto iterable_tracks = forward->scalar();
          return iterable_tracks[trackSeed().cast()].nHits();
        } else {
          if ( !forward ) return int_v{0};
          auto const indices         = trackSeed();
          auto const iterable_tracks = forward->template simd<simd>();
          auto const tracks          = iterable_tracks.gather( indices, loop_mask() );
          return tracks.nHits();
        }
      }

      auto nVPHits() const {
        auto const* forward = this->container()->getForwardAncestors();
        if constexpr ( std::is_same_v<simd_t, SIMDWrapper::scalar::types> ) {
          if ( !forward ) return SIMDWrapper::scalar::int_v{0};
          auto iterable_tracks = forward->scalar();
          return iterable_tracks[trackSeed().cast()].nVPHits();
        } else {
          if ( !forward ) return int_v{0};
          auto const indices         = trackSeed();
          auto const iterable_tracks = forward->template simd<simd>();
          auto const tracks          = iterable_tracks.gather( indices, loop_mask() );
          return tracks.nVPHits();
        }
      }

      auto nUTHits() const {
        auto const* forward = this->container()->getForwardAncestors();
        if constexpr ( std::is_same_v<simd_t, SIMDWrapper::scalar::types> ) {
          if ( !forward ) return SIMDWrapper::scalar::int_v{0};
          auto iterable_tracks = forward->scalar();
          return iterable_tracks[trackSeed().cast()].nUTHits();
        } else {
          if ( !forward ) return int_v{0};
          auto const indices         = trackSeed();
          auto const iterable_tracks = forward->template simd<simd>();
          auto const tracks          = iterable_tracks.gather( indices, loop_mask() );
          return tracks.nUTHits();
        }
      }

      auto nFTHits() const {
        auto const* forward = this->container()->getForwardAncestors();
        if constexpr ( std::is_same_v<simd_t, SIMDWrapper::scalar::types> ) {
          if ( !forward ) return SIMDWrapper::scalar::int_v{0};
          auto iterable_tracks = forward->scalar();
          return iterable_tracks[trackSeed().cast()].nFTHits();
        } else {
          if ( !forward ) return int_v{0};
          auto const indices         = trackSeed();
          auto const iterable_tracks = forward->template simd<simd>();
          auto const tracks          = iterable_tracks.gather( indices, loop_mask() );
          return tracks.nFTHits();
        }
      }

      [[nodiscard, gnu::always_inline]] friend auto referencePoint( const FittedProxy& fp ) {
        return fp.closestToBeamStatePos();
      }
      static constexpr auto canBeExtrapolatedDownstream = Event::CanBeExtrapolatedDownstream::yes;

      [[nodiscard, gnu::always_inline]] friend auto trackState( FittedProxy const& fp ) {
        return fp.closestToBeamState();
      }
    };
    template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
    using proxy_type = FittedProxy<simd, behaviour, ContainerType>;

    auto const& unique_id_gen_tag() const { return m_unique_id_gen_tag; }

  private:
    LHCb::Pr::Long::Tracks const* m_forward_ancestors{nullptr};

    /// Keep the identifier of the generator used to build this container
    boost::uuids::uuid m_unique_id_gen_tag;
    History            m_history{};
  };
} // namespace LHCb::Pr::Fitted::Forward

REGISTER_HEADER( LHCb::Pr::Fitted::Forward::Tracks, "Event/PrFittedForwardTracks.h" );
