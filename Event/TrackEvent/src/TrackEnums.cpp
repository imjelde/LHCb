/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/TrackEnums.h"
#include "Gaudi/Parsers/CommonParsers.h"
#include "GaudiKernel/GaudiException.h"

namespace {
  template <typename Range, typename Inserter>
  StatusCode convert( const Range& from, Inserter to ) {
    try {
      std::transform( begin( from ), end( from ), to, []( const std::string& str ) {
        using Inner = typename Inserter::container_type::value_type;
        Inner t{};
        parse( t, str ).orThrow( "Bad Parse", "" );
        return t;
      } );
      return StatusCode::SUCCESS;
    } catch ( const GaudiException& e ) { return e.code(); }
  }

  template <typename Inner>
  StatusCode vparse( std::vector<Inner>& v, const std::string& in ) {
    v.clear();
    using Gaudi::Parsers::parse;
    std::vector<std::string> vs;
    return parse( vs, in ).andThen( [&] {
      v.reserve( vs.size() );
      return convert( vs, std::back_inserter( v ) );
    } );
  }
} // namespace

namespace Gaudi::Parsers {
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::History>& e, const std::string& s ) { return vparse( e, s ); }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitHistory>& e, const std::string& s ) {
    return vparse( e, s );
  }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Type>& e, const std::string& s ) { return vparse( e, s ); }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::PatRecStatus>& e, const std::string& s ) {
    return vparse( e, s );
  }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::FitStatus>& e, const std::string& s ) {
    return vparse( e, s );
  }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::Flag>& e, const std::string& s ) { return vparse( e, s ); }
  StatusCode parse( std::vector<LHCb::Event::Enum::Track::AdditionalInfo>& e, const std::string& s ) {
    return vparse( e, s );
  }
} // namespace Gaudi::Parsers
