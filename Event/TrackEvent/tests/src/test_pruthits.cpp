/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestPrUTHits
#include <boost/test/unit_test.hpp>
#include <type_traits>

#include "Event/PrUTHits.h"

// The test fixture: Create a container of hits
// This can be used as a common starting point for all tests
using namespace LHCb::Pr::UT;

BOOST_AUTO_TEST_CASE( test_hits_size ) {

  Hits myHits{};

  using simd   = SIMDWrapper::best::types;
  using scalar = SIMDWrapper::scalar::types;

  auto maskV  = simd::mask_true();
  auto myhitV = myHits.compress_back<SIMDWrapper::InstructionSet::Best>( maskV );
  myhitV.field<UTHitsTag::xAtYEq0>().set( 1.234f );

  auto maskS  = scalar::mask_true();
  auto myhitS = myHits.compress_back<SIMDWrapper::InstructionSet::Scalar>( maskS );
  myhitS.field<UTHitsTag::xAtYEq0>().set( 1.234f );

  maskS  = scalar::mask_false();
  myhitS = myHits.compress_back<SIMDWrapper::InstructionSet::Scalar>( maskS );
  myhitS.field<UTHitsTag::xAtYEq0>().set( 1.234f );

  BOOST_CHECK( ( myHits.size() == simd::size + scalar::size ) );
}

BOOST_AUTO_TEST_CASE( test_hits_constructors ) {
  static_assert( std::is_default_constructible_v<Hits> );
  static_assert( std::is_move_constructible_v<Hits> );
  static_assert( !std::is_copy_constructible_v<Hits> );
}
