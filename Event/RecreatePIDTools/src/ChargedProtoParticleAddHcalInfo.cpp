/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.h
 *
 * Header file for algorithm FutureChargedProtoParticleAddHcalInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"
//-----------------------------------------------------------------------------
/** @file FutureChargedProtoParticleAddHcalInfo.cpp
 *
 * Implementation file for algorithm FutureChargedProtoParticleAddHcalInfo
 *  Updates the CALO HCAL information stored in the ProtoParticles
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 28/08/2009
 */
//-----------------------------------------------------------------------------
namespace LHCb::Rec::ProtoParticle::Charged {

  class AddHcalInfo final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode operator()( ProtoParticles&, IGeometryInfo const& ) const override; ///< Algorithm execution

  private:
    using TrAccTable  = Relation1D<Track, bool>;
    using TrEvalTable = Relation1D<Track, float>;

    /// Add Calo Hcal information to the given ProtoParticle
    bool addHcal( LHCb::ProtoParticle& proto, TrAccTable const& InHcalTable, TrEvalTable const& HcalETable,
                  TrEvalTable const& dlleHcalTable, TrEvalTable const& dllmuHcalTable ) const;

    DataObjectReadHandle<TrAccTable>  m_InHcalTable{this, "InputInHcalLocation", CaloFutureIdLocation::InHcal};
    DataObjectReadHandle<TrEvalTable> m_HcalETable{this, "InputHcalELocation", CaloFutureIdLocation::HcalE};
    DataObjectReadHandle<TrEvalTable> m_dlleHcalTable{this, "InputHcalPIDeLocation", CaloFutureIdLocation::HcalPIDe};
    DataObjectReadHandle<TrEvalTable> m_dllmuHcalTable{this, "InputHcalPIDmuLocation", CaloFutureIdLocation::HcalPIDmu};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddHcalInfo, "ChargedProtoParticleAddHcalInfo" )

  //=============================================================================
  // Main execution
  //=============================================================================
  StatusCode AddHcalInfo::operator()( ProtoParticles& protos, IGeometryInfo const& ) const {
    const auto& inHcalTable    = *m_InHcalTable.get();
    const auto& hcalETable     = *m_HcalETable.get();
    const auto& dlleHcalTable  = *m_dlleHcalTable.get();
    const auto& dllmuHcalTable = *m_dllmuHcalTable.get();
    // Loop over proto particles and update HCAL info
    for ( auto* proto : protos ) addHcal( *proto, inHcalTable, hcalETable, dlleHcalTable, dllmuHcalTable );
    return StatusCode::SUCCESS;
  }

  //=============================================================================

  //=============================================================================
  // Add Calo Hcal info to the protoparticle
  //=============================================================================
  bool AddHcalInfo::addHcal( LHCb::ProtoParticle& proto, TrAccTable const& InHcalTable, TrEvalTable const& HcalETable,
                             TrEvalTable const& dlleHcalTable, TrEvalTable const& dllmuHcalTable ) const {
    // clear HCAL info
    proto.removeCaloHcalInfo();

    const auto aRange = InHcalTable.relations( proto.track() );
    if ( aRange.empty() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> No entry for that track in the Hcal acceptance table" << endmsg;
      return false;
    }

    if ( !aRange.front().to() ) {
      if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is NOT in Hcal acceptance" << endmsg;
      return false;
    }

    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The track is in Hcal acceptance" << endmsg;
    proto.addInfo( LHCb::ProtoParticle::additionalInfo::InAccHcal, true );

    // Get the HcalE (intermediate) estimator
    if ( const auto r = HcalETable.relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloHcalE, r.front().to() );
    }

    // Get the Hcal DLL(e)
    if ( const auto r = dlleHcalTable.relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDe, r.front().to() );
    }

    // Get the Hcal DLL(mu)
    if ( const auto r = dllmuHcalTable.relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, r.front().to() );
    }

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " -> Hcal PID  : "
                << " HcalE      =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloHcalE, -999. )
                << " Dlle (Hcal) =" << proto.info( LHCb::ProtoParticle::additionalInfo::HcalPIDe, -999. )
                << " Dllmu (Hcal) =" << proto.info( LHCb::ProtoParticle::additionalInfo::HcalPIDmu, -999. ) << endmsg;

    return true;
  }
} // namespace LHCb::Rec::ProtoParticle::Charged
