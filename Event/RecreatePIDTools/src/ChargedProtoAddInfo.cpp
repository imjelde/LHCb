/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/IDetectorElement.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Interfaces/IProtoParticleTool.h"

namespace LHCb::Rec::ProtoParticle::Charged {

  class AddInfo final : public GaudiAlgorithm {
  public:
    using GaudiAlgorithm::GaudiAlgorithm;

    StatusCode execute() override {
      // Get the defaulf geometry FIXME, use functional algo
      auto lhcb = getDet<IDetectorElement>( m_standardGeometry_address );
      if ( !lhcb ) { throw GaudiException( "Could not load geometry", name(), StatusCode::FAILURE ); }
      auto& geometry = *lhcb->geometry();

      // update the proto particles from the TES 'in situ' (for backwards compatibility)
      LHCb::ProtoParticles* protos = m_protoPath.get();
      for ( auto& addInfo : m_addInfo ) ( *addInfo )( *protos, geometry ).ignore();
      return StatusCode::SUCCESS;
    }

  private:
    Gaudi::Property<std::string> m_standardGeometry_address{this, "StandardGeometryTop", "/dd/Structure/LHCb"};
    ToolHandleArray<LHCb::Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
    DataObjectReadHandle<LHCb::ProtoParticles>              m_protoPath{
        this, "ProtoParticleLocation", LHCb::ProtoParticleLocation::Charged}; ///< Location in TES of ProtoParticles
  };
  DECLARE_COMPONENT_WITH_ID( AddInfo, "ChargedProtoParticleAddInfo" )
} // namespace LHCb::Rec::ProtoParticle::Charged
