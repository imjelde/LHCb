/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
/** @file ChargedProtoParticleAddBremInfo.h
 *
 * Implementation file for algorithm AddBremInfo
 *
 * @author Chris Jones   Christopher.Rob.Jones@cern.ch
 * @date 29/03/2006
 */
//-----------------------------------------------------------------------------
#include "CaloFutureInterfaces/ICaloFutureHypoEstimator.h"
#include "CaloFutureUtils/CaloFuture2Track.h"
#include "DetDesc/IDetectorElement.h"
#include "Event/ProtoParticle.h"
#include "GaudiAlg/GaudiTool.h"
#include "Interfaces/IProtoParticleTool.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted2D.h"

/** @class FutureChargedProtoParticleAddBremInfo FutureChargedProtoParticleAddBremInfo.h
 *
 *  Updates the CALO 'BREM' information stored in the ProtoParticles
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date 28/08/2009
 */

namespace LHCb::Rec::ProtoParticle::Charged {

  class AddBremInfo final : public extends<GaudiTool, Interfaces::IProtoParticles> {

  public:
    /// Standard constructor
    using extends::extends;

    StatusCode operator()( ProtoParticles&, IGeometryInfo const& ) const override; ///< Algorithm execution

    using TrAccTable    = Relation1D<Track, bool>;
    using HypoTrTable2D = RelationWeighted2D<CaloHypo, Track, float>;
    using TrEvalTable   = Relation1D<Track, float>;

  private:
    /// Add Calo Brem information to the given ProtoParticle
    bool addBrem( LHCb::ProtoParticle& proto, TrAccTable const& InBremTable, HypoTrTable2D const& BremTrTable,
                  TrEvalTable const& BremChi2Table, TrEvalTable const& dlleBremTable ) const;

    ToolHandle<Calo::Interfaces::IHypoEstimator> m_estimator{this, "CaloHypoEstimator", "CaloFutureHypoEstimator"};

    Gaudi::Property<std::string> m_standardGeometry_address{this, "StandardGeometryTop", "/dd/Structure/LHCb"};

    DataObjectReadHandle<TrAccTable>    m_InBremTable{this, "InputInBremLocation", CaloFutureIdLocation::InBrem};
    DataObjectReadHandle<HypoTrTable2D> m_bremTrTable{this, "InputBremMatchLocation", CaloFutureIdLocation::BremMatch};
    DataObjectReadHandle<TrEvalTable>   m_BremChi2Table{this, "InputBremChi2Location", CaloFutureIdLocation::BremChi2};
    DataObjectReadHandle<TrEvalTable>   m_dlleBremTable{this, "InputBremPIDeLocation", CaloFutureIdLocation::BremPIDe};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( AddBremInfo, "ChargedProtoParticleAddBremInfo" )

  //=============================================================================
  // Main execution
  //=============================================================================
  StatusCode AddBremInfo::operator()( ProtoParticles& protos, IGeometryInfo const& ) const {
    const auto& inBremTable   = *m_InBremTable.get();
    const auto& bremTrTable   = *m_bremTrTable.get();
    const auto& bremChi2Table = *m_BremChi2Table.get();
    const auto& dlleBremTable = *m_dlleBremTable.get();
    // Loop over proto particles and fill brem info
    for ( auto* proto : protos ) addBrem( *proto, inBremTable, bremTrTable, bremChi2Table, dlleBremTable );
    return StatusCode::SUCCESS;
  }

  //=============================================================================

  //=============================================================================
  // Add Calo Brem info to the protoparticle
  //=============================================================================
  bool AddBremInfo::addBrem( LHCb::ProtoParticle& proto, TrAccTable const& InBremTable,
                             HypoTrTable2D const& BremTrTable, TrEvalTable const& BremChi2Table,
                             TrEvalTable const& dlleBremTable ) const {
    // First remove existing BREM info
    proto.removeCaloBremInfo();

    // Add new info

    const auto aRange = InBremTable.relations( proto.track() );
    if ( aRange.empty() ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " ->  No entry for that track in the Brem acceptance table" << endmsg;
      return false;
    }
    if ( auto hasBremPID = aRange.front().to(); !hasBremPID ) {
      if ( msgLevel( MSG::VERBOSE ) )
        verbose() << " -> The Brem. extrapolated line is NOT in Ecal acceptance" << endmsg;
      return false;
    }
    if ( msgLevel( MSG::VERBOSE ) ) verbose() << " -> The Brem. extrapolated line is in Ecal acceptance" << endmsg;
    proto.addInfo( LHCb::ProtoParticle::additionalInfo::InAccBrem, true );

    // Get the highest weight associated brem. CaloHypo (3D matching)
    if ( const auto r = BremTrTable.inverse()->relations( proto.track() ); !r.empty() ) {
      const auto* hypo = r.front().to();
      proto.addToCalo( hypo );
      using namespace Calo::Enum;
      auto value_or_default = [data = m_estimator->get_data( *hypo )]( auto type ) {
        auto it = data.find( type );
        if ( it != data.end() ) return it->second;
        return Default;
      };
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, value_or_default( DataType::ClusterE ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, value_or_default( DataType::BremMatch ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralID, value_or_default( DataType::CellID ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralHcal2Ecal,
                     value_or_default( DataType::Hcal2Ecal ) );
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloNeutralE49, value_or_default( DataType::E49 ) );
    }

    // Get the BremChi2 (intermediate) estimator
    if ( const auto r = BremChi2Table.relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, r.front().to() );
    }

    // Get the Brem DLL(e)
    if ( const auto r = dlleBremTable.relations( proto.track() ); !r.empty() ) {
      proto.addInfo( LHCb::ProtoParticle::additionalInfo::BremPIDe, r.front().to() );
    }

    if ( msgLevel( MSG::VERBOSE ) )
      verbose() << " -> BremStrahlung PID : "
                << " Chi2-Brem  =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloBremMatch, -999. )
                << " BremChi2   =" << proto.info( LHCb::ProtoParticle::additionalInfo::CaloBremChi2, -999. )
                << " Dlle (Brem) =" << proto.info( LHCb::ProtoParticle::additionalInfo::BremPIDe, -999. )
                << " Spd Digits " << proto.info( LHCb::ProtoParticle::additionalInfo::CaloNeutralSpd, 0. )
                << " Prs Digits " << proto.info( LHCb::ProtoParticle::additionalInfo::CaloNeutralPrs, 0. )
                << " Ecal Cluster " << proto.info( LHCb::ProtoParticle::additionalInfo::CaloNeutralEcal, 0. ) << endmsg;

    return true;
  }
} // namespace LHCb::Rec::ProtoParticle::Charged
