# Some explanations to the SOACollection

An SOACollection is a container of muliple fields in Struct of Array layout.
The minimal code to define it is as follow:

```cpp
namespace MyNamespace {
  namespace Tag {
    struct MyFirstTag : LHCb::Event::int_field {}; // define a single integer tag
    struct MyArrayTag : LHCb::Event::floats_field<10> {}; // define an array of 10 floats

    // Create the SOACollection base class with the tags defined above:
    template <typename T>
    using my_struct_t = LHCb::Event::SOACollection<T, MyFirstTag, MyArrayTag>
  }

  // Declare the new container type, inheriting from the SOACollection using CRTP
  struct MyStruct : Tag::my_struct_t<MyStruct> {
    using base_t = typename Tag::my_struct_t<MyStruct>;
    using base_t::base_t;
  };
}

// Lastly, register the header for this class so it can be used in functors
REGISTER_HEADER( MyNamespace::MyStruct, "ThisHeader.h" );
```

This structure will comme with all the SOACollections features and a default proxy that will allow to
read and write the fields.

```cpp
// Iterate on the container:
for (auto proxy : mystruct.scalar()) {
  auto a = proxy.get<MyNamespace::Tag::MyFirstTag>();
  auto b = proxy.get<MyNamespace::Tag::MyArrayTag>( 0 ); // get the first element of the array
}
```

```cpp
// Add some elements at the end
auto proxy = mystruct.emplace_back<SIMDWrapper::InstructionSet::AVX2>(); // emplace 8 objects at a time
proxy.field<MyNamespace::Tag::MyFirstTag>().set( 42 );
proxy.field<MyNamespace::Tag::MyArrayTag>( 0 ).set( 2.f ); // fill the first element of the array
```

Additionally, an optional custom proxy can be defined. It is needed when the proxy must define a function
with a specific name, eg in the functors. This proxy must be defined in the container struct and override
the proxy_type property:

```cpp
// Define an optional custom proxy for this track
template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
struct CustomProxy : LHCb::Event::Proxy<simd, behaviour, ContainerType> {
   // use default constructor
   using LHCb::Event::Proxy<simd, behaviour, ContainerType>::Proxy;

   // declare custom functions
   [[nodiscard]] auto customFunction() const {
     return this->template get<MyNamespace::Tag::MyFirstTag>() + 5; // can include custom computations
   }
};

// Register the proxy:
template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour, typename ContainerType>
using proxy_type = CustomProxy<simd, behaviour, ContainerType>;
```

When zipped with other containers' Proxy in a ZipProxy, the custom defined functions will be available as
function of the zip proxy. As a result, in case of collision of custom function names defined by different
Proxies, these functions will be unusable

For an example, see [`PrSeedTracks.h`](/Event/TrackEvent/Event/PrSeedTracks.h).
