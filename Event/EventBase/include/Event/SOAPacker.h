/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "SOAPath.h"
#include <ratio>

namespace LHCb::Event {

  template <typename T, bool Unique = false>
  struct SOAPackNumeric {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      auto* dataIn = collection.data( path );
      if constexpr ( Unique ) {
        buffer.write( dataIn[0] );
      } else {
        for ( std::size_t i = 0; i < collection.size(); i++ ) {
          auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
          if ( mask.cast() ) { buffer.write( dataIn[i] ); }
        }
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      auto* dataOut = collection.data( path );
      if constexpr ( Unique ) {
        buffer.read( dataOut[0] );
      } else {
        for ( std::size_t i = 0; i < collection.size(); i++ ) {
          auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
          if ( mask.cast() ) { buffer.read( dataOut[i] ); }
        }
      }
    }
  };

  template <typename T, typename scale = std::ratio<1, 1>>
  struct SOAPackFloatAs {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      auto*       dataIn = collection.data( path );
      const float scalef = (float)scale::num / scale::den;
      for ( std::size_t i = 0; i < collection.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
        if ( mask.cast() ) {
          T val = std::min( std::max( std::roundf( dataIn[i] * scalef ), (float)std::numeric_limits<T>::lowest() ),
                            (float)std::numeric_limits<T>::max() );
          buffer.write( val );
        }
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      auto*       dataOut = collection.data( path );
      const float scalef  = (float)scale::den / scale::num;
      for ( std::size_t i = 0; i < collection.size(); i++ ) {
        auto mask = path.template isValid<SIMDWrapper::scalar::types>( collection, i );
        if ( mask.cast() ) {
          T val;
          buffer.read( val );
          dataOut[i] = (float)val * scalef;
        }
      }
    }
  };

  struct SOADontPack {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer&, const Collection&, Path ) {}
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer&, Collection&, Path ) {}
  };

  template <std::size_t version, typename trueType, typename falseType = SOADontPack>
  struct SOAPackIfVersionNewerOrEqual {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      if ( collection.version() >= version ) {
        trueType::pack( buffer, collection, path );
      } else {
        falseType::pack( buffer, collection, path );
      }
    }
    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      if ( collection.version() >= version ) {
        trueType::unpack( buffer, collection, path );
      } else {
        falseType::unpack( buffer, collection, path );
      }
    }
  };

  template <typename T, typename SizeTag>
  struct SOAPackVector {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      // serialize sizes
      auto path2size = path.template append<SizeTag>();
      SizeTag::packer_t::pack( buffer, collection, path2size );
      // compress-serialize elements
      const auto* vector_tree = collection.data( path.template append<std::true_type>() );
      for ( std::size_t i = 0; i < vector_tree->maxSize(); i++ ) {
        T::pack( buffer, collection, path.append( SOAPathIfSmallerThan{i, path2size} ) );
      }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      // deserialize sizes
      auto path2size = path.template append<SizeTag>();
      SizeTag::packer_t::unpack( buffer, collection, path2size );
      auto*             sizes    = collection.data( path2size );
      const std::size_t max_size = *std::max_element( sizes, sizes + collection.size() );
      // expand-deserialize elements
      auto* vector_tree = collection.data( path.template append<std::true_type>() );
      vector_tree->resize( max_size );
      for ( std::size_t i = 0; i < max_size; i++ ) {
        T::unpack( buffer, collection, path.append( SOAPathIfSmallerThan{i, path2size} ) );
      }
    }
  };

  template <typename T, std::size_t... Ns>
  struct SOAPackNDArray {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) { T::pack( buffer, collection, path.append( i ) ); }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < ( 1 * ... * Ns ); i++ ) { T::unpack( buffer, collection, path.append( i ) ); }
    }
  };

  template <typename T, std::size_t N>
  struct SOAPackArray {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < N; i++ ) { T::pack( buffer, collection, path.append( i ) ); }
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      for ( std::size_t i = 0; i < N; i++ ) { T::unpack( buffer, collection, path.append( i ) ); }
    }
  };

  template <typename... Tags>
  struct SOAPackStruct {
    template <typename Buffer, typename Collection, typename Path>
    static void pack( Buffer& buffer, const Collection& collection, Path path ) {
      ( Tags::packer_t::pack( buffer, collection, path.template append<Tags>() ), ... );
    }

    template <typename Buffer, typename Collection, typename Path>
    static void unpack( Buffer& buffer, Collection& collection, Path path ) {
      ( Tags::packer_t::unpack( buffer, collection, path.template append<Tags>() ), ... );
    }

    template <typename Buffer, typename Collection>
    static void pack( Buffer& buffer, const Collection& collection ) {
      ( Tags::packer_t::pack( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
    }

    template <typename Buffer, typename Collection>
    static void unpack( Buffer& buffer, Collection& collection ) {
      ( Tags::packer_t::unpack( buffer, collection, SOAPath<Tags>{std::make_tuple( Tags{} )} ), ... );
    }
  };
} // namespace LHCb::Event