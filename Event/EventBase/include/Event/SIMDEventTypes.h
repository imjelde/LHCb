/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "Event/SOACollection.h"
#include "Kernel/LHCbID.h"
#include "LHCbMath/SIMDWrapper.h"

namespace LHCb::Event {
  // SIMDWrapper compatible type for representing an enum
  template <typename simd, typename enumType>
  struct enum_v {
    using int_v  = typename simd::int_v;
    using mask_v = typename simd::mask_v;

    constexpr enum_v( enumType id ) : _data{bit_cast<int>( id )} {};
    constexpr enum_v( int_v id ) : _data{id} {};

    constexpr int_v data() const { return _data; }

    template <typename U = simd, std::enable_if_t<std::is_same_v<U, SIMDWrapper::scalar::types>>* = nullptr>
    constexpr enumType cast() const {
      return enumType( bit_cast<unsigned int>( _data.cast() ) );
    }

    friend mask_v operator==( enum_v lhs, enum_v rhs ) { return lhs._data == rhs._data; }
    friend mask_v operator!=( enum_v lhs, enum_v rhs ) { return !( lhs._data == rhs._data ); }

    friend std::ostream& operator<<( std::ostream& os, const enum_v id ) {
      std::array<int, simd::size> tmp;
      id._data.store( tmp );
      os << "enum_v"
         << "{";
      for ( std::size_t i = 0; i < simd::size - 1; ++i ) { os << enumType( bit_cast<unsigned int>( tmp[i] ) ) << ", "; }
      return os << enumType( bit_cast<unsigned int>( tmp[simd::size - 1] ) ) << "}";
    }

  private:
    int_v _data{};
  };

  // * Tag and Proxy baseclasses for representing a field of enum
  template <typename enumType, bool Unique = false>
  struct enum_field : int_field_<Unique> {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct EnumProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      EnumProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[gnu::always_inline]] inline constexpr void set( enum_v<simd_t, enumType> const& val ) {
        proxy().set( val.data() );
      }

      [[gnu::always_inline]] inline constexpr auto get() const { return enum_v<simd_t, enumType>{proxy().get()}; }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = EnumProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <typename enumType, std::size_t... N>
  using enums_field = ndarray_field<enum_field<enumType>, N...>;

  // SIMDWrapper compatible type for representing an LHCbID
  template <typename dType>
  struct lhcbid_v {

    using I = typename dType::int_v;
    using M = typename dType::mask_v;

    constexpr lhcbid_v( LHCb::LHCbID id ) : _data{static_cast<int>( id.lhcbID() )} {};
    constexpr lhcbid_v( I id ) : _data{id} {};

    template <typename U = dType, std::enable_if_t<std::is_same_v<U, SIMDWrapper::scalar::types>>* = nullptr>
    constexpr LHCb::LHCbID LHCbID() const {
      return LHCb::LHCbID( bit_cast<unsigned int>( _data.cast() ) );
    }

    constexpr I data() const { return _data; }

    [[gnu::always_inline]] inline constexpr lhcbid_v const& store( int* ptr ) const {
      _data.store( ptr );
      return *this;
    }

    [[gnu::always_inline]] inline constexpr const lhcbid_v& compressstore( M mask, int* ptr ) const {
      _data.compressstore( mask, ptr );
      return *this;
    }

    friend lhcbid_v select( M mask, lhcbid_v id1, lhcbid_v id2 ) {
      I val = select( mask, id1.data(), id2.data() );
      return lhcbid_v( val );
    }

    constexpr lhcbid_v& operator=( I f ) {
      _data = f;
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& os, const lhcbid_v id ) {
      std::array<int, dType::size> tmp;
      id.data().store( tmp );
      os << "lhcbid_v"
         << "{";
      for ( std::size_t i = 0; i < dType::size - 1; ++i ) {
        os << LHCb::LHCbID( static_cast<unsigned int>( tmp[i] ) ) << ", ";
      }
      return os << LHCb::LHCbID( static_cast<unsigned int>( tmp[dType::size - 1] ) ) << "}";
    }

  private:
    I _data{};
  };

  // * Tag and Proxy baseclasses for representing a field of LHCbIDs
  struct lhcbid_field : int_field {
    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerTypeBase,
              typename Path>
    struct LHCbIDsProxy {
      using simd_t        = SIMDWrapper::type_map_t<Simd>;
      using OffsetType    = LHCb::Pr::detail::offset_t<Behaviour, Simd>;
      using ContainerType = LHCb::Pr::detail::container_t<Behaviour, Simd, ContainerTypeBase>;
      using proxy_type    = NumericProxy<Simd, Behaviour, ContainerTypeBase, Path, int>;

      LHCbIDsProxy( ContainerType container, OffsetType offset, Path path )
          : m_container( container ), m_offset( offset ), m_path( path ) {}

      [[nodiscard, gnu::always_inline]] inline constexpr auto proxy() const {
        return proxy_type{m_container, m_offset, m_path};
      }

      [[gnu::always_inline]] inline constexpr void set( lhcbid_v<simd_t> const& lhcbID ) {
        proxy().set( lhcbID.data() );
      }

      [[nodiscard, gnu::always_inline]] constexpr lhcbid_v<simd_t> get() const {
        return lhcbid_v<simd_t>{proxy().get()};
      }

    private:
      ContainerType m_container;
      OffsetType    m_offset;
      const Path    m_path;
    };

    template <SIMDWrapper::InstructionSet Simd, LHCb::Pr::ProxyBehaviour Behaviour, typename ContainerType,
              typename Path>
    using proxy_type = LHCbIDsProxy<Simd, Behaviour, ContainerType, Path>;
  };
  template <std::size_t... N>
  using lhcbids_field = ndarray_field<lhcbid_field, N...>;

} // namespace LHCb::Event
