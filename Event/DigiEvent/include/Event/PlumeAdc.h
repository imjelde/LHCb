/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "Kernel/CaloCellIDKeyTraits.h"
#include "Kernel/PlumePmtIDKeyTraits.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_PlumeAdc = 14100;

  // Namespace for locations in TDS
  namespace PlumeAdcLocation {
    inline const std::string Default = "Raw/Plume/Adcs";

  } // namespace PlumeAdcLocation

  /** @class PlumeAdc PlumeAdc.h
   *
   * @brief The ADC content for given PMT * * * The class represents the
   * digitised value in a Plume PMT
   *
   * @author Vladyslav Orlov
   *
   */

  class PlumeAdc final : public KeyedObject<LHCb::PlumePmtID> {
  public:
    /// typedef for KeyedContainer of PlumeAdc
    using Container = KeyedContainer<PlumeAdc, Containers::HashMap>;

    /// Non-default constructor
    PlumeAdc( const LHCb::PlumePmtID& id, int adc, bool overThreshold )
        : KeyedObject<LHCb::PlumePmtID>( id ), m_adc( adc ), m_overThreshold( overThreshold ) {}

    /// Copy Constructor
    PlumeAdc( const PlumeAdc& src ) : KeyedObject<LHCb::PlumePmtID>( src.PmtID() ), m_adc( src.adc() ) {}

    /// Default Constructor
    PlumeAdc() = default;

    /// Default copy asignment
    PlumeAdc& operator=( const PlumeAdc& ) = default;

    // Retrieve pointer to class definition structure
    [[nodiscard]] const CLID& clID() const override;
    static const CLID&        classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve pmt identifier/key @attention alias to Base::key() method!
    [[nodiscard]] const LHCb::PlumePmtID& PmtID() const;

    /// Retrieve const  ADC value for the given PMT
    [[nodiscard]] int adc() const;

    /// Retrieve const  overThreshold value for the given PMT
    [[nodiscard]] int overThreshold() const;

    /// Update  ADC value for the given PMT
    void setAdc( int value );

    void setOverThreshold( bool value );

    friend std::ostream& operator<<( std::ostream& str, const PlumeAdc& obj ) { return obj.fillStream( str ); }

  private:
    int  m_adc{0}; ///< ADC value for the given pmt
    bool m_overThreshold{false};
  }; // class PlumeAdc

  /// Definition of Keyed Container for PlumeAdc
  using PlumeAdcs = KeyedContainer<PlumeAdc, Containers::HashMap>;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::PlumeAdc::clID() const { return LHCb::PlumeAdc::classID(); }

inline const CLID& LHCb::PlumeAdc::classID() { return CLID_PlumeAdc; }

inline int LHCb::PlumeAdc::adc() const { return m_adc; }

inline int LHCb::PlumeAdc::overThreshold() const { return m_overThreshold; }

inline void LHCb::PlumeAdc::setAdc( int value ) { m_adc = value; }

inline void LHCb::PlumeAdc::setOverThreshold( bool value ) { m_overThreshold = value; }

inline const LHCb::PlumePmtID& LHCb::PlumeAdc::PmtID() const { return key(); }
