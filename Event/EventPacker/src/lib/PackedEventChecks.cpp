/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedEventChecks.h"

#include "Gaudi/Accumulators.h"

#include <cmath>
#include <mutex>
#include <sstream>

using namespace DataPacking;

namespace {

  using Counter = Gaudi::Accumulators::StatCounter<double, Gaudi::Accumulators::atomicity::none>;
  // Local implementation of dynamic counter creation
  // This code mimics the an old bahavior for backward compatibility reasons
  // but will be horribly inefficient, especially in multithreaded cases
  // It should be reworked at some stage -> FIXME

  // lock protecting local counters
  static std::mutex m_countersMutex{};
  // local counters, split by algo and then by name
  std::map<Gaudi::Algorithm const*, std::map<std::string, Counter>> m_counters;

  Counter& counter( Gaudi::Algorithm const& parent, std::string const& tag ) {
    // This method is fully serialized and is thus a huge point of contention
    // in a multithreaded world -> FIXME
    auto lock = std::scoped_lock{m_countersMutex};
    // Return referenced StatCounter if it already exists, else create it
    auto& counterMap = m_counters[&parent];
    auto  p          = counterMap.find( tag );
    if ( p == counterMap.end() ) {
      auto [iter, b] = counterMap.try_emplace( tag );
      assert( b );
      parent.serviceLocator()->monitoringHub().registerEntity( parent.name(), iter->first, Counter::typeString,
                                                               iter->second );
      p = iter;
    }
    return p->second;
  }

  template <typename ArithmeticType>
  void fillCounters( Gaudi::Algorithm const& parent, std::string const name, ArithmeticType a, ArithmeticType b ) {
    counter( parent, "Original - " + name ) += a;
    counter( parent, "Unpacked - " + name ) += b;
    counter( parent, "Diff.    - " + name ) += a - b;
  }
} // namespace

//-----------------------------------------------------------------------------
// Implementation file for class : PackedEventChecks
//
// 2012-04-01 : Chris Jones
//-----------------------------------------------------------------------------

bool DataChecks::compareLorentzVectors( const std::string& name, const Gaudi::LorentzVector& a,
                                        const Gaudi::LorentzVector& b, const double tolV, const double tolE ) const {
  const Gaudi::XYZVector av( a.px(), a.py(), a.pz() );
  const Gaudi::XYZVector bv( b.px(), b.py(), b.pz() );
  return compareVectors( name + ":Vect", av, bv, tolV ) && compareEnergies( name + ":Energy", a.E(), b.E(), tolE );
}

bool DataChecks::comparePoints( const std::string& name, const Gaudi::XYZPoint& a, const Gaudi::XYZPoint& b,
                                const double tol ) const {
  const bool ok =
      ( std::abs( a.x() - b.x() ) < tol && std::abs( a.y() - b.y() ) < tol && std::abs( a.z() - b.z() ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original " << a.x() << " " << a.y() << " "
                      << a.z() << endmsg << " Unpacked " << b.x() << " " << b.y() << " " << b.z() << endmsg
                      << " Diff.    " << std::abs( a.x() - b.x() ) << " " << std::abs( a.y() - b.y() ) << " "
                      << std::abs( a.z() - b.z() ) << endmsg;
  }
  fillCounters( *parent, name + " x", a.x(), b.x() );
  fillCounters( *parent, name + " y", a.y(), b.y() );
  fillCounters( *parent, name + " z", a.z(), b.z() );
  return ok;
}

bool DataChecks::compareVectors( const std::string& name, const Gaudi::XYZVector& a, const Gaudi::XYZVector& b,
                                 const double tol ) const {
  const bool ok =
      ( std::abs( a.x() - b.x() ) < tol && std::abs( a.y() - b.y() ) < tol && std::abs( a.z() - b.z() ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original " << a.x() << " " << a.y() << " "
                      << a.z() << endmsg << " Unpacked " << b.x() << " " << b.y() << " " << b.z() << endmsg
                      << "  Diff    " << std::abs( a.x() - b.x() ) << " " << std::abs( a.y() - b.y() ) << " "
                      << std::abs( a.z() - b.z() ) << endmsg;
  }
  fillCounters( *parent, name + " x", a.x(), b.x() );
  fillCounters( *parent, name + " y", a.y(), b.y() );
  fillCounters( *parent, name + " z", a.z(), b.z() );
  return ok;
}

bool DataChecks::compareVectors( const std::string& name, const Gaudi::Vector3& a, const Gaudi::Vector3& b,
                                 const double tol ) const {
  const bool ok = ( std::abs( a[0] - b[0] ) < tol && std::abs( a[1] - b[1] ) < tol && std::abs( a[2] - b[2] ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original " << a[0] << " " << a[1] << " " << a[2]
                      << endmsg << " Unpacked " << b[0] << " " << b[1] << " " << b[2] << endmsg << "  Diff    "
                      << std::abs( a[0] - b[0] ) << " " << std::abs( a[1] - b[1] ) << " " << std::abs( a[2] - b[2] )
                      << endmsg;
  }
  fillCounters( *parent, name + " [0]", a[0], b[0] );
  fillCounters( *parent, name + " [1]", a[1], b[1] );
  fillCounters( *parent, name + " [2]", a[2], b[2] );
  return ok;
}

bool DataChecks::compareVectors( const std::string& name, const Gaudi::Vector2& a, const Gaudi::Vector2& b,
                                 const double tol ) const {
  const bool ok = ( std::abs( a[0] - b[0] ) < tol && std::abs( a[1] - b[1] ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original " << a[0] << " " << a[1] << endmsg
                      << " Unpacked " << b[0] << " " << b[1] << endmsg << "  Diff    " << std::abs( a[0] - b[0] ) << " "
                      << std::abs( a[1] - b[1] ) << endmsg;
  }
  fillCounters( *parent, name + " [0]", a[0], b[0] );
  fillCounters( *parent, name + " [1]", a[1], b[1] );
  return ok;
}

bool DataChecks::compareDoubles( const std::string& name, const double& a, const double& b, const double tol ) const {
  const bool ok = ( std::abs( a - b ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original = " << a << endmsg
                      << " Unpacked = " << b << endmsg << "  Diff = " << std::abs( a - b ) << " > " << tol << endmsg;
  }
  fillCounters( *parent, name, a, b );
  return ok;
}

bool DataChecks::compareFloats( const std::string& name, const float& a, const float& b, const float tol ) const {
  const bool ok = ( std::abs( a - b ) < tol );
  if ( !ok ) {
    parent->warning() << name << " comparison failed :-" << endmsg << " Original = " << a << endmsg
                      << " Unpacked = " << b << endmsg << "  Diff = " << std::abs( a - b ) << " > " << tol << endmsg;
  }
  fillCounters( *parent, name, a, b );
  return ok;
}
