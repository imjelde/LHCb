/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedRecVertex.h"
#include "Event/PackedEventChecks.h"
#include "Event/PackerUtils.h"

using namespace LHCb;

void RecVertexPacker::pack( const Data& vert, PackedData& pvert, const DataVector& /* verts */,
                            PackedDataVector& pverts ) const {
  const auto ver = pverts.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  pvert.technique = vert.technique();
  pvert.chi2      = StandardPacker::fltPacked( vert.chi2() );
  pvert.nDoF      = vert.nDoF();
  pvert.x         = StandardPacker::position( vert.position().x() );
  pvert.y         = StandardPacker::position( vert.position().y() );
  pvert.z         = StandardPacker::position( vert.position().z() );

  // convariance Matrix
  const auto err0 = safe_sqrt( vert.covMatrix()( 0, 0 ) );
  const auto err1 = safe_sqrt( vert.covMatrix()( 1, 1 ) );
  const auto err2 = safe_sqrt( vert.covMatrix()( 2, 2 ) );
  pvert.cov00     = StandardPacker::position( err0 );
  pvert.cov11     = StandardPacker::position( err1 );
  pvert.cov22     = StandardPacker::position( err2 );
  pvert.cov10     = StandardPacker::fraction( vert.covMatrix()( 1, 0 ), err1 * err0 );
  pvert.cov20     = StandardPacker::fraction( vert.covMatrix()( 2, 0 ), err2 * err0 );
  pvert.cov21     = StandardPacker::fraction( vert.covMatrix()( 2, 1 ), err2 * err1 );

  //== Store the Tracks and weights
  pvert.firstTrack = pverts.refs().size();
  {
    auto iW = vert.weights().begin();
    for ( auto itT = vert.tracks().begin(); vert.tracks().end() != itT; ++itT, ++iW ) {
      if ( *itT ) {
        pverts.refs().push_back(
            0 == ver ? StandardPacker::reference32( &parent(), &pverts, ( *itT )->parent(), ( *itT )->key() )
                     : StandardPacker::reference64( &pverts, ( *itT )->parent(), ( *itT )->key() ) );
        pverts.weights().push_back( StandardPacker::fraction( *iW ) );
      } else {
        parent().warning() << "Null Track SmartRef in '" + LHCb::Packer::Utils::location( vert.parent() ) + "'"
                           << endmsg;
      }
    }
  }
  pvert.lastTrack = pverts.refs().size();

  //== Handles the ExtraInfo
  pvert.firstInfo = pverts.extras().size();
  for ( const auto& [k, v] : vert.extraInfo() ) { pverts.addExtra( k, StandardPacker::fltPacked( v ) ); }
  pvert.lastInfo = pverts.extras().size();
}

void RecVertexPacker::pack( const DataVector& verts, PackedDataVector& pverts ) const {
  pverts.data().reserve( verts.size() );

  for ( const auto* vert : verts ) {
    // new packed data object
    auto& pvert = pverts.data().emplace_back();

    // Key
    pvert.key = vert->key();

    // fill physics info from vert to pvert
    pack( *vert, pvert, verts, pverts );
  }
}

void RecVertexPacker::unpack( const PackedData& pvert, Data& vert, const PackedDataVector& pverts,
                              DataVector& verts ) const {
  const auto ver = pverts.packingVersion();
  if ( isSupportedVer( ver ) ) {

    vert.setTechnique( static_cast<LHCb::RecVertex::RecVertexType>( pvert.technique ) );
    vert.setChi2AndDoF( StandardPacker::fltPacked( pvert.chi2 ), pvert.nDoF );
    vert.setPosition( Gaudi::XYZPoint( StandardPacker::position( pvert.x ), StandardPacker::position( pvert.y ),
                                       StandardPacker::position( pvert.z ) ) );

    // convariance Matrix
    const auto err0 = StandardPacker::position( pvert.cov00 );
    const auto err1 = StandardPacker::position( pvert.cov11 );
    const auto err2 = StandardPacker::position( pvert.cov22 );
    auto&      cov  = *( const_cast<Gaudi::SymMatrix3x3*>( &vert.covMatrix() ) );
    cov( 0, 0 )     = err0 * err0;
    cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pvert.cov10 );
    cov( 1, 1 )     = err1 * err1;
    cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pvert.cov20 );
    cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pvert.cov21 );
    cov( 2, 2 )     = err2 * err2;

    //== Store the Tracks and weights
    int hintID( 0 ), tKey( 0 );
    for ( auto kk = pvert.firstTrack; kk < pvert.lastTrack; ++kk ) {
      // Get the track
      const auto trk = pverts.refs()[kk];
      if ( ( 0 != ver && StandardPacker::hintAndKey64( trk, &pverts, &verts, hintID, tKey ) ) ||
           ( 0 == ver && StandardPacker::hintAndKey32( trk, &pverts, &verts, hintID, tKey ) ) ) {
        SmartRef<LHCb::Track> ref( &verts, hintID, tKey );
        // If available, get the weight
        const float weight = ( (int)pverts.version() > 1 ? StandardPacker::fraction( pverts.weights()[kk] ) : 1.f );
        // save with weight
        vert.addToTracks( ref, weight );
      } else {
        parent().error() << "Corrupt RecVertex Track SmartRef detected." << endmsg;
      }
    }

    //== Handles the ExtraInfo
    for ( const auto& [k, v] : ::Packer::subrange( pverts.extras(), pvert.firstInfo, pvert.lastInfo ) ) {
      vert.addInfo( k, StandardPacker::fltPacked( v ) );
    }
  }
}

void RecVertexPacker::unpack( const PackedDataVector& pverts, DataVector& verts ) const {
  verts.reserve( pverts.data().size() );

  for ( const auto& pvert : pverts.data() ) {
    // make and save new pid in container
    auto* vert = new Data();
    verts.insert( vert, pvert.key );

    // Fill data from packed object
    unpack( pvert, *vert, pverts, verts );
  }
}
