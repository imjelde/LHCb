/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedProtoParticle.h"
#include "Event/PackedEventChecks.h"

#include "fmt/format.h"

#include <boost/numeric/conversion/bounds.hpp>

using namespace LHCb;

//-----------------------------------------------------------------------------

void ProtoParticlePacker::pack( const Data& proto, PackedData& pproto, PackedDataVector& pprotos ) const {
  // packing version
  const auto ver = pprotos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  if ( proto.track() ) {
    pproto.track =
        ( 0 == ver ? StandardPacker::reference32( &parent(), &pprotos, proto.track()->parent(), proto.track()->key() )
                   : StandardPacker::reference64( &pprotos, proto.track()->parent(), proto.track()->key() ) );
  } else {
    pproto.track = -1;
  }

  if ( proto.richPID() ) {
    pproto.richPID =
        ( 0 == ver
              ? StandardPacker::reference32( &parent(), &pprotos, proto.richPID()->parent(), proto.richPID()->key() )
              : StandardPacker::reference64( &pprotos, proto.richPID()->parent(), proto.richPID()->key() ) );
  } else {
    pproto.richPID = -1;
  }

  if ( proto.muonPID() ) {
    pproto.muonPID =
        ( 0 == ver
              ? StandardPacker::reference32( &parent(), &pprotos, proto.muonPID()->parent(), proto.muonPID()->key() )
              : StandardPacker::reference64( &pprotos, proto.muonPID()->parent(), proto.muonPID()->key() ) );
  } else {
    pproto.muonPID = -1;
  }

  //== Store the CaloHypos
  pproto.firstHypo = pprotos.refs().size();
  for ( const auto& caloH : proto.calo() ) {
    pprotos.refs().push_back( 0 == ver
                                  ? StandardPacker::reference32( &parent(), &pprotos, caloH->parent(), caloH->key() )
                                  : StandardPacker::reference64( &pprotos, caloH->parent(), caloH->key() ) );
  }
  pproto.lastHypo = pprotos.refs().size();

  //== Handles the ExtraInfo
  pproto.firstExtra = pprotos.extras().size();
  const double high = boost::numeric::bounds<float>::highest();
  const double low  = boost::numeric::bounds<float>::lowest();
  for ( const auto& einfo : proto.extraInfo() ) {
    const auto& info = einfo.second;
    if ( info > high || info < low ) {
      parent().warning() << fmt::format( "ExtraInfo '{}' out of floating point range. Truncating value.",
                                         (LHCb::ProtoParticle::additionalInfo)einfo.first )
                         << endmsg;
    }
    pprotos.extras().emplace_back( einfo.first, StandardPacker::fltPacked( info ) );
  }
  pproto.lastExtra = pprotos.extras().size();
}

void ProtoParticlePacker::pack( const DataVector& protos, PackedDataVector& pprotos ) const {
  pprotos.data().reserve( protos.size() );
  for ( const auto* proto : protos ) {
    if ( !proto ) continue;
    auto& pproto = pprotos.data().emplace_back();
    // save the key
    pproto.key = proto->key();
    // fill the rest
    pack( *proto, pproto, pprotos );
  }
}

void ProtoParticlePacker::unpack( const PackedData& pproto, Data& proto, const PackedDataVector& pprotos,
                                  DataVector& protos ) const {
  // packing version
  const auto ver = pprotos.packingVersion();
  if ( !isSupportedVer( ver ) ) return;

  int hintID( 0 ), key( 0 );

  if ( -1 != pproto.track ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.track, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.track, &pprotos, &protos, hintID, key ) ) ) {
      proto.setTrack( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle Track SmartRef detected." << endmsg;
    }
  }

  if ( -1 != pproto.richPID ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.richPID, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.richPID, &pprotos, &protos, hintID, key ) ) ) {
      proto.setRichPID( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle RichPID SmartRef detected." << endmsg;
    }
  }

  if ( -1 != pproto.muonPID ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( pproto.muonPID, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( pproto.muonPID, &pprotos, &protos, hintID, key ) ) ) {
      proto.setMuonPID( {&protos, hintID, key} );
    } else {
      parent().error() << "Corrupt ProtoParticle MuonPID SmartRef detected." << endmsg;
    }
  }

  for ( auto reference : Packer::subrange( pprotos.refs(), pproto.firstHypo, pproto.lastHypo ) ) {
    if ( ( 0 != ver && StandardPacker::hintAndKey64( reference, &pprotos, &protos, hintID, key ) ) ||
         ( 0 == ver && StandardPacker::hintAndKey32( reference, &pprotos, &protos, hintID, key ) ) ) {
      SmartRef<LHCb::CaloHypo> ref( &protos, hintID, key );
      proto.addToCalo( ref );
    } else {
      parent().error() << "Corrupt ProtoParticle CaloHypo SmartRef detected." << endmsg;
    }
  }

  for ( const auto& [k, v] : Packer::subrange( pprotos.extras(), pproto.firstExtra, pproto.lastExtra ) ) {
    proto.addInfo( k, StandardPacker::fltPacked( v ) );
  }
}

void ProtoParticlePacker::unpack( const PackedDataVector& pprotos, DataVector& protos ) const {
  protos.reserve( pprotos.data().size() );
  for ( const auto& pproto : pprotos.data() ) {
    auto* part = new LHCb::ProtoParticle();
    protos.insert( part, pproto.key );
    unpack( pproto, *part, pprotos, protos );
  }
}

StatusCode ProtoParticlePacker::check( const Data& dataA, const Data& dataB ) const {
  // checker
  const DataPacking::DataChecks ch( parent() );

  bool isOK = true;

  // key
  isOK &= ch.compareInts( "Key", dataA.key(), dataB.key() );

  // check referenced objects
  isOK &= ch.comparePointers( "Track", dataA.track(), dataB.track() );
  isOK &= ch.comparePointers( "RichPID", dataA.richPID(), dataB.richPID() );
  isOK &= ch.comparePointers( "MuonPID", dataA.muonPID(), dataB.muonPID() );

  // calo hypos
  isOK &= ch.compareInts( "#CaloHypos", dataA.calo().size(), dataB.calo().size() );
  if ( isOK ) {
    for ( auto iC = std::make_pair( dataA.calo().begin(), dataB.calo().begin() );
          iC.first != dataA.calo().end() && iC.second != dataB.calo().end(); ++iC.first, ++iC.second ) {
      isOK &= ch.comparePointers( "CaloHypo", iC.first->target(), iC.second->target() );
    }
  }

  // extra info
  isOK &= ch.compareInts( "#ExtraInfo", dataA.extraInfo().size(), dataB.extraInfo().size() );
  if ( isOK ) {
    for ( auto iE = std::make_pair( dataA.extraInfo().begin(), dataB.extraInfo().begin() );
          iE.first != dataA.extraInfo().end() && iE.second != dataB.extraInfo().end(); ++iE.first, ++iE.second ) {
      isOK &= ch.compareInts( "ExtraInfoKey", iE.first->first, iE.second->first );
      if ( isOK ) {
        if ( ( iE.second->second == 0 && iE.second->second != iE.first->second ) ||
             ( iE.second->second != 0 &&
               1.e-7 < std::abs( ( iE.second->second - iE.first->second ) / iE.second->second ) ) )
          isOK = false;
      }
    }
  }

  // isOK = false; // force false for testing

  if ( !isOK || MSG::DEBUG >= parent().msgLevel() ) {
    parent().info() << "===== ProtoParticle key " << dataA.key() << " Check OK = " << isOK << endmsg;
    parent().info() << format( "Old   track %8x  richPID %8X  muonPID%8x  nCaloHypo%4d nExtra%4d", dataA.track(),
                               dataA.richPID(), dataA.muonPID(), dataA.calo().size(), dataA.extraInfo().size() )
                    << endmsg;
    parent().info() << format( "Test  track %8x  richPID %8X  muonPID%8x  nCaloHypo%4d nExtra%4d", dataB.track(),
                               dataB.richPID(), dataB.muonPID(), dataB.calo().size(), dataB.extraInfo().size() )
                    << endmsg;
    for ( auto iC = std::make_pair( dataA.calo().begin(), dataB.calo().begin() );
          iC.first != dataA.calo().end() && iC.second != dataB.calo().end(); ++iC.first, ++iC.second ) {
      parent().info() << format( "   old CaloHypo %8x   new %8x", iC.first->target(), iC.second->target() ) << endmsg;
    }
    for ( auto iE = std::make_pair( dataA.extraInfo().begin(), dataB.extraInfo().begin() );
          iE.first != dataA.extraInfo().end() && iE.second != dataB.extraInfo().end(); ++iE.first, ++iE.second ) {
      parent().info() << format( "   old Extra %5d %12.4f     new %5d %12.4f", iE.first->first, iE.first->second,
                                 iE.second->first, iE.second->second )
                      << endmsg;
    }
  }

  return ( isOK ? StatusCode::SUCCESS : StatusCode::FAILURE );
}

StatusCode ProtoParticlePacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  if ( dataA.size() != dataB.size() ) {
    parent().err() << "Old ProtoParticle size " << dataA.size() << " differs form Test " << dataB.size() << endmsg;
    return StatusCode::FAILURE;
  }

  for ( auto it = std::make_pair( dataA.begin(), dataB.begin() ); it.first != dataA.end() && it.second != dataB.end();
        ++it.first, ++it.second ) {
    if ( sc ) sc = check( **it.first, **it.second );
  }

  return sc;
}
