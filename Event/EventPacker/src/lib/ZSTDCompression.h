/*****************************************************************************\
 * (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// This file is hosting modified code from
// https://github.com/root-project/root/blob/5736fd207fdfba782dd8bb7608c02b932cdc2fd0/core/zstd/src/ZipZSTD.cxx
// and
// https://github.com/root-project/root/blob/5736fd207fdfba782dd8bb7608c02b932cdc2fd0/core/zip/src/RZip.cxx
// Credits to the ROOT team for the original code
//
// The goal is to add support for larger data chunks in the ZSTD compression algorithm
// See also the discussion on the ROOT forum at https://root-forum.cern.ch/t/zipping-data-of-more-than-16mb/47878

#include "Compression.h"
#include "Event/PackedData.h"
#include "RZip.h"

#include <iostream>

// zstd.h forward declarations
extern "C" {
size_t      ZSTD_compress( void* dst, size_t dstCapacity, const void* src, size_t srcSize, int compressionLevel );
size_t      ZSTD_decompress( void* dst, size_t dstCapacity, const void* src, size_t srcSize );
unsigned    ZSTD_isError( size_t code );
const char* ZSTD_getErrorName( size_t code );
}

namespace LHCb::Hlt::PackedData {

  namespace {

    auto to_root_compression_algorithm( Compression c ) {
      static_assert( ROOT_VERSION_CODE >= ROOT_VERSION( 6, 16, 0 ) );
      switch ( c ) {
      case Compression::NoCompression:
        return ROOT::RCompressionSetting::EAlgorithm::kUndefined;
      case Compression::ZLIB:
        return ROOT::RCompressionSetting::EAlgorithm::kZLIB;
      case Compression::LZMA:
        return ROOT::RCompressionSetting::EAlgorithm::kLZMA;
      case Compression::LZ4:
        return ROOT::RCompressionSetting::EAlgorithm::kLZ4;
      case Compression::ZSTD:
        return ROOT::RCompressionSetting::EAlgorithm::kZSTD;
      }
      throw std::runtime_error( "unknown compression scheme" );
    }

    /**
     * This method replaces the corresponding ROOT method from ZipZSTD.h
     * for data larger than 16MB (up to 2GB). It produces new type of header
     * with magic number 'ZS\2'. Note that the header is now 11 bytes and
     * no more 9 with sizes 4 bytes each rather than 3
     * See also discussion at https://root-forum.cern.ch/t/zipping-data-of-more-than-16mb/47878
     * In the future, this code should be replaced by the ROOT internal implementation when it comes
     */
    void zipZSTD_large( int cxlevel, int* srcsize, char* src, int* tgtsize, char* tgt, int* irep ) {
      if ( cxlevel <= 0 ) {
        *irep = 0;
        return;
      }
      *irep = 0;

      const int headerSize = 11;
      // specific case of ZSTD large data
      if ( *srcsize < headerSize + 1 ) return;
      size_t retval = ZSTD_compress( &tgt[headerSize], static_cast<size_t>( *tgtsize - headerSize ), src,
                                     static_cast<size_t>( *srcsize ), 2 * cxlevel );

      if ( R__unlikely( ZSTD_isError( retval ) ) ) {
        std::cerr << "Error in zip ZSTD. Type = " << ZSTD_getErrorName( retval ) << " . Code = " << retval << std::endl;
        return;
      } else {
        *irep = static_cast<size_t>( retval + headerSize );
      }

      size_t deflate_size = retval;
      size_t inflate_size = static_cast<size_t>( *srcsize );
      tgt[0]              = 'Z';
      tgt[1]              = 'S';
      tgt[2]              = '\2';
      tgt[3]              = deflate_size & 0xff;
      tgt[4]              = ( deflate_size >> 8 ) & 0xff;
      tgt[5]              = ( deflate_size >> 16 ) & 0xff;
      tgt[6]              = ( deflate_size >> 24 ) & 0xff;
      tgt[7]              = inflate_size & 0xff;
      tgt[8]              = ( inflate_size >> 8 ) & 0xff;
      tgt[9]              = ( inflate_size >> 16 ) & 0xff;
      tgt[10]             = ( inflate_size >> 24 ) & 0xff;
    }

    /**
     * This method replaces the corresponding ROOT method from RZipZSTD.h
     * for data streams of more than 16MB (up to 2GB)
     */
    void unzipZSTD( int headerSize, int* srcsize, unsigned char* src, int* tgtsize, unsigned char* tgt, int* irep ) {
      *irep         = 0;
      size_t retval = ZSTD_decompress( (char*)tgt, static_cast<size_t>( *tgtsize ), (char*)&src[headerSize],
                                       static_cast<size_t>( *srcsize - headerSize ) );

      if ( R__unlikely( ZSTD_isError( retval ) ) ) {
        std::cerr << "Error in unzip ZSTD. Type = " << ZSTD_getErrorName( retval ) << " . Code = " << retval
                  << std::endl;
        return;
      } else {
        *irep = retval;
      }
    }

    /**
     * This method replaces the corresponding ROOT method from RZip.h
     * It allows to support ZSTD compression with data streams of more
     * than 16MB (up to 2GB) while ROOT is restricted to 16MB
     * For this purpose, it understands new header format for large
     * ZSTD data, starting with 'ZS\2'
     */
    int unzip_header( int datasize, int* srcsize, unsigned char* src, int* tgtsize ) {
      // special case of large ZSTD data
      if ( src[0] == 'Z' && src[1] == 'S' && src[2] == '\2' ) {
        // check we have at least 11 bytes
        if ( datasize < 11 ) return 0;
        *srcsize = 11 + ( (long)src[3] | ( (long)src[4] << 8 ) | ( (long)src[5] << 16 ) | ( (long)src[6] << 24 ) );
        *tgtsize = (long)src[7] | ( (long)src[8] << 8 ) | ( (long)src[9] << 16 ) | ( (long)src[10] << 24 );
        return 2;
      }
      // standard case, use ROOT
      if ( R__unzip_header( srcsize, src, tgtsize ) ) return 0;
      return 1;
    }

    /**
     * This method replaces the corresponding ROOT method from RZip.h
     * It allows to support ZSTD compression with data streams of more
     * than 16MB (up to 2GB) while ROOT is restricted to 16MB
     * For this purpose, it understands new header format for large
     * ZSTD data, starting with 'ZS\2'
     */
    void unzip( unsigned int hdrVersion, int* srcsize, unsigned char* src, int* tgtsize, unsigned char* tgt,
                int* irep ) {
      // standard case, use ROOT original method
      if ( hdrVersion == 1 ) { return R__unzip( srcsize, src, tgtsize, tgt, irep ); }
      // header version 2, for large ZSTD buffers
      *irep        = 0L;
      long ibufcnt = (long)src[3] | ( (long)src[4] << 8 ) | ( (long)src[5] << 16 ) | ( (long)src[6] << 24 );
      if ( ibufcnt + 11 != *srcsize ) {
        fprintf( stderr, "R__unzip: discrepancy in source length\n" );
        return;
      }
      long isize = (long)src[7] | ( (long)src[8] << 8 ) | ( (long)src[9] << 16 ) | ( (long)src[10] << 24 );
      if ( *tgtsize < isize ) {
        fprintf( stderr, "R__unzip: too small target\n" );
        return;
      }
      unzipZSTD( 11, srcsize, src, tgtsize, tgt, irep );
    }

    /**
     * This method replaces the corresponding ROOT method from RZip.h
     * so that we can use our implementation of ZSTD compression which allows
     * more than 16MB to be compressed (up to 2GB) while ROOT imposes
     * the 16B limitation.
     */
    void zipMultipleAlgorithm( int cxlevel, int* srcsize, char* src, int* tgtsize, char* tgt, int* irep,
                               Compression compressionAlgorithm ) {
      if ( compressionAlgorithm == Compression::ZSTD && *srcsize > 0xffffff ) {
        // call here our own function to work around ROOT limitation of 16MB
        zipZSTD_large( cxlevel, srcsize, src, tgtsize, tgt, irep );
      } else {
        // all other cases, go the standard way
        R__zipMultipleAlgorithm( cxlevel, srcsize, src, tgtsize, tgt, irep,
                                 to_root_compression_algorithm( compressionAlgorithm ) );
      }
    }

  } // namespace

} // namespace LHCb::Hlt::PackedData
