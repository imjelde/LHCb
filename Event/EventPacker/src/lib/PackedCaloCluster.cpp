/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloCluster.h"
#include "Event/PackedEventChecks.h"

using namespace LHCb;

void CaloClusterPacker::pack( const DataVector& clus, PackedDataVector& pclus ) const {
  if ( !isSupportedVer( pclus.packingVersion() ) ) return;
  pclus.data().reserve( clus.size() );
  for ( const auto* clu : clus ) {
    // make a new packed object
    auto& pclu = pclus.data().emplace_back();

    // fill data

    // general
    pclu.key  = clu->key();
    pclu.type = (int)clu->type();
    pclu.seed = clu->seed().all();

    // position object
    pclu.pos_x = StandardPacker::position( clu->position().x() );
    pclu.pos_y = StandardPacker::position( clu->position().y() );
    pclu.pos_z = StandardPacker::position( clu->position().z() );
    pclu.pos_e = StandardPacker::energy( clu->position().e() );

    pclu.pos_c0 = StandardPacker::position( clu->position().center()[0] );
    pclu.pos_c1 = StandardPacker::position( clu->position().center()[1] );

    const auto err0 = safe_sqrt( clu->position().covariance()( 0, 0 ) );
    const auto err1 = safe_sqrt( clu->position().covariance()( 1, 1 ) );
    const auto err2 = safe_sqrt( clu->position().covariance()( 2, 2 ) );
    pclu.pos_cov00  = StandardPacker::position( err0 );
    pclu.pos_cov11  = StandardPacker::position( err1 );
    pclu.pos_cov22  = StandardPacker::energy( err2 );
    pclu.pos_cov10  = StandardPacker::fraction( clu->position().covariance()( 1, 0 ), err1 * err0 );
    pclu.pos_cov20  = StandardPacker::fraction( clu->position().covariance()( 2, 0 ), err2 * err0 );
    pclu.pos_cov21  = StandardPacker::fraction( clu->position().covariance()( 2, 1 ), err2 * err1 );

    const auto serr0  = safe_sqrt( clu->position().spread()( 0, 0 ) );
    const auto serr1  = safe_sqrt( clu->position().spread()( 1, 1 ) );
    pclu.pos_spread00 = StandardPacker::position( serr0 );
    pclu.pos_spread11 = StandardPacker::position( serr1 );
    pclu.pos_spread10 = StandardPacker::fraction( clu->position().spread()( 1, 0 ), serr1 * serr0 );

    // entries
    pclu.firstEntry = pclus.entries().size();
    pclus.entries().reserve( pclus.entries().size() + clu->entries().size() );
    for ( const auto& En : clu->entries() ) {
      pclus.entries().emplace_back();
      auto& pEnt = pclus.entries().back();
      if ( En.digit().target() ) {
        pEnt.digit = StandardPacker::reference64( &pclus, En.digit()->parent(), En.digit()->key().all() );
      }
      pEnt.status   = En.status().data();
      pEnt.fraction = StandardPacker::fraction( En.fraction() );
    }
    pclu.lastEntry = pclus.entries().size();
  }
}

void CaloClusterPacker::unpack( const PackedDataVector& pclus, DataVector& clus ) const {
  if ( !isSupportedVer( pclus.packingVersion() ) ) return;
  clus.reserve( pclus.data().size() );
  for ( const auto& pclu : pclus.data() ) {
    // make and save new clUster container, with original key
    auto* clu = new Data();
    clus.insert( clu, pclu.key );

    // Fill data from packed object

    // general
    clu->setType( (LHCb::CaloCluster::Type)pclu.type );
    clu->setSeed( LHCb::Calo::CellID( pclu.seed ) );

    // position
    typedef LHCb::CaloPosition CaloP;

    clu->position().setZ( StandardPacker::position( pclu.pos_z ) );
    clu->position().setParameters( CaloP::Parameters( StandardPacker::position( pclu.pos_x ),
                                                      StandardPacker::position( pclu.pos_y ),
                                                      StandardPacker::energy( pclu.pos_e ) ) );

    clu->position().setCenter(
        CaloP::Center( StandardPacker::position( pclu.pos_c0 ), StandardPacker::position( pclu.pos_c1 ) ) );

    auto&      cov  = clu->position().covariance();
    const auto err0 = StandardPacker::position( pclu.pos_cov00 );
    const auto err1 = StandardPacker::position( pclu.pos_cov11 );
    const auto err2 = StandardPacker::energy( pclu.pos_cov22 );
    cov( 0, 0 )     = err0 * err0;
    cov( 1, 0 )     = err1 * err0 * StandardPacker::fraction( pclu.pos_cov10 );
    cov( 1, 1 )     = err1 * err1;
    cov( 2, 0 )     = err2 * err0 * StandardPacker::fraction( pclu.pos_cov20 );
    cov( 2, 1 )     = err2 * err1 * StandardPacker::fraction( pclu.pos_cov21 );
    cov( 2, 2 )     = err2 * err2;

    auto&      spr   = clu->position().spread();
    const auto serr0 = StandardPacker::position( pclu.pos_spread00 );
    const auto serr1 = StandardPacker::position( pclu.pos_spread11 );
    spr( 0, 0 )      = serr0 * serr0;
    spr( 1, 0 )      = serr1 * serr0 * StandardPacker::fraction( pclu.pos_spread10 );
    spr( 1, 1 )      = serr1 * serr1;

    // entries
    clu->entries().reserve( pclu.lastEntry - pclu.firstEntry );
    for ( auto iE = pclu.firstEntry; iE < pclu.lastEntry; ++iE ) {
      // get the packed entry
      const auto& pEnt = pclus.entries()[iE];
      // make a new unpacked one
      auto& ent = clu->entries().emplace_back();
      // Set data
      if ( -1 != pEnt.digit ) {
        int hintID( 0 ), key( 0 );
        if ( StandardPacker::hintAndKey64( pEnt.digit, &pclus, &clus, hintID, key ) ) {
          ent.setDigit( LHCb::CaloClusterEntry::Digit( &clus, hintID, key ) );
        } else {
          parent().error() << "Corrupt CaloCluster Digit SmartRef found" << endmsg;
        }
      }
      ent.setStatus( LHCb::CaloDigitStatus::Status{pEnt.status} );
      ent.setFraction( StandardPacker::fraction( pEnt.fraction ) );
    }
  }
}

StatusCode CaloClusterPacker::check( const DataVector& dataA, const DataVector& dataB ) const {
  StatusCode sc = StatusCode::SUCCESS;

  // Loop over data containers together and compare
  auto iA( dataA.begin() ), iB( dataB.begin() );
  for ( ; iA != dataA.end() && iB != dataB.end(); ++iA, ++iB ) {
    if ( sc ) sc = check( **iA, **iB );
  }

  // Return final status
  return sc;
}

StatusCode CaloClusterPacker::check( const Data& dataA, const Data& dataB ) const {
  // assume OK from the start
  bool ok = true;

  // checker
  const DataPacking::DataChecks ch( parent() );

  // checks here

  // key
  ok &= ch.compareInts( "key", dataA.key(), dataB.key() );
  // type
  ok &= ch.compareInts( "type", dataA.type(), dataB.type() );
  // seed
  ok &= ch.compareInts( "seed", dataA.seed().all(), dataB.seed().all() );

  // 'positions'
  ok &= ch.compare( "Position-X", dataA.position().x(), dataB.position().x() );
  ok &= ch.compare( "Position-Y", dataA.position().y(), dataB.position().y() );
  ok &= ch.compare( "Position-Z", dataA.position().z(), dataB.position().z() );
  ok &= ch.compare( "Position-E", dataA.position().e(), dataB.position().e() );
  ok &= ch.compareVectors( "Position-Center", dataA.position().center(), dataB.position().center() );

  //   ok &= ch.compareMatrices<Gaudi::SymMatrix3x3,3,3>( "Position-Covariance",
  //                                                      dataA.position().covariance(),
  //                                                      dataB.position().covariance() );
  //   ok &= ch.compareMatrices<Gaudi::SymMatrix2x2,2,2>( "Position-Spread",
  //                                                      dataA.position().spread(),
  //                                                      dataB.position().spread() );

  // Same checks as in caloHypo for covariance and spread
  // -------------------------------------------------------------------------
  std::vector<double> oDiag, tDiag, oFrac, tFrac;

  oDiag.push_back( safe_sqrt( dataA.position().covariance()( 0, 0 ) ) );
  oDiag.push_back( safe_sqrt( dataA.position().covariance()( 1, 1 ) ) );
  oDiag.push_back( safe_sqrt( dataA.position().covariance()( 2, 2 ) ) );
  oDiag.push_back( safe_sqrt( dataA.position().spread()( 0, 0 ) ) );
  oDiag.push_back( safe_sqrt( dataA.position().spread()( 1, 1 ) ) );

  tDiag.push_back( safe_sqrt( dataB.position().covariance()( 0, 0 ) ) );
  tDiag.push_back( safe_sqrt( dataB.position().covariance()( 1, 1 ) ) );
  tDiag.push_back( safe_sqrt( dataB.position().covariance()( 2, 2 ) ) );
  tDiag.push_back( safe_sqrt( dataB.position().spread()( 0, 0 ) ) );
  tDiag.push_back( safe_sqrt( dataB.position().spread()( 1, 1 ) ) );

  if ( 5.e-5 < std::abs( oDiag[0] - tDiag[0] ) ) ok = false;
  if ( 5.e-5 < std::abs( oDiag[1] - tDiag[1] ) ) ok = false;
  if ( 5.e-3 < std::abs( oDiag[2] - tDiag[2] ) ) ok = false;
  if ( 5.e-5 < std::abs( oDiag[3] - tDiag[3] ) ) ok = false;
  if ( 5.e-5 < std::abs( oDiag[4] - tDiag[4] ) ) ok = false;

  oFrac.push_back( dataA.position().covariance()( 1, 0 ) / oDiag[1] / oDiag[0] );
  oFrac.push_back( dataA.position().covariance()( 2, 0 ) / oDiag[2] / oDiag[0] );
  oFrac.push_back( dataA.position().covariance()( 2, 1 ) / oDiag[2] / oDiag[1] );
  oFrac.push_back( dataA.position().spread()( 1, 0 ) / oDiag[3] / oDiag[4] );

  tFrac.push_back( dataB.position().covariance()( 1, 0 ) / tDiag[1] / tDiag[0] );
  tFrac.push_back( dataB.position().covariance()( 2, 0 ) / tDiag[2] / tDiag[0] );
  tFrac.push_back( dataB.position().covariance()( 2, 1 ) / tDiag[2] / tDiag[1] );
  tFrac.push_back( dataB.position().spread()( 1, 0 ) / tDiag[3] / tDiag[4] );

  for ( unsigned int kk = 0; oFrac.size() > kk; ++kk ) {
    if ( 2.e-5 < std::abs( oFrac[kk] - tFrac[kk] ) ) ok = false;
  }
  // -------------------------------------------------------------------------

  // Entries
  const bool entsSizeOK = dataA.entries().size() == dataB.entries().size();
  ok &= entsSizeOK;
  if ( entsSizeOK ) {
    auto iEA( dataA.entries().begin() ), iEB( dataB.entries().begin() );
    for ( ; iEA != dataA.entries().end() && iEB != dataB.entries().end(); ++iEA, ++iEB ) {
      ok &= ( *iEA ).digit() == ( *iEB ).digit();
      ok &= ( *iEA ).status() == ( *iEB ).status();
      ok &= ch.compare( "Entry-Fraction", ( *iEA ).fraction(), ( *iEB ).fraction() );
    }
  }

  // force printout for tests
  // ok = false;
  // If comparison not OK, print full information
  if ( !ok ) {
    const std::string loc =
        ( dataA.parent() && dataA.parent()->registry() ? dataA.parent()->registry()->identifier() : "Not in TES" );
    parent().warning() << "Problem with CaloCluster data packing :-" << endmsg
                       << "  Original Cluster key=" << dataA.key() << " in '" << loc << "'" << endmsg << dataA << endmsg
                       << "  Unpacked Cluster" << endmsg << dataB << endmsg;
  }

  return ( ok ? StatusCode::SUCCESS : StatusCode::FAILURE );
}
