/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedTrack.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"

#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   *  Unpack the PackedTrack
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct UnpackTrack final : Algorithm::Transformer<Tracks( PackedTracks const& )> {

    UnpackTrack( std::string const& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"InputName", PackedTrackLocation::Default},
                       KeyValue{"OutputName", TrackLocation::Default} ) {}

    Tracks operator()( PackedTracks const& in ) const override {
      Tracks out;
      TrackPacker{this}.unpack( in, out );
      m_unpackedTracks += out.size();
      return out;
    }

    mutable Gaudi::Accumulators::AveragingCounter<unsigned long> m_unpackedTracks{this, "# Unpacked Tracks"};
  };

} // namespace LHCb

// Declaration of the Algorithm Factory
DECLARE_COMPONENT_WITH_ID( LHCb::UnpackTrack, "UnpackTrack" )
