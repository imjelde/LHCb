/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedTrack.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"

#include "LHCbAlgs/Producer.h"

namespace LHCb {

  /**
   *  Pack a track container
   *
   *  Note that the inheritance from Producer is misleading. The algorithm is
   *  reading from TES, just via a Handle so that it can deal with non existant
   *  input, which is not authorized in the functional world
   *
   *  @author Olivier Callot
   *  @date   2008-11-12
   */
  struct PackTrack : Algorithm::Producer<PackedTracks()> {

    PackTrack( std::string const& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, KeyValue{"OutputName", PackedTrackLocation::Default} ) {}

    PackedTracks operator()() const override;

    DataObjectReadHandle<Tracks> m_tracks{this, "InputName", TrackLocation::Default};

    Gaudi::Property<bool> m_enableCheck{this, "EnableCheck",
                                        false}; ///< Flag to turn on automatic unpacking and checking
    ///< of the output post-packing

    mutable Gaudi::Accumulators::StatCounter<> m_nbPackedTracks{this, "# PackedTracks"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackTrack, "PackTrack" )

LHCb::PackedTracks LHCb::PackTrack::operator()() const {
  PackedTracks out;
  out.setVersion( 5 );

  if ( !m_tracks.exist() ) return out;
  auto const* tracks = m_tracks.get();

  // Pack the tracks
  const TrackPacker packer( this );
  packer.pack( *tracks, out );

  // Packing checks
  if ( m_enableCheck ) {
    // make new unpacked output data object
    Tracks unpacked;
    packer.unpack( out, unpacked );
    // run checks
    packer.check( *tracks, unpacked ).ignore();
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  auto* pReg = tracks->registry();
  if ( pReg ) pReg->setAddress( nullptr );

  // Summary of the size of the PackTracks container
  m_nbPackedTracks += out.data().size();

  return out;
}
