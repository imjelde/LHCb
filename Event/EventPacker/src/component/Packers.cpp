/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "PackerBaseAlg.h"

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichHitPacker>, "MCRichHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichSegmentPacker>, "MCRichSegmentPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichTrackPacker>, "MCRichTrackPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPrsHitPacker>, "MCPrsHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCSpdHitPacker>, "MCSpdHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCEcalHitPacker>, "MCEcalHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCHcalHitPacker>, "MCHcalHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCVeloHitPacker>, "MCVeloHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCVPHitPacker>, "MCVPHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCTTHitPacker>, "MCTTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCUTHitPacker>, "MCUTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCITHitPacker>, "MCITHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCOTHitPacker>, "MCOTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCMuonHitPacker>, "MCMuonHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::RichPIDPacker>, "RichPIDPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MuonPIDPacker>, "MuonPIDPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::ParticlePacker>, "ParticlePacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::VertexPacker>, "VertexPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::WeightsVectorPacker>, "WeightsVectorPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::CaloClusterPacker>, "CaloClusterPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::CaloDigitPacker>, "CaloDigitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::CaloAdcPacker>, "CaloAdcPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCFTHitPacker>, "MCFTHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCSLHitPacker>, "MCSLHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::FlavourTagPacker>, "FlavourTagPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCHCHitPacker>, "MCHCHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCBcmHitPacker>, "MCBcmHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCBlsHitPacker>, "MCBlsHitPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::RelatedInfoRelationsPacker>, "RelatedInfoRelationsPacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Pack<LHCb::MCPlumeHitPacker>, "MCPlumeHitPacker" )
