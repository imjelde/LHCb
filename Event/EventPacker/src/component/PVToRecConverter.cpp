/*****************************************************************************\
 * (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
 *                                                                             *
 * This software is distributed under the terms of the GNU General Public      *
 * Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
 *                                                                             *
 * In applying this licence, CERN does not waive the privileges and immunities *
 * granted to it by virtue of its status as an Intergovernmental Organization  *
 * or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PrimaryVertex.h"
#include "Event/RecVertex.h"

#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   * @author Wouter Hulsbergen
   * @date 2015-10-30
   */
  class PVToRecConverter
      : public Algorithm::Transformer<RecVertex::Container( PrimaryVertex::Range const&, Track::Range const& )> {
    Gaudi::Property<bool> m_rescaleWeights{this, "RescaleWeights", true};

  public:
    PVToRecConverter( const std::string& name, ISvcLocator* svc )
        : Transformer{name,
                      svc,
                      {KeyValue{"InputLocation", PrimaryVertexLocation::Default},
                       KeyValue{"InputTrackLocation", TrackLocation::Default}},
                      {"OutputLocation", RecVertexLocation::Primary}} {}

    RecVertex::Container operator()( PrimaryVertex::Range const& pvs, Track::Range const& tracks ) const override {

      // create a map from veloIDs to tracks in the best track list. only
      // store the first one (since long tracks are first!)
      std::map<PrimaryVertex::VeloSegmentID, std::vector<const Track*>> idToTracks;
      for ( const auto& tr : tracks ) {
        auto id = PrimaryVertex::uniqueVeloSegmentID( *tr );
        idToTracks[id].push_back( tr );
      }

      // now loop over input vertices and create RecVertices
      RecVertex::Container recvertices;
      for ( const auto& pv : pvs ) {
        // it would have been nice to have a constructor from a VertexBase!
        auto                   recvertex = new RecVertex( pv->position() );
        recvertex->VertexBase::operator  =( *pv );
        recvertex->setTechnique( RecVertex::RecVertexType::Primary );
        // add the daughters
        for ( const auto& trk : pv->tracks() ) {
          auto trkinmap = idToTracks.find( trk.id() );
          if ( trkinmap != idToTracks.end() ) {
            // add them all!
            for ( const auto& itrk : trkinmap->second ) {
              double weight = trk.weight();
              if ( m_rescaleWeights ) {
                // track cov matrix may have changed due to track
                // fit. rescale weights such that track gets effectively
                // still the same weight. the unbiasing will then still
                // work.
                State newstate = itrk->firstState();
                newstate.linearTransportTo( pv->refZ() );
                // compute the ratio of the covariances of the transverse
                // IP: dx * diry - dy * dirx. (don't need to divide by
                // sqrt(tx^2+ty^2) since we take ratio anyway)
                const double tx     = newstate.tx();
                const double ty     = newstate.ty();
                auto         oldcov = trk.invcov();
                oldcov.Invert();
                const auto&  newcov = newstate.covariance();
                const double covA = oldcov( 0, 0 ) * ty * ty + oldcov( 1, 1 ) * tx * tx - 2 * oldcov( 1, 0 ) * tx * ty;
                const double covB = newcov( 0, 0 ) * ty * ty + newcov( 1, 1 ) * tx * tx - 2 * newcov( 1, 0 ) * tx * ty;
                weight *= covB / covA;
              }
              recvertex->addToTracks( itrk, weight );
            }
          }
        }
        recvertices.insert( recvertex );
        // info() << "Created RecVertex with daughters: "
        //        << pv->tracks().size() << " " << recvertex->tracks().size() << endreq ;
      }
      return recvertices;
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PVToRecConverter, "PVToRecConverter" )
