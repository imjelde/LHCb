/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/DataObjectHandle.h"

namespace LHCb {

  /**
   *  Pack a protoparticle container
   *
   *  Note that the inheritance from Consumer and the void input are misleading
   *  but the algorithm is reading from and writing to TES, just via a Handles
   *  so that it can push result at the begining of the operator(), as cross
   *  pointers are used in the TES and requires this and it can also ignore missing
   *  input. FIXME This should not be the way to do it. The configuration needs
   *  to be fixed and then we could behave normally here.
   *
   *  @author Olivier Callot
   *  @date   2008-11-13
   */
  struct PackProtoParticle : Algorithm::Consumer<void()> {
    using Consumer::Consumer;

    void operator()() const override;

    DataObjectReadHandle<ProtoParticles>        m_protos{this, "InputName", ProtoParticleLocation::Charged};
    DataObjectWriteHandle<PackedProtoParticles> m_packedProtos{this, "OutputName",
                                                               PackedProtoParticleLocation::Charged};

    Gaudi::Property<bool> m_enableCheck{this, "EnableCheck",
                                        false}; ///< Flag to turn on automatic unpacking and checking
    ///< of the output post-packing
    Gaudi::Property<bool> m_clearRegistry{this, "ClearRegistry",
                                          true}; ///< Flag to turn on the clearing of the registry
    ///< if the input data is not kept
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackProtoParticle, "PackProtoParticle" )

void LHCb::PackProtoParticle::operator()() const {

  // NOTE: the output container _must be on the TES_ prior to passing it to the
  //      unpacker, as otherwise filling the references to other objects does
  //      not work
  auto* out = m_packedProtos.put( std::make_unique<PackedProtoParticles>() );
  out->setVersion( 2 ); // CRJ : Why set this ?
  out->setPackingVersion( PackedProtoParticles::defaultPackingVersion() );

  if ( !m_protos.exist() ) return;
  auto const* parts = m_protos.get();

  // pack
  const ProtoParticlePacker packer( this );
  packer.pack( *parts, *out );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Created " << out->data().size() << " PackedProtoParticles at '" << m_packedProtos.objKey() << "'"
            << endmsg;

  // Packing checks
  if ( m_enableCheck ) {
    ProtoParticles* unpacked = new ProtoParticles();
    auto            location = m_protos.fullKey().key() + "_PackingCheck";
    /// FIXME This (temporary) TES usage should not be needed
    /// Sadly the pack structure expects it. To be improved
    evtSvc()->registerObject( location, unpacked ).ignore();

    // unpack
    packer.unpack( *out, *unpacked );
    // run checks
    packer.check( *parts, *unpacked ).ignore();

    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      error() << "Failed to delete test data after unpacking check" << endmsg;
    }
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( m_clearRegistry ) {
    auto* pReg = parts->registry();
    if ( pReg ) pReg->setAddress( nullptr );
  }
}
