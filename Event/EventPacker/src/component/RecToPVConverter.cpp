/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PrimaryVertex.h"
#include "Event/RecVertex.h"

#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   *  @author Wouter Hulsbergen
   *  @date   2015-09-25
   */
  class RecToPVConverter : public Algorithm::Transformer<PrimaryVertex::Container( RecVertex::Range const& )> {
    Gaudi::Property<bool> m_doFit{this, "DoFit", false};

  public:
    RecToPVConverter( const std::string& name, ISvcLocator* svc )
        : Transformer{name,
                      svc,
                      {"InputLocation", RecVertexLocation::Primary},
                      {"OutputLocation", PrimaryVertexLocation::Default}} {}

    PrimaryVertex::Container operator()( const RecVertex::Range& in ) const override {
      PrimaryVertex::Container out{};
      for ( const auto& i : in ) { out.insert( new PrimaryVertex( *i, m_doFit ) ); }
      return out;
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::RecToPVConverter, "RecToPVConverter" )
