/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"

#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   *  Compare two containers of ProtoParticles
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct CompareProtoParticle : Algorithm::Consumer<void( ProtoParticles const&, ProtoParticles const& )> {

    /// Standard constructor
    CompareProtoParticle( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {KeyValue{"InputName", ProtoParticleLocation::Charged},
                    KeyValue{"TestName", ProtoParticleLocation::Charged + "Test"}}} {}

    void operator()( ProtoParticles const& old, ProtoParticles const& test ) const override {
      // check and return
      const ProtoParticlePacker packer( this );
      packer.check( old, test )
          .orThrow( "CompareProtoParticle failed", "CompareProtoParticle" )
          .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::CompareProtoParticle, "CompareProtoParticle" )
