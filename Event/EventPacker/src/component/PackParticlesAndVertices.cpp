/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//-----------------------------------------------------------------------------
// Implementation file for class : PackParticlesAndVertices
//
// 2012-01-23 : Olivier Callot
//-----------------------------------------------------------------------------
#include "Event/FlavourTag.h"
#include "Event/MCParticle.h"
#include "Event/MuonPID.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/Particle.h"
#include "Event/ProtoParticle.h"
#include "Event/RecVertex.h"
#include "Event/RelatedInfoMap.h"
#include "Event/RichPID.h"
#include "Event/StandardPacker.h"
#include "Event/Track.h"
#include "Event/Vertex.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "GaudiKernel/IDataManagerSvc.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/LHCbID.h"
#include "Kernel/Particle2LHCbIDs.h"
#include "Relations/Relation1D.h"
#include "Relations/RelationWeighted1D.h"
#include "boost/algorithm/string.hpp"
#include "fmt/format.h"

namespace {
  /// Add `prefix` to the beginning of `s` if not already present.
  /// Assumes `prefix` does not end with a forward slash.
  std::string ensure_prefix( std::string_view s, std::string_view prefix = "/Event" ) {
    return boost::algorithm::starts_with( s, prefix ) ? std::string{s} : fmt::format( "{}/{}", prefix, s );
  }
  /// Get an objects location in the TES
  std::string objectLocation( const DataObject& pObj ) {
    return pObj.registry() ? pObj.registry()->identifier() : std::string{};
  }
} // namespace

/** @class PackParticlesAndVertices PackParticlesAndVertices.h
 *
 *  Packs Particles, Vertices and related information.
 *
 *  If one always wants to create the packed locations, regardless of whether there
 *  are any related objects to be packed, one should set the `AlwaysCreateOutput` to
 *  true and set the `AlwaysCreateContainers` list to the desired locations.
 *
 *  For example, the following configures the tool to always create the packed
 *  particles location:
 *
 *  @code
 *  from Configurables import PackParticlesAndVertices
 *  input_stream = '/Event'
 *  ppvs = PackParticlesAndVertices()
 *  ppvs.InputStream = input_stream
 *  ppvs.AlwaysCreateOutput = True
 *  ppvs.AlwaysCreateContainers = [
 *      '{0}/pPhys/Particles'.format(input_stream)
 *  ]
 *  @endcode
 *
 *  @author Olivier Callot
 *  @date   2012-01-23
 */
class PackParticlesAndVertices : public GaudiAlgorithm {

public:
  /// Standard constructor
  using GaudiAlgorithm::GaudiAlgorithm;

  StatusCode execute() override; ///< Algorithm execution

  StatusCode initialize() override; ///< Initialize the algorithm instance.

private:
  // Relation types
  typedef LHCb::Relation1D<LHCb::Particle, LHCb::VertexBase>                      P2VRELATION;
  typedef LHCb::Relation1D<LHCb::Particle, LHCb::MCParticle>                      P2MCPRELATION;
  typedef LHCb::Relation1D<LHCb::Particle, int>                                   Part2IntRelations;
  typedef LHCb::Relation1D<LHCb::Particle, LHCb::RelatedInfoMap>                  Part2InfoRelations;
  typedef LHCb::RelationWeighted1D<LHCb::ProtoParticle, LHCb::MCParticle, double> Proto2MCPRelation;

private:
  /// Test if a TES container is Veto'ed from being packed
  bool isVetoed( const std::string& id ) const;

  /// Test if an output location should always be created
  bool alwaysCreate( const std::string& id ) const;

  /// Pack a Particle container
  void packContainer( const LHCb::Particles* parts, LHCb::PackedParticles& pparts ) const;

  /// Pack a ProtoParticle container
  void packContainer( const LHCb::ProtoParticles* protos, LHCb::PackedProtoParticles& pprotos ) const;

  /// Pack a MuonPID container
  void packContainer( const LHCb::MuonPIDs* pids, LHCb::PackedMuonPIDs& ppids ) const;

  /// Pack a RichPID container
  void packContainer( const LHCb::RichPIDs* pids, LHCb::PackedRichPIDs& ppids ) const;

  /// Pack a Track container
  void packContainer( const LHCb::Tracks* tracks, LHCb::PackedTracks& ptracks ) const;

  /// pack a vertex container
  void packContainer( const LHCb::Vertices* verts, LHCb::PackedVertices& pverts ) const;

  /// Pack a FlavourTag container
  void packContainer( const LHCb::FlavourTags* fts, LHCb::PackedFlavourTags& pfts ) const;

  /// Pack a RecVertex container
  void packContainer( const LHCb::RecVertices* rverts, LHCb::PackedRecVertices& prverts ) const;

  /// Pack a 'SmartRef to SmartRef' relations container
  template <typename RELATION>
  void packAP2PRelationContainer( const RELATION* rels, LHCb::PackedRelations& prels ) const;

  /// Pack a 'SmartRef to SmartRef' weighted relations container
  template <typename RELATION>
  void packAP2PRelationContainer( const RELATION* rels, LHCb::PackedWeightedRelations& prels ) const;

  /// Pack a 'SmartRef to int' relations container
  template <typename RELATION>
  void packAP2IntRelationContainer( const RELATION* rels, LHCb::PackedRelations& prels ) const;

  /// Pack a 'SmartRef to RelatedInfoMap' relations container
  void packContainer( const Part2InfoRelations* rels, LHCb::PackedRelatedInfoRelations& prels,
                      const std::string& location ) const;

  template <typename Packed, typename Input>
  void pack( std::string_view instream, LHCb::span<const std::string> names, std::string_view label,
             std::vector<DataObject*>& ) const;

  /// Copy data object version
  template <typename INPUT, typename OUTPUT>
  void saveVersion( const INPUT& in, OUTPUT& out ) const {
    const int i_ver = in.version();
    const int o_ver = out.version();
    // sanity check
    if ( o_ver != 0 && o_ver != i_ver ) {
      Warning( fmt::format( "{} version {} != current packed version {}", objectLocation( in ), i_ver, o_ver ) )
          .ignore();
    }
    out.setVersion( i_ver );
  }

private:
  /// Related Info Packer
  const LHCb::RelatedInfoRelationsPacker m_rInfoPacker{this};

  Gaudi::Property<std::string> m_inputStream{this, "InputStream", "/Event"}; ///< Input stream root
  Gaudi::Property<bool>        m_alwaysOutput{this, "AlwaysCreateOutput",
                                       false}; ///< Flag to turn on the creation of output, even when input is missing
  Gaudi::Property<bool> m_deleteInput{this, "DeleteInput", false}; ///< Delete the containers after packing if true.
  Gaudi::Property<bool> m_enableCheck{this, "EnableCheck", false}; ///< Flag to turn on automatic unpacking and checking
                                                                   ///< of the output post-packing
  Gaudi::Property<std::vector<std::string>> m_vetoedConts{this, "VetoedContainers", {}}; ///< Vetoed containers. Will
                                                                                         ///< not be packed.
  Gaudi::Property<std::vector<std::string>> m_createConts{this,
                                                          "AlwaysCreateContainers"}; ///< Always create these containers
};

//=============================================================================
// Declaration of the Algorithm Factory
DECLARE_COMPONENT( PackParticlesAndVertices )
//=============================================================================

template <typename Packed, typename Input>
void PackParticlesAndVertices::pack( std::string_view instream, LHCb::span<const std::string> names,
                                     std::string_view label, std::vector<DataObject*>& toBeDeleted ) const {
  auto outputLocation = fmt::format( "{}{}", m_inputStream, instream );
  if ( !names.empty() || alwaysCreate( outputLocation ) ) {
    auto* packed = new Packed();
    put( packed, outputLocation );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process " << label << " containers :" << endmsg;
    toBeDeleted.reserve( names.size() + toBeDeleted.size() );
    for ( const auto& name : names ) {
      auto* input = get<Input>( name );
      saveVersion( *input, *packed );
      if ( m_deleteInput ) toBeDeleted.push_back( input );
      if ( input->empty() ) continue;
      if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d %s in ", input->size(), label ) << name << endmsg;
      packContainer( input, *packed );
    }
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << packed->data().size() << " packed " << label << "s" << endmsg;
  }
}
//=========================================================================
//  Pack a 'SmartRef to SmartRef' relations container
//=========================================================================
template <typename RELATION>
void PackParticlesAndVertices::packAP2PRelationContainer( const RELATION* rels, LHCb::PackedRelations& prels ) const {
  // Make a new packed data object and save
  auto& prel = prels.data().emplace_back();

  // reference to original container and key
  prel.container = StandardPacker::reference64( &prels, rels, 0 );

  // First object
  prel.start = prels.sources().size();

  // reserve size
  const auto newSize = prels.sources().size() + rels->relations().size();
  prels.sources().reserve( newSize );
  prels.dests().reserve( newSize );

  // Loop over relations
  for ( const auto& R : rels->relations() ) {
    prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
    prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to()->parent(), R.to()->key() ) );
  }

  // last object
  prel.end = prels.sources().size();

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( !m_deleteInput ) rels->registry()->setAddress( nullptr );
}

//=========================================================================
//  Pack a 'SmartRef to SmartRef' relations container
//=========================================================================
template <typename RELATION>
void PackParticlesAndVertices::packAP2PRelationContainer( const RELATION*                rels,
                                                          LHCb::PackedWeightedRelations& prels ) const {
  // Make a new packed data object and save
  auto& prel = prels.data().emplace_back();

  // reference to original container and key
  prel.container = StandardPacker::reference64( &prels, rels, 0 );

  // First object
  prel.start = prels.sources().size();

  // reserve size
  const auto newSize = prels.sources().size() + rels->relations().size();
  prels.sources().reserve( newSize );
  prels.dests().reserve( newSize );
  prels.weights().reserve( newSize );

  // Loop over relations
  for ( const auto& R : rels->relations() ) {
    prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
    prels.dests().emplace_back( StandardPacker::reference64( &prels, R.to()->parent(), R.to()->key() ) );
    prels.weights().emplace_back( R.weight() );
  }

  // last object
  prel.end = prels.sources().size();

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( !m_deleteInput ) rels->registry()->setAddress( nullptr );
}

//=========================================================================
//  Pack a 'SmartRef to int' relations container
//=========================================================================
template <typename RELATION>
void PackParticlesAndVertices::packAP2IntRelationContainer( const RELATION* rels, LHCb::PackedRelations& prels ) const {
  // Make a new packed data object and save
  auto& prel = prels.data().emplace_back();

  // reference to original container and key
  prel.container = StandardPacker::reference64( &prels, rels, 0 );

  // First object
  prel.start = prels.sources().size();

  // reserve size
  const auto newSize = prels.sources().size() + rels->relations().size();
  prels.sources().reserve( newSize );
  prels.dests().reserve( newSize );

  // Loop over relations
  for ( const auto& R : rels->relations() ) {
    prels.sources().emplace_back( StandardPacker::reference64( &prels, R.from()->parent(), R.from()->key() ) );
    prels.dests().emplace_back( R.to() );
  }

  // last object
  prel.end = prels.sources().size();

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( !m_deleteInput ) rels->registry()->setAddress( nullptr );
}

//==============================================================================
// Test if a TES location is veto'ed
//==============================================================================
bool PackParticlesAndVertices::isVetoed( const std::string& id ) const {
  const bool vetoed = ( std::find( m_vetoedConts.begin(), m_vetoedConts.end(), id ) != m_vetoedConts.end() );
  if ( msgLevel( MSG::DEBUG ) ) {
    if ( vetoed ) {
      debug() << "  --> VETO'ed  ... " << id << endmsg;
    } else {
      debug() << "  --> Selected ... " << id << endmsg;
    }
  }
  return vetoed;
}

//==============================================================================
// Test if an output TES location should always be created
//==============================================================================
bool PackParticlesAndVertices::alwaysCreate( const std::string& id ) const {
  return m_alwaysOutput && ( std::find( m_createConts.begin(), m_createConts.end(), id ) != m_createConts.end() );
}

StatusCode PackParticlesAndVertices::initialize() {
  return GaudiAlgorithm::initialize().andThen( [&] {
    if ( m_alwaysOutput && m_createConts.empty() )
      warning() << "AlwaysCreateOutput is true but AlwaysCreateContainers "
                   "is empty!"
                << endmsg;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode PackParticlesAndVertices::execute() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // Class IDs for handled data
  const CLID clIdParticles      = 0x60000 + LHCb::CLID_Particle;
  const CLID clIdVertices       = 0x60000 + LHCb::CLID_Vertex;
  const CLID clIdRecVertices    = 0x60000 + LHCb::CLID_RecVertex;
  const CLID clIdFlavourTags    = 0x60000 + LHCb::CLID_FlavourTag;
  const CLID clIdTracks         = 0x60000 + LHCb::CLID_Track;
  const CLID clIdProtoParticles = 0x60000 + LHCb::CLID_ProtoParticle;
  const CLID clIdMuonPIDs       = 0x60000 + LHCb::CLID_MuonPID;
  const CLID clIdRichPIDs       = 0x60000 + LHCb::CLID_RichPID;
  const CLID clIdPart2Vert      = 0xEA9168DC; // Particle to Vertex relation
  const CLID clIdPart2MCPart    = 0x7B880798; // Particle to MCParticle relations
  const CLID clIdProto2MCPart   = 0x6540787E; // ProtoParticle to MCParticle weighted relations
  const CLID clIdPart2Int       = 0xF94852E4; // Particle to int relations
  const CLID clIdPart2RelInfo   = 0x90F0684D; // particle to related info map

  // list of objects to remove at the end
  std::vector<DataObject*> toBeDeleted;

  //==============================================================================
  // Traverse the TES to build the map of ClassIDs to TES locations
  //==============================================================================
  std::map<CLID, std::vector<std::string>> tesMap;
  eventSvc()
      .as<IDataManagerSvc>()
      ->traverseSubTree( m_inputStream,
                         [this, &tesMap]( IRegistry* leaf, int ) {
                           // TES location identifier
                           const std::string& id = ensure_prefix( leaf->identifier() );
                           // load the object
                           if ( const auto* tmp = leaf->object(); tmp ) {
                             // If not a data node, save in the map
                             if ( tmp->clID() != CLID_DataObject ) {
                               if ( this->msgLevel( MSG::DEBUG ) )
                                 this->debug() << "Found '" << id << "' ClassID=" << tmp->clID() << " Type='"
                                               << System::typeinfoName( typeid( *tmp ) ) << endmsg;
                               // Save in the map if not veto'ed
                               if ( !this->isVetoed( id ) ) { tesMap[tmp->clID()].push_back( id ); }
                             }
                           }
                           return true;
                         } )
      .ignore();

  //==============================================================================
  // Find Particles
  //==============================================================================
  pack<LHCb::PackedParticles, LHCb::Particles>( LHCb::PackedParticleLocation::InStream, tesMap[clIdParticles],
                                                "Particle", toBeDeleted );

  //==============================================================================
  // Find Vertices
  //==============================================================================
  pack<LHCb::PackedVertices, LHCb::Vertices>( LHCb::PackedVertexLocation::InStream, tesMap[clIdVertices], "Vertex",
                                              toBeDeleted );

  //==============================================================================
  // Find Flavour Tags
  //==============================================================================
  pack<LHCb::PackedFlavourTags, LHCb::FlavourTags>( LHCb::PackedFlavourTagLocation::InStream, tesMap[clIdFlavourTags],
                                                    "FlavourTag", toBeDeleted );

  //==============================================================================
  // Find Rec Vertices
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRecVertexLocation::InStream;
    const auto& names          = tesMap[clIdRecVertices];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRecVertices* prverts = new LHCb::PackedRecVertices();
      prverts->setPackingVersion( LHCb::PackedRecVertices::defaultPackingVersion() );
      put( prverts, outputLocation );
      prverts->setVersion( 2 ); // CRJ - Increment version for new RecVertex with weights
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process RecVertices containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        LHCb::RecVertices* rverts = get<LHCb::RecVertices>( name );
        if ( m_deleteInput ) toBeDeleted.push_back( rverts );
        if ( rverts->empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d RecVertices in ", rverts->size() ) << name << endmsg;
        packContainer( rverts, *prverts );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << prverts->data().size() << " packed RecVertices" << endmsg;
    }
  }

  //==============================================================================
  // Find Particle2 Vert Relations
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRelationsLocation::InStream;
    const auto& names          = tesMap[clIdPart2Vert];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRelations* prels = new LHCb::PackedRelations();
      put( prels, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process Particle2Vertex Relation containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        P2VRELATION* rels = get<P2VRELATION>( name );
        saveVersion( *rels, *prels );
        if ( m_deleteInput ) toBeDeleted.push_back( rels );
        if ( rels->relations().empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << format( "%4d relations in ", rels->relations().size() ) << name << endmsg;
        packAP2PRelationContainer( rels, *prels );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << prels->data().size() << " packed Particle2Vertex relations" << endmsg;
    }
  }

  //==============================================================================
  // Find Particle to Ints
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRelationsLocation::P2Int;
    const auto& names          = tesMap[clIdPart2Int];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRelations* pPartIds = new LHCb::PackedRelations();
      put( pPartIds, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process Particle2Int Relation containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        Part2IntRelations* partIds = get<Part2IntRelations>( name );
        saveVersion( *partIds, *pPartIds );
        if ( m_deleteInput ) toBeDeleted.push_back( partIds );
        if ( partIds->relations().empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << format( "%4d Particle2Ints in ", partIds->relations().size() ) << name << endmsg;
        packAP2IntRelationContainer( partIds, *pPartIds );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << pPartIds->data().size() << " packed Particle2Ints" << endmsg;
    }
  }

  //==============================================================================
  // Find Particle to Related Info
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRelatedInfoLocation::InStream;
    const auto& names          = tesMap[clIdPart2RelInfo];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRelatedInfoRelations* pPartIds = new LHCb::PackedRelatedInfoRelations();
      put( pPartIds, outputLocation );
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Found " << names.size() << " RelatedInfo containers : " << names << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      pPartIds->containers().reserve( names.size() );
      for ( const auto& name : names ) {
        Part2InfoRelations* partIds = get<Part2InfoRelations>( name );
        saveVersion( *partIds, *pPartIds );
        if ( m_deleteInput ) toBeDeleted.push_back( partIds );
        if ( msgLevel( MSG::DEBUG ) )
          debug() << " -> Processing " << name << " with " << partIds->relations().size() << " relations" << endmsg;
        if ( partIds->relations().empty() ) continue;
        packContainer( partIds, *pPartIds, name );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << pPartIds->data().size() << " packed Particle2RelatedInfo in "
                << pPartIds->containers().size() << " containers."
                << " Total info pairs = " << pPartIds->info().size() << endmsg;
    }
  }

  //==============================================================================
  // Find ProtoParticles
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedProtoParticleLocation::InStream;
    const auto& names          = tesMap[clIdProtoParticles];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedProtoParticles* pprotos = new LHCb::PackedProtoParticles();
      pprotos->setVersion( 2 ); // CRJ : Why set this ?
      pprotos->setPackingVersion( LHCb::PackedProtoParticles::defaultPackingVersion() );
      put( pprotos, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process ProtoParticle containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        LHCb::ProtoParticles* protos = get<LHCb::ProtoParticles>( name );
        if ( m_deleteInput ) toBeDeleted.push_back( protos );
        if ( protos->empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d protoparticles in ", protos->size() ) << name << endmsg;
        packContainer( protos, *pprotos );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << pprotos->data().size() << " packed ProtoParticles" << endmsg;
    }
  }

  //==============================================================================
  // Find MuonPIDs
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedMuonPIDLocation::InStream;
    const auto& names          = tesMap[clIdMuonPIDs];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedMuonPIDs* ppids = new LHCb::PackedMuonPIDs();
      ppids->setPackingVersion( LHCb::PackedMuonPIDs::defaultPackingVersion() );
      put( ppids, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process MuonPID containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        LHCb::MuonPIDs* pids = get<LHCb::MuonPIDs>( name );
        saveVersion( *pids, *ppids );
        if ( m_deleteInput ) toBeDeleted.push_back( pids );
        if ( pids->empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d MuonPIDs in ", pids->size() ) << name << endmsg;
        packContainer( pids, *ppids );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << ppids->data().size() << " packed MuonPIDs" << endmsg;
    }
  }

  //==============================================================================
  // Find RichPIDs
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRichPIDLocation::InStream;
    const auto& names          = tesMap[clIdRichPIDs];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRichPIDs* ppids = new LHCb::PackedRichPIDs();
      ppids->setPackingVersion( LHCb::PackedRichPIDs::defaultPackingVersion() );
      put( ppids, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process RichPID containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        LHCb::RichPIDs* pids = get<LHCb::RichPIDs>( name );
        saveVersion( *pids, *ppids );
        if ( m_deleteInput ) toBeDeleted.push_back( pids );
        if ( pids->empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d RichPIDs in ", pids->size() ) << name << endmsg;
        packContainer( pids, *ppids );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << ppids->data().size() << " packed RichPIDs" << endmsg;
    }
  }

  //==============================================================================
  // Find Tracks
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedTrackLocation::InStream;
    const auto& names          = tesMap[clIdTracks];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedTracks* ptracks = new LHCb::PackedTracks();
      ptracks->setVersion( 5 );
      put( ptracks, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process Track containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        LHCb::Tracks* tracks = get<LHCb::Tracks>( name );
        if ( m_deleteInput ) toBeDeleted.push_back( tracks );
        if ( tracks->empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) ) debug() << format( "%4d tracks in ", tracks->size() ) << name << endmsg;
        packContainer( tracks, *ptracks );
      }
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Stored " << ptracks->data().size() << " packed Tracks" << endmsg;
    }
  }

  // MC Information next

  //==============================================================================
  // Find Particle -> MC relations
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedRelationsLocation::P2MCP;
    const auto& names          = tesMap[clIdPart2MCPart];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedRelations* prels = new LHCb::PackedRelations();
      put( prels, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process Particle2MCParticle Relation containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        P2MCPRELATION* rels = get<P2MCPRELATION>( name );
        saveVersion( *rels, *prels );
        if ( m_deleteInput ) toBeDeleted.push_back( rels );
        if ( rels->relations().empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << format( "%4d relations in ", rels->relations().size() ) << name << endmsg;
        packAP2PRelationContainer( rels, *prels );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << prels->data().size() << " packed Particle2MCParticle relations" << endmsg;
    }
  }

  //==============================================================================
  // Find Proto -> MC relations
  //==============================================================================
  {
    auto        outputLocation = m_inputStream + LHCb::PackedWeightedRelationsLocation::PP2MCP;
    const auto& names          = tesMap[clIdProto2MCPart];
    if ( !names.empty() || alwaysCreate( outputLocation ) ) {
      LHCb::PackedWeightedRelations* prels = new LHCb::PackedWeightedRelations();
      put( prels, outputLocation );
      if ( msgLevel( MSG::DEBUG ) ) debug() << "=== Process ProtoParticle2MCParticle Relation containers :" << endmsg;
      toBeDeleted.reserve( names.size() + toBeDeleted.size() );
      for ( const auto& name : names ) {
        Proto2MCPRelation* rels = get<Proto2MCPRelation>( name );
        saveVersion( *rels, *prels );
        if ( m_deleteInput ) toBeDeleted.push_back( rels );
        if ( rels->relations().empty() ) continue;
        if ( msgLevel( MSG::DEBUG ) )
          debug() << format( "%4d relations in ", rels->relations().size() ) << name << endmsg;
        packAP2PRelationContainer( rels, *prels );
      }
      if ( msgLevel( MSG::DEBUG ) )
        debug() << "Stored " << prels->data().size() << " packed ProtoParticle2MCParticle relations" << endmsg;
    }
  }

  //==============================================================================
  // Remove the converted containers if requested
  //==============================================================================

  if ( m_deleteInput ) {
    StatusCode sc = StatusCode::SUCCESS;
    for ( auto*& it : toBeDeleted ) {
      const StatusCode ssc = evtSvc()->unregisterObject( it ).andThen( [&] {
        delete it;
        it = nullptr;
      } );
      if ( sc ) sc = ssc;
    }
    if ( sc.isFailure() ) { return Error( "Failed to delete input data as requested", sc ); }
  }

  //==============================================================================

  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Finished..." << endmsg; }
  return StatusCode::SUCCESS;
}

//=========================================================================
// Pack a container of FlavourTags in the PackedParticles object
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::FlavourTags* fts, LHCb::PackedFlavourTags& pfts ) const {
  const LHCb::FlavourTagPacker ftPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::FlavourTags() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( fts->version() );
    put( unpacked, "/Event/Transient/PsAndVsFTTest" );
  }

  // reserve size
  pfts.data().reserve( pfts.data().size() + fts->size() );

  // loop over FTs
  for ( const auto* ft : *fts ) {
    // Make a new packed data object and save
    pfts.data().emplace_back();
    auto& pft = pfts.data().back();

    // reference to original container and key
    pft.key = StandardPacker::reference64( &pfts, ft->parent(), ft->key() );

    // pack the physics info
    ftPacker.pack( *ft, pft, pfts );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey64( pft.key, linkID, key );
      LHCb::FlavourTag* testObj = new LHCb::FlavourTag();
      unpacked->insert( testObj, key );
      ftPacker.unpack( pft, *testObj, pfts, *unpacked );
      ftPacker.check( *ft, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) fts->registry()->setAddress( nullptr );
}

//=========================================================================
// Pack a container of particles in the PackedParticles object
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::Particles* parts, LHCb::PackedParticles& pparts ) const {
  const LHCb::ParticlePacker pPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::Particles() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( parts->version() );
    put( unpacked, "/Event/Transient/PsAndVsParticleTest" );
  }

  // reserve size
  pparts.data().reserve( pparts.data().size() + parts->size() );

  // loop
  for ( const auto* part : *parts ) {
    // Make a new packed data object and save
    pparts.data().emplace_back();
    auto& ppart = pparts.data().back();

    // reference to original container and key
    ppart.key = StandardPacker::reference64( &pparts, part->parent(), part->key() );

    // pack the physics info
    pPacker.pack( *part, ppart, pparts );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey64( ppart.key, linkID, key );
      LHCb::Particle* testObj = new LHCb::Particle();
      unpacked->insert( testObj, key );
      pPacker.unpack( ppart, *testObj, pparts, *unpacked );
      pPacker.check( *part, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) parts->registry()->setAddress( nullptr );
}

//=========================================================================
// Pack a container of MuonPIDs
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::MuonPIDs* pids, LHCb::PackedMuonPIDs& ppids ) const {
  const LHCb::MuonPIDPacker pPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::MuonPIDs() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( pids->version() );
    put( unpacked, "/Event/Transient/PsAndVsMuonPIDTest" );
  }

  // reserve size
  ppids.data().reserve( ppids.data().size() + pids->size() );

  // loop
  for ( const auto* pid : *pids ) {
    // Make a new packed data object and save
    ppids.data().emplace_back();
    auto& ppid = ppids.data().back();

    // reference to original container and key
    ppid.key = StandardPacker::reference64( &ppids, pid->parent(), pid->key() );

    // pack the physics info
    pPacker.pack( *pid, ppid, ppids );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey32( ppid.key, linkID, key );
      LHCb::MuonPID* testObj = new LHCb::MuonPID();
      unpacked->insert( testObj, key );
      pPacker.unpack( ppid, *testObj, ppids, *unpacked );
      pPacker.check( *pid, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) pids->registry()->setAddress( nullptr );
}

//=========================================================================
// Pack a container of RichPIDs
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::RichPIDs* pids, LHCb::PackedRichPIDs& ppids ) const {
  const LHCb::RichPIDPacker pPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::RichPIDs() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( pids->version() );
    put( unpacked, "/Event/Transient/PsAndVsRichPIDTest" );
  }

  // reserve size
  ppids.data().reserve( ppids.data().size() + pids->size() );

  // loop
  for ( const auto* pid : *pids ) {
    // Make a new packed data object and save
    ppids.data().emplace_back();
    auto& ppid = ppids.data().back();

    // reference to original container and key
    ppid.key = StandardPacker::reference64( &ppids, pid->parent(), pid->key() );

    // pack the physics info
    pPacker.pack( *pid, ppid, ppids );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey32( ppid.key, linkID, key );
      LHCb::RichPID* testObj = new LHCb::RichPID();
      unpacked->insert( testObj, key );
      pPacker.unpack( ppid, *testObj, ppids, *unpacked );
      pPacker.check( *pid, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) pids->registry()->setAddress( nullptr );
}

//=========================================================================
// Pack a container of protoparticles
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::ProtoParticles* protos,
                                              LHCb::PackedProtoParticles& pprotos ) const {
  const LHCb::ProtoParticlePacker pPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::ProtoParticles() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( protos->version() );
    put( unpacked, "/Event/Transient/PsAndVsProtoParticleTest" );
  }

  // reserve size
  pprotos.data().reserve( pprotos.data().size() + protos->size() );

  // loop
  for ( const auto* proto : *protos ) {
    // Make a new packed data object and save
    auto& pproto = pprotos.data().emplace_back();

    // reference to original container and key
    pproto.key = StandardPacker::reference64( &pprotos, proto->parent(), proto->key() );

    // pack the physics info
    pPacker.pack( *proto, pproto, pprotos );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey32( pproto.key, linkID, key );
      LHCb::ProtoParticle* testObj = new LHCb::ProtoParticle();
      unpacked->insert( testObj, key );
      pPacker.unpack( pproto, *testObj, pprotos, *unpacked );
      pPacker.check( *proto, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) protos->registry()->setAddress( nullptr );
}

//=========================================================================
// Pack a container of tracks
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::Tracks* tracks, LHCb::PackedTracks& ptracks ) const {
  const LHCb::TrackPacker tPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::Tracks() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( tracks->version() );
    put( unpacked, "/Event/Transient/PsAndVsTrackTest" );
  }

  // reserve size
  ptracks.data().reserve( ptracks.data().size() + tracks->size() );

  // loop
  for ( const auto* track : *tracks ) {
    // Make a new packed data object and save
    auto& ptrack = ptracks.data().emplace_back();

    // reference to original container and key
    ptrack.key = StandardPacker::reference64( &ptracks, track->parent(), track->key() );

    // pack the physics info
    tPacker.pack( *track, ptrack, ptracks );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey32( ptrack.key, linkID, key );
      LHCb::Track* testObj = new LHCb::Track();
      unpacked->insert( testObj, key );
      tPacker.unpack( ptrack, *testObj, ptracks, *unpacked );
      tPacker.check( *track, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) tracks->registry()->setAddress( nullptr );
}

//=========================================================================
//  Pack a container of vertices in the PackedVertices object
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::Vertices* verts, LHCb::PackedVertices& pverts ) const {
  const LHCb::VertexPacker vPacker( this );

  // checks
  auto* unpacked = ( m_enableCheck ? new LHCb::Vertices() : nullptr );
  if ( unpacked ) {
    unpacked->setVersion( verts->version() );
    put( unpacked, "/Event/Transient/PsAndVsVertexTest" );
  }

  // reserve size
  pverts.data().reserve( pverts.data().size() + verts->size() );

  // loop
  for ( const auto* vert : *verts ) {
    // Make a new packed data object and save
    pverts.data().emplace_back();
    auto& pvert = pverts.data().back();

    // reference to original container and key
    pvert.key = StandardPacker::reference64( &pverts, vert->parent(), vert->key() );

    // fill remaining physics info
    vPacker.pack( *vert, pvert, pverts );

    // checks ?
    if ( unpacked ) {
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey64( pvert.key, linkID, key );
      LHCb::Vertex* testObj = new LHCb::Vertex();
      unpacked->insert( testObj, key );
      vPacker.unpack( pvert, *testObj, pverts, *unpacked );
      vPacker.check( *vert, *testObj ).ignore();
    }
  }

  // clean up test data
  if ( unpacked ) {
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  if ( !m_deleteInput ) verts->registry()->setAddress( nullptr );
}

//=========================================================================
//  Pack a container of RecVertex
//=========================================================================
void PackParticlesAndVertices::packContainer( const LHCb::RecVertices* rVerts,
                                              LHCb::PackedRecVertices& pRVerts ) const {
  const LHCb::RecVertexPacker rvPacker( this );

  // reserve size
  pRVerts.data().reserve( pRVerts.data().size() + rVerts->size() );

  // loop
  for ( const auto* rVert : *rVerts ) {
    // Make a new packed data object and save
    auto& pRVert = pRVerts.data().emplace_back();

    // reference to original container and key
    pRVert.key       = rVert->key();
    pRVert.container = (int)StandardPacker::linkID( &pRVerts, rVert->parent() );

    // Physics info
    rvPacker.pack( *rVert, pRVert, *rVerts, pRVerts );
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( !m_deleteInput ) { rVerts->registry()->setAddress( nullptr ); }
}

//=========================================================================
//  Pack a container of Related info
//=========================================================================

void PackParticlesAndVertices::packContainer( const PackParticlesAndVertices::Part2InfoRelations* rels,
                                              LHCb::PackedRelatedInfoRelations&                   prels,
                                              const std::string&                                  location ) const {
  // Make a entry in the containers vector, for this TES location
  prels.containers().emplace_back();
  auto& pcont = prels.containers().back();

  // reference to original container and key
  pcont.reference = StandardPacker::reference64( &prels, rels, 0 );

  // First entry in the relations vector
  pcont.first = prels.data().size();

  // Loop over the relations and fill
  prels.data().reserve( prels.data().size() + rels->relations().size() );

  // Use the packer to pack this location ...
  m_rInfoPacker.pack( *rels, prels );

  // last entry in the relations vector
  pcont.last = prels.data().size();

  // checks
  if ( m_enableCheck ) {
    // Make a temporary object
    auto* unpacked = new Part2InfoRelations();
    unpacked->setVersion( rels->version() );
    put( unpacked, "/Event/Transient/Part2RelatedInfoRelations" );

    // unpack
    m_rInfoPacker.unpack( prels, *unpacked, location );

    // check
    if ( !m_rInfoPacker.check( *rels, *unpacked ) ) { Warning( "Problem running packing checks" ).ignore(); }

    // remove temporary data
    const StatusCode sc = evtSvc()->unregisterObject( unpacked );
    if ( sc.isSuccess() ) {
      delete unpacked;
    } else {
      Exception( "Failed to delete test data after unpacking check" );
    }
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  if ( !m_deleteInput ) { rels->registry()->setAddress( nullptr ); }
}
