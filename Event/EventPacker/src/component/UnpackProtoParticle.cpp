/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Interfaces/IProtoParticleTool.h"

#include "DetDesc/DetectorElement.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

#include "Event/PackedProtoParticle.h"
#include "Event/ProtoParticle.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/FixTESPath.h"
#include "GaudiKernel/DataObjectHandle.h"

namespace LHCb {

  /**
   *  Unpack a protoparticle container
   *
   *  Note that the inheritance from Consumer is misleading. The algorithm is
   *  writing to TES, just via a Handle so that it can do it at the begining of
   *  the operator(), as cross pointers are used in the TES and requires this.
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct UnpackProtoParticle
      : Algorithm::Consumer<void( PackedProtoParticles const&, DetectorElement const& ),
                            DetDesc::usesBaseAndConditions<FixTESPath<Gaudi::Algorithm>, DetectorElement>> {

    UnpackProtoParticle( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {KeyValue{"InputName", PackedProtoParticleLocation::Charged},
                    KeyValue{"StandardGeometryTop", "/dd/Structure/LHCb"}}} {}
    void operator()( PackedProtoParticles const&, DetectorElement const& ) const override;

    DataObjectWriteHandle<ProtoParticles> m_protos{this, "OutputName", ProtoParticleLocation::Charged};

    ToolHandleArray<Rec::Interfaces::IProtoParticles> m_addInfo{this, "AddInfo", {}};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::UnpackProtoParticle, "UnpackProtoParticle" )

void LHCb::UnpackProtoParticle::operator()( LHCb::PackedProtoParticles const& dst, DetectorElement const& lhcb ) const {
  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Found " << dst.data().size() << " PackedProtoParticles at " << inputLocation<PackedProtoParticles>()
            << endmsg;

  // NOTE: the output container _must be on the TES_ prior to passing it to the
  //      unpacker, as otherwise filling the references to other objects does
  //      not work
  auto* newProtoParticles = m_protos.put( std::make_unique<ProtoParticles>() );

  // unpack
  ProtoParticlePacker{this}.unpack( dst, *newProtoParticles );

  if ( msgLevel( MSG::DEBUG ) )
    debug() << "Created " << newProtoParticles->size() << " ProtoParticles at " << m_protos.fullKey() << endmsg;

  for ( auto& addInfo : m_addInfo ) ( *addInfo )( *newProtoParticles, *lhcb.geometry() ).ignore();
}
