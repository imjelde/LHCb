/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedTwoProngVertex.h"
#include "Event/StandardPacker.h"
#include "Event/TwoProngVertex.h"

#include "LHCbAlgs/Producer.h"

namespace LHCb {

  /**
   *  Pack a two prong vertex.
   *
   *  Note that the inheritance from Producer is misleading. The algorithm is
   *  reading from TES, just via a Handle so that it can deal with non existant
   *  input, which is not authorized in the functional world
   *
   *  @author Olivier Callot
   *  @date   2009-01-21
   */
  struct PackTwoProngVertex : Algorithm::Producer<PackedTwoProngVertices()> {

    PackTwoProngVertex( std::string const& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, KeyValue{"OutputName", PackedTwoProngVertexLocation::Default} ) {}

    PackedTwoProngVertices operator()() const override;

    DataObjectReadHandle<TwoProngVertices> m_verts{this, "InputName", TwoProngVertexLocation::Default};

    Gaudi::Property<bool> m_alwaysOutput{this, "AlwaysCreateOutput", false,
                                         "Flag to turn on the creation of output, even when input is missing"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackTwoProngVertex, "PackTwoProngVertex" )

LHCb::PackedTwoProngVertices LHCb::PackTwoProngVertex::operator()() const {

  PackedTwoProngVertices out;
  if ( !m_alwaysOutput.value() && !m_verts.exist() ) return out;
  auto const* verts = m_verts.getOrCreate();

  out.data().reserve( verts->size() );
  out.setVersion( 1 );

  // packing version
  out.setPackingVersion( PackedTwoProngVertices::defaultPackingVersion() );

  for ( const auto* vert : *verts ) {
    // save the packed vertex
    auto& pVert = out.data().emplace_back( PackedTwoProngVertex() );

    if ( msgLevel( MSG::DEBUG ) ) debug() << "Found vertex key " << vert->key() << endmsg;

    pVert.key       = vert->key();
    pVert.technique = vert->technique();
    pVert.chi2      = StandardPacker::fltPacked( vert->chi2() );
    pVert.nDoF      = vert->nDoF();
    pVert.x         = StandardPacker::position( vert->position().x() );
    pVert.y         = StandardPacker::position( vert->position().y() );
    pVert.z         = StandardPacker::position( vert->position().z() );

    double pA = 1. / vert->momA()( 2 );
    double pB = 1. / vert->momB()( 2 );

    pVert.txA = StandardPacker::slope( vert->momA()( 0 ) );
    pVert.tyA = StandardPacker::slope( vert->momA()( 1 ) );
    pVert.pA  = StandardPacker::energy( pA );

    pVert.txB = StandardPacker::slope( vert->momB()( 0 ) );
    pVert.tyB = StandardPacker::slope( vert->momB()( 1 ) );
    pVert.pB  = StandardPacker::energy( pB );

    // convariance Matrix
    double err0 = std::sqrt( vert->covMatrix()( 0, 0 ) );
    double err1 = std::sqrt( vert->covMatrix()( 1, 1 ) );
    double err2 = std::sqrt( vert->covMatrix()( 2, 2 ) );
    double err3 = std::sqrt( vert->momcovA()( 0, 0 ) );
    double err4 = std::sqrt( vert->momcovA()( 1, 1 ) );
    double err5 = std::sqrt( vert->momcovA()( 2, 2 ) );
    double err6 = std::sqrt( vert->momcovB()( 0, 0 ) );
    double err7 = std::sqrt( vert->momcovB()( 1, 1 ) );
    double err8 = std::sqrt( vert->momcovB()( 2, 2 ) );

    pVert.cov00 = StandardPacker::position( err0 );
    pVert.cov11 = StandardPacker::position( err1 );
    pVert.cov22 = StandardPacker::position( err2 );
    pVert.cov33 = StandardPacker::slope( err3 );
    pVert.cov44 = StandardPacker::slope( err4 );
    pVert.cov55 = StandardPacker::energy( err5 * std::abs( pA ) * 1.e5 );
    pVert.cov66 = StandardPacker::slope( err6 );
    pVert.cov77 = StandardPacker::slope( err7 );
    pVert.cov88 = StandardPacker::energy( err8 * std::abs( pB ) * 1.e5 );

    pVert.cov10 = StandardPacker::fraction( vert->covMatrix()( 1, 0 ) / err1 / err0 );

    pVert.cov20 = StandardPacker::fraction( vert->covMatrix()( 2, 0 ) / err2 / err0 );
    pVert.cov21 = StandardPacker::fraction( vert->covMatrix()( 2, 1 ) / err2 / err1 );

    pVert.cov30 = StandardPacker::fraction( vert->momposcovA()( 0, 0 ) / err3 / err0 );
    pVert.cov31 = StandardPacker::fraction( vert->momposcovA()( 0, 1 ) / err3 / err1 );
    pVert.cov32 = StandardPacker::fraction( vert->momposcovA()( 0, 2 ) / err3 / err2 );

    pVert.cov40 = StandardPacker::fraction( vert->momposcovA()( 1, 0 ) / err4 / err0 );
    pVert.cov41 = StandardPacker::fraction( vert->momposcovA()( 1, 1 ) / err4 / err1 );
    pVert.cov42 = StandardPacker::fraction( vert->momposcovA()( 1, 2 ) / err4 / err2 );
    pVert.cov43 = StandardPacker::fraction( vert->momcovA()( 1, 0 ) / err4 / err3 );

    pVert.cov50 = StandardPacker::fraction( vert->momposcovA()( 2, 0 ) / err5 / err0 );
    pVert.cov51 = StandardPacker::fraction( vert->momposcovA()( 2, 1 ) / err5 / err1 );
    pVert.cov52 = StandardPacker::fraction( vert->momposcovA()( 2, 2 ) / err5 / err2 );
    pVert.cov53 = StandardPacker::fraction( vert->momcovA()( 2, 0 ) / err5 / err3 );
    pVert.cov54 = StandardPacker::fraction( vert->momcovA()( 2, 1 ) / err5 / err4 );

    pVert.cov60 = StandardPacker::fraction( vert->momposcovB()( 0, 0 ) / err6 / err0 );
    pVert.cov61 = StandardPacker::fraction( vert->momposcovB()( 0, 1 ) / err6 / err1 );
    pVert.cov62 = StandardPacker::fraction( vert->momposcovB()( 0, 2 ) / err6 / err2 );
    pVert.cov63 = StandardPacker::fraction( vert->mommomcov()( 0, 0 ) / err6 / err3 );
    pVert.cov64 = StandardPacker::fraction( vert->mommomcov()( 0, 1 ) / err6 / err4 );
    pVert.cov65 = StandardPacker::fraction( vert->mommomcov()( 0, 2 ) / err6 / err5 );

    pVert.cov70 = StandardPacker::fraction( vert->momposcovB()( 1, 0 ) / err7 / err0 );
    pVert.cov71 = StandardPacker::fraction( vert->momposcovB()( 1, 1 ) / err7 / err1 );
    pVert.cov72 = StandardPacker::fraction( vert->momposcovB()( 1, 2 ) / err7 / err2 );
    pVert.cov73 = StandardPacker::fraction( vert->mommomcov()( 1, 0 ) / err7 / err3 );
    pVert.cov74 = StandardPacker::fraction( vert->mommomcov()( 1, 1 ) / err7 / err4 );
    pVert.cov75 = StandardPacker::fraction( vert->mommomcov()( 1, 2 ) / err7 / err5 );
    pVert.cov76 = StandardPacker::fraction( vert->momcovB()( 1, 0 ) / err7 / err6 );

    pVert.cov80 = StandardPacker::fraction( vert->momposcovB()( 2, 0 ) / err8 / err0 );
    pVert.cov81 = StandardPacker::fraction( vert->momposcovB()( 2, 1 ) / err8 / err1 );
    pVert.cov82 = StandardPacker::fraction( vert->momposcovB()( 2, 2 ) / err8 / err2 );
    pVert.cov83 = StandardPacker::fraction( vert->mommomcov()( 2, 0 ) / err8 / err3 );
    pVert.cov84 = StandardPacker::fraction( vert->mommomcov()( 2, 1 ) / err8 / err4 );
    pVert.cov85 = StandardPacker::fraction( vert->mommomcov()( 2, 2 ) / err8 / err5 );
    pVert.cov86 = StandardPacker::fraction( vert->momcovB()( 2, 0 ) / err8 / err6 );
    pVert.cov87 = StandardPacker::fraction( vert->momcovB()( 2, 1 ) / err8 / err7 );

    //== Store the Tracks
    pVert.firstTrack = out.refs().size();
    for ( auto itT = vert->tracks().begin(); vert->tracks().end() != itT; ++itT ) {
      out.refs().push_back( StandardPacker::reference64( &out, ( *itT )->parent(), ( *itT )->key() ) );
    }
    pVert.lastTrack = out.refs().size();

    //== Store the ParticleID
    pVert.firstPid = out.refs().size();
    for ( auto itP = vert->compatiblePIDs().begin(); vert->compatiblePIDs().end() != itP; ++itP ) {
      out.refs().emplace_back( ( *itP ).pid() );
    }
    pVert.lastPid = out.refs().size();

    //== Handles the ExtraInfo
    pVert.firstInfo = out.extras().size();
    for ( const auto& itE : vert->extraInfo() ) {
      out.extras().emplace_back( itE.first, StandardPacker::fltPacked( itE.second ) );
    }
    pVert.lastInfo = out.extras().size();
  }

  return out;
}
