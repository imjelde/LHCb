/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/StandardPacker.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"
#include "MapperToolBase.h"
#include <algorithm>
#include <map>
#include <string>

/** @class ParticlesAndVerticesMapper ParticlesAndVerticesMapper.h
 *
 *  Tool for automatic conversions in the transient store.
 *
 *  Bespoke mapping tool for the UnpackParticlesAndVertices unpacker
 *
 * @author Chris Jones
 * @date 06/02/2012
 */
class ParticlesAndVerticesMapper : public extends<MapperToolBase, IIncidentListener> {

public:
  /// Standard constructor
  using extends::extends;

  /// Initialize the tool instance.
  StatusCode initialize() override;

public:
  /** Implement the handle method for the Incident service.
   *  This is used to inform the tool of software incidents.
   *
   *  @param incident The incident identifier
   */
  void handle( const Incident& incident ) override;

public:
  /** Returns the correctly configured and name instance of the
   *  Particles and Vertices unpacker, for the given path
   */
  Gaudi::Utils::TypeNameString algorithmForPath( const std::string& path ) override;

public:
  /** Instruct the DataOnDemandSvc to create DataObjects for the
   *  intermediate levels of a path we can handle.
   */
  std::string nodeTypeForPath( const std::string& path ) override;

private:
  template <typename Packed>
  void load( const std::string& location );

  /// Load the packed data and update the mappings of paths to Node Type
  void updateNodeTypeMap( const std::string& path );

  /// Add a path to the node type mappings
  void addPath( const std::string& path );

  /// Check if a given path is in the list of data locations created
  bool pathIsHandled( const std::string& path ) const {
    // See if we have an entry for this path
    auto it = m_nodeTypeMap.find( fixPath( path ) );
    return ( it != m_nodeTypeMap.end() );
  }

private:
  /// Mapping between TES path and node type
  typedef std::map<std::string, std::string> NodeTypeMap;
  NodeTypeMap                                m_nodeTypeMap;

  /// Map to say which stream roots have been processed each event
  std::map<std::string, bool> m_streamsDone;

  /// Unpacker class type
  Gaudi::Property<std::string> m_unpackerType{this, "UnpackerType", "UnpackParticlesAndVertices"};

  /// Outputlevel for unpackers created
  Gaudi::Property<int> m_unpackersOutputLevel{this, "UnpackerOutputLevel", -1};

  // List of streams we should ignore paths under, e.g. `['/Event/Something']`
  Gaudi::Property<std::vector<std::string>> m_vetoStreams{this, "VetoStreams", {}};
};

// ============================================================================

DECLARE_COMPONENT( ParticlesAndVerticesMapper )

// ============================================================================
// Initialize
// ============================================================================
StatusCode ParticlesAndVerticesMapper::initialize() {
  return MapperToolBase::initialize().andThen( [&] {
    // Incident service
    incSvc()->addListener( this, IncidentType::BeginEvent );
  } );
}

// ============================================================================

void ParticlesAndVerticesMapper::handle( const Incident& ) {
  // Only one incident type registered, so do not bother to check type
  m_streamsDone.clear();
}

// ============================================================================

Gaudi::Utils::TypeNameString ParticlesAndVerticesMapper::algorithmForPath( const std::string& path ) {
  LOG_VERBOSE << "ParticlesAndVerticesMapper::algorithmForPath '" << path << "'" << endmsg;

  updateNodeTypeMap( path );

  // Is this path in the list of output locations this packer can create
  if ( pathIsHandled( path ) ) {
    // Choose a unique name for the algorithm instance
    const std::string algName = streamName( path ) + "_PsAndVsUnpack";

    // Add the configuration of algorithm instance to the JobOptionsSvc
    setProp( algName, "InputStream", streamRoot( path ) );
    if ( m_unpackersOutputLevel > 0 ) { setProp( algName, "OutputLevel", m_unpackersOutputLevel ); }

    // Return the algorithm type/name.
    LOG_VERBOSE << " -> Use algorithm type '" << m_unpackerType << "'"
                << " name '" << algName << "'" << endmsg;
    return Gaudi::Utils::TypeNameString( algName, m_unpackerType );
  }

  return "";
}

// ============================================================================
template <typename Packed>
void ParticlesAndVerticesMapper::load( const std::string& location ) {

  // TODO: change Packed classes to have uniform method/member names...
  //      until then: add some lambdas to adapt...

  auto getData = []( const auto& obj ) -> decltype( auto ) {
    if constexpr ( std::is_same_v<Packed, LHCb::PackedRelatedInfoRelations> ) {
      return obj.containers();
    } else {
      return obj.data();
    }
  };

  auto getLink = []( const auto& obj ) {
    if constexpr ( std::is_same_v<Packed, LHCb::PackedRecVertices> ) {
      return obj.container;
    } else {
      auto getKey = []( const auto& obj ) -> decltype( auto ) {
        if constexpr ( std::is_same_v<Packed, LHCb::PackedRelations> ||
                       std::is_same_v<Packed, LHCb::PackedWeightedRelations> ) {
          return obj.container;
        } else if constexpr ( std::is_same_v<Packed, LHCb::PackedRelatedInfoRelations> ) {
          return obj.reference;
        } else {
          return obj.key;
        }
      };
      int key( 0 ), linkID( 0 );
      StandardPacker::indexAndKey64( getKey( obj ), linkID, key );
      return linkID;
    }
  };

  if ( const auto* packed = getIfExists<Packed>( evtSvc(), location ); packed ) {
    for ( const auto& P : getData( *packed ) ) { addPath( packed->linkMgr()->link( getLink( P ) )->path() ); }
  }
}

// ============================================================================

std::string ParticlesAndVerticesMapper::nodeTypeForPath( const std::string& path ) {
  updateNodeTypeMap( path );

  const auto it   = m_nodeTypeMap.find( fixPath( path ) );
  auto       retS = ( it != m_nodeTypeMap.end() ? it->second : std::string{} );

  LOG_VERBOSE << "ParticlesAndVerticesMapper::nodeTypeForPath '" << path << "' NodeType '" << retS << "'" << endmsg;

  return retS;
}

// ============================================================================

void ParticlesAndVerticesMapper::updateNodeTypeMap( const std::string& path ) {
  // The stream TES root
  const std::string streamR = streamRoot( path );

  if ( !m_streamsDone[streamR] &&
       std::find( m_vetoStreams.begin(), m_vetoStreams.end(), streamR ) == m_vetoStreams.end() ) {
    m_streamsDone[streamR] = true;
    LOG_VERBOSE << "ParticlesAndVerticesMapper::updateNodeTypeMap '" << path << "'" << endmsg;

    load<LHCb::PackedTracks>( streamR + LHCb::PackedTrackLocation::InStream );
    load<LHCb::PackedMuonPIDs>( streamR + LHCb::PackedMuonPIDLocation::InStream );
    load<LHCb::PackedRichPIDs>( streamR + LHCb::PackedRichPIDLocation::InStream );
    load<LHCb::PackedProtoParticles>( streamR + LHCb::PackedProtoParticleLocation::InStream );
    load<LHCb::PackedParticles>( streamR + LHCb::PackedParticleLocation::InStream );
    load<LHCb::PackedVertices>( streamR + LHCb::PackedVertexLocation::InStream );
    load<LHCb::PackedFlavourTags>( streamR + LHCb::PackedFlavourTagLocation::InStream );
    // Load the Rec vertices
    load<LHCb::PackedRecVertices>( streamR + LHCb::PackedRecVertexLocation::InStream );
    // Particle 2 Vertex relations
    load<LHCb::PackedRelations>( streamR + LHCb::PackedRelationsLocation::InStream );
    // Particle 2 MCParticle relations
    load<LHCb::PackedRelations>( streamR + LHCb::PackedRelationsLocation::P2MCP );
    // ProtoParticle 2 MCParticle weighted relations
    load<LHCb::PackedWeightedRelations>( streamR + LHCb::PackedWeightedRelationsLocation::PP2MCP );
    // Particle 2 Int relations
    load<LHCb::PackedRelations>( streamR + LHCb::PackedRelationsLocation::P2Int );
    // Load the packed RelatedInfo relations
    load<LHCb::PackedRelatedInfoRelations>( streamR + LHCb::PackedRelatedInfoLocation::InStream );
  }
}

// ============================================================================

void ParticlesAndVerticesMapper::addPath( const std::string& path ) {
  // Make sure paths start with /Event/
  const auto npath = fixPath( path );

  // if not already there, add.
  if ( m_nodeTypeMap.find( npath ) == m_nodeTypeMap.end() ) {
    LOG_VERBOSE << " -> Path " << npath << endmsg;

    // Main path
    m_nodeTypeMap[npath] = "";

    // Data Node paths ...
    auto tmp   = npath;
    auto slash = tmp.find_last_of( "/" );
    while ( !tmp.empty() && slash != std::string::npos ) {
      tmp = tmp.substr( 0, slash );
      if ( !tmp.empty() ) {
        LOG_VERBOSE << "  -> Node " << tmp << endmsg;
        m_nodeTypeMap[tmp] = "DataObject";
      }
      slash = tmp.find_last_of( "/" );
    }
  }
}

// ============================================================================
