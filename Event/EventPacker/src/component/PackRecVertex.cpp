/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedRecVertex.h"
#include "Event/RecVertex.h"
#include "Event/StandardPacker.h"

#include "LHCbAlgs/Producer.h"

namespace LHCb {

  /**
   *  Pack the RecVertex
   *
   *  Note that the inheritance from Producer is misleading. The algorithm is
   *  reading from TES, just via a Handle so that it can deal with non existant
   *  input, which is not authorized in the functional world
   *
   *  @author Olivier Callot
   *  @date   2008-11-14
   */
  struct PackRecVertex : Algorithm::Producer<PackedRecVertices()> {

    PackRecVertex( std::string const& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, KeyValue{"OutputName", PackedRecVertexLocation::Primary} ) {}

    PackedRecVertices operator()() const override;

    DataObjectReadHandle<RecVertices> m_verts{this, "InputName", RecVertexLocation::Primary};

    Gaudi::Property<unsigned int> m_version{this, "Version", 2, "Version schema number"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackRecVertex, "PackRecVertex" )

LHCb::PackedRecVertices LHCb::PackRecVertex::operator()() const {

  PackedRecVertices out;
  out.setPackingVersion( PackedRecVertices::defaultPackingVersion() );
  out.setVersion( (unsigned char)m_version );

  if ( !m_verts.exist() ) return out;
  auto const* verts = m_verts.get();

  out.data().reserve( verts->size() );
  const RecVertexPacker rvPacker( this );
  for ( const auto* vert : *verts ) {
    // Make and save a new packed object
    auto& pVert = out.data().emplace_back();
    pVert.key   = vert->key();
    // Physics info
    rvPacker.pack( *vert, pVert, *verts, out );
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  auto* pReg = verts->registry();
  if ( pReg ) pReg->setAddress( nullptr );

  return out;
}
