/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/HltDecReports.h"
#include "Event/PackedDecReport.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/LinkManager.h"
#include "LHCbAlgs/Transformer.h"

namespace LHCb {

  /**
   *  Packs DecReports
   *
   *  @author Olivier Callot
   *  @date   2012-01-20
   */
  struct PackDecReport : Algorithm::Transformer<PackedDecReport( HltDecReports const& )> {

    PackDecReport( std::string const& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator, KeyValue{"InputName", ""},
                       KeyValue{"OutputName", PackedDecReportLocation::Default} ) {}

    PackedDecReport operator()( HltDecReports const& ) const override;

    Gaudi::Property<bool> m_filter{this, "Filter", true};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackDecReport, "PackDecReport" )

LHCb::PackedDecReport LHCb::PackDecReport::operator()( LHCb::HltDecReports const& reports ) const {
  PackedDecReport out;
  out.setConfiguredTCK( reports.configuredTCK() );
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "TCK = " << out.configuredTCK() << endmsg; }

  // loop and pack
  for ( const auto& tR : reports.decReports() ) {
    // Get the report
    HltDecReport tmp = tR.second.decReport();

    // If configured to do so, filter out null entries
    if ( m_filter.value() && tmp.decision() == 0 ) continue;

    // store the result
    LinkManager::Link* myLink = out.linkMgr()->link( tR.first );
    if ( !myLink ) {
      out.linkMgr()->addLink( tR.first, nullptr );
      myLink = out.linkMgr()->link( tR.first );
    }
    tmp.setIntDecisionID( myLink->ID() + 1 ); // Store numbers starting at 1 as HltDecReport dislike 0!
    out.reports().emplace_back( tmp.decReport() );
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << format( "Stored report %8.8x  link ID %3d", tmp.decReport(), myLink->ID() ) << " name " << tR.first
              << endmsg;
    }
  }

  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "from " << reports.size() << " reports, stored " << out.reports().size() << " entries." << endmsg;
  }

  // Clear the registry address of the unpacked container, to prevent reloading
  auto* pReg = reports.registry();
  if ( pReg ) pReg->setAddress( nullptr );

  return out;
}
