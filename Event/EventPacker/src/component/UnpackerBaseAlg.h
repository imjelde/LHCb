/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "LHCbAlgs/Consumer.h"

namespace DataPacking {

  /**
   *  Templated base algorithm for all unpacking algorithms
   *
   *  Note that the inheritance from Consumer and the void input are misleading.
   *  The algorithm is reading from and writing to TES, just via Handles so that
   *  it can deal with non existant input and create the output at the begining
   *  of the operator() to support cross links in the TES. Both features are not
   *  authorized in the functional world.
   *  FIXME this should not be necessary, but some other code needs fixing first
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-14
   */

  template <class PACKER>
  class Unpack : public LHCb::Algorithm::Consumer<void()> {

  public:
    using Consumer::Consumer;
    StatusCode initialize() override;
    void       operator()() const override;

  private:
    DataObjectReadHandle<typename PACKER::PackedDataVector> m_pdata{this, "InputName", PACKER::packedLocation()};
    DataObjectWriteHandle<typename PACKER::DataVector>      m_data{this, "OutputName", PACKER::unpackedLocation()};
    mutable Gaudi::Accumulators::StatCounter<>              m_unpackedData{this, "# UnPackedData"};
    const PACKER                                            m_packer{this};
  };

  template <class PACKER>
  StatusCode Unpack<PACKER>::initialize() {
    return Consumer::initialize().andThen( [&] {
      if ( this->msgLevel( MSG::DEBUG ) )
        this->debug() << "Input " << m_pdata.fullKey() << " Output " << m_data.fullKey() << "" << endmsg;
    } );
  }

  template <class PACKER>
  void Unpack<PACKER>::operator()() const {
    // create empty output
    auto* data = m_data.put( std::make_unique<typename PACKER::DataVector>() );

    // deal with missing input
    if ( !m_pdata.exist() ) return;
    auto const* pdata = m_pdata.get();

    data->setVersion( pdata->version() );
    // Fill unpacked data
    m_packer.unpack( *pdata, *data );
    if ( this->msgLevel( MSG::DEBUG ) ) {
      this->debug() << "Created " << data->size() << " data objects at " << m_data.fullKey() << endmsg;
      this->debug() << " Packed Data Version    = " << (unsigned int)pdata->version() << endmsg;
      this->debug() << " Packed Packing Version = " << (unsigned int)pdata->packingVersion() << endmsg;
      this->debug() << " Unpacked Data Version  = " << (unsigned int)data->version() << endmsg;
    }
    // Count packed output
    m_unpackedData += data->size();
  }

} // namespace DataPacking
