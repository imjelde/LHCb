/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "UnpackerBaseAlg.h"

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichHitPacker>, "MCRichHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichSegmentPacker>, "MCRichSegmentUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichTrackPacker>, "MCRichTrackUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPrsHitPacker>, "MCPrsHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCSpdHitPacker>, "MCSpdHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCEcalHitPacker>, "MCEcalHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCHcalHitPacker>, "MCHcalHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCVeloHitPacker>, "MCVeloHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCVPHitPacker>, "MCVPHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCTTHitPacker>, "MCTTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCUTHitPacker>, "MCUTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCITHitPacker>, "MCITHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCOTHitPacker>, "MCOTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCMuonHitPacker>, "MCMuonHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::RichPIDPacker>, "RichPIDUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MuonPIDPacker>, "MuonPIDUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::ParticlePacker>, "ParticleUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::VertexPacker>, "VertexUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::WeightsVectorPacker>, "WeightsVectorUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::CaloClusterPacker>, "CaloClusterUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::CaloDigitPacker>, "CaloDigitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::CaloAdcPacker>, "CaloAdcUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCFTHitPacker>, "MCFTHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCSLHitPacker>, "MCSLHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::FlavourTagPacker>, "FlavourTagUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCHCHitPacker>, "MCHCHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCBcmHitPacker>, "MCBcmHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCBlsHitPacker>, "MCBlsHitUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::RelatedInfoRelationsPacker>, "RelatedInfoRelationsUnpacker" )
DECLARE_COMPONENT_WITH_ID( DataPacking::Unpack<LHCb::MCPlumeHitPacker>, "MCPlumeHitUnpacker" )
