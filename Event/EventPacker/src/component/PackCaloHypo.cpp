/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/PackedCaloHypo.h"

#include "LHCbAlgs/Producer.h"

namespace LHCb {

  /**
   * Packer for CaloHypo
   *
   *  Note that the inheritance from Producer is misleading. The algorithm is
   *  reading from TES, just via a Handle so that it can deal with non existant
   *  input, which is not authorized in the functional world
   */
  struct PackCaloHypo : Algorithm::Producer<PackedCaloHypos()> {

    PackCaloHypo( const std::string& name, ISvcLocator* pSvcLocator )
        : Producer( name, pSvcLocator, KeyValue{"OutputName", PackedCaloHypoLocation::Electrons} ) {}

    PackedCaloHypos operator()() const override;

    DataObjectReadHandle<CaloHypos> m_hypos{this, "InputName", CaloHypoLocation::Electrons};

    Gaudi::Property<bool> m_enableCheck{this, "EnableCheck", false,
                                        "Flag to turn on automatic unpacking and checking of the output post-packing"};
  };

} // namespace LHCb

DECLARE_COMPONENT_WITH_ID( LHCb::PackCaloHypo, "PackCaloHypo" )

LHCb::PackedCaloHypos LHCb::PackCaloHypo::operator()() const {

  // create ouput
  PackedCaloHypos out;
  out.setVersion( 1 );
  out.setPackingVersion( PackedCaloHypos::defaultPackingVersion() );

  if ( !m_hypos.exist() ) return out;
  auto const* hypos = m_hypos.get();

  // pack
  const CaloHypoPacker packer( this );
  packer.pack( *hypos, out );

  // Packing checks
  if ( m_enableCheck ) {
    // make new unpacked output data object
    CaloHypos unpacked;
    // unpack
    packer.unpack( out, unpacked );
    // run checks
    packer.check( *hypos, unpacked ).ignore();
  }

  return out;
}
