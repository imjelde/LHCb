/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Consumer.h"

#include "GaudiKernel/GaudiException.h"

namespace LHCb::DataPacking {

  /**
   *  Templated base algorithm for checking data packing
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-14
   */

  template <typename PACKER>
  using Data = typename PACKER::DataVector;

  template <typename PACKER>
  using Consumer = Algorithm::Consumer<void( Data<PACKER> const&, Data<PACKER> const& )>;

  template <class PACKER>
  class Check : public Consumer<PACKER> {
    using KeyValue = typename Consumer<PACKER>::KeyValue;

  public:
    /// Standard constructor
    Check( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer<PACKER>{name,
                           pSvcLocator,
                           {KeyValue{"DataA", PACKER::unpackedLocation()},
                            KeyValue{"DataB", PACKER::unpackedLocation() + "Test"}}} {}

    void operator()( const Data<PACKER>& dataA, const Data<PACKER>& dataB ) const override {

      // Compare versions
      if ( dataA.version() != dataB.version() ) {
        throw GaudiException( "Containers '" + this->template inputLocation<0>() + "' and '" +
                                  this->template inputLocation<1>() + "' have different versions",
                              "Check", StatusCode::FAILURE );
      }

      // check data sizes
      if ( dataA.size() != dataB.size() ) {
        throw GaudiException( "Size of containers '" + this->template inputLocation<0>() + "' and '" +
                                  this->template inputLocation<1>() + "' differs",
                              "Check", StatusCode::FAILURE );
      }

      // compare the containers
      // For the moment just carry on if an error is returned (with a message)
      m_packer.check( dataA, dataB ).ignore();
    }

  private:
    const PACKER m_packer{this}; ///< Packer
  };

} // namespace LHCb::DataPacking

#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMCCaloHit.h"
#include "Event/PackedMCHit.h"
#include "Event/PackedMCRichDigitSummary.h"
#include "Event/PackedMCRichHit.h"
#include "Event/PackedMCRichOpticalPhoton.h"
#include "Event/PackedMCRichSegment.h"
#include "Event/PackedMCRichTrack.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedVertex.h"
#include "Event/PackedWeightsVector.h"

namespace LHCb::DataPacking {

  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCRichHitPacker>, "MCRichHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCRichOpticalPhotonPacker>, "MCRichOpticalPhotonChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCRichSegmentPacker>, "MCRichSegmentChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCRichTrackPacker>, "MCRichTrackChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCPrsHitPacker>, "MCPrsHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCSpdHitPacker>, "MCSpdHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCEcalHitPacker>, "MCEcalHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCHcalHitPacker>, "MCHcalHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCVeloHitPacker>, "MCVeloHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCPuVetoHitPacker>, "MCPuVetoHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCVPHitPacker>, "MCVPHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCTTHitPacker>, "MCTTHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCUTHitPacker>, "MCUTHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCITHitPacker>, "MCITHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCOTHitPacker>, "MCOTHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCMuonHitPacker>, "MCMuonHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCRichDigitSummaryPacker>, "MCRichDigitSummaryChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::RichPIDPacker>, "RichPIDChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MuonPIDPacker>, "MuonPIDPChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::ParticlePacker>, "ParticleChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::VertexPacker>, "VertexPChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::WeightsVectorPacker>, "WeightsVectorChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::CaloClusterPacker>, "CaloClusterChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::CaloDigitPacker>, "CaloDigitPChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::CaloAdcPacker>, "CaloAdcChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCFTHitPacker>, "MCFTHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCSLHitPacker>, "MCSLHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::FlavourTagPacker>, "FlavourTagChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCHCHitPacker>, "MCHCHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCBcmHitPacker>, "MCBcmHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::MCBlsHitPacker>, "MCBlsHitChecker" )
  DECLARE_COMPONENT_WITH_ID( Check<LHCb::RelatedInfoRelationsPacker>, "RelatedInfoRelationsChecker" )

} // namespace LHCb::DataPacking
