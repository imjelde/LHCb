/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/FlavourTag.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

#include <string>

namespace LHCb {

  /**
   *  Packed FlavourTag
   *
   *  @author Christopher Rob Jones
   *  @date   2013-05-03
   */
  struct PackedFlavourTag {
    long long key{0}; ///< reference to the original container + key

    short int decision{0}; ///< The result of the tagging algorithm
    short int omega{0};    ///< Wrong tag fraction (predicted)

    short int decisionOS{0}; ///< decision of opposite side taggers only
    short int omegaOS{0};    ///< Wrong tag fraction (predicted) using opposite side only

    long long taggedB{-1}; ///< The B for which this tag has been made

    // Taggers
    unsigned int firstTagger{0}, lastTagger{0};

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  /**
   *  Packed Flavour Tagger
   *
   *  @author Christopher Rob Jones
   *  @date   2013-05-03
   */
  struct PackedTagger {
    unsigned short int type{0};                   ///< The type of tagger
    short int          decision{0};               ///< Decision of tagger
    short int          omega{0};                  ///< Wrong tag fraction of tagger
    unsigned int       firstTagP{0}, lastTagP{0}; ///< Tagging particles
    int                mvaValue{0};               ///< MVA used for classification
    short int          charge{0};                 ///< Charge used for classification

#ifndef __CLING__
    template <typename T>
    void save( T& buf ) const {
      Packer::io( buf, *this );
    }
    template <typename T>
    void load( T& buf, unsigned int version ) {
      switch ( version ) {
      case 0:
        buf.io( type, decision, omega, firstTagP, lastTagP );
        break;
      default:
        Packer::io( buf, *this );
        break;
      }
    }
#endif
  };

  constexpr CLID CLID_PackedFlavourTags = 1583;

  /// Namespace for locations in TDS
  namespace PackedFlavourTagLocation {
    inline const std::string Default  = "pPhys/FlavourTags";
    inline const std::string InStream = "/pPhys/FlavourTags";
  } // namespace PackedFlavourTagLocation

  /**
   *  Packed FlavourTags
   *
   *  @author Christopher Rob Jones
   *  @date   2013-05-03
   */
  class PackedFlavourTags : public DataObject {

  public:
    /// Vector of packed Flavour Tags
    typedef std::vector<LHCb::PackedFlavourTag> Vector;

    /// Vector of packed Taggers
    typedef std::vector<LHCb::PackedTagger> Taggers;

    /// Tagging Particles
    typedef std::vector<long long> TaggingParticles;

    /// Default Packing Version
    [[nodiscard]] static char defaultPackingVersion() { return 1; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedFlavourTags; }

    /// Class ID
    const CLID& clID() const override { return PackedFlavourTags::classID(); }

    /// Write access to the data vector
    [[nodiscard]] Vector& data() { return m_vect; }

    /// Read access to the data vector
    [[nodiscard]] const Vector& data() const { return m_vect; }

    /// Set the packing version
    PackedFlavourTags& setPackingVersion( const char ver ) {
      m_packingVersion = ver;
      return *this;
    }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Const access to taggers
    [[nodiscard]] const Taggers& taggers() const { return m_taggers; }
    /// Access to taggers
    [[nodiscard]] Taggers& taggers() { return m_taggers; }

    /// Const access to tagging Particles
    [[nodiscard]] const TaggingParticles& taggeringPs() const { return m_taggingPs; }
    /// Access to taggers
    [[nodiscard]] TaggingParticles& taggeringPs() { return m_taggingPs; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_taggers );
      buf.save( m_taggingPs );
    }
    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      setPackingVersion( buf.template load<uint8_t>() );
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedFlavourTags packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_taggers, m_packingVersion );
      buf.load( m_taggingPs );
    }

  private:
    /// Data packing version (not used as yet, but for any future schema evolution)
    char m_packingVersion{defaultPackingVersion()};

    /// The packed flavour tags
    Vector m_vect;

    /// Packed Taggers
    Taggers m_taggers;

    /// Tagging Particles
    TaggingParticles m_taggingPs;
  };

  /**
   *  Utility class to handle the packing and unpacking of the FlavourTags
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class FlavourTagPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::FlavourTag        Data;
    typedef LHCb::PackedFlavourTag  PackedData;
    typedef LHCb::FlavourTags       DataVector;
    typedef LHCb::PackedFlavourTags PackedDataVector;
    static const std::string&       packedLocation() { return LHCb::PackedFlavourTagLocation::Default; }
    static const std::string&       unpackedLocation() { return LHCb::FlavourTagLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack a single FlavourTag
    void pack( const Data& ft, PackedData& pft, PackedDataVector& pfts ) const;

    /// Pack FlavourTags
    void pack( const DataVector& fts, PackedDataVector& pfts ) const;

    /// Unpack a single FlavourTag
    void unpack( const PackedData& pft, Data& ft, const PackedDataVector& pfts, DataVector& fts ) const;

    /// Unpack FlavourTags
    void unpack( const PackedDataVector& pfts, DataVector& fts ) const;

    /// Compare two FlavourTag vectors to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

    /// Compare two FlavourTags to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Check if the given packing version is supported
    [[nodiscard]] bool isSupportedVer( const char& ver ) const {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "FlavourTagPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
