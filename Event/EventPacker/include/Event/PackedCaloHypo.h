/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/CaloHypo.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/GaudiException.h"
#include "GaudiKernel/StatusCode.h"

#include <string>
#include <vector>

namespace LHCb {

  /**
   *  Packed description of a CaloHypo
   *
   *  @author Olivier Callot
   *  @date   2008-11-10
   */
  struct PackedCaloHypo {
    int key{0};
    int hypothesis{0};
    int lh{0};
    // from CaloPosition
    int z{0};
    // position (3) + 3x3 symmetric covariance matrix
    int posX{0}, posY{0}, posE{0};

    int       cov00{0}, cov11{0}, cov22{0};
    short int cov10{0}, cov20{0}, cov21{0};
    short int cerr10{0}; // non diagonal terms of the x,y spread matrix.
    // center in x,y + 2x2 symetric covariance matrix
    int centX{0};
    int centY{0};
    int cerr00{0};
    int cerr11{0};

    unsigned short int firstDigit{0};
    unsigned short int lastDigit{0};
    unsigned short int firstCluster{0};
    unsigned short int lastCluster{0};
    unsigned short int firstHypo{0};
    unsigned short int lastHypo{0};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int /*version*/ ) {
      Packer::io( buf, *this ); // identical operation until version is incremented
    }
#endif
  };

  constexpr CLID CLID_PackedCaloHypos = 1551;

  // Namespace for locations in TDS
  namespace PackedCaloHypoLocation {
    inline const std::string Photons      = "pRec/Calo/Photons";
    inline const std::string Electrons    = "pRec/Calo/Electrons";
    inline const std::string MergedPi0s   = "pRec/Calo/MergedPi0s";
    inline const std::string SplitPhotons = "pRec/Calo/SplitPhotons";
  } // namespace PackedCaloHypoLocation

  /**
   *  Vector of packed CaloHypos
   *
   *  @author Olivier Callot
   *  @date   2008-11-10
   */

  class PackedCaloHypos : public DataObject {

  public:
    /// Default Packing Version
    static char defaultPackingVersion() { return 1; }

    /// Standard constructor
    PackedCaloHypos() {
      m_vect.reserve( 100 );
      m_refs.reserve( 1000 );
    }

    const CLID&        clID() const override { return PackedCaloHypos::classID(); }
    static const CLID& classID() { return CLID_PackedCaloHypos; }

    std::vector<PackedCaloHypo>&       data() { return m_vect; }
    const std::vector<PackedCaloHypo>& data() const { return m_vect; }

    std::vector<long long>&       refs() { return m_refs; }
    const std::vector<long long>& refs() const { return m_refs; }

    /// Set the packing version
    PackedCaloHypos& setPackingVersion( const char ver ) {
      m_packingVersion = ver;
      return *this;
    }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
      buf.save( m_refs );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      setPackingVersion( buf.template load<uint8_t>() );
      setVersion( buf.template load<uint8_t>() );
      if ( m_packingVersion < 1 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedCaloHypos packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
      buf.load( m_refs );
    }

  private:
    std::vector<PackedCaloHypo> m_vect;
    std::vector<long long>      m_refs;

    /// Data packing version
    char m_packingVersion{0};
  };

  /**
   *  Utility class to handle the packing and unpacking of CaloHypos
   *
   *  @author Christopher Rob Jones
   *  @date   05/04/2012
   */
  class CaloHypoPacker : public PackerBase {

  public:
    typedef LHCb::CaloHypo        Data;
    typedef LHCb::PackedCaloHypo  PackedData;
    typedef LHCb::CaloHypos       DataVector;
    typedef LHCb::PackedCaloHypos PackedDataVector;

    using PackerBase::PackerBase;

    /// Pack CaloHypos
    void pack( const DataVector& hypos, PackedDataVector& phypos ) const;

    /// Unpack CaloHypos
    void unpack( const PackedDataVector& phypos, DataVector& hypos ) const;

    /// Compare two CaloHypos to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

  private:
    /// Safe sqrt ...
    [[nodiscard]] double safe_sqrt( const double x ) const { return ( x > 0 ? std::sqrt( x ) : 0.0 ); }

    /// Check if the given packing version is supported
    [[nodiscard]] bool isSupportedVer( const char& ver ) const {
      const bool OK = ( 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( "Unknown packed data version " + std::to_string( (int)ver ), "CaloHypoPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
