/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Event/MuonPID.h"
#include "Event/PackerBase.h"
#include "Event/StandardPacker.h"

#include "GaudiKernel/DataObject.h"
#include "GaudiKernel/StatusCode.h"

#include "fmt/format.h"

namespace LHCb {

  /**
   *  Packed MuonPID
   *
   *  Version = 3, adds new variables to the muonPID: chi2 of correlated hits + MVA methods
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  struct PackedMuonPID {
    int       MuonLLMu{0};
    int       MuonLLBg{0};
    int       nShared{0};
    int       status{0};
    long long idtrack{-1};
    long long mutrack{-1};
    long long key{-1};
    int       chi2Corr{0};
    int       muonMVA1{0};
    int       muonMVA2{0};
    int       muonMVA3{0};
    int       muonMVA4{0};

#ifndef __CLING__
    template <typename Buf>
    void save( Buf& buf ) const {
      Packer::io( buf, *this );
    }

    template <typename Buf>
    void load( Buf& buf, unsigned int version ) {
      if ( version == 2 ) {
        buf.io( MuonLLMu, MuonLLBg, nShared, status, idtrack, mutrack, key );
      } else {
        Packer::io( buf, *this ); // identical operation for the latest version
      }
    }
#endif
  };

  constexpr CLID CLID_PackedMuonPIDs = 1571;

  /// Namespace for locations in TDS
  namespace PackedMuonPIDLocation {
    inline const std::string Default  = "pRec/Muon/MuonPID";
    inline const std::string InStream = "/pRec/Muon/CustomPIDs";
  } // namespace PackedMuonPIDLocation

  /**
   *  Packed MuonPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class PackedMuonPIDs : public DataObject {

  public:
    /// Vector of packed objects
    typedef std::vector<LHCb::PackedMuonPID> Vector;

    /// Default Packing Version
    static char defaultPackingVersion() { return 3; }

    /// Class ID
    static const CLID& classID() { return CLID_PackedMuonPIDs; }

    /// Class ID
    const CLID& clID() const override { return PackedMuonPIDs::classID(); }

    /// Write access to the data vector
    Vector& data() { return m_vect; }

    /// Read access to the data vector
    const Vector& data() const { return m_vect; }

    /// Set the packing version
    void setPackingVersion( const char ver ) { m_packingVersion = ver; }

    /// Access the packing version
    [[nodiscard]] char packingVersion() const { return m_packingVersion; }

    /// Describe serialization of object
    template <typename T>
    void save( T& buf ) const {
      buf.save( static_cast<uint8_t>( m_packingVersion ) );
      buf.save( static_cast<uint8_t>( version() ) );
      buf.save( m_vect );
    }

    /// Describe de-serialization of object
    template <typename T>
    void load( T& buf ) {
      setPackingVersion( buf.template load<uint8_t>() );
      setVersion( buf.template load<uint8_t>() );

      if ( m_packingVersion < 2 || m_packingVersion > defaultPackingVersion() ) {
        throw std::runtime_error( "PackedMuonPIDs packing version is not supported: " +
                                  std::to_string( m_packingVersion ) );
      }
      buf.load( m_vect, m_packingVersion );
    }

  private:
    /// Data packing version
    char m_packingVersion{defaultPackingVersion()};

    /// The packed data objects
    Vector m_vect;
  };

  /**
   *  Utility class to handle the packing and unpacking of the MuonPIDs
   *
   *  @author Christopher Rob Jones
   *  @date   2009-10-13
   */
  class MuonPIDPacker : public PackerBase {
  public:
    // These are required by the templated algorithms
    typedef LHCb::MuonPID        Data;
    typedef LHCb::PackedMuonPID  PackedData;
    typedef LHCb::MuonPIDs       DataVector;
    typedef LHCb::PackedMuonPIDs PackedDataVector;
    static const std::string&    packedLocation() { return LHCb::PackedMuonPIDLocation::Default; }
    static const std::string&    unpackedLocation() { return LHCb::MuonPIDLocation::Default; }

    using PackerBase::PackerBase;

    /// Pack a MuonPID
    void pack( const Data& pid, PackedData& ppid, PackedDataVector& ppids ) const;

    /// Pack MuonPIDs
    void pack( const DataVector& pids, PackedDataVector& ppids ) const;

    /// Unpack a single MuonPID
    void unpack( const PackedData& ppid, Data& pid, const PackedDataVector& ppids, DataVector& pids ) const;

    /// Unpack MuonPIDs
    void unpack( const PackedDataVector& ppids, DataVector& pids ) const;

    /// Compare two MuonPID containers to check the packing -> unpacking performance
    StatusCode check( const DataVector& dataA, const DataVector& dataB ) const;

    /// Compare two MuonPIDs to check the packing -> unpacking performance
    StatusCode check( const Data& dataA, const Data& dataB ) const;

  private:
    /// Check if the given packing version is supported
    bool isSupportedVer( const char& ver ) const {
      const bool OK = ( 3 == ver || 2 == ver || 1 == ver || 0 == ver );
      if ( !OK ) {
        throw GaudiException( fmt::format( "Unknown packed data version {}", (int)ver ), "MuonPIDPacker",
                              StatusCode::FAILURE );
      }
      return OK;
    }
  };

} // namespace LHCb
