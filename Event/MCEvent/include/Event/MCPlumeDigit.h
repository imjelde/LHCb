/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

// Include files
#include "Event/MCHit.h"
#include "GaudiKernel/KeyedContainer.h"
#include "GaudiKernel/KeyedObject.h"
#include "GaudiKernel/SmartRefVector.h"
#include "Kernel/PlumePmtID.h"
#include "Kernel/PlumePmtIDKeyTraits.h"
#include <ostream>

// Forward declarations

namespace LHCb {

  // Forward declarations

  // Class ID definition
  static const CLID CLID_MCPlumeDigit = 14150;

  // Namespace for locations in TDS
  namespace MCPlumeDigitLocation {
    inline const std::string Default = "MC/Plume/Digits";
  }

  /** @class MCPlumeDigit MCPlumeDigit.h
   *
   * Monte Carlo history for the given cell * * * The class represents the
   * number of photoelectrons generated in a PMT from all hits in the
   * window/tablet
   *
   */

  class MCPlumeDigit : public KeyedObject<LHCb::PlumePmtID> {
  public:
    /// typedef for KeyedContainer of MCPlumeDigit
    typedef KeyedContainer<MCPlumeDigit, Containers::HashMap> Container;

    /// Shortcut for references to Monte Carlo Hits
    using Hits = SmartRefVector<LHCb::MCHit>;

    /// Non-default constructor
    MCPlumeDigit( const LHCb::PlumePmtID& id, int e, const Hits& d ) : Base( id ), m_numPE( e ), m_hits( d ) {}

    /// Default constructor
    MCPlumeDigit() = default;

    /// Copy Constructor
    MCPlumeDigit( const LHCb::MCPlumeDigit& right )
        : Base( right.key() ), m_numPE( right.numPE() ), m_hits( right.hits() ) {}

    // Retrieve pointer to class definition structure
    const CLID&        clID() const override;
    static const CLID& classID();

    /// Fill the ASCII output stream
    std::ostream& fillStream( std::ostream& s ) const override;

    /// Retrieve cell identifier/key @attention alias to Base::key() method!
    const LHCb::PlumePmtID& PmtID() const;

    /// update cell identifier/key @attention alias to Base::setKey() method!
    MCPlumeDigit& setPmtID( LHCb::PlumePmtID PmtID );

    /// Add number of photoelectrons
    MCPlumeDigit& addNumPE( const int value );

    /// Cloning of the object ('virtual constructor')
    virtual MCPlumeDigit* clone() const;

    /// Retrieve const  number of photoelectrons in the given cell
    int numPE() const;

    /// Update  Monte Carlo number of photoelectrons in the given cell
    MCPlumeDigit& setNumPE( int value );

    /// Retrieve (const)  References to the Monte Carlo hits
    const SmartRefVector<LHCb::MCHit>& hits() const;

    /// Update  References to the Monte Carlo hits
    MCPlumeDigit& setHits( SmartRefVector<LHCb::MCHit> value );

    /// Add to  References to the Monte Carlo hits
    MCPlumeDigit& addToHits( SmartRef<LHCb::MCHit> value );

    /// Att to (pointer)  References to the Monte Carlo hits
    MCPlumeDigit& addToHits( const LHCb::MCHit* value );

    /// Remove from  References to the Monte Carlo hits
    MCPlumeDigit& removeFromHits( const SmartRef<LHCb::MCHit>& value );

    /// Clear  References to the Monte Carlo hits
    MCPlumeDigit& clearHits();

    friend std::ostream& operator<<( std::ostream& str, const MCPlumeDigit& obj ) { return obj.fillStream( str ); }

  protected:
    /// Shortcut for own base class
    using Base = KeyedObject<LHCb::PlumePmtID>;

  private:
    int                         m_numPE{0}; ///< number of photoelectrons generated in PMT
    SmartRefVector<LHCb::MCHit> m_hits;     ///< References to the Monte Carlo hits

  }; // class MCPlumeDigit

  /// Definition of Keyed Container for MCPlumeDigit
  typedef KeyedContainer<MCPlumeDigit, Containers::HashMap> MCPlumeDigits;

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------

// Including forward declarations

inline const CLID& LHCb::MCPlumeDigit::clID() const { return LHCb::MCPlumeDigit::classID(); }

inline const CLID& LHCb::MCPlumeDigit::classID() { return CLID_MCPlumeDigit; }

inline std::ostream& LHCb::MCPlumeDigit::fillStream( std::ostream& s ) const {
  s << "{ "
    << "numPE :	" << m_numPE << std::endl
    << " }";
  return s;
}

inline int LHCb::MCPlumeDigit::numPE() const { return m_numPE; }

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::setNumPE( int value ) {
  m_numPE = value;
  return *this;
}

inline const SmartRefVector<LHCb::MCHit>& LHCb::MCPlumeDigit::hits() const { return m_hits; }

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::setHits( SmartRefVector<LHCb::MCHit> value ) {
  m_hits = value;
  return *this;
}

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::addToHits( SmartRef<LHCb::MCHit> value ) {
  m_hits.push_back( std::move( value ) );
  return *this;
}

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::addToHits( const LHCb::MCHit* value ) {
  m_hits.push_back( value );
  return *this;
}

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::removeFromHits( const SmartRef<LHCb::MCHit>& value ) {
  auto i = std::remove( m_hits.begin(), m_hits.end(), value );
  m_hits.erase( i, m_hits.end() );
  return *this;
}

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::clearHits() {
  m_hits.clear();
  return *this;
}

inline const LHCb::PlumePmtID& LHCb::MCPlumeDigit::PmtID() const { return key(); }

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::setPmtID( LHCb::PlumePmtID PmtID ) {
  setKey( std::move( PmtID ) );
  return *this;
}

inline LHCb::MCPlumeDigit& LHCb::MCPlumeDigit::addNumPE( const int value ) {
  m_numPE += value;
  return *this;
}

inline LHCb::MCPlumeDigit* LHCb::MCPlumeDigit::clone() const { return new LHCb::MCPlumeDigit( *this ); }
