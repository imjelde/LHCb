###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/MCEvent
-------------
#]=======================================================================]

gaudi_add_library(MCEvent
    SOURCES
        src/GhostTrackInfo.cpp
        src/MCFun.cpp
        src/MCParticle.cpp
        src/MCProperty.cpp
        src/MCRichDigit.cpp
        src/MCRichDigitHistoryCode.cpp
        src/MCRichHit.cpp
        src/MCRichSegment.cpp
        src/MCRichTrack.cpp
        src/MCTrackGeomCriteria.cpp
        src/MCTruth.cpp
        src/MCVertex.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::LHCbKernel
            LHCb::PartPropLib
            LHCb::EventBase
)

gaudi_add_dictionary(MCEventDict
    HEADERFILES dict/dictionary.h
    SELECTION dict/selection.xml
    LINK LHCb::MCEvent
)
