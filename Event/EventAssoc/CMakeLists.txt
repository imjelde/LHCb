###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Event/EventAssoc
----------------
#]=======================================================================]

gaudi_add_dictionary(EventAssocPhysDict
    HEADERFILES dict/selPhys.h
    SELECTION dict/selPhys.xml
    LINK
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
)

gaudi_add_dictionary(EventAssocMCDict
    HEADERFILES dict/selMC.h
    SELECTION dict/selMC.xml
    LINK
        HepMC::HepMC
        LHCb::DigiEvent
        LHCb::GenEvent
        LHCb::MCEvent
        LHCb::PhysEvent
        LHCb::RecEvent
        LHCb::RelationsLib
        LHCb::TrackEvent
)
