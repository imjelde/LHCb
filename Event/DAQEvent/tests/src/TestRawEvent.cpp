/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE utestRawEvent
#include "Event/RawEvent.h"
#include <TFile.h>
#include <TTree.h>
#include <boost/test/unit_test.hpp>
#include <iostream>
#include <memory>

BOOST_AUTO_TEST_CASE( basic_tests ) { LHCb::RawEvent raw_event; }

static auto constexpr test_fname = "TestRawEvent.root";
static auto constexpr test_tname = "Test";

template <typename T, typename F, typename G>
void do_io_test( F get_resource, G fill_default_obj ) {
  auto flag_resource = reinterpret_cast<LHCb::Allocators::MemoryResource*>( 0xdeadbeef );
  T    obj{flag_resource}, obj_default;
  BOOST_CHECK( get_resource( obj ) == flag_resource );
  BOOST_CHECK( get_resource( obj_default ) == LHCb::defaultMemResource() );
  fill_default_obj( obj_default );
  auto constexpr test_bname         = "obj";
  auto constexpr test_bname_default = "obj_default";
  {
    std::unique_ptr<TFile> ofile{TFile::Open( test_fname, "recreate" )};
    BOOST_CHECK( ofile );
    ofile->cd();
    auto otree = new TTree( test_tname, "" );
    otree->Branch( test_bname, &obj );
    otree->Branch( test_bname_default, &obj_default );
    otree->Fill();
    otree->Write();
    ofile->Close();
  }
  std::unique_ptr<TFile> ifile{TFile::Open( test_fname, "read" )};
  BOOST_CHECK( ifile );
  TTree* itree{nullptr};
  ifile->GetObject( test_tname, itree );
  BOOST_CHECK( itree );
  T *obj_in{nullptr}, *obj_in_default{nullptr};
  itree->SetBranchAddress( test_bname, &obj_in );
  itree->SetBranchAddress( test_bname_default, &obj_in_default );
  itree->GetEntry( 0 );
  BOOST_CHECK( obj_in && obj_in_default );
  // The resource pointer should *not* be persisted, so now both objects should have the default
  BOOST_CHECK( get_resource( *obj_in ) == LHCb::defaultMemResource() );
  BOOST_CHECK( get_resource( *obj_in_default ) == LHCb::defaultMemResource() );
}

BOOST_AUTO_TEST_CASE( test_allocator_io ) {
  do_io_test<LHCb::RawEvent::allocator_type>( []( auto const& x ) { return x.resource(); }, []( auto& ) {} );
}

BOOST_AUTO_TEST_CASE( test_vector_io ) {
  unsigned int data = 42;
  auto         ptr  = &data;
  do_io_test<std::vector<LHCb::RawEvent::Bank, LHCb::RawEvent::allocator_type>>(
      []( auto const& x ) { return x.get_allocator().resource(); },
      [ptr]( auto& x ) { x.emplace_back( 1, false, ptr ); } );
}

BOOST_AUTO_TEST_CASE( test_raw_event_io ) {
  do_io_test<LHCb::RawEvent>( []( auto const& x ) { return x.get_allocator().resource(); },
                              []( auto& x ) {
                                std::vector<unsigned int> dummy_data{42u, 7u, 24u};
                                x.addBank( 0, LHCb::RawBank::BankType::HltDecReports, 0, dummy_data );
                              } );
}