/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawEvent.h"
#include <cassert>
#include <cstring> // for memcpy with gcc 4.3

namespace {
  LHCb::RawBank* allocateBank( size_t len ) {
    size_t        mem_len = LHCb::RawEvent::paddedBankLength( len );
    size_t        new_len = mem_len / sizeof( unsigned int );
    unsigned int* mem     = new unsigned int[new_len];
    if ( mem_len != len ) mem[new_len - 1] = 0;
    return reinterpret_cast<LHCb::RawBank*>( mem );
  }
} // namespace

LHCb::RawEvent::MapView::MapView( LHCb::RawEvent const& raw ) : m_banks{raw.get_allocator()} {
  std::array<uint16_t, RawBank::BankType::LastType> counts{}; // zero-initialize
  std::for_each( raw.m_banks.begin(), raw.m_banks.end(), [&counts]( const Bank& b ) {
    auto ptr = reinterpret_cast<const RawBank*>( b.buffer() );
    assert( ptr != nullptr );
    if ( auto type = ptr->type(); type < RawBank::BankType::LastType ) ++counts[type];
  } );
  m_indices.front() = 0;
  std::partial_sum( counts.begin(), counts.end(), std::next( m_indices.begin() ) );
  m_banks.resize( m_indices.back(), nullptr );
  std::for_each( raw.m_banks.begin(), raw.m_banks.end(), [&]( const Bank& b ) {
    auto ptr = reinterpret_cast<const RawBank*>( b.buffer() );
    if ( auto type = ptr->type(); type < RawBank::BankType::LastType ) {
      assert( counts[type] > 0 );
      m_banks[m_indices[type + 1] - ( counts[type]-- )] = ptr;
    }
  } );
  assert( std::all_of( counts.begin(), counts.end(), []( auto i ) { return i == 0; } ) );
  assert( std::all_of( m_banks.begin(), m_banks.end(), []( auto* p ) { return p != nullptr; } ) );
}

// Default Destructor
LHCb::RawEvent::~RawEvent() {
  for ( Bank& b : m_banks ) {
    if ( b.ownsMemory() ) delete[] b.buffer();
  }
}

size_t LHCb::RawEvent::paddedBankLength( size_t len ) {
  size_t mem_len = len + sizeof( RawBank ) - sizeof( unsigned int );
  if ( mem_len % sizeof( unsigned int ) ) { // Need padding
    mem_len = ( mem_len / sizeof( unsigned int ) + 1 ) * sizeof( unsigned int );
  }
  return mem_len;
}

LHCb::RawBank* LHCb::RawEvent::createBank( int srcID, LHCb::RawBank::BankType typ, int vsn, size_t len,
                                           const void* data ) {
  RawBank* bank = allocateBank( len );
  bank->setMagic().setType( typ ).setVersion( vsn ).setSourceID( srcID ).setSize( len );
  if ( data ) std::memcpy( bank->data(), data, len );
  return bank;
}

/// For offline use only: copy data into a bank, adding bank header internally.
// TODO: remove all the calls to this function (in deepCopyRawEvent) and remove it
void LHCb::RawEvent::addBank( const RawBank* data ) {
  size_t len  = data->totalSize();
  auto   bank = reinterpret_cast<RawBank*>( new unsigned int[len / sizeof( unsigned int )] );
  std::memcpy( bank, data, len );
  adoptBank( bank, true );
}

/// Take ownership of a bank, including the header
void LHCb::RawEvent::adoptBank( const LHCb::RawBank* bank, bool adopt_memory ) {
  size_t len = bank->totalSize();
  m_banks.emplace_back( len / sizeof( unsigned int ), adopt_memory, reinterpret_cast<const unsigned int*>( bank ) );
  // brute force solution: invalidate map.. TODO: just modify instead of rebuilding later from scratch
  m_map.reset();
}

/// Remove bank identified by its pointer
bool LHCb::RawEvent::removeBank( const RawBank* bank ) {
  // locate bank in persistent array.
  auto i = std::find_if(
      m_banks.begin(), m_banks.end(),
      [ptr = reinterpret_cast<const unsigned int*>( bank )]( const Bank& b ) { return ptr == b.buffer(); } );
  if ( i == m_banks.end() ) return false;
  // The bank is owned by RawEvent: delete the allocated buffer
  // to prevent memory leak when reading data from a ROOT file...
  if ( i->ownsMemory() ) delete[] i->buffer();
  m_banks.erase( i );
  // brute force solution: invalidate map.. TODO: just modify instead of rebuilding later from scratch
  m_map.reset();
  return true;
}
