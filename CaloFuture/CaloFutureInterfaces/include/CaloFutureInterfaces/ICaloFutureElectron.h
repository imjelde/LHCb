/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/IGeometryInfo.h"
#include "Event/ProtoParticle.h"
#include "Event/State.h"
#include "Event/Track.h"
#include "GaudiKernel/IAlgTool.h"
#include <optional>
#include <string>

// Forward declarations
namespace LHCb {
  class CaloHypo;
  namespace Calo {
    class Momentum;
  }
} // namespace LHCb

/** @class ICaloFutureElectron ICaloFutureElectron.h Kernel/ICaloFutureElectron.h
 *
 *
 *  @author Olivier Deschamps
 *  @date   2006-11-30
 */
namespace LHCb::Calo::Interfaces {
  struct hypoPairStruct {
    const LHCb::CaloHypo* electronHypo = nullptr;
    const LHCb::CaloHypo* bremHypo     = nullptr;
    explicit              operator bool() const { return ( electronHypo != nullptr ); }
  };

  struct IElectron : public extend_interfaces<IAlgTool> {

    // Return the interface ID
    DeclareInterfaceID( IElectron, 5, 0 );

    virtual hypoPairStruct getElectronBrem( LHCb::ProtoParticle const& proto ) const = 0;

    virtual State                 caloState( ProtoParticle const& proto, IGeometryInfo const& geometry ) const    = 0;
    virtual State                 closestState( ProtoParticle const& proto, IGeometryInfo const& geometry ) const = 0;
    virtual Momentum              bremMomentum( ProtoParticle const& proto ) const                                = 0;
    virtual std::optional<double> caloTrajectoryL( ProtoParticle const& proto, IGeometryInfo const& geometry,
                                                   CaloPlane::Plane refPlane = CaloPlane::ShowerMax ) const       = 0;

    // interface that does not require ProtoParticle or Hypo, just basic track and cluster...
    virtual State caloState( const LHCb::Track&, IGeometryInfo const& geometry ) const = 0;

    // functions implemented in terms of other functions...
    double eOverP( ProtoParticle const& proto ) const {
      auto el = electron( proto );
      return el ? el->e() / proto.track()->p() : 0;
    }
    const CaloHypo* electron( ProtoParticle const& proto ) const { return getElectronBrem( proto ).electronHypo; }
    const CaloHypo* bremstrahlung( ProtoParticle const& proto ) const { return getElectronBrem( proto ).bremHypo; }
  };
} // namespace LHCb::Calo::Interfaces
