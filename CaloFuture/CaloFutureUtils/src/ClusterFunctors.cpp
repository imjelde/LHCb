/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/ClusterFunctors.h"
#include "CaloDet/DeCalorimeter.h"
#include "CaloKernel/CaloException.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDigit.h"

// ============================================================================
/** @file ClusterFunctors.cpp
 *
 *  Implementation of non-inline method from ClusterFunctors namespace
 *  @see ClusterFunctors
 *
 *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date 04/07/2001
 */
// ============================================================================

namespace LHCb::Calo::Functor::Cluster {
  // ============================================================================
  /**  Calculate the "energy" of the cluster as a sum of
   *   energies of its digits, weighted with energy fractions
   *   @param   cl  pointer to cluster
   *   @return      "energy" of cluster
   */
  // ============================================================================
  float energy( const LHCb::CaloCluster* cl ) {
    if ( !cl || cl->entries().empty() ) { return 0; }
    return clusterEnergy( cl->entries().begin(), cl->entries().end() );
  }

  // ===========================================================================
  /**    useful function to determine, if clusters have
   *     at least one common cell.
   *
   *     For invalid arguments return "false"
   *
   *     @param   cl1   pointer to first  cluster
   *     @param   cl2   pointer to second cluster
   *     @return "true" if clusters have at least 1 common cell
   */
  // ===========================================================================
  bool overlapped( const LHCb::CaloCluster* cl1, const LHCb::CaloCluster* cl2 ) {
    if ( !cl1 || !cl2 || cl1->entries().empty() || cl2->entries().empty() ) { return false; }
    auto p = commonDigit( cl1->entries().begin(), cl1->entries().end(), cl2->entries().begin(), cl2->entries().end() );
    return cl1->entries().end() != p.first;
  }

  // ===========================================================================
  /** throw the exception
   *  @param message exception message
   *  @return status code (fictive)
   */
  // ===========================================================================
  void throwException( const std::string& message ) { throw CaloException( " ClusterFunctors::" + message ); }
} // namespace LHCb::Calo::Functor::Cluster
