/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFutureNeighbours.h"
#include <set>
#include <vector>

namespace {

  bool _neighbours( std::set<LHCb::Calo::CellID>& cells, const unsigned int level, const DeCalorimeter& detector ) {
    if ( 0 >= level || cells.empty() ) { return true; } // RETURN
    // local copy:
    auto local = cells;
    for ( auto loc : local ) {
      const auto& nei = detector.neighborCells( loc );
      cells.insert( nei.begin(), nei.end() );
    }
    return _neighbours( cells, level - 1, detector ); // RECURSION!
  }

} //                                                 end of anonymous namespace

/*  find all neighbours for the given set of cells for the givel level
 *  @param cells    (UPDATE) list of cells
 *  @param level    (INPUT)  level
 *  @param detector (INPUT) the detector
 *  @return true if neighbours are added
 */
bool LHCb::CaloFutureFunctors::neighbours( std::set<LHCb::Calo::CellID>& cells, const unsigned int level,
                                           const DeCalorimeter* detector ) {
  return detector && _neighbours( cells, level, *detector );
}

/*  find all neighbours for the given set of cells for the givel level
 *  @param cells    (UPDATE) list of cells
 *  @param level    (INPUT)  level
 *  @param detector (INPUT)  the detector
 *  @return true if neighbours are added
 */
bool LHCb::CaloFutureFunctors::neighbours( std::vector<LHCb::Calo::CellID>& cells, const unsigned int level,
                                           const DeCalorimeter* detector ) {
  if ( !detector ) { return false; }    // RETURN
  if ( 0 == level ) { return true; }    // RETURN
  if ( cells.empty() ) { return true; } // RETURN
  // local copy:
  auto       local  = std::set<LHCb::Calo::CellID>{cells.begin(), cells.end()};
  const bool result = _neighbours( local, level, *detector );
  if ( !result ) { return false; } // RETURN
  cells.clear();
  cells.insert( cells.begin(), local.begin(), local.end() );
  return true; // RETURN
}
