/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDigits_v2.h"
#include "Event/CaloHypos_v2.h"
#include "Event/RelationTable.h"
#include "Event/Track_v3.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "GaudiKernel/Plane3DTypes.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "LHCbMath/Utils.h"
#include "TFile.h"

namespace LHCb::Calo {

  namespace TrackUtils {

    // miscellaneous definitions
    using Tracks = LHCb::Event::v3::Tracks;

    using SL = LHCb::Event::v3::detail::StateLocation;
    using TT = LHCb::Event::v3::TrackType;

    using simd_t = SIMDWrapper::scalar::types;
    using mask_v = simd_t::mask_v;

    using CaloDigits   = LHCb::Event::Calo::Digits;
    using CaloClusters = LHCb::Event::Calo::Clusters;
    using CaloHypos    = LHCb::Event::Calo::Hypotheses;

    // definitions of track to calo relations tables
    // note: all needs individual classes for zipping to work
    struct InEcalCellID : LHCb::Event::int_field {};
    struct InHcalCellID : LHCb::Event::int_field {};
    struct InBremCellID : LHCb::Event::int_field {};

    using TracksInEcal = LHCb::Event::RelationTable1D<Tracks, InEcalCellID>;
    using TracksInHcal = LHCb::Event::RelationTable1D<Tracks, InHcalCellID>;
    using TracksInBrem = LHCb::Event::RelationTable1D<Tracks, InBremCellID>;

    struct EcalEnergy : LHCb::Event::float_field {};
    struct HcalEnergy : LHCb::Event::float_field {};
    struct BremEnergy : LHCb::Event::float_field {};

    using TracksEcalEnergy = LHCb::Event::RelationTable1D<Tracks, EcalEnergy>;
    using TracksHcalEnergy = LHCb::Event::RelationTable1D<Tracks, HcalEnergy>;
    using TracksBremEnergy = LHCb::Event::RelationTable1D<Tracks, BremEnergy>;

    struct ElectronShowerEoP : LHCb::Event::float_field {};
    struct ElectronShowerDLL : LHCb::Event::float_field {};
    using TracksElectronShower = LHCb::Event::RelationTable1D<Tracks, ElectronShowerEoP, ElectronShowerDLL>;

    struct ClusterMatch : LHCb::Event::float_field {};
    struct ElectronMatch : LHCb::Event::float_field {};
    struct BremMatch : LHCb::Event::float_field {};
    struct BremDeltaX : LHCb::Event::float_field {};

    using Tracks2Clusters  = LHCb::Event::RelationTable2D<Tracks, CaloClusters, ClusterMatch>;
    using Tracks2Electrons = LHCb::Event::RelationTable2D<Tracks, CaloHypos, ElectronMatch>;
    using Tracks2Brems     = LHCb::Event::RelationTable2D<Tracks, CaloHypos, BremMatch, BremDeltaX>;

    using Clusters2BestTrackMatch = LHCb::Event::RelationTable1D<CaloClusters, ClusterMatch>;

    /////////////////////////////////////////////////////////////////
    //                       Functions                             //
    /////////////////////////////////////////////////////////////////

    // state location getter for appropriate for linear extrapolation
    inline auto extrapolation_stateloc = []( Tracks const& tracks ) {
      auto const trktype = tracks.type();
      // type to state location (if not avaible, not valid type)
      auto const typemap = std::map<TT, SL>{
          {TT::Long, SL::EndRich2},
          {TT::Downstream, SL::EndRich2},
          {TT::Ttrack, SL::LastMeasurement},
      };
      // if not available, not valid linear extrapolation + track type combination
      return ( typemap.find( trktype ) != typemap.end() ) ? std::optional<SL>{typemap.at( trktype )} : std::nullopt;
    };

    // to get calo area of track position
    inline int getAreaForTrack( const Gaudi::XYZPoint& pointatcalo, const DeCalorimeter& calo ) {
      auto area = calo.Area( pointatcalo );
      if ( area == CellCode::CaloArea::UndefinedArea ) {
        area = ( fabs( pointatcalo.x() ) < 2. * Gaudi::Units::m && fabs( pointatcalo.y() ) < 2. * Gaudi::Units::m )
                   ? CellCode::CaloArea::Inner
                   : CellCode::CaloArea::Outer;
      }
      return area;
    }

    // for linear propagation of track state to calo plane
    namespace {

      // helper function, calculating quick linear extrapolation assuming only tilt of y-axis
      template <typename CaloState, typename RefState>
      std::optional<float> _propagateToCalo( CaloState& calostate, RefState const& state,
                                             Gaudi::Plane3D const& plane ) {
        using LHCb::Utils::as_arithmetic;
        // load start values
        calostate.setZ( as_arithmetic( state.z() ) );
        auto get_trackvector = []( CaloState& state ) -> Gaudi::TrackVector& {
          if constexpr ( std::is_same<CaloState, LHCb::State>::value ) {
            return state.stateVector();
          } else {
            return state.parameters();
          }
        };
        Gaudi::TrackVector& vec = get_trackvector( calostate );
        vec                     = {as_arithmetic( state.x() ), as_arithmetic( state.y() ), as_arithmetic( state.tx() ),
               as_arithmetic( state.ty() ), as_arithmetic( state.qOverP() )};
        // new z location, assuming tilt in y-axis
        auto denom = plane.C() + plane.B() * calostate.ty();
        if ( denom == 0. ) return std::nullopt;
        auto const zintersect =
            ( -plane.HesseDistance() - plane.B() * ( calostate.y() - calostate.ty() * calostate.z() ) ) / denom;
        // linear transport
        auto const dz = zintersect - calostate.z();
        calostate.setZ( zintersect );
        vec( 0 ) += dz * vec( 2 );
        vec( 1 ) += dz * vec( 3 );
        return std::optional<float>{dz};
      }

    } // end namespace

    // linear extrapolation of a state to new plane, just updating position
    template <typename RefState>
    inline bool propagateToCalo( LHCb::StateVector& calostate, RefState const& state, Gaudi::Plane3D const& plane ) {
      return _propagateToCalo<LHCb::StateVector, RefState>( calostate, state, plane ).has_value();
    }

    // above, but also updating (only!) covariance of x/y/qoverp
    template <typename RefState>
    inline bool propagateToCaloWithCov( LHCb::State& calostate, RefState const& state, Gaudi::Plane3D const& plane ) {
      using LHCb::Utils::as_arithmetic;
      // linear extrapolation
      auto _dz = _propagateToCalo<LHCb::State, RefState>( calostate, state, plane );
      if ( !_dz.has_value() ) return false;
      // get covariance objects
      auto const             dz      = _dz.value();
      auto const             dz2     = dz * dz;
      auto const             cov     = state.covariance();
      Gaudi::TrackSymMatrix& calocov = calostate.covariance();
      // update x/y/qoverp covariance
      calocov( 0, 0 ) =
          as_arithmetic( cov( 0, 0 ) ) + dz2 * as_arithmetic( cov( 2, 2 ) ) + 2 * dz * as_arithmetic( cov( 2, 0 ) );
      calocov( 1, 0 ) = as_arithmetic( cov( 1, 0 ) ) + dz2 * as_arithmetic( cov( 3, 2 ) ) +
                        dz * ( as_arithmetic( cov( 3, 0 ) ) + as_arithmetic( cov( 2, 1 ) ) );
      calocov( 1, 1 ) =
          as_arithmetic( cov( 1, 1 ) ) + dz2 * as_arithmetic( cov( 3, 3 ) ) + 2 * dz * as_arithmetic( cov( 3, 1 ) );
      calocov( 4, 0 ) = as_arithmetic( cov( 4, 0 ) ) + dz * as_arithmetic( cov( 4, 2 ) );
      calocov( 4, 1 ) = as_arithmetic( cov( 4, 1 ) ) + dz * as_arithmetic( cov( 4, 3 ) );
      calocov( 4, 4 ) = as_arithmetic( cov( 4, 4 ) );
      return true;
    }

    // helper class to store histograms for parametrizations (electron shower, DLLs, ...)
    template <typename HistoType, typename Histo, typename AIDAHisto>
    class HistoStore {
    private:
      // the to-be-stored histograms
      std::map<HistoType, Histo> m_histos;

    public:
      // access to histograms
      Histo const* hist( HistoType htype ) const { return &m_histos.at( htype ); }

      // constuctors
      using HistoMap = std::map<HistoType, std::string>;

      // constructor with THS
      HistoStore( IHistogramSvc* ths, const std::string histoLoc, HistoMap const& histoMap ) {
        try {
          // locate histograms
          StatusCode sc;
          AIDAHisto* aida_histo = 0;
          for ( auto const& el : histoMap ) {
            std::string hname = histoLoc + "/" + el.second;
            ths->retrieveObject( hname, aida_histo ).ignore();
            m_histos[el.first] = *Gaudi::Utils::Aida2ROOT::aida2root( aida_histo );
          }
        } catch ( GaudiException& exc ) {
          throw GaudiException( "histogram getting from THS failed!", __func__, StatusCode::FAILURE );
        }
      }

      // constructor with ParamFiles
      HistoStore( std::string const fileLoc, std::string const histoLoc,
                  const std::map<HistoType, std::string> histoMap ) {
        try {
          // locate file in ParamFiles
          std::unique_ptr<TFile> paramfile( TFile::Open( fileLoc.c_str(), "READ" ) );
          // histogram cloning
          for ( auto const& el : histoMap ) {
            std::string hname     = ( histoLoc != "" ) ? histoLoc + "/" + el.second : el.second;
            auto        temp_hist = paramfile->Get<Histo>( hname.c_str() );
            m_histos[el.first]    = Histo( *temp_hist );
          }
        } catch ( GaudiException& exc ) {
          throw GaudiException( "histogram getting from ParamFiles failed!", __func__, StatusCode::FAILURE );
        }
      }
    };

  } // namespace TrackUtils

} // namespace LHCb::Calo
