/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once

#include "Event/CaloDigit.h"

namespace LHCb {

  /** @namespace LHCb::CaloFutureHelpers
   *  Helper namespace for decoration of Calo-objects
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-11-28
   */
  namespace CaloFutureHelpers {

    /// vector -> set converter
    std::set<Calo::CellID> toSet( const std::vector<Calo::CellID>& inp );
    /// vector -> set converter
    CaloDigit::Set toSet( const CaloDigit::Vector& inp );
    /// set -> vector converter
    std::vector<Calo::CellID> toVector( const std::set<Calo::CellID>& inp );
    /// set -> vector converter
    CaloDigit::Vector toVector( const CaloDigit::Set& inp );

    /// vector -> set converter
    void toSet( const std::vector<Calo::CellID>& inp, std::set<Calo::CellID>& out );
    /// vector -> set converter
    void toSet( const CaloDigit::Vector& inp, CaloDigit::Set& out );
    /// set -> vector converter
    void toVector( const std::set<Calo::CellID>& inp, std::vector<Calo::CellID>& out );
    /// set -> vector converter
    void toVector( const CaloDigit::Set& inp, CaloDigit::Vector& out );

    /** get the cell form the set
     *  @param cells (INPUT) the input set
     *  @param index (INPUT) the index
     *  @return the cell
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-28
     */
    Calo::CellID _get_at_( const std::set<Calo::CellID>& cells, const size_t index );

    /** get the digit from the set
     *  @param cells (INPUT) the input set
     *  @param index (INPUT) the index
     *  @return the digit
     *  @author Vanya BELYAEV Ivan.BElyaev@nikhef.nl
     *  @date 2009-11-28
     */
    const CaloDigit* _get_at_( const CaloDigit::Set& cells, const size_t index );

    /// insert the object to set
    size_t _insert_( std::set<Calo::CellID>& cells, const Calo::CellID& cell );

    /// insert the object to set
    size_t _insert_( CaloDigit::Set& cells, const CaloDigit* digit );

  } // namespace CaloFutureHelpers

} //                                                      end of namespace LHCb
