/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/BankWriter.h"
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/UTDigit.h"
#include "Event/UTSummary.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTRawBankMap.h"
#include "UTDAQ/UTADCWord.h"
#include "UTDAQ/UTHeaderWord.h"
#include <algorithm>
#include <array>
#include <map>
#include <string>
#include <utility>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : DigitsToRawBank
//
// 2020-06-24 : Xuhao Yuan (based on codes from M. Needham)
//-----------------------------------------------------------------------------

/** @class DigitsToRawBank DigitsToRawBank.h
 *
 *  Algorithm to fill the Raw buffer with UT information from UTDigits
 *
 *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
 *  @date   2020-06-24
 */

namespace LHCb::UT {
  namespace {
    /** @class BoardToBankMap
     *
     *  Helper class for mapping boards to banks
     *  basically hides a map....
     *
     *  @author Xuhao Yuan (based on code by A Beiter and M Needham)
     *  @date   2020-05-11
     */

    class BoardToBankMap final {
      std::map<UTDAQID, unsigned int> m_map;

    public:
      // add entry to map
      BoardToBankMap& insert( UTDAQID aBoard, unsigned int aBank ) {
        m_map.insert_or_assign( aBoard, aBank );
        return *this;
      }

      // board to bank
      [[nodiscard]] UTDAQID findBoard( unsigned int aBank ) const {
        auto i = std::find_if( m_map.begin(), m_map.end(), [&]( const auto& p ) { return p.second == aBank; } );
        return i != m_map.end() ? i->first : UTDAQID( UTDAQID::nullBoard );
      }

      // bank to board
      [[nodiscard]] unsigned int findBank( UTDAQID aBoard ) const { return m_map.at( aBoard ); }

      BoardToBankMap& clear() {
        m_map.clear();
        return *this;
      }
    };

    /** @class DigitsOnBoard
     *
     *  Helper class for keeping track of digits...
     *
     *  @author Xuhao Yaun (based on code by A Beiter, M Needham)
     *  @date   2020-06-24
     */

    class DigitsOnBoard final {
      // maximum # of lanes is 6 in all UT boards
      constexpr static auto maxNumLanes{6};
      //# of DAQBoard is 216
      constexpr static auto NumUTDAQBoard{216};

    public:
      using DigitVector = std::vector<std::pair<UTDigit*, UTDAQID>>;

      explicit DigitsOnBoard( unsigned int nMax ) : m_maxDigitsPerPPx( nMax ) {
        m_digitCont.reserve( DigitsOnBoard::NumUTDAQBoard );
        clear();
      }

      void addDigit( UTDigit& aDigit ) {
        UTDAQID            daqChanID( aDigit.getdaqID() );
        const unsigned int ppx = daqChanID.lane();

        if ( m_ppxCount[ppx] < m_maxDigitsPerPPx ) {
          m_digitCont.insert(
              std::partition_point( m_digitCont.begin(), m_digitCont.end(),
                                    [&]( const auto& obj ) { return obj.second.id() < daqChanID.id(); } ),
              {&aDigit, daqChanID} );
          ++m_ppxCount[ppx];
        } else {
          // data went into the void
        }
      }

      const DigitVector& digits() const { return m_digitCont; }

      bool inOverflow() const {
        return std::any_of( m_ppxCount.begin(), m_ppxCount.end(),
                            [&]( unsigned int ppx ) { return ppx >= m_maxDigitsPerPPx; } );
      }

      std::array<unsigned int, maxNumLanes> nHitsinLine() const { return m_ppxCount; };

      unsigned int maxHitsinLine() const { return *std::max_element( m_ppxCount.begin(), m_ppxCount.end() ); }

      void clear() {
        m_digitCont.clear();
        m_ppxCount.fill( 0 );
      }

    private:
      unsigned int                          m_maxDigitsPerPPx{0};
      DigitVector                           m_digitCont;
      std::array<unsigned int, maxNumLanes> m_ppxCount;
    };

    template <auto m>
    [[nodiscard]] constexpr unsigned int extract( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      return ( i & static_cast<unsigned int>( m ) ) >> b;
    }

    template <auto m>
    [[nodiscard]] constexpr unsigned int shift( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      auto v = ( i << static_cast<unsigned int>( b ) );
      assert( extract<m>( v ) == i );
      assert( !( v & ~static_cast<unsigned int>( m ) ) );
      return v;
    }

    [[nodiscard]] constexpr unsigned int encode( UTDAQID id, const UTDigit& digit ) {
      enum class mask { adc = 0x1F, strip = 0xFFE0 };
      return shift<mask::strip>( id.channel() ) | shift<mask::adc>( digit.depositedCharge() );
    }

    unsigned int bankSize( const DigitsOnBoard& digitBoard ) {
      unsigned int nline = ( digitBoard.maxHitsinLine() + 1 ) / 2;
      return ( 256u / ( sizeof( unsigned int ) * 8u ) ) * nline;
    }
  } // namespace

  class DigitsToRawBank : public GaudiAlgorithm {

  public:
    /// Standard constructor
    using GaudiAlgorithm::GaudiAlgorithm;

    StatusCode initialize() override; ///< Algorithm initialization
    StatusCode execute() override;    ///< Algorithm execution
    StatusCode finalize() override;   ///< Algorithm finalization

  private:
    /// fill the banks
    StatusCode groupByBoard( const UTDigits& digitCont );

    // create a new bank
    void writeBank( const DigitsOnBoard& digitCont, BankWriter& bWriter );

    Gaudi::Property<int>           m_maxDigitsPerPPx{this, "maxDigits", 252};
    DataObjectReadHandle<RawEvent> m_raw{this, "rawLocation", RawEventLocation::Default};
    DataObjectReadHandle<UTDigits> m_digits{this, "tightdigitLocation", UTDigitLocation::UTTightDigits};

    BoardToBankMap m_bankMapping;

    std::map<UTDAQID::BoardID, DigitsOnBoard*> m_digitMap;
    std::vector<DigitsOnBoard>                 m_digitVectors;

    ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_bank_truncated{this,
                                                                           "RawBank overflow -- some banks truncated"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR>   m_board_awol{this, "Failed to find board in map"};

    enum class banksize { even_bit = 16, odd_bit = 0 };
  };

  DECLARE_COMPONENT_WITH_ID( DigitsToRawBank, "UTDigitsToRawBankAlg" )

  // Finalisation.
  StatusCode DigitsToRawBank::finalize() {
    m_digitVectors.clear();
    m_bankMapping.clear();
    return StatusCode::SUCCESS;
  }

  // Initialisation.
  StatusCode DigitsToRawBank::initialize() {

    return GaudiAlgorithm::initialize().andThen( [&] {
      // init the map
      unsigned int nBoard = m_readoutTool->nBoard();
      m_digitVectors.reserve( nBoard );
      for ( unsigned int iVal = 0; iVal < nBoard; ++iVal ) {
        auto aBoard = m_readoutTool->findByDAQOrder( iVal );
        m_bankMapping.insert( ( aBoard->boardID() ).board(), iVal );
        m_digitMap[( aBoard->boardID() ).board()] = &m_digitVectors.emplace_back( m_maxDigitsPerPPx );
      } // iVal
    } );
  }

  StatusCode DigitsToRawBank::execute() {

    // Retrieve the RawBank
    RawEvent* tEvent = m_raw.get();

    // intialize temp bank structure each event
    std::for_each( m_digitVectors.begin(), m_digitVectors.end(), []( DigitsOnBoard& i ) { i.clear(); } );
    unsigned int n_overflow = 0;

    // group the data by banks..
    StatusCode sc = groupByBoard( *m_digits.get() );
    if ( sc.isFailure() ) return sc;

    // convert to a bank and add to buffer
    const unsigned int nBoard = m_readoutTool->nBoard();
    for ( unsigned int iBoard = 0u; iBoard < nBoard; ++iBoard ) {
      // get the data ....
      const UTDAQID aBoardID = m_bankMapping.findBoard( iBoard );

      if ( m_digitVectors[iBoard].inOverflow() ) ++n_overflow;

      // make the a bankwriter....
      auto bWriter = BankWriter{bankSize( m_digitVectors[iBoard] )};
      writeBank( m_digitVectors[iBoard], bWriter );
      RawBank* tBank =
          tEvent->createBank( UTDAQ::rawInt( aBoardID.board() ), RawBank::UT, static_cast<int>( UTDAQ::version::v5 ),
                              bWriter.byteSize(), bWriter.dataBank().data() );
      tEvent->adoptBank( tBank, true );

    } // iBoard

    // flag overflow
    if ( n_overflow > 0 ) ++m_bank_truncated;
    return sc;
  }

  StatusCode DigitsToRawBank::groupByBoard( const UTDigits& digitCont ) {
    // divide up the digits by readout board
    for ( auto digit : digitCont ) {
      UTDAQID utdaqID = m_readoutTool->channelIDToDAQID( digit->channelID() );
      auto    iterMap = m_digitMap.find( utdaqID.board() );
      if ( iterMap == m_digitMap.end() ) {
        ++m_board_awol;
        return StatusCode::FAILURE;
      }
      iterMap->second->addDigit( *digit );
    } // digitCont
    return StatusCode::SUCCESS;
  }

  void DigitsToRawBank::writeBank( const DigitsOnBoard& digitBoard, BankWriter& bWriter ) {
    auto         n_digithits = digitBoard.nHitsinLine();
    unsigned int nline       = ( digitBoard.maxHitsinLine() + 1 ) / 2;

    for ( unsigned int iline = 0; iline < nline; iline++ ) {
      if ( iline == 0 ) {
        bWriter << UTHeaderWord( n_digithits, 1 ).value();
        bWriter << UTHeaderWord( n_digithits, 2 ).value();
      } else {
        bWriter << UTHeaderWord().value();
        bWriter << UTHeaderWord().value();
      }
      for ( unsigned int ilane = 0; ilane < 6; ilane++ ) {
        unsigned int n_digit_in_lane = 0;
        if ( ( n_digithits[5 - ilane] + 1 ) / 2 < iline + 1 ) {
          bWriter << static_cast<unsigned int>( 0 );
        } else {
          unsigned int even_value = 0, odd_value = 0;
          for ( const auto& digit : digitBoard.digits() ) {
            const UTDigit* aDigit  = digit.first;
            UTDAQID        utdaqID = m_readoutTool->channelIDToDAQID( aDigit->channelID() );
            if ( utdaqID.lane() == 5 - ilane ) {
              ++n_digit_in_lane;
              if ( n_digit_in_lane == 2 * ( iline + 1 ) - 1 ) {
                assert( odd_value == 0 );
                odd_value = encode( utdaqID, *aDigit );
              }
              if ( n_digit_in_lane == 2 * ( iline + 1 ) ) {
                assert( even_value == 0 );
                even_value = encode( utdaqID, *aDigit );
              }
            }
          }
          unsigned int adcvalue = shift<0xFFFF0000>( even_value ) | shift<0x0000FFFF>( odd_value );
          bWriter << adcvalue;
        }
      }
    }
  }

} // namespace LHCb::UT
