/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "Event/UTCluster.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTBoardMapping.h"
#include "Kernel/UTChannelID.h"
#include "Kernel/UTDAQBoard.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDAQID.h"
#include "Kernel/UTTell1Board.h"
#include "Kernel/UTTell1ID.h"
#include "Kernel/UTXMLUtils.h"
#include "UTDet/DeUTDetector.h"
#include "fmt/format.h"
#include <algorithm>
#include <fstream>
#include <string>
#include <vector>

/**
 *  Concrete class for things related to the Readout of the UT Tell1 Boards
 */

class UTReadoutTool : public extends<GaudiTool, IUTReadoutTool> {

public:
  /// Constructer
  UTReadoutTool( const std::string& type, const std::string& name, const IInterface* parent );

  /// nBoard
  unsigned int nBoard() const override;

  /// return vector of Tell1IDs
  std::vector<UTTell1ID> boardIDs() const override;

  // convert offline UTChannelID to readout UTDAQID
  UTDAQID channelIDToDAQID( const LHCb::UTChannelID offlineChan ) const override;

  // convert readout UTDAQID to offline UTChannelID
  LHCb::UTChannelID daqIDToChannelID( const UTDAQID daqid ) const override;

  /// convert ITChannelID to DAQ ChannelID
  UTDAQ::chanPair offlineChanToDAQ( const LHCb::UTChannelID aOfflineChan, double isf ) const override;

  /// convert offline interStripFraction to DAQ interStripFraction
  double interStripToDAQ( const LHCb::UTChannelID aOfflineChan, const UTTell1ID aBoardID,
                          const double isf ) const override;

  bool ADCOfflineToDAQ( const LHCb::UTChannelID aOfflineChan, const UTTell1ID aBoardID,
                        LHCb::UTCluster::ADCVector& adcs ) const override;

  /// find the Tell1 board given a board ID
  const UTTell1Board* findByBoardID( const UTTell1ID aBoardID ) const override;

  /// find Tell1 board by storage order
  const UTTell1Board* findByOrder( const unsigned int aValue ) const override;

  /// find DAQ board by storage order
  UTDAQ::Board* findByDAQOrder( const unsigned int aValue ) const override;

  boost::container::static_vector<LHCb::UTChannelID, 6> findBySourceID( const unsigned int aValue ) const override;

  /// Add the mapping of source ID to TELL1 board number
  unsigned int SourceIDToTELLNumber( unsigned int sourceID ) const override;

  /** Add the mapping of source ID to board number for UT */
  const std::map<unsigned int, unsigned int>& SourceIDToTELLNumberMap() const override;

  /// list of the readout sector ids on the board
  std::vector<LHCb::UTChannelID> sectorIDs( const UTTell1ID board ) const override;

  /// apply function to all sectors on board
  void applyToAllBoardSectors( const UTTell1ID                                 board,
                               const std::function<void( DeUTSector const& )>& func ) const override {
    m_tracker->applyToSectors( sectorIDs( board ), func );
  }

  /// service box
  unsigned int nServiceBox() const override;

  /// service box number
  std::string serviceBox( const LHCb::UTChannelID& aChan ) const override;

  /// list of the readout sectors ids in a service box
  std::vector<LHCb::UTChannelID> sectorIDsOnServiceBox( const std::string& serviceBox ) const override;

  /// list of service boxes
  const std::vector<std::string>& serviceBoxes() const override;

  /// Add the mapping of TELL1 board number to source ID
  unsigned int TELLNumberToSourceID( unsigned int TELL ) const override;

  /// print mapping
  void printMapping() const override;

  /// write out the mapping as xml
  StatusCode writeMappingToXML() const override;

  /// finalize
  StatusCode finalize() override;

  /// init
  StatusCode initialize() override;

  /** Add the mapping of board number to source ID for UT */
  const std::map<unsigned int, unsigned int>& TELLNumberToSourceIDMap() const override;

  bool getstripflip() const override;

private:
  void clear();

  bool validate() const;

  std::string footer() const;
  std::string header( const std::string& conString ) const;
  std::string strip( const std::string& conString ) const;

  unsigned int                               m_hybridsPerBoard{0};
  unsigned int                               m_nBoard{0};
  std::vector<std::unique_ptr<UTTell1Board>> m_boards;
  std::map<UTTell1ID, UTTell1Board*>         m_boardsMap;
  std::vector<std::string>                   m_serviceBoxes;
  std::vector<unsigned int>                  m_firstBoardInRegion;

  std::vector<std::unique_ptr<UTDAQ::Board>> m_daqBoards;
  std::map<LHCb::UTChannelID, UTDAQ::Board*> m_sectorToBoardMap;
  std::map<UTDAQID, LHCb::UTChannelID>       m_daqToSectorMap;

  Gaudi::Property<bool>        m_printMapping{this, "printMapping", false};
  DeUTDetector*                m_tracker = nullptr;
  Gaudi::Property<std::string> m_conditionLocation{this, "conditionLocation",
                                                   "/dd/Conditions/ReadoutConf/UT/ReadoutMap"};

  Gaudi::Property<bool>         m_writeXML{this, "writeMappingToXML", false};
  Gaudi::Property<std::string>  m_footer{this, "footer", "</DDDB>"};
  Gaudi::Property<std::string>  m_startTag{this, "startTag", "<condition"};
  Gaudi::Property<std::string>  m_outputFileName{this, "outputFile", "ReadoutMap.xml"};
  std::ofstream                 m_outputFile;
  Gaudi::Property<std::string>  m_author{this, "author", "Joe Bloggs"};
  Gaudi::Property<std::string>  m_tag{this, "tag", "None"};
  Gaudi::Property<std::string>  m_desc{this, "description", "BlahBlahBlah"};
  Gaudi::Property<bool>         m_removeCondb{this, "removeCondb", false};
  Gaudi::Property<unsigned int> m_precision{this, "precision", 16u};
  Gaudi::Property<unsigned int> m_depth{this, "depths", 3u};

private:
  StatusCode createBoards();
  StatusCode createTell1Map();
  bool       m_stripflip = true;

  unsigned int m_nRegionA     = 512;
  unsigned int m_firstStation = 512;
};

using namespace LHCb;
// using namespace LHCb::DAQUT;

DECLARE_COMPONENT( UTReadoutTool )

UTReadoutTool::UTReadoutTool( const std::string& type, const std::string& name, const IInterface* parent )
    : base_class( type, name, parent ) {
  // constructor
  m_boards.reserve( 100 ); // about correct
}

void UTReadoutTool::clear() {
  // clear the boards
  m_boards.clear();
  m_nBoard = 0;
}

StatusCode UTReadoutTool::initialize() {
  // initialization phase...
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) { return Error( "Failed to initialize", sc ); }

  // tracker
  m_tracker = getDet<DeUTDetector>( DeUTDetLocation::location() );

  registerCondition( m_conditionLocation, &UTReadoutTool::createTell1Map );

  registerCondition( m_conditionLocation, &UTReadoutTool::createBoards );

  sc = runUpdate(); // force update
  if ( sc.isFailure() ) return Error( "Failed first UMS update for readout tool", sc );

  if ( m_printMapping ) printMapping();

  return StatusCode::SUCCESS;
}

StatusCode UTReadoutTool::finalize() {

  if ( m_writeXML ) writeMappingToXML().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  return base_class::finalize();
}

StatusCode UTReadoutTool::writeMappingToXML() const {

  // load conditions
  auto* rInfo = getDet<Condition>( m_conditionLocation );

  std::ofstream outputFile( m_outputFileName.value() );
  if ( outputFile.fail() ) { return Warning( "Failed to open output file", StatusCode::FAILURE ); }

  // write the xml headers
  outputFile << header( rInfo->toXml( "", true, m_precision ) ) << '\n';

  // add comments
  std::ostringstream comment;
  UT::XMLUtils::fullComment( comment, m_author, m_tag, m_desc );
  outputFile << comment.str() << '\n';

  std::string temp = strip( rInfo->toXml( "", false, m_precision ) );
  outputFile << temp << "\n\n";

  // footer
  outputFile << footer() << '\n';

  return StatusCode::SUCCESS;
}

unsigned int UTReadoutTool::nBoard() const {
  // number of boards
  return m_nBoard;
}

unsigned int UTReadoutTool::nServiceBox() const { return m_serviceBoxes.size(); }

std::string UTReadoutTool::serviceBox( const LHCb::UTChannelID& aChan ) const {

  // find the board

  static const std::string InValidBox = "Unknown";
  bool                     isFound    = false;
  unsigned int             waferIndex = 999u;
  unsigned int             iBoard     = m_firstBoardInRegion[0];
  while ( ( iBoard != m_nBoard ) && !isFound ) {
    if ( m_boards[iBoard]->isInside( aChan, waferIndex ) ) {
      isFound = true;
    } else {
      ++iBoard;
    }
  } // iBoard
  return ( isFound ? m_boards[iBoard]->serviceBoxes()[waferIndex] : InValidBox );
}

std::vector<UTTell1ID> UTReadoutTool::boardIDs() const {
  std::vector<UTTell1ID> ids;
  ids.reserve( m_boards.size() );
  std::transform( m_boards.begin(), m_boards.end(), std::back_inserter( ids ),
                  []( const auto& b ) { return b->boardID(); } );
  return ids;
}

UTDAQID UTReadoutTool::channelIDToDAQID( const UTChannelID aOfflineChan ) const {

  // need to rebuild the ChannelID without the strip number.
  // unsigned int secnum(aOfflineChan.uniqueSector());

  // info() << "Input channel ID=", aOfflineChan

  UTChannelID secID( aOfflineChan.type(), aOfflineChan.station(), aOfflineChan.layer(), aOfflineChan.detRegion(),
                     aOfflineChan.sector(), 0 );

  // while channelID is offset by 1
  unsigned int tempchan = aOfflineChan.strip();
  if ( tempchan > 0 ) tempchan -= 1;
  auto channelNum = static_cast<UTDAQID::ChannelID>( tempchan );

  auto board = m_sectorToBoardMap.at( secID );

  // need to search for the sector in the boards list: but if there are *two* matches the sector is split in half
  size_t lfound   = 0;
  size_t nmatches = 0;

  for ( size_t si = 0; si < board->sectorIDs().size(); ++si ) {

    if ( board->sectorIDs()[si] == secID ) {
      nmatches++;
      lfound = si;
    }
  }

  // lfound will be set to the higher matching lane number

  return UTDAQID( board->boardID().board(),
                  static_cast<UTDAQID::LaneID>( nmatches > 1 && tempchan < 256 ? lfound - 1 : lfound ), channelNum );
}

UTChannelID UTReadoutTool::daqIDToChannelID( const UTDAQID daqid ) const {

  UTDAQID laneOnly( daqid.board(), daqid.lane() );

  // while channelID is offset by 1
  unsigned int strip = static_cast<unsigned int>( daqid.channel() ) + 1;

  UTChannelID secID = m_daqToSectorMap.at( laneOnly );

  return UTChannelID( secID.type(), secID.station(), secID.layer(), secID.detRegion(), secID.sector(), strip );
}

UTDAQ::chanPair UTReadoutTool::offlineChanToDAQ( const UTChannelID aOfflineChan, double isf ) const {
  // look up region start.....
  unsigned int iBoard     = m_firstBoardInRegion[0];
  unsigned int waferIndex = 999u;

  bool isFound = false;
  while ( ( iBoard != m_nBoard ) && !isFound ) {
    if ( m_boards[iBoard]->isInside( aOfflineChan, waferIndex ) ) {
      isFound = true;
    } else {
      ++iBoard;
    }
  } // iBoard

  if ( !isFound ) {
    return {UTTell1ID( UTTell1ID::nullBoard, false ), 0};
  } else {
    return {m_boards[iBoard]->boardID(), m_boards[iBoard]->offlineToDAQ( aOfflineChan, waferIndex, isf )};
  }
}

double UTReadoutTool::interStripToDAQ( const UTChannelID aOfflineChan, const UTTell1ID aBoardID,
                                       const double isf ) const {
  unsigned int waferIndex = 999u;

  auto   aBoard = findByBoardID( aBoardID );
  double newisf = 0;

  if ( aBoard->isInside( aOfflineChan, waferIndex ) ) {
    unsigned int orientation = aBoard->orientation()[waferIndex];
    if ( orientation == 0 && isf > 0.01 ) {
      newisf = 1 - isf;
    } else {
      newisf = isf;
    }
  } else { // Can not find board!
    newisf = -1;
  }

  return newisf;
}

bool UTReadoutTool::ADCOfflineToDAQ( const UTChannelID aOfflineChan, const UTTell1ID aBoardID,
                                     UTCluster::ADCVector& adcs ) const {
  unsigned int waferIndex = 999u;
  auto         aBoard     = findByBoardID( aBoardID );

  if ( !aBoard->isInside( aOfflineChan, waferIndex ) ) return false; // can not find board!

  if ( aBoard->orientation()[waferIndex] == 0 ) { std::reverse( std::begin( adcs ), std::end( adcs ) ); }
  return true;
}

const UTTell1Board* UTReadoutTool::findByBoardID( const UTTell1ID aBoardID ) const {
  // find by board id
  auto i = m_boardsMap.find( aBoardID );
  return i != m_boardsMap.end() ? i->second : nullptr;
}

const UTTell1Board* UTReadoutTool::findByOrder( const unsigned int aValue ) const {
  // find by order
  return aValue < m_nBoard ? m_boards[aValue].get() : nullptr;
}

UTDAQ::Board* UTReadoutTool::findByDAQOrder( const unsigned int aValue ) const {
  // find by order
  return aValue < m_nBoard ? m_daqBoards[aValue].get() : nullptr;
}

boost::container::static_vector<LHCb::UTChannelID, 6> UTReadoutTool::findBySourceID( const unsigned int aValue ) const {

  boost::container::static_vector<LHCb::UTChannelID, 6> mBoardMap;
  if ( m_nBoard == 216 && aValue < m_nBoard )
    mBoardMap = ( m_daqBoards[aValue].get() )->UTDAQ::Board::sectorIDs(); // for new ReadoutMap
  else if ( m_nBoard != 216 && aValue < m_nBoard )
    mBoardMap = ( m_boards[aValue].get() )->sectorIDs(); // for old ReadoutMap
  return mBoardMap;
}

void UTReadoutTool::printMapping() const {
  // dump out the readout mapping
  info() << "print mapping for: " << name() << " tool" << endmsg;
  info() << " Number of boards " << m_nBoard << endmsg;
  for ( const auto& b : m_boards ) info() << *b << endmsg;
}

/// Add the mapping of source ID to TELL1 board number
unsigned int UTReadoutTool::SourceIDToTELLNumber( unsigned int sourceID ) const {
  return SourceIDToTELLNumberMap().find( sourceID )->second;
}

/// Add the mapping of TELL1 board number to source ID
unsigned int UTReadoutTool::TELLNumberToSourceID( unsigned int TELL ) const {
  return ( m_nBoard == 216 ) ? TELL - 1 : TELLNumberToSourceIDMap().find( TELL )->second;
}

bool UTReadoutTool::validate() const {
  // validate the map - every sector must go somewhere !
  return m_tracker->none_of_sectors( [this]( auto const& s ) {
    UTChannelID chan     = s.elementID();
    auto        chanPair = offlineChanToDAQ( chan, 0.0 );
    return chanPair.first == UTTell1ID( UTTell1ID::nullBoard, false );
  } );
}

std::vector<LHCb::UTChannelID> UTReadoutTool::sectorIDs( const UTTell1ID board ) const {

  std::vector<LHCb::UTChannelID> sectors;
  sectors.reserve( 8 );
  auto theBoard = findByBoardID( board );
  if ( theBoard ) {
    sectors.insert( sectors.begin(), theBoard->sectorIDs().begin(), theBoard->sectorIDs().end() );
  } else {
    Error( "Failed to find Board", StatusCode::SUCCESS, 100 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
  return sectors;
}

std::vector<LHCb::UTChannelID> UTReadoutTool::sectorIDsOnServiceBox( const std::string& serviceBox ) const {
  // loop over all boards
  std::vector<LHCb::UTChannelID> sectors;
  sectors.reserve( 16 );
  for ( const auto& board : m_boards ) {
    const auto& sectorVec = board->sectorIDs();
    const auto& sBoxes    = board->serviceBoxes();
    for ( unsigned int iS = 0u; iS < board->nSectors(); ++iS ) {
      if ( sBoxes[iS] == serviceBox ) sectors.push_back( sectorVec[iS] );
    } // iS
  }   // iterB
  return sectors;
}

const std::vector<std::string>& UTReadoutTool::serviceBoxes() const { return m_serviceBoxes; }

std::string UTReadoutTool::footer() const {
  std::string temp = m_footer;
  temp.insert( 0, "</catalog>" );
  return temp;
}

std::string UTReadoutTool::header( const std::string& conString ) const {
  // get the header
  auto startpos = conString.find( m_startTag );
  auto temp     = conString.substr( 0, startpos );
  temp.insert( startpos, "<catalog name=\"ReadoutSectors\">" );

  // correct the location of the DTD
  if ( m_removeCondb ) {
    UT::XMLUtils::replace( temp, "conddb:", "" );
    std::string location;
    for ( unsigned int i = 0; i < m_depth; ++i ) location += "../";
    auto pos = temp.find( "/DTD/" );
    temp.insert( pos, location );
    UT::XMLUtils::replace( temp, "//", "/" );
  }

  return temp;
}

std::string UTReadoutTool::strip( const std::string& conString ) const {
  auto startpos = conString.find( m_startTag );
  auto endpos   = conString.find( m_footer );
  return conString.substr( startpos, endpos - startpos );
}

// Add the mapping of source ID to TELL1 board number
const std::map<unsigned int, unsigned int>& UTReadoutTool::SourceIDToTELLNumberMap() const {
  return UTBoardMapping::UTSourceIDToNumberMap();
}

// Add the mapping of TELL1 board number to source ID
const std::map<unsigned int, unsigned int>& UTReadoutTool::TELLNumberToSourceIDMap() const {
  return UTBoardMapping::UTNumberToSourceIDMap();
}

StatusCode UTReadoutTool::createTell1Map() {
  auto rInfo = getDet<Condition>( m_conditionLocation );
  if ( rInfo->exists( "nTell40InUT" ) ) {
    const auto ntell = rInfo->param<int>( "nTell40InUT" );

    UTBoardMapping::ClearUTMap();

    for ( int i = 0; i < 2 * ntell; ++i ) { UTBoardMapping::AddUTMapEntry( i, i ); }

    return StatusCode::SUCCESS;
  } else {
    const auto& layers = rInfo->param<std::vector<std::string>>( "layers" );

    UTBoardMapping::ClearUTMap();

    unsigned int sourceIDBase = 0;
    for ( const auto& l : layers ) {
      std::string tell1Loc = l + "TELL1";
      if ( rInfo->exists( tell1Loc ) ) {
        //      printf("Extracting TELL1 map from %s\n", tell1Loc.c_str());

        const auto& tell1 = rInfo->param<std::vector<int>>( tell1Loc );
        for ( unsigned int i = 0; i < tell1.size(); i++ ) {
          UTBoardMapping::AddUTMapEntry( sourceIDBase + i, tell1.at( i ) );
        }
      }
      sourceIDBase += 64;
    }

    return StatusCode::SUCCESS;
  }
}

StatusCode UTReadoutTool::createBoards() {

  clear();

  // load conditions
  auto rInfo = getDet<Condition>( m_conditionLocation );

  if ( rInfo->exists( "nTell40InUT" ) ) {
    // New style conditions map
    const auto ntell = rInfo->param<int>( "nTell40InUT" );
    m_nBoard         = ntell * 2;

    // fill the new style map in this tool
    for ( unsigned int i = 0; i < m_nBoard; ++i ) {

      const auto& sectorlist = rInfo->param<std::vector<int>>( fmt::format( "UTTell{}Map", i ) );
      assert( !sectorlist.empty() );

      auto daqBoard = std::make_unique<UTDAQ::Board>( static_cast<UTDAQID::BoardID>( i ) );
      m_daqBoards.push_back( std::move( daqBoard ) );

      for ( auto& s : sectorlist ) {
        UTChannelID sectorID( (unsigned int)s );
        UTDAQID     thisDAQID        = m_daqBoards.back()->addSector( sectorID, 1 );
        m_sectorToBoardMap[sectorID] = m_daqBoards.back().get();
        m_daqToSectorMap[thisDAQID]  = sectorID;
      }
    }

    // this is the old style map, which we keep for now to put off breaking things
    const auto   nBoards = rInfo->paramAsIntVect( "nBoardsPerQuarter" );
    unsigned int i       = 0;
    for ( unsigned int iReg = 0; iReg < nBoards.size(); ++iReg ) {

      m_firstBoardInRegion.push_back( m_boards.size() );

      for ( int j = 0; j < nBoards[iReg]; ++j ) {
        const auto& sectorlist = rInfo->param<std::vector<int>>( fmt::format( "UTTell{}Map", i ) );
        assert( !sectorlist.empty() );

        const UTTell1ID anID( iReg, j, true );
        auto            aBoard = std::make_unique<UTTell1Board>( anID, 512 );
        // info() << "Make board for " << iReg << ", " << j << ", " << i << " = " << anID << ". sectors:" << endmsg;

        for ( auto& s : sectorlist ) {
          UTChannelID sectorID( (unsigned int)s );
          // info() << "  " << s <<" "<<sectorID.station()<<" ; "<<sectorID.layer()<<" ; "<<sectorID.detRegion()<<" ;
          // "<<sectorID.sector()<< endmsg;
          aBoard->addSector( sectorID, 1, "CB3" );
        }

        m_boards.push_back( std::move( aBoard ) );

        if ( m_boardsMap.find( anID ) == m_boardsMap.end() ) { m_boardsMap[anID] = m_boards.back().get(); }

        ++i;
      }
    }

    m_serviceBoxes.emplace_back( "CB3" );
  } else {
    // old style conditions map

    const auto layers  = rInfo->param<std::vector<std::string>>( "layers" );
    const auto nBoards = rInfo->paramAsIntVect( "nBoardsPerLayer" );
    m_stripflip        = false;

    m_hybridsPerBoard           = rInfo->param<int>( "hybridsPerBoard" );
    m_nRegionA                  = rInfo->param<int>( "nRegionsInUTa" );
    const auto nStripsPerHybrid = UTDAQ::nStripsPerBoard / m_hybridsPerBoard;

    for ( unsigned int iReg = 0; iReg < layers.size(); ++iReg ) {
      assert( iReg < layers.size() );
      assert( iReg < nBoards.size() );

      m_firstBoardInRegion.push_back( m_boards.size() );
      m_nBoard += nBoards[iReg];

      const auto& tMap         = rInfo->param<std::vector<int>>( layers[iReg] );
      const auto& orientation  = rInfo->param<std::vector<int>>( layers[iReg] + "HybridOrientation" );
      const auto& serviceBoxes = rInfo->param<std::vector<std::string>>( layers[iReg] + "ServiceBox" );

      unsigned int vecLoc = 0;
      assert( !tMap.empty() );
      if ( 0 == iReg ) { m_firstStation = UTChannelID( tMap[0] ).station(); }

      for ( unsigned int iBoard = 0; iBoard < (unsigned int)nBoards[iReg]; ++iBoard ) {

        // make new board for old style map
        const UTTell1ID anID( iReg, iBoard, true );
        auto            aBoard = std::make_unique<UTTell1Board>( anID, nStripsPerHybrid );

        // make new board for new style map
        auto daqBoard = std::make_unique<UTDAQ::Board>( static_cast<UTDAQID::BoardID>( anID.id() ) );
        m_daqBoards.push_back( std::move( daqBoard ) );

        for ( unsigned iH = 0; iH < m_hybridsPerBoard; ++iH, ++vecLoc ) {
          assert( vecLoc < tMap.size() );
          assert( vecLoc < orientation.size() );
          assert( vecLoc < serviceBoxes.size() );
          if ( 0 != tMap[vecLoc] ) { // skip strange 0's in conditions vector !!

            // old style
            UTChannelID sectorID( (unsigned int)tMap[vecLoc] );
            aBoard->addSector( sectorID, (unsigned int)orientation[vecLoc], serviceBoxes[vecLoc] );

            // new style
            UTDAQID thisDAQID            = m_daqBoards.back()->addSector( sectorID, 1 );
            m_sectorToBoardMap[sectorID] = m_daqBoards.back().get();
            m_daqToSectorMap[thisDAQID]  = sectorID;

            // add to the list of service boxs if not already there
            if ( std::find( m_serviceBoxes.begin(), m_serviceBoxes.end(), serviceBoxes[vecLoc] ) ==
                 m_serviceBoxes.end() ) {
              m_serviceBoxes.push_back( serviceBoxes[vecLoc] );
            }
          }
        } // iH

        m_boards.push_back( std::move( aBoard ) );

        if ( m_boardsMap.find( anID ) == m_boardsMap.end() ) { m_boardsMap[anID] = m_boards.back().get(); }

      } // boards per region
    }   // regions
  }

  // validate the mapping --> all sectors should go somewhere !
  return ( validate() ? StatusCode::SUCCESS : Error( "Failed to validate mapping" ) );
}

bool UTReadoutTool::getstripflip() const { return m_stripflip; }
