/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include <iostream>
#include <string>

/** @class UTADCWord UTADCWord.h "UTDAQ/UTADCWord.h"
 *
 *  Class for encapsulating header word in RAW data format
 *  for the UT Si detectors.
 *
 *  @author Xuhao Yuan (based on codes by M.Needham)
 *  @date   2020-06-17
 */

class UTADCWord final {
  // FIXME: if maxstrip is 512, why is `strip` not 0x3FE0 ???
  enum class mask { adc = 0x1F, strip = 0xFFE0 };
  static constexpr unsigned int maxstrip = 512;

  template <mask m>
  [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    return ( i & static_cast<unsigned int>( m ) ) >> b;
  }

  template <mask m>
  [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
    constexpr auto b =
        __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
    auto v = ( i << static_cast<unsigned int>( b ) );
    assert( extract<m>( v ) == i );
    return v;
  }

public:
  /** constructer with int
    @param value
    */
  explicit constexpr UTADCWord( unsigned int value ) : m_value( value ) {}

  constexpr UTADCWord( unsigned int value, unsigned int nlane ) : m_value( value ), m_lane( nlane ){};

  /** The actual value
    @return value
    */
  [[nodiscard]] constexpr unsigned int value() const { return m_value; };

  [[nodiscard]] constexpr unsigned int channelID() const { return extract<mask::strip>( m_value ) + maxstrip * m_lane; }

  [[nodiscard]] constexpr unsigned int adc() const { return extract<mask::adc>( m_value ); }

  [[nodiscard]] constexpr unsigned int fracStripBits() const { return 0; }

  [[nodiscard]] constexpr unsigned int pseudoSizeBits() const { return 0; }

  [[nodiscard]] constexpr bool hasHighThreshold() const { return true; }

  /** Operator overloading for stringoutput */
  friend std::ostream& operator<<( std::ostream& s, const UTADCWord& obj ) { return obj.fillStream( s ); }

  /** Fill the ASCII output stream */
  std::ostream& fillStream( std::ostream& s ) const {
    return s << "{ "
             << " value:\t" << value() << " }\n";
  }

private:
  unsigned int m_value = 0;
  unsigned int m_lane  = 0;
};
