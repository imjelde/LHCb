/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

/** @namespace UTRawBankMap UTRawBankMap.h SKernel/UTRawBank
 *
 *  Map string to corresponding bank type enum
 *  @author A. Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

#include "Event/RawBank.h"
#include <string_view>

namespace UTRawBankMap {

  inline LHCb::RawBank::BankType stringToType( std::string_view type ) {
    // use the Velo as NULL
    // clang-format off
    return ( type == "UT" )         ? LHCb::RawBank::UT
         : ( type == "UTFull" )     ? LHCb::RawBank::UTFull
         : ( type == "UTError" )    ? LHCb::RawBank::UTError
         : ( type == "UTPedestal" ) ? LHCb::RawBank::UTPedestal
         :                            LHCb::RawBank::Velo;
    // clang-format on
  }

} // namespace UTRawBankMap
