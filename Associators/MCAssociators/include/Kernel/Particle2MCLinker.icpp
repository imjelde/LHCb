/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "Event/MCParticle.h"
#include "Event/Particle.h"
#include "Kernel/Particle2MCLinker.h"
#include <numeric>

#include "GaudiKernel/ThreadLocalContext.h"

//-----------------------------------------------------------------------------
// Implementation file for class : Particle2MCLink
//
// 2004-04-29 : Philippe Charpentier
//-----------------------------------------------------------------------------

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
//=============================================================================
template <typename SOURCE, typename PARENT>
StatusCode Object2MCLinker<SOURCE, PARENT>::setAlgorithm( const int method, std::vector<std::string> containerList ) {
  return setAlgorithm( Particle2MCMethod::algType[method], Particle2MCMethod::extension[method],
                       std::move( containerList ) );
}
template <typename SOURCE, typename PARENT>
StatusCode Object2MCLinker<SOURCE, PARENT>::setAlgorithm( const std::string& algType, const std::string& extension,
                                                          std::vector<std::string> containerList ) {
  m_containerList = std::move( containerList );
  if ( m_linkerAlg ) {
    if ( algType != m_linkerAlgType ) {
      // We change the algorithm on the fly... risky!
      m_linkerAlg = nullptr;
    } else {
      return setAlgInputData( m_linkerAlg, m_containerList );
    }
  }
  m_linkerAlgType = algType;
  m_extension     = extension;
  return StatusCode::SUCCESS;
}
template <typename SOURCE, typename PARENT>
StatusCode Object2MCLinker<SOURCE, PARENT>::locateAlgorithm( const std::string& algType, const std::string& algName,
                                                             IAlgorithm*&                    alg,
                                                             const std::vector<std::string>& inputData ) {
  if ( hasValidParent() )
    debug() << "==> Calling locateAlgorithm with type " << algType << ", name " << algName << endmsg;
  if ( alg ) return StatusCode::SUCCESS;
  if ( algType.empty() ) {
    ++m_noAlgTypeErr;
    return StatusCode::FAILURE;
  }

  SmartIF<IAlgManager> algMgr( svcLocator() );
  if ( !algMgr.isValid() ) {
    ++m_noIAlgMgrErr;
    return StatusCode::FAILURE;
  }
  // check the existence of the algorithm
  for ( const auto& ia : algMgr->getAlgorithms() ) {
    if ( 0 == ia ) { continue; }
    if ( ia->name() != algName ) { continue; }
    // algorithm is found !
    alg = ia;
    alg->addRef();
    // Set its jobOptions
    return setAlgInputData( alg, inputData );
  }
  // algorithm is noT found: try to create it!
  StatusCode sc = algMgr->createAlgorithm( algType, algName, alg );
  //

  if ( sc.isFailure() ) {
    ++m_algoCreationErr;
    return sc;
  }
  IProperty* prop = dynamic_cast<IProperty*>( alg );
  sc              = prop->setProperty( "Context", this->context() );
  if ( !sc.isSuccess() ) {
    ++m_setContextErr;
    return sc;
  }

  // add the reference to the new algorithm
  alg->addRef();

  // Now initialise the algorithm
  sc = alg->sysInitialize();
  if ( sc.isFailure() ) {
    ++m_algInitErr;
    return sc;
  }
  // Set jobOptions to the algorithm, supersede options file
  sc = setAlgInputData( alg, inputData );
  if ( sc.isFailure() ) {
    auto err = std::accumulate( inputData.begin(), inputData.end(),
                                "Error setting InputData property for algorithm " + algName + " to ",
                                []( std::string e, const std::string& l ) { return e + ", " + l; } );
    error() << err << endmsg;
  }
  return sc;
}
template <typename SOURCE, typename PARENT>
std::string Object2MCLinker<SOURCE, PARENT>::getGaudiRootInTES() {
  return m_parent ? m_parent->rootInTES() : std::string{};
}
template <typename SOURCE, typename PARENT>
StatusCode Object2MCLinker<SOURCE, PARENT>::setAlgInputData( IAlgorithm*&                    alg,
                                                             const std::vector<std::string>& inputData ) {
  /* If requested, pass an InputData property to the algorithm */
  if ( inputData.empty() ) return StatusCode::SUCCESS;

  IProperty* prop = dynamic_cast<IProperty*>( alg );

  if ( !prop ) {
    ++m_ipropWarn;
    return StatusCode::SUCCESS;
  }
  // Context property is contagious (from myMother)
  auto sc = prop->setProperty( "Context", context() );
  if ( !sc.isSuccess() ) {
    ++m_setContextErr;
    return sc;
  }

  // Set OutputTable property to "" avoiding Relations table
  sc = prop->setProperty( "OutputTable", "" );
  if ( !sc.isSuccess() ) {
    ++m_setOutTableErr;
    return sc;
  }
  std::string propString = "[";
  std::string sep        = "\"";
  for ( const auto& inpStr : inputData ) {
    propString += sep;
    if ( std::string::npos == inpStr.find( "/Particles" ) && std::string::npos == inpStr.find( "/ProtoP" ) )
      propString += "/Particles";
    propString += inpStr;
    sep = "\",\"";
  }
  propString = propString + "\"]";
  sc         = prop->setProperty( "InputData", propString );
  if ( sc.isFailure() ) {
    ++m_setInputErr;
    return sc;
  }
  debug() << "Property InputData set to " << propString << " in algo " << alg->name() << endmsg;
  const std::string rit( getGaudiRootInTES() );
  if ( !rit.empty() ) {
    std::string check; // check current RootInTES, if it exists
    sc = prop->getProperty( "RootInTES", check );
    if ( sc.isSuccess() && !check.empty() && check.compare( rit ) != 0 ) { // if and only if RootInTES is already set
                                                                           // and different from parent, throw warning
      this->warning() << "Algo " << alg->name() << " has RootInTES " << check
                      << " which is different from its parent: " << rit << "\n"
                      << "Replacing it! Consider setting it inside " << name() << endmsg;
      return StatusCode::RECOVERABLE;
    }
    sc = prop->setProperty( "RootInTES", rit );
    if ( sc.isFailure() ) {
      ++m_setRITErr;
      return sc;
    }
    debug() << "Property RootInTES set to " << rit << " in algo " << alg->name() << endmsg;
  }
  return sc;
}

template <typename SOURCE, typename PARENT>
bool Object2MCLinker<SOURCE, PARENT>::notFound( const std::string& contName ) {
  To test( evtSvc(), nullptr, contName + m_extension );
  return test.notFound();
}
template <typename SOURCE, typename PARENT>
bool Object2MCLinker<SOURCE, PARENT>::notFound() {
  return std::all_of( m_containerList.begin(), m_containerList.end(),
                      [&]( const std::string& s ) { return notFound( s ); } );
}
template <typename SOURCE, typename PARENT>
void Object2MCLinker<SOURCE, PARENT>::createLinks( const std::string& contName ) {
  // First find the contname is in the list
  if ( m_containerList.end() == std::find( m_containerList.begin(), m_containerList.end(), contName ) ) {
    // Container was not in the list... add it!
    m_containerList.push_back( contName );
    if ( m_linkerAlg ) {
      debug() << "    Add " << contName << " to InputData of " << m_linkerAlg->name() << endmsg;
      StatusCode sc = setAlgInputData( m_linkerAlg, m_containerList );
      if ( sc.isFailure() ) {
        this->error() << "Error adding " << contName << " to InputData of " << m_linkerAlg->name() << endmsg;
      }
    }
  }
  for ( auto contIt = m_containerList.begin(); m_containerList.end() != contIt; contIt++ ) {
    const std::string name = *contIt + m_extension;
    To                test( evtSvc(), nullptr, name );
    if ( test.notFound() && "" != m_linkerAlgType ) {
      debug() << "Link table for " << name << " not found, creating it" << endmsg;
      if ( !m_linkerAlg ) {
        // Create the algorithm, as it has to be called
        StatusCode sc =
            locateAlgorithm( m_linkerAlgType, this->name() + "." + m_linkerAlgType, m_linkerAlg, m_containerList );
        if ( !sc.isSuccess() ) {
          err() << "Cannot locate algorithm of type " << m_linkerAlgType << endmsg;
          break;
        }
      }
      // Call the algorithm to get the table done
      debug() << "==> Executing Linker builder algorithm " << m_linkerAlg->name() << endmsg;
      m_linkerAlg->sysExecute( Gaudi::Hive::currentContext() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      test = To( evtSvc(), nullptr, name );
    }
    if ( *contIt == contName ) m_linkTo = test;
  }
}
template <typename SOURCE, typename PARENT>
typename Object2MCLinker<SOURCE, PARENT>::To* Object2MCLinker<SOURCE, PARENT>::getLink( const std::string& contName ) {
  // If the lists are empty, create them
  std::string name = contName + m_extension;
  To          test( evtSvc(), nullptr, name );
  if ( test.notFound() ) {
    createLinks( contName );
  } else {
    m_linkTo = test;
  }
  return &m_linkTo;
}
template <typename SOURCE, typename PARENT>
int Object2MCLinker<SOURCE, PARENT>::associatedMCP( const SOURCE* obj ) {
  int n = 0;
  for ( const auto* mcPart = firstMCP( obj ); mcPart; mcPart = nextMCP() ) { ++n; }
  return n;
}
template <typename SOURCE, typename PARENT>
typename Object2MCLinker<SOURCE, PARENT>::ToRange Object2MCLinker<SOURCE, PARENT>::rangeFrom( const SOURCE* part ) {
  ToRange associatedRange;
  for ( const auto* tempMCP = firstMCP( part ); tempMCP; tempMCP = nextMCP() ) {
    associatedRange.emplace_back( tempMCP, weightMCP() );
  }
  return associatedRange;
}

template <typename SOURCE, typename PARENT>
const LHCb::MCParticle* Object2MCLinker<SOURCE, PARENT>::firstMCP( const SOURCE* part, double& weight ) {
  const LHCb::MCParticle* mcPart = firstMCP( part );
  weight                         = ( mcPart ? weightMCP() : 0. );
  return mcPart;
}

template <typename SOURCE, typename PARENT>
const LHCb::MCParticle* Object2MCLinker<SOURCE, PARENT>::firstMCP( const SOURCE* part ) {
  if ( !hasValidParent() ) return nullptr;

  std::string contName = containerName( dynamic_cast<const ContainedObject*>( part ) );
  if ( contName.compare( 0, 7, "/Event/" ) == 0 ) { contName = contName.substr( 7 ); }
  if ( contName.empty() ) {
    m_linkTo = To( evtSvc(), nullptr, "" );
    return nullptr;
  }
  To* link = getLink( contName );

  return link ? link->first( part ) : nullptr;
}

// Helper methods to create a LinkerWithKey table if needed
template <typename SOURCE, typename PARENT>
typename Object2MCLinker<SOURCE, PARENT>::Linker*
Object2MCLinker<SOURCE, PARENT>::linkerTable( const std::string& name ) {
  if ( !hasValidParent() ) return nullptr;
  To test( evtSvc(), nullptr, name );
  return linkerTable( name, test );
}

template <typename SOURCE, typename PARENT>
typename Object2MCLinker<SOURCE, PARENT>::Linker* Object2MCLinker<SOURCE, PARENT>::linkerTable( const std::string& name,
                                                                                                To& test ) {
  if ( !hasValidParent() ) return nullptr;

  if ( test.notFound() ) {
    m_linkerTable = typename Object2MCLinker<SOURCE, PARENT>::Linker( evtSvc(), 0, name );
    debug() << "Linker table " << name << " created" << endmsg;
    test = To( evtSvc(), nullptr, name );
    return &m_linkerTable;
  } else {
    debug() << "Linker table " << name << " found" << endmsg;
  }
  return nullptr;
}
template <typename SOURCE, typename PARENT>
bool Object2MCLinker<SOURCE, PARENT>::checkAssociation( const SOURCE* obj, const LHCb::MCParticle* mcPart ) {
  for ( const auto* mcp = firstMCP( obj ); mcp; mcp = nextMCP() ) {
    if ( mcp == mcPart ) return true;
  }
  return false;
}
