/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
//// TODO: split this up into RawEvent -> [ loc, Out_i ]
//         and then use SuperAlgorithm to have a vector over i....
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedCluster.h"
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/GaudiAlgorithm.h"
#include "HltRawBankDecoderBase.h"
#include "PackedDataChecksum.h"
#include "RZip.h"

/** @class HltPackedDataDecoder HltPackedDataDecoder.h
 *  Algorithm that reads packed objects from raw banks.
 *
 *  @author Rosen Matev, Sean Benson
 *  @date   2016-01-03
 */

namespace LHCb::Hlt::PackedData {

  class Decoder : public HltRawBankDecoderBase {
  public:
    /// Standard constructor
    Decoder( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode execute() override; ///< Algorithm execution

  private:
    /// Property giving the mapping between packed containers and containers
    Gaudi::Property<std::map<std::string, std::string>> m_containerMap{this, "ContainerMap"};
    /// Property enabling calculation and print of checksums
    Gaudi::Property<bool> m_enableChecksum{this, "EnableChecksum", false};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_missing_raw{this, "Raw event not found"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_bad_version{
        this, "HltPackedData raw bank version is not supported."};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_not_sequential{
        this, "Part IDs for HltPackedData banks are not sequential."};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_decompression_failure{this,
                                                                                "Failed to decompress HltPackedData."};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_compression{this, "Unknown compression method."};
    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unknown_packed_location{
        this, "Packed object location not found in ANNSvc. Skipping this link, unpacking may fail"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_no_packed_bank{
        this, "No HltPackedData raw bank (the DstData bank) in raw event."};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( Decoder, "HltPackedDataDecoder" )

  namespace {

    struct ObjectHeader {
      uint32_t             classID;
      int32_t              locationID;
      std::vector<int32_t> linkLocationIDs;
      uint32_t             storedSize;
    };

    /// create map between CLIDs and load functions
    template <typename T>
    std::pair<std::unique_ptr<DataObject>, size_t> loadObject( PackedDataInBuffer& buffer, std::string_view location,
                                                               PackedDataChecksum* checksum ) {
      auto object     = std::make_unique<T>();
      auto nBytesRead = buffer.load( *object );
      if ( checksum ) checksum->processObject( *object, location );
      return {std::move( object ), nBytesRead};
    }

    template <typename... PackedData>
    auto createLoaders() {
      std::map<CLID, std::pair<std::unique_ptr<DataObject>, size_t> ( * )( PackedDataInBuffer&, std::string_view,
                                                                           PackedDataChecksum* )>
          loaders;
      ( loaders.emplace( PackedData::classID(), &loadObject<PackedData> ), ... );
      return loaders;
    }

    const auto loaders = createLoaders<PackedTracks, PackedRichPIDs, PackedMuonPIDs, PackedCaloHypos,
                                       PackedProtoParticles, PackedCaloClusters, PackedParticles, PackedVertices,
                                       PackedRecVertices, PackedFlavourTags, PackedRelations, PackedWeightedRelations,
                                       PackedRelatedInfoRelations, PackedCaloDigits, PackedClusters, PackedCaloAdcs>();

  } // namespace

  Decoder::Decoder( const std::string& name, ISvcLocator* pSvcLocator ) : HltRawBankDecoderBase( name, pSvcLocator ) {
    // new for decoders, initialize search path, and then call the base method
    m_rawEventLocations = {
        RawEventLocation::Trigger,
        RawEventLocation::Copied,
        RawEventLocation::Default,
    };
    initRawEventSearch();
    // The default m_sourceID=0 triggers a warning in HltRawBankDecoderBase::initialize
    // Since we only care about HLT2 persistence, set it explicitly:
    setProperty( "SourceID", SourceIDs::Hlt2 ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    m_rawEventLocations.push_back( RawEventLocation::PersistReco );
  }

  StatusCode Decoder::execute() {

    // Hard coded - HltPackedDataDecoder will be replaced for HltPackedBufferDecoder
    auto const* view     = getIfExists<RawBank::View>( "/Event/DAQ/RawBanks/DstData" );
    auto        rawBanks = std::vector<const RawBank*>{begin( *view ), end( *view )};
    const auto* rawBank0 = rawBanks.front();

    if ( !view || view->empty() ) {
      ++m_no_packed_bank;
      return StatusCode::SUCCESS;
    }

    // Check we know how to decode this version
    if ( rawBank0->version() < 2 || rawBank0->version() > kVersionNumber ) {
      if ( msgLevel( MSG::VERBOSE ) ) {
        debug() << "rawBank0->version() " << rawBank0->version() << endmsg;
        debug() << "rawBank0->type() " << rawBank0->type() << endmsg;
      }

      ++m_bad_version;
      return StatusCode::FAILURE;
    }

    // Put the banks into the right order
    std::sort( begin( rawBanks ), end( rawBanks ),
               []( const RawBank* lhs, const RawBank* rhs ) { return partID( *lhs ) < partID( *rhs ); } );

    // Check that the banks have sequential part IDs
    unsigned int payloadSize = 0;
    for ( auto [i, rawBank] : range::enumerate( rawBanks ) ) {
      if ( partID( *rawBank ) != i ) {
        ++m_not_sequential;
        return StatusCode::SUCCESS; // FIXME: really???
      }
      payloadSize += rawBank->size();
    }

    // Collect the data into a contiguous buffer
    std::vector<uint8_t> payload;
    payload.reserve( payloadSize );
    for ( const auto* bank : rawBanks ) {
      auto r = bank->range<uint8_t>();
      payload.insert( end( payload ), begin( r ), end( r ) );
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Compression " << compression( *rawBank0 ) << ", payload size " << payload.size() << endmsg;
    }

    /// Buffer for de-serialization of the packed objects
    PackedDataInBuffer buffer;

    // Decompress the payload and load into the buffer
    switch ( compression( *rawBank0 ) ) {
    case Compression::NoCompression: {
      buffer.init( payload );
      break;
    }
    case Compression::ZLIB:
    case Compression::LZMA:
    case Compression::LZ4:
    case Compression::ZSTD: {
      if ( !buffer.init( payload, true ) ) {
        ++m_decompression_failure;
        return StatusCode::FAILURE;
      }
      break;
    }
    default: {
      ++m_unknown_compression;
      return StatusCode::SUCCESS; // FIXME: really???
      break;
    }
    }

    // Get the map of ids to locations (may differ between events)
    auto* rawEvent = findFirstRawEvent();
    if ( !rawEvent ) {
      ++m_missing_raw;
      return StatusCode::FAILURE;
    }
    const auto& locationsMap = packedObjectLocation2string( tck( *rawEvent ) );

    /// Helper for computing checksums
    std::optional<PackedDataChecksum> checksum;
    if ( m_enableChecksum ) checksum.emplace();

    // Do the actual loading of the objects
    ObjectHeader header;
    while ( !buffer.eof() ) {
      // Load the metadata
      Packer::io( buffer, header );

      auto locationIt = locationsMap.find( header.locationID );
      if ( locationIt == end( locationsMap ) ) {
        error() << "Packed object location not found in ANNSvc for id=" << header.locationID
                << ". Skipping reading the container!" << endmsg;
        buffer.skip( header.storedSize );
        continue;
      }
      const auto& containerPath = locationIt->second;

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Reading " << header.storedSize << " bytes "
                << "for object with CLID " << header.classID << " into TES location " << containerPath << endmsg;
      }

      const auto it = loaders.find( header.classID );
      if ( it == loaders.end() ) {
        error() << "Unknown class ID " << header.classID << endmsg;
        buffer.skip( header.storedSize );
        continue;
      }
      // Load the packed object
      auto [dataObject, readObjectSize] = ( it->second )( buffer, containerPath.str(), get_pointer( checksum ) );

      if ( readObjectSize != header.storedSize ) {
        fatal() << "Loading of object (CLID=" << header.classID << ") "
                << "to " << containerPath << " "
                << "consumed " << readObjectSize << " bytes, "
                << "but " << header.storedSize << " were stored!" << endmsg;
        return StatusCode::FAILURE;
      }

      auto object = put( std::move( dataObject ), containerPath );

      // Restore the links to other containers on the TES
      for ( const auto& linkLocationID : header.linkLocationIDs ) {
        auto persistedLocation = locationsMap.find( linkLocationID );
        if ( persistedLocation == end( locationsMap ) ) {
          ++m_unknown_packed_location;
          continue;
        }

        auto location       = persistedLocation->second;
        auto mappedLocation = m_containerMap.find( persistedLocation->second );
        if ( mappedLocation != end( m_containerMap ) ) { location = mappedLocation->second; }

        object->linkMgr()->addLink( location, nullptr );
      }
    }

    if ( msgLevel( MSG::DEBUG ) && checksum ) {
      for ( const auto& x : checksum->checksums() )
        debug() << "Packed data checksum for '" << x.first << "' = " << x.second << endmsg;
    }

    return StatusCode::SUCCESS;
  }
} // namespace LHCb::Hlt::PackedData
