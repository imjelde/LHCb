/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "HltSourceID.h"
#include <string_view>

namespace LHCb::Hlt::DAQ {
  namespace {
    using namespace std::literals;
    constexpr auto names = std::array{std::pair{SourceID::Dummy, "Dummy"sv}, std::pair{SourceID::Hlt, "Hlt"sv},
                                      std::pair{SourceID::Hlt1, "Hlt1"sv}, std::pair{SourceID::Hlt2, "Hlt2"sv},
                                      std::pair{SourceID::Spruce, "Spruce"sv}};
  } // namespace

  StatusCode parse( SourceID& id, const std::string& in ) {
    auto sv = std::string_view{in};
    if ( sv.size() > 1 && ( sv.front() == '\'' || sv.front() == '\"' ) && sv.front() == sv.back() )
      sv = sv.substr( 1, sv.size() - 2 );
    auto i = std::find_if( names.begin(), names.end(), [sv]( const auto& p ) { return p.second == sv; } );
    if ( i == names.end() ) return StatusCode::FAILURE;
    id = i->first;
    return StatusCode::SUCCESS;
  }

  std::string toString( SourceID id ) {
    auto i = std::find_if( names.begin(), names.end(), [id]( const auto& p ) { return p.first == id; } );
    if ( i == names.end() ) throw std::runtime_error( "Bad LHCb::Hlt::DAQ::SourceID value" );
    return std::string{i->second};
  }
} // namespace LHCb::Hlt::DAQ
