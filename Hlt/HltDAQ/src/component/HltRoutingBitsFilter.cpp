/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/FunctionalUtilities.h"
#include "GaudiKernel/StdArrayAsProperty.h"
#include "LHCbAlgs/FilterPredicate.h"
#include <vector>

class HltRoutingBitsFilter : public LHCb::Algorithm::FilterPredicate<bool( const LHCb::RawEvent& )> {
public:
  HltRoutingBitsFilter( const std::string& name, ISvcLocator* pSvcLocator );
  bool operator()( const LHCb::RawEvent& ) const override; ///< Algorithm execution
private:
  Gaudi::Property<std::array<unsigned int, 3>> m_r{this, "RequireMask", {~0u, ~0u, ~0u}};
  Gaudi::Property<std::array<unsigned int, 3>> m_v{this, "VetoMask", {0u, 0u, 0u}};
  Gaudi::Property<bool>                        m_passOnError{this, "PassOnError", true};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpectedRawbanks{
      this, "#unexpected number of HltRoutingBits rawbanks"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_unexpectedRawbanksSize{
      this, "#unexpected HltRoutingBits rawbank size"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_accept{this, "#accept"};
};

//-----------------------------------------------------------------------------
// Implementation file for class : HltRoutingBitsFilter
//
// 2008-07-29 : Gerhard Raven
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( HltRoutingBitsFilter )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
HltRoutingBitsFilter::HltRoutingBitsFilter( const std::string& name, ISvcLocator* pSvcLocator )
    : FilterPredicate(
          name, pSvcLocator,
          KeyValue{"RawEventLocations", Gaudi::Functional::concat_alternatives( LHCb::RawEventLocation::Trigger,
                                                                                LHCb::RawEventLocation::Copied,
                                                                                LHCb::RawEventLocation::Default )} ) {}

//=============================================================================
// Main execution
//=============================================================================
bool HltRoutingBitsFilter::operator()( const LHCb::RawEvent& rawEvent ) const {

  const auto& banks = rawEvent.banks( LHCb::RawBank::HltRoutingBits );
  if ( banks.size() != 1 ) {
    ++m_unexpectedRawbanks;
    return m_passOnError;
  }
  if ( banks[0]->size() != 3 * sizeof( unsigned int ) ) {
    ++m_unexpectedRawbanksSize;
    return m_passOnError;
  }
  const unsigned int* data = banks[0]->data();

  bool veto = false;
  bool req  = false;
  for ( unsigned i = 0; i < 3 && !veto; ++i ) {
    veto = veto || ( data[i] & m_v[i] );
    req  = req || ( data[i] & m_r[i] );
  }
  bool accept = ( req & !veto );
  m_accept += accept;

  return accept;
}
