/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/PackedCaloAdc.h"
#include "Event/PackedCaloCluster.h"
#include "Event/PackedCaloDigit.h"
#include "Event/PackedCaloHypo.h"
#include "Event/PackedCluster.h"
#include "Event/PackedData.h"
#include "Event/PackedDataBuffer.h"
#include "Event/PackedFlavourTag.h"
#include "Event/PackedMuonPID.h"
#include "Event/PackedPartToRelatedInfoRelation.h"
#include "Event/PackedParticle.h"
#include "Event/PackedProtoParticle.h"
#include "Event/PackedRecVertex.h"
#include "Event/PackedRelations.h"
#include "Event/PackedRichPID.h"
#include "Event/PackedTrack.h"
#include "Event/PackedVertex.h"
#include "Event/RawEvent.h"
#include "Kernel/IANNSvc.h"
#include "LHCbAlgs/MergingTransformer.h"
#include "PackedDataChecksum.h"
#include "RZip.h"
#include <optional>
//#include <Gaudi/Accumulators/Histogram.h>

/** @class HltPackedDataWriter HltPackedDataWriter.h
 *  Algorithm that writes packed objects to raw banks.
 *
 *  @author Rosen Matev, Sean Benson
 *  @date   2016-01-03
 */

template <typename T>
using VOC = Gaudi::Functional::vector_of_const_<T>;

namespace LHCb::Hlt::PackedData {
  namespace {
    const Gaudi::StringKey PackedObjectLocations{"PackedObjectLocations"};
    /// Save an object to the buffer.
    template <typename T>
    size_t saveObject( const DataObject& dataObject, const std::string& location, PackedDataOutBuffer& buffer,
                       PackedDataChecksum* checksum ) {
      const T& object = dynamic_cast<const T&>( dataObject );
      // Reserve bytes for the size of the object
      auto posObjectSize = buffer.saveSize( 0 ).first;
      // Save the object actual object and see how many bytes were written
      auto objectSize = buffer.save( object ).second;
      // Save the object's size in the correct position
      buffer.saveAt<uint32_t>( objectSize, posObjectSize );
      if ( checksum ) { checksum->processObject( object, location ); }
      return objectSize;
    }

    template <typename... PackedData>
    auto createSavers() {
      std::map<CLID, size_t ( * )( const DataObject&, const std::string&, PackedDataOutBuffer&, PackedDataChecksum* )>
          mapping;
      ( mapping.emplace( PackedData::classID(), &saveObject<PackedData> ), ... );
      return mapping;
    }

    /// Map between CLIDs and save functions
    static auto const savers =
        createSavers<PackedTracks, PackedRichPIDs, PackedMuonPIDs, PackedCaloHypos, PackedProtoParticles,
                     PackedCaloClusters, PackedParticles, PackedVertices, PackedRecVertices, PackedFlavourTags,
                     PackedRelations, PackedWeightedRelations, PackedRelatedInfoRelations, PackedCaloDigits,
                     PackedClusters, PackedCaloAdcs>();
  } // namespace

  class Writer : public LHCb::Algorithm::MergingTransformer<LHCb::RawBank::View( VOC<DataObject> const& )> {
  public:
    /// Standard constructor
    Writer( std::string const& name, ISvcLocator* isvc );

    StatusCode          initialize() override; ///< Algorithm initialization
    LHCb::RawBank::View operator()( VOC<DataObject> const& ) const override;

  private:
    /// Put the (compressed) data buffer into raw banks and register them.
    void addBanks( RawEvent& rawEvent, span<const uint8_t> data, Compression compression ) const;

    StatusCode pack( const DataObject&, const std::string& location, PackedDataOutBuffer& buffer,
                     PackedDataChecksum* ) const;

    /// Property giving the mapping between containers and packed containers
    Gaudi::Property<std::map<std::string, std::string>> m_containerMap{this, "ContainerMap"};
    /// Property giving the location of the raw event
    DataObjectWriteHandle<RawEvent> m_outputRawEvent{this, "OutputRawEvent", RawEventLocation::Default};
    /// Property setting the compression algorithm
    Gaudi::Property<Compression> m_compression{this, "Compression", Compression::ZSTD};
    /// Property setting the compression level
    Gaudi::Property<int> m_compressionLevel{this, "CompressionLevel", 5};
    /// Property enabling calculation and print of checksums
    Gaudi::Property<bool> m_enableChecksum{this, "EnableChecksum", false};

    mutable Gaudi::Accumulators::StatCounter<> m_serializedDataSize{this, "Size of serialized data"};
    mutable Gaudi::Accumulators::StatCounter<> m_compressedDataSize{this, "Size of compressed data"};

    mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_packedobjectsize{this, "Packed objects too large to save",
                                                                           50};

    /// HltANNSvc for making selection names to int selection ID
    ServiceHandle<IANNSvc> m_hltANNSvc{this, "ANNSvc", "HltANNSvc", "Service to retrieve DecReport IDs"};

    // Histograms will stay commented out until the resulting build warnings are fixed.
    // mutable Gaudi::Accumulators::Histogram<1> m_hBufferSize{
    //    this, "BufferSize", "BufferSize, atomic", {100, 0, 14000, "Elements"}};
    // mutable Gaudi::Accumulators::Histogram<1> m_hCompressedBufferSize{
    //    this, "CompressedBufferSize", "CompressedBufferSize, atomic", {100, 0, 7000, "Elements"}};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( Writer, "HltPackedDataWriter" )

  Writer::Writer( std::string const& name, ISvcLocator* isvc )
      : LHCb::Algorithm::MergingTransformer<LHCb::RawBank::View( VOC<DataObject> const& )>{
            name, isvc, {"PackedContainers", {}}, {"OutputView", {}}} {}

  StatusCode Writer::initialize() {
    return MergingTransformer::initialize().andThen( [&] {
      info() << "Configured to persist containers ";
      for ( size_t i = 0; i < inputLocationSize(); ++i ) { info() << " '" << inputLocation( i ) << "',"; }
      info() << endmsg;
    } );
  }

  StatusCode Writer::pack( const DataObject& dataObject, const std::string& containerPath, PackedDataOutBuffer& buffer,
                           PackedDataChecksum* checksum ) const {
    // Obtain the integer ID to be saved instead of the location string
    auto locationID = m_hltANNSvc->value( PackedObjectLocations, containerPath );
    // TODO fail if unknown location, even if HltANNSvc.allowUndefined=True
    if ( !locationID ) {
      error() << "Requested to persist " << containerPath
              << " but no ID is registered for it in the HltANNSvc, skipping!" << endmsg;
      return StatusCode::FAILURE;
    }

    // Obtain the function which saves the object with this CLID
    auto       classID = dataObject.clID();
    const auto it      = savers.find( classID );
    if ( it == savers.end() ) {
      fatal() << "Unknown class ID " << classID << " for container " << containerPath << endmsg;
      return StatusCode::FAILURE;
    }
    const auto saveObjectFun = it->second;

    // Save the CLID and location
    buffer.save<uint32_t>( classID );
    buffer.save<int32_t>( locationID->second );

    // Save the links to other containers on the TES
    StatusCode   status{StatusCode::SUCCESS};
    auto*        linkMgr = dataObject.linkMgr();
    unsigned int nlinks  = linkMgr->size();
    buffer.saveSize( nlinks );
    for ( unsigned int id = 0; id < nlinks; ++id ) {

      auto location = linkMgr->link( id )->path();
      if ( location.front() != '/' ) { location = "/Event/" + location; }

      auto packedLocation = m_containerMap.find( location );
      if ( packedLocation != end( m_containerMap ) ) { location = packedLocation->second; }

      auto linkID = m_hltANNSvc->value( PackedObjectLocations, location );
      if ( !linkID ) {
        error() << "Requested to persist link to " << Gaudi::Utils::toString( location )
                << " but no ID is registered for it in the ANNSvc!" << endmsg;
        return StatusCode::FAILURE;
      }

      buffer.save<int32_t>( linkID->second );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Packed link " << id << "/" << nlinks << " to " << linkMgr->link( id )->path() << " (" << location
                << ") with ID " << linkID->second << endmsg;
      }
    }
    if ( status ) {

      // Save the packed object itself
      auto objectSize = saveObjectFun( dataObject, containerPath, buffer, checksum );

      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Packed " << containerPath << " with ID " << locationID->second << " and CLID " << classID
                << " into " << objectSize << " bytes" << endmsg;
      }
    }

    return status;
  }

  LHCb::RawBank::View Writer::operator()( VOC<DataObject> const& containers ) const {

    // Get the raw event -- FIXME: we should be creating a dedicated RawEvent, whose contents get merged by
    // a dedicated algorithm...
    auto* rawEvent = m_outputRawEvent.getOrCreate();

    /// Buffer for serialization of the packed objects
    PackedDataOutBuffer buffer;
    buffer.reserve( 16000 ); // buffer size still needs fine tuning
    /// Buffer for the compressed data
    std::vector<uint8_t> compressedBuffer;
    compressedBuffer.reserve( 8000 ); // compressedBuffer size still needs fine tuning
    /// Helper for computing checksums
    std::optional<PackedDataChecksum> checksum;
    if ( m_enableChecksum ) checksum.emplace();

    for ( const auto& [i, dataObject] : range::enumerate( containers ) ) {
      pack( dataObject, inputLocation( i ), buffer, get_pointer( checksum ) ).orThrow();
    }

    // Compress the buffer
    auto compressed = ( m_compression != Compression::NoCompression ) &&
                      buffer.compress( m_compression, m_compressionLevel, compressedBuffer );
    const auto& output = compressed ? compressedBuffer : buffer.buffer();

    // Write the data to the raw event
    addBanks( *rawEvent, output, compressed ? m_compression.value() : Compression::NoCompression );

    m_serializedDataSize += buffer.buffer().size();
    m_compressedDataSize += output.size();
    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << "Total size of serialized data " << buffer.buffer().size() << endmsg;
      debug() << "Wrote " << output.size() << " compressed bytes" << endmsg;
      if ( checksum ) {
        for ( const auto& x : checksum->checksums() )
          debug() << "Packed data checksum for '" << x.first << "' = " << x.second << endmsg;
      }
    }
    return rawEvent->banks( RawBank::DstData );
    //++m_hBufferSize[buffer.buffer().size()];
    //++m_hCompressedBufferSize[compressedBuffer.size()];
  }

  void Writer::addBanks( RawEvent& rawEvent, span<const uint8_t> data, Compression compression ) const {
    /// Maximum bank payload size = 65535 (max uint16) - 8 (header) - 3 (alignment)
    constexpr size_t MAX_PAYLOAD_SIZE{65524};

    uint16_t sourceIDCommon = shift<SourceIDMasks::Compression>( static_cast<uint16_t>( compression ) );

    auto chunks = range::chunk( data, MAX_PAYLOAD_SIZE );
    if ( chunks.size() > extract<SourceIDMasks::PartID>( ~uint16_t{0} ) ) {
      ++m_packedobjectsize;
      return;
    }
    if ( msgLevel( MSG::DEBUG ) ) { debug() << "Writing " << chunks.size() << " banks" << endmsg; }
    for ( auto [ibank, chunk] : range::enumerate( chunks ) ) {
      uint16_t sourceID = sourceIDCommon | shift<SourceIDMasks::PartID>( ibank );
      if ( msgLevel( MSG::DEBUG ) ) {
        debug() << "Adding raw bank with sourceID=" << sourceID << ", length=" << chunk.size()
                << ", offset=" << ( chunk.data() - data.data() ) << endmsg;
      }
      rawEvent.addBank( sourceID, RawBank::DstData, kVersionNumber, chunk );
    }
  }
} // namespace LHCb::Hlt::PackedData
