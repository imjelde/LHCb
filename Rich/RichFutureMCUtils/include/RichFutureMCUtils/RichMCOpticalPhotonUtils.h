/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Kernel
#include "Kernel/RichSmartID.h"

// Event model
#include "Event/MCRichOpticalPhoton.h"

// Rich Utils
#include "RichUtils/RichMap.h"

// STL
#include <algorithm>

namespace Rich::Future::MC::Relations {

  /// Helper class for RichMCHits
  class MCOpticalPhotonUtils {

  public:
    /// Constructor from MC Optical Photons
    MCOpticalPhotonUtils( const LHCb::MCRichOpticalPhotons& mcphotons );

  public:
    // access methods

    /// Finds the MCRichOpticalPhoton associated to a given MCRichHit
    auto mcOpticalPhoton( const LHCb::MCRichHit* mcHit ) const {
      const auto i = m_hitToPhot.find( mcHit );
      return ( i != m_hitToPhot.end() ? i->second : nullptr );
    }

    /// Finds the MCRichOpticalPhotons associated to a list of MCRichHits
    template <typename HITS>
    auto mcOpticalPhotons( const HITS& hits ) const {
      LHCb::MCRichOpticalPhoton::ConstVector photons;
      photons.reserve( hits.size() );
      for ( const auto hit : hits ) {
        const auto phot = mcOpticalPhoton( hit );
        if ( phot ) { photons.push_back( phot ); }
      }
      return photons;
    }

    /** Returns the MCRichOpticalPhotons associated to a list of hits, that are also
     *  linked to a given set of MCParticles */
    template <typename HITS>
    auto mcOpticalPhotons( const HITS&                          hits, //
                           const LHCb::MCParticle::ConstVector& mcPs ) const {
      // Get the list of all MC photons associated to the hits
      auto mcPhots = mcOpticalPhotons( hits );
      // filter out those not linked to mcPs
      const auto it = std::remove_if( mcPhots.begin(), mcPhots.end(), //
                                      [&mcPs]( auto&& phot ) {
                                        // Get the MCParticle associated to this photon
                                        const auto mcP =
                                            ( phot && phot->mcRichHit() ? phot->mcRichHit()->mcParticle() : nullptr );
                                        // if MCP not in supplied list return true to erase
                                        return ( !mcP || std::find( mcPs.begin(), mcPs.end(), mcP ) == mcPs.end() );
                                      } );
      // remove the entries selected for deletion
      mcPhots.erase( it, mcPhots.end() );
      // return
      return mcPhots;
    }

  private:
    // types

    /// Type for mapping from MCRichHit to MCRichOpticalPhoton
    using MCRichHitToOpPhot = Rich::Map<const LHCb::MCRichHit*, const LHCb::MCRichOpticalPhoton*>;

  private:
    // cached data

    /// Mapping of hits to photons
    MCRichHitToOpPhot m_hitToPhot;
  };

} // namespace Rich::Future::MC::Relations
