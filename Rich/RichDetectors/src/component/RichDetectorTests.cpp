/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Rich Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "GaudiAlg/Consumer.h"

// Detectors
#include "RichDet/DeRichLocations.h"
#include "RichDetectors/Condition.h"
#include "RichDetectors/Handle.h"
#include "RichDetectors/Rich1.h"
#include "RichDetectors/Rich1Gas.h"
#include "RichDetectors/Rich2.h"
#include "RichDetectors/Rich2Gas.h"

// DAQ
#include "RichFutureDAQ/RichTel40CableMapping.h"

// STL
#include <string>

using namespace Rich::Future::DAQ;
using namespace Rich::Detector;

namespace Rich::Future {

  // Use the functional framework
  using namespace Gaudi::Functional;

#ifdef USE_DD4HEP
  using DR1 = LHCb::Detector::DeRich1;
  using DR2 = LHCb::Detector::DeRich2;
  using DG1 = LHCb::Detector::DeRich1Gas;
  using DG2 = LHCb::Detector::DeRich2Gas;
#else
  using DR1 = DeRich1;
  using DR2 = DeRich2;
  using DG1 = DeRichRadiator;
  using DG2 = DeRichRadiator;
#endif

  class TestDBAccess final : public Consumer<void( const DR1&, const DR2&, const DG1&, const DG2& ),
                                             LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, DR1, DR2, DG1, DG2>> {
  public:
    TestDBAccess( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                                    //
                    {KeyValue{"R1Loc", DeRichLocations::location<DR1>()},                 //
                     KeyValue{"R2Loc", DeRichLocations::location<DR2>()},                 //
                     KeyValue{"G1Loc", DeRichLocations::location<DG1>( Rich::Rich1Gas )}, //
                     KeyValue{"G2Loc", DeRichLocations::location<DG2>( Rich::Rich2Gas )}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { return setProperty( "OutputLevel", MSG::VERBOSE ); } );
    }

    void operator()( const DR1& r1, const DR2& r2, const DG1& g1, const DG2& g2 ) const override {
      debug() << DeRichLocations::location<DG1>( Rich::Rich1Gas ) << endmsg;
      debug() << DeRichLocations::location<DG2>( Rich::Rich2Gas ) << endmsg;
      debug() << "R1 " << &r1 << " name='" << r1.name() << "' access()=" << r1.access() << endmsg;
      debug() << "R2 " << &r2 << " name='" << r2.name() << "' access()=" << r2.access() << endmsg;
      debug() << "G1 " << &g1 << " name='" << g1.name() << "' access()=" << g1.access() << endmsg;
      debug() << "G2 " << &g2 << " name='" << g2.name() << "' access()=" << g2.access() << endmsg;
    }
  };

  namespace {
    template <typename R>
    struct TestDervCond {
      inline static const std::string R1Key = DeRichLocations::derivedCondition<DR1>( "TestDervCond" );
      inline static const std::string R2Key = DeRichLocations::derivedCondition<DR2>( "TestDervCond" );
      TestDervCond()                        = default;
      TestDervCond( const R& d ) : r( d ) {}
      Rich::Detector::Handle<R> r;
    };
  } // namespace

  class TestDerivedElem final
      : public Consumer<void( const TestDervCond<DR1>&, const TestDervCond<DR2>& ),
                        LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, TestDervCond<DR1>, TestDervCond<DR2>>> {
  public:
    TestDerivedElem( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,                                //
                    {KeyValue{"R1DervLoc", TestDervCond<DR1>::R1Key}, //
                     KeyValue{"R2DervLoc", TestDervCond<DR2>::R2Key}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        auto sc = setProperty( "OutputLevel", MSG::VERBOSE );
        addConditionDerivation( {DeRichLocations::location<DR1>()}, //
                                TestDervCond<DR1>::R1Key,           //
                                [p = this]( const DR1& d1 ) {
                                  p->debug() << "Creating DR1 derived object from " << &d1 << " name='" << d1.name()
                                             << "' loc='" << DeRichLocations::location<DR1>()
                                             << "' access()=" << d1.access() << endmsg;
                                  return TestDervCond<DR1>{d1};
                                } );
        addConditionDerivation( {DeRichLocations::location<DR2>()}, //
                                TestDervCond<DR2>::R2Key,           //
                                [p = this]( const DR2& d2 ) {
                                  p->debug() << "Creating DR2 derived object from " << &d2 << " name='" << d2.name()
                                             << "' loc='" << DeRichLocations::location<DR2>()
                                             << "' access()=" << d2.access() << endmsg;
                                  return TestDervCond<DR2>{d2};
                                } );
        return sc;
      } );
    }

    void operator()( const TestDervCond<DR1>& d1, const TestDervCond<DR2>& d2 ) const override {
      debug() << "R1 " << d1.r << " name='" << d1.r->name() << "' access()=" << d1.r->access() << endmsg;
      debug() << "R2 " << d2.r << " name='" << d2.r->name() << "' access()=" << d2.r->access() << endmsg;
    }
  };

  class TestDerivedDetObjects final
      : public Consumer<void( const Detector::Rich1&, const Detector::Rich2&, //
                              const Detector::Rich1Gas&, const Detector::Rich2Gas& ),
                        LHCb::DetDesc::usesBaseAndConditions<AlgBase<>,                        //
                                                             Detector::Rich1, Detector::Rich2, //
                                                             Detector::Rich1Gas, Detector::Rich2Gas>> {
  public:
    /// Standard constructor
    TestDerivedDetObjects( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer( name, pSvcLocator,
                    {KeyValue{"Rich1", Detector::Rich1::DefaultConditionKey},
                     KeyValue{"Rich2", Detector::Rich2::DefaultConditionKey},
                     KeyValue{"Rich1Gas", Detector::Rich1Gas::DefaultConditionKey},
                     KeyValue{"Rich2Gas", Detector::Rich2Gas::DefaultConditionKey}} ) {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] {
        auto sc = setProperty( "OutputLevel", MSG::VERBOSE );
        Detector::Rich1::addConditionDerivation( this );
        Detector::Rich2::addConditionDerivation( this );
        Detector::Rich1Gas::addConditionDerivation( this );
        Detector::Rich2Gas::addConditionDerivation( this );
        return sc;
      } );
    }

    void operator()( const Detector::Rich1& r1, const Detector::Rich2& r2, //
                     const Detector::Rich1Gas& g1, const Detector::Rich2Gas& g2 ) const override {
      debug() << r1 << endmsg;
      debug() << r2 << endmsg;
      debug() << g1 << endmsg;
      debug() << g2 << endmsg;
    }
  };

  // Example of algorithm accessing conditions
  struct TestConds
      : Gaudi::Functional::Consumer<void( const Rich::Detector::Condition& ),
                                    LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Rich::Detector::Condition>> {
    // constructor
    TestConds( const std::string& name, ISvcLocator* loc )
        : Consumer{name, loc, {KeyValue{"CondPath", Tel40CableMapping::ConditionPaths[0]}}} {}

    StatusCode initialize() override {
      return Consumer::initialize().andThen( [&] { return setProperty( "OutputLevel", MSG::VERBOSE ); } );
    }

    void operator()( const Rich::Detector::Condition& cond ) const override {
      debug() << "NumberOfLinks=" << condition_param<int>( cond, "NumberOfLinks" ) << endmsg;
    }
  };

  DECLARE_COMPONENT( TestDBAccess )
  DECLARE_COMPONENT( TestDerivedElem )
  DECLARE_COMPONENT( TestDerivedDetObjects )
  DECLARE_COMPONENT( TestConds )

} // namespace Rich::Future
