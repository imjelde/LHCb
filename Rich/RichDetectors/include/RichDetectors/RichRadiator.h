/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/SystemOfUnits.h"

// RichDet
#include "RichDet/DeRichRadiator.h"

// local
#include "RichDetectors/Utilities.h"

// STL
#include <cassert>
#include <memory>

namespace Rich::Detector {

  namespace details {

    //-----------------------------------------------------------------------------
    /** @class RichRadiator.h
     *
     *  RICH Radiator
     *
     *  @author Chris Jones
     *  @date   2021-12-07
     */
    //-----------------------------------------------------------------------------

    template <typename DETELEM>
    class Radiator {

    public:
      // types

      // expose underlying detector element type
      using DetElem = DETELEM;

    public:
      // constructors

      /// Default Constructor
      Radiator() = default;

      /// Constructor from Det Elem
      template <typename DET>
      Radiator( const DET& rad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( rad )                //
          , m_refIndex( rad.refIndex() ) {}

      /// Constructor from Det Elem and old object (for dd4hep comissioning)
      template <typename DET, typename OLDDET>
      Radiator( const DET& rad, const OLDDET& oldrad )
          : m_radiatorID( rad.radiatorID() ) //
          , m_rich( rad.rich() )             //
          , m_radiator( oldrad )
          , m_refIndex( std::make_shared<const Rich::TabulatedFunction1D>( rad.GasRefIndex() ) ) {}

    private:
      /// Get access to the underlying object
      inline auto get() const noexcept { return m_radiator; }

    public:
      // accessors

      /// The radiator ID
      inline auto radiatorID() const noexcept { return m_radiatorID; }

      /// The RICH type
      inline auto rich() const noexcept { return m_rich; }

      /// refractive index interpolator
      inline auto refIndex() const noexcept { return m_refIndex.get(); }

      /// refractive index for given photon energy
      template <typename TYPE>
      inline auto refractiveIndex( const TYPE energy ) const noexcept {
        assert( refIndex() );
        return refIndex()->value( energy * Gaudi::Units::eV );
      }

    public:
      // methods forwarded to Det Elem
      // to be implemented locally eventually

      FORWARD_TO_DET_OBJ( intersectionPoints )
      FORWARD_TO_DET_OBJ( isInside )

    protected:
      /// messaging
      void fillStream( std::ostream& s ) const {
        s << "[ " << rich() << " " << radiatorID() //
          << " refIndex=" << *refIndex()           //
          << " ]";
      }

    private:
      // data

      /// The Radiator ID
      Rich::RadiatorType m_radiatorID = Rich::InvalidRadiator;

      /// The Rich detector of this radiator
      Rich::DetectorType m_rich = Rich::InvalidDetector;

      /// DetDesc panel object
      Handle<DETELEM> m_radiator;

      /// Refractive index
      std::shared_ptr<const Rich::TabulatedFunction1D> m_refIndex;
    };

  } // namespace details

  using OldRadiator = details::Radiator<DeRichRadiator>;
#ifdef USE_DD4HEP
  // using DD4Radiator = details::Radiator<...>;
  using Radiator = OldRadiator; // TODO: Change for equivalent DD4HEP class
#else
  using Radiator = OldRadiator;
#endif

} // namespace Rich::Detector
