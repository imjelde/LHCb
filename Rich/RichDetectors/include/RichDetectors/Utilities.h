/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

#include <DetDesc/IConditionDerivationMgr.h>
#include <GaudiKernel/IUpdateManagerSvc.h>

// Annoyance from DD4HEP
// https://indico.cern.ch/event/995976/contributions/4207355/attachments/2179603/3681346/DD4hepLimitations.pdf
#ifdef USE_DD4HEP
#  include <DD4hep/GrammarUnparsed.h>
#endif

#include "RichDetectors/Handle.h"

#include <type_traits>

#ifndef FORWARD_TO_DET_OBJ
#  define FORWARD_TO_DET_OBJ( method )                                                                                 \
    template <typename... ARGS>                                                                                        \
    inline decltype( auto ) method( ARGS&&... args ) const {                                                           \
      return get()->method( std::forward<ARGS>( args )... );                                                           \
    }
#endif
