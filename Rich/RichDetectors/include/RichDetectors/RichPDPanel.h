/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// LHCbKernel
#include "Kernel/RichSide.h"
#include "Kernel/RichSmartID.h"

// LHCbMath
#include "LHCbMath/FastMaths.h"
#include "LHCbMath/SIMDTypes.h"

// Local
#include "RichDetectors/RichPD.h"
#include "RichDetectors/Utilities.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichPhDetPanel.h"
#endif
#include "RichDet/DeRichPDPanel.h"

// RichUtils
#include "RichUtils/RichDAQDefinitions.h"
#include "RichUtils/RichSIMDRayTracing.h"

// STL
#include <array>
#include <cassert>
#include <map>
#include <memory>
#include <ostream>
#include <tuple>
#include <type_traits>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class RichPDPanel.h
   *
   *  RICH PD panel class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class PDPanel {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
#ifdef USE_DD4HEP
    using DetElem = DeRichPDPanel; // TODO: Change for equivalent DD4HEP class
#else
    using DetElem = DeRichPDPanel;
#endif

    /// type for SIMD ray tracing result
    using SIMDRayTResult = Rich::RayTracingUtils::SIMDResult;
    /// scalar FP type for SIMD objects
    using FP = Rich::SIMD::DefaultScalarFP;
    /// SIMD float type
    using SIMDFP = Rich::SIMD::FP<FP>;
    /// SIMD Int32 type
    using SIMDINT32 = Rich::SIMD::Int32;
    /// SIMD Point
    using SIMDPoint = Rich::SIMD::Point<FP>;
    /// SIMD Vector
    using SIMDVector = Rich::SIMD::Vector<FP>;
    /// Array of PD pointers
    using SIMDPDs = Rich::SIMD::STDArray<const PD*>;
    /// Array of SmartIDs
    using SIMDSmartIDs = SIMDRayTResult::SmartIDs;

    /// Return types for ray-tracing methods
    using RayTStruct = std::tuple<Gaudi::XYZPoint, LHCb::RichSmartID, const PD*, LHCb::RichTraceMode::RayTraceResult>;
    using RayTStructSIMD = std::tuple<SIMDPoint, SIMDSmartIDs, SIMDPDs, SIMDRayTResult::Results>;

  private:
    // types

    // types for PD lookup storage
    using PDsArray    = std::array<std::unique_ptr<const PD>, LHCb::RichSmartID::MaPMT::MaxPDsPerModule>;
    using ModuleArray = std::array<PDsArray, LHCb::RichSmartID::MaPMT::MaxModulesPerPanel>;

  private:
    /// Get access to the underlying object
    inline const DetElem* get() const noexcept { return m_panel; }

    /// Panel local module number
    inline auto panelLocalModNum( const LHCb::RichSmartID pdID ) const noexcept {
      // for now retain our own offset to support classic PMTs
      // in future can just use RichSmartID::panelLocalModuleNum()
      return ( pdID.pdMod() - m_modNumOffset );
    }

    /// Convert a transform to SIMD version
    template <typename TRANS, typename SIMDTRANS>
    inline void toSIMDTrans( const TRANS& t, SIMDTRANS& simdT ) const noexcept {
      double xx{0}, xy{0}, xz{0}, dx{0}, yx{0}, yy{0};
      double yz{0}, dy{0}, zx{0}, zy{0}, zz{0}, dz{0};
      t.GetComponents( xx, xy, xz, dx, yx, yy, yz, dy, zx, zy, zz, dz );
      simdT.SetComponents( (FP)xx, (FP)xy, (FP)xz, (FP)dx, //
                           (FP)yx, (FP)yy, (FP)yz, (FP)dy, //
                           (FP)zx, (FP)zy, (FP)zz, (FP)dz );
    }

  public:
    /// Access all owned PD Modules
    const auto& pdModules() const noexcept { return m_PDs; }

  public:
    // constructors

    /// Default Constructor
    PDPanel() = default;

    /// Templated constructor for dd4hep types
    /// For now also take DetDesc object as well
    template <typename PANEL>
    PDPanel( const Rich::DetectorType rich,  //
             const Rich::Side         side,  //
             const PANEL&             panel, //
             const DetElem&           old_panel )
        : m_rich( rich ), m_side( side ), m_panel( &old_panel ) {

      // transforms
      m_locToGloM = panel.localToGlobalMatrix();
      m_gloToLocM = m_locToGloM.Inverse();
      toSIMDTrans( m_gloToLocM, m_gloToLocMSIMD );

      using UINT = decltype( m_modNumOffset );
      UINT maxlocalModNum{0}, maxPDNumInMod{0};
      m_modNumOffset = std::numeric_limits<UINT>::max();

      // compute the SmartID for a given PMT
      auto getSmartID = [&rich, &side]( const auto& pmt ) {
        // PMT number in module
        const LHCb::RichSmartID::DataType nInMod = pmt.numInModule();
        // local module number
        const LHCb::RichSmartID::DataType locModN = pmt.panelLocalModuleN();
        // global module number
        const auto gloModN = LHCb::RichSmartID::MaPMT::panelLocalToGlobalModN( rich, side, locModN );
        assert( gloModN == ( LHCb::RichSmartID::DataType )( pmt.moduleCopyNum() ) );
        // build the PMT ID
        // FIXME : Note we are forcing the type to PMTs below. At some point this will need fixing.
        const LHCb::RichSmartID id( rich, side, nInMod, gloModN, LHCb::RichSmartID::MaPMTID, pmt.isHType() );
        assert( id.rich() == rich );
        assert( id.panel() == side );
        assert( id.pdMod() == gloModN );
        assert( id.panelLocalModuleNum() == locModN );
        assert( id.pdNumInMod() == nInMod );
        return id;
      };

      // pre-loop over IDs to determine min/max module numbers for this panel
      auto checkNumbers = [&]( const auto& pmts ) {
        for ( const auto& pmt : pmts ) {
          const auto id     = getSmartID( pmt );
          const auto modNum = id.pdMod();
          const auto pdNum  = id.pdNumInMod();
          if ( modNum > maxlocalModNum ) { maxlocalModNum = modNum; }
          if ( modNum < m_modNumOffset ) { m_modNumOffset = modNum; }
          if ( pdNum > maxPDNumInMod ) { maxPDNumInMod = pdNum; }
        }
      };

      // Whilst comissioning dd4hep load all PMTs from both worlds and link via SmartID
      const auto old_pmts = old_panel.dePDs();
      // form map for id -> DetDesc PMT
      std::map<LHCb::RichSmartID, const DeRichPD*> id_to_old_pmt;
      for ( const auto* pmt : old_pmts ) {
        auto id = pmt->pdSmartID();
#ifdef USE_DD4HEP
        // correct for different numbering scheme between DD4HEP and DetDesc
        const Rich::DetectorArray<Rich::PanelArray<LHCb::RichSmartID::DataType>> mod_corr{{{0, 0}, {6, 18}}};
        const auto pdMod      = id.pdMod() + mod_corr[id.rich()][id.panel()];
        const auto pdNumInMod = id.pdNumInMod();
        id.setPD( pdMod, pdNumInMod );
#endif
        auto*& m = id_to_old_pmt[id];
        assert( nullptr == m ); // should never initialise same ptr twice
        m = pmt;
      }

      // create owned PMTs
      auto createPDs = [&]( const auto& pmts ) {
        for ( const auto& pmt : pmts ) {
          const auto id      = getSmartID( pmt );
          auto*&     old_pmt = id_to_old_pmt[id];
          //   assert( nullptr != old_pmt );
          if ( old_pmt ) {
            const auto localModNum = panelLocalModNum( id );
            assert( localModNum < m_PDs.size() );
            auto& mod = m_PDs[localModNum];
            assert( id.pdNumInMod() < mod.size() );
            auto& pd = mod[id.pdNumInMod()];
            assert( nullptr == pd.get() ); // should not be set yet
            pd = std::make_unique<PD>( id, pmt, *old_pmt );
          }
        }
      };

      // Load DD4HEP PMTs
      const auto r_pmts = panel.allPMTsR();
      const auto h_pmts = panel.allPMTsH();

      // run pre-check on ranges
      checkNumbers( r_pmts );
      checkNumbers( h_pmts );
      assert( ( maxlocalModNum - m_modNumOffset ) < m_PDs.size() );
      assert( maxPDNumInMod < m_PDs[0].size() );

      // create the owned PD instances
      createPDs( r_pmts );
      createPDs( h_pmts );
    }

    /// Constructor from det elem
    PDPanel( const DetElem& panel )
        : m_rich( panel.rich() )
        , m_side( panel.side() )
        , m_panel( &panel )
        , m_locToGloM( panel.PDPanelToGlobalMatrix() )
        , m_gloToLocM( panel.globalToPDPanelMatrix() )
        , m_gloToLocMSIMD( panel.globalToPDPanelMatrixSIMD() ) {

      // pre-loop over IDs to determine min/max module numbers for this panel
      // note, doing this here rather than relying on RichSmartID::panelLocalModuleNum()
      // as that does not work correctly for 'classic' PMTs. Once support for this
      // older scheme is no londer needed we remove the local offsets here.
      using UINT = decltype( m_modNumOffset );
      UINT maxlocalModNum{0}, maxPDNumInMod{0};
      m_modNumOffset = std::numeric_limits<UINT>::max();
      for ( const auto dePD : panel.dePDs() ) {
        const auto id     = dePD->pdSmartID();
        const auto modNum = id.pdMod();
        const auto pdNum  = id.pdNumInMod();
        if ( modNum > maxlocalModNum ) { maxlocalModNum = modNum; }
        if ( modNum < m_modNumOffset ) { m_modNumOffset = modNum; }
        if ( pdNum > maxPDNumInMod ) { maxPDNumInMod = pdNum; }
      }

      // sanity checks
      assert( ( maxlocalModNum - m_modNumOffset ) < m_PDs.size() );
      assert( maxPDNumInMod < m_PDs[0].size() );

      // access the PDs and make owned wrapped objects
      for ( const auto dePD : panel.dePDs() ) {
        const auto id          = dePD->pdSmartID();
        const auto localModNum = panelLocalModNum( id );
        assert( localModNum < m_PDs.size() );
        auto& mod = m_PDs[localModNum];
        assert( id.pdNumInMod() < mod.size() );
        auto& pd = mod[id.pdNumInMod()];
        assert( nullptr == pd.get() ); // should not be set yet
        pd = std::make_unique<PD>( *dePD );
      }
    }

  public:
    // accessors

    /// Access the RICH detector type
    inline auto rich() const noexcept { return m_rich; }

    /// Access the PD panel side
    inline auto side() const noexcept { return m_side; }

    /// Get the PD for given ID
    decltype( auto ) dePD( const LHCb::RichSmartID pdID ) const
#ifdef NDEBUG
        noexcept
#endif
    {
      const PD* dePD = nullptr;
      if ( pdID.pdIsSet() ) {
        const auto localModNum = panelLocalModNum( pdID );
        // Note, whilst comissioning dd4hep, but still using old DetDesc MC samples with
        // different PMTs, we cannot apply assers here by instead must using runtime size checks.
        // to be reverted once dd4hep is fully comissioned and DetDesc completely dropped.
        // assert( localModNum < m_PDs.size() );
        if ( localModNum < pdModules().size() ) {
          const auto& mod = pdModules()[localModNum];
          // assert( pdID.pdNumInMod() < mod.size() );
          if ( pdID.pdNumInMod() < mod.size() ) {
            const auto& pd = mod[pdID.pdNumInMod()];
            /** As long as we need to support 'classic' PMTs cannot apply this assert.
             *  @todo put assert back once old DetDesc is dropped. */
            // assert( pd.get() ); // should always have an object
            dePD = pd.get();
          }
        }
      }
      return dePD;
    }

  public:
    // ray tracing methods

    /// Intersect PD panel (SIMD)
    inline auto detPlanePointSIMD( const SIMDPoint&          pGlobal, //
                                   const SIMDVector&         vGlobal, //
                                   const LHCb::RichTraceMode mode ) const {
      auto data = RayTStructSIMD{};
      // work arounds to deal with old DetElems
      // To Be done better with DD4Hep
      auto&                  ids = std::get<SIMDSmartIDs>( data );
      auto&                  pds = std::get<SIMDPDs>( data );
      DeRichPDPanel::SIMDPDs oldPDs;
      std::get<SIMDRayTResult::Results>( data ) =                //
          get()->detPlanePointSIMD( pGlobal, vGlobal,            //
                                    std::get<SIMDPoint>( data ), //
                                    ids, oldPDs, mode );
      for ( std::size_t i = 0; i < oldPDs.size(); ++i ) {
        pds[i] = dePD( ids[i] );
#ifndef USE_DD4HEP
        // FIXME : Check known to fail for dd4hep builds for now.
        // Ultimately, the whole API will be streamlined here
        // once functionality is fully ported locally.
        assert( !pds[i] || oldPDs[i] == pds[i]->get() );
#endif
      }
      return data;
    }

    /// Intersect PD window (SIMD)
    inline decltype( auto ) PDWindowPointSIMD( const SIMDPoint&          pGlobal, //
                                               const SIMDVector&         vGlobal, //
                                               const LHCb::RichTraceMode mode ) const {
      auto data = RayTStructSIMD{};
      // work arounds to deal with old DetElems
      // To Be done better with DD4Hep
      auto&                  ids = std::get<SIMDSmartIDs>( data );
      auto&                  pds = std::get<SIMDPDs>( data );
      DeRichPDPanel::SIMDPDs oldPDs;
      std::get<SIMDRayTResult::Results>( data ) =                //
          get()->PDWindowPointSIMD( pGlobal, vGlobal,            //
                                    std::get<SIMDPoint>( data ), //
                                    ids, oldPDs, mode );
      for ( std::size_t i = 0; i < oldPDs.size(); ++i ) {
        pds[i] = dePD( ids[i] );
#ifndef USE_DD4HEP
        // FIXME : Check known to fail for dd4hep builds for now.
        // Ultimately, the whole API will be streamlined here
        // once functionality is fully ported locally.
        assert( !pds[i] || oldPDs[i] == pds[i]->get() );
#endif
      }
      return data;
    }

    /// Intersect PD panel (scalar)
    inline decltype( auto ) detPlanePoint( const Gaudi::XYZPoint&    pGlobal, //
                                           const Gaudi::XYZVector&   vGlobal, //
                                           const LHCb::RichTraceMode mode ) const {
      auto data = RayTStruct{};
      // work arounds to deal with old DetElems
      // To Be done better with DD4Hep
      auto&           id = std::get<LHCb::RichSmartID>( data );
      auto&           pd = std::get<const PD*>( data );
      const DeRichPD* oldPD{nullptr};
      std::get<LHCb::RichTraceMode::RayTraceResult>( data ) =      //
          get()->detPlanePoint( pGlobal, vGlobal,                  //
                                std::get<Gaudi::XYZPoint>( data ), //
                                id, oldPD, mode );
      pd = dePD( id );
#ifndef USE_DD4HEP
      // FIXME : Check known to fail for dd4hep builds for now.
      // Ultimately, the whole API will be streamlined here
      // once functionality is fully ported locally.
      assert( !pd || pd->get() == oldPD );
#endif
      return data;
    }

    /// Intersect PD window (scalar)
    inline decltype( auto ) PDWindowPoint( const Gaudi::XYZPoint&    pGlobal, //
                                           const Gaudi::XYZVector&   vGlobal, //
                                           const LHCb::RichTraceMode mode ) const {
      auto data = RayTStruct{};
      // work arounds to deal with old DetElems
      // To Be done better with DD4Hep
      auto&           id = std::get<LHCb::RichSmartID>( data );
      auto&           pd = std::get<const PD*>( data );
      const DeRichPD* oldPD{nullptr};
      std::get<LHCb::RichTraceMode::RayTraceResult>( data ) =      //
          get()->PDWindowPoint( pGlobal, vGlobal,                  //
                                std::get<Gaudi::XYZPoint>( data ), //
                                id, oldPD, mode );
      pd = dePD( id );
#ifndef USE_DD4HEP
      // FIXME : Check known to fail for dd4hep builds for now.
      // Ultimately, the whole API will be streamlined here
      // once functionality is fully ported locally.
      assert( !pd || pd->get() == oldPD );
#endif
      return data;
    }

    /// Check if a given PD ID is a 'large' H-Type PMT
    inline bool isLargePD( const LHCb::RichSmartID pdID ) const noexcept {
      const auto pd = dePD( pdID );
      assert( pd );
      return ( pd ? pd->isHType() : false );
    }

    /// Access the local to global transform
    inline const auto& localToGlobal() const noexcept { return m_locToGloM; }

    /// Convert a given panel local point to the global frame
    template <typename POINT>
    inline POINT localToGlobal( const POINT& lPtn ) const noexcept {
      return localToGlobal() * lPtn;
    }

    /// Access the global to local transform
    inline const auto& globalToLocal() const noexcept { return m_gloToLocM; }

    /// Convert a given global point to the panel local frame
    template <typename POINT>
    inline POINT globalToLocal( const POINT& gPtn ) const noexcept {
      return globalToLocal() * gPtn;
    }

    /// Access the global to local transform (SIMD)
    inline const auto& globalToLocalSIMD() const noexcept { return m_gloToLocMSIMD; }

    /// Convert a given global point to the panel local frame (SIMD)
    template <typename POINT>
    inline POINT globalToLocalSIMD( const POINT& gPtn ) const noexcept {
      return globalToLocalSIMD() * gPtn;
    }

    /// Fill the given RichSmartID for the given point
#ifdef USE_DD4HEP
    bool smartID( const Gaudi::XYZPoint&, LHCb::RichSmartID& ) const {
      // TODO: Implement for DD4HEP (e.g. if needed by Boole)
      throw LHCb::Detector::detail::RichNotImplemented( "PDPanel::smartID" );
    }
#else
    auto smartID( const Gaudi::XYZPoint& globalPoint, LHCb::RichSmartID& id ) const noexcept {
      return get()->smartID( globalPoint, id );
    }
#endif

  public:
    // Methods that just forward to underlying detector object

    FORWARD_TO_DET_OBJ( detectionPoint )

  public:
    // messaging

    /// Overload ostream operator
    friend inline std::ostream& operator<<( std::ostream& s, const PDPanel& p ) {
      return s << "[ " << p.rich() << " " << Rich::text( p.rich(), p.side() ) //
               << " NumPDModules=" << p.pdModules().size()                    //
               << " locToGlo=" << p.localToGlobal()                           //
               << " gloToLoc=" << p.globalToLocal() << " ]";
    }

  private:
    // data

    /// The RICH detector type
    Rich::DetectorType m_rich = Rich::InvalidDetector;

    /// The RICH PD panel (up, down, left or right)
    Rich::Side m_side = Rich::InvalidSide;

    /// Owned PD objects
    ModuleArray m_PDs;

    /// DetDesc panel object
    const DetElem* m_panel{nullptr};

    /// Panel local module number offset
    LHCb::RichSmartID::DataType m_modNumOffset{0};

    /// Panel local to global frame transformation
    ROOT::Math::Transform3D m_locToGloM;

    /// Global to panel local frame transformation
    ROOT::Math::Transform3D m_gloToLocM;

    /// SIMD Global to panel local frame transformation
    Rich::SIMD::Transform3D<Rich::SIMD::DefaultScalarFP> m_gloToLocMSIMD;
  };

} // namespace Rich::Detector
