/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// Local
#include "RichDetectors/RichX.h"

// detector element
#ifdef USE_DD4HEP
#  include "Detector/Rich1/DetElemAccess/DeRich1.h"
#endif
#include "RichDet/DeRich1.h"

// STL
#include <array>
#include <ostream>
#include <type_traits>

namespace Rich::Detector {

  namespace details {

    //-----------------------------------------------------------------------------
    /** @class Rich1 Rich1.h
     *
     *  Rich1 helper class
     *
     *  @author Chris Jones
     *  @date   2020-10-05
     */
    //-----------------------------------------------------------------------------

    template <typename DETELEM, typename BASEDETELEM>
    class Rich1 final : public RichX<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = RichX<BASEDETELEM>;

    public:
      /// Default constructor
      Rich1() = default;

      /// temporary constructor that takes both old and dd4hep detelem
      template <typename OLDRICH>
      Rich1( const DETELEM& deRich, const OLDRICH& oldRich ) : RichX<BASEDETELEM>( deRich, oldRich ) {}

      /// Constructor from detector element
      Rich1( const DETELEM& deRich ) : RichX<BASEDETELEM>( deRich ) {}

    public:
      // messaging

      /// My name
      inline static const std::string Name = "Rich::Detector::Rich1";

      /// Overload ostream operator
      friend inline MsgStream& operator<<( MsgStream& s, const Rich1<DETELEM, BASEDETELEM>& r ) {
        s << Name << " ";
        r.fillStream( s );
        return s;
      }

      /// Overload ostream operator
      friend inline std::ostream& operator<<( std::ostream& s, const Rich1<DETELEM, BASEDETELEM>& r ) {
        s << Name << " ";
        r.fillStream( s );
        return s;
      }

    public:
      // conditions handling

      /// Default conditions name
      inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition<DETELEM>( "DerivedDet" );

      /// static generator function
      static auto generate( const DETELEM& r1 ) {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        MsgStream log( msgSvc, Name );
        log << MSG::DEBUG << "Creating instance from " << (void*)&r1 << " name='" << r1.name() << "' loc='"
            << DeRichLocations::location<DETELEM>() << "' access()=" << (void*)r1.access() << endmsg;
        if constexpr ( std::is_base_of_v<DetectorElement, DETELEM> ) {
          return Rich1{r1};
        } else {
          // whilst comissioning dd4hep 'side' load old DetDesc class
          // pass to constructor as well as dd4hep object for comparison etc.
          log << MSG::WARNING << "Some functionality is still being accessed from DetDesc DeRich1" << endmsg;
          auto detSvc = Gaudi::svcLocator()->service<IDataProviderSvc>( "DetectorDataSvc" );
          assert( detSvc );
          const auto oldr1 = Gaudi::Utils::getFromTS<DeRich1>( detSvc, DeRichLocations::location<DeRich1>() );
          assert( oldr1 );
          return Rich1{r1, *oldr1};
        }
      }

      /// Creates a condition derivation for the given key
      template <typename PARENT>
      static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = DefaultConditionKey ) {
        assert( parent );
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << "Rich1::addConditionDerivation : " << DeRichLocations::location<DETELEM>() << " " << key
                          << endmsg;
        }
        return parent->addSharedConditionDerivation( //
            {DeRichLocations::location<DETELEM>()},  // input condition locations
            std::move( key ),                        // output derived condition location
            &generate );
      }
    }; // namespace details

  } // namespace details

#ifdef USE_DD4HEP
  using Rich1 = details::Rich1<LHCb::Detector::DeRich1, LHCb::Detector::DeRich>;
#else
  using Rich1 = details::Rich1<DeRich1, DeRich>;
#endif

} // namespace Rich::Detector
