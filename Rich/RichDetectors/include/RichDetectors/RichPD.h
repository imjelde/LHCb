/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich/DeRichMapmt.h"
#endif
#include "RichDet/DeRichPD.h"
// eventually should be moved elsewhere
#include "RichDet/Rich1DTabProperty.h"

// local
#include "RichDetectors/Utilities.h"

#include <cstdint>
#include <ostream>

namespace Rich::Detector {

  //-----------------------------------------------------------------------------
  /** @class PD RichPD.h
   *
   *  RICH photon detector helper class
   *
   *  @author Chris Jones
   *  @date   2020-10-05
   */
  //-----------------------------------------------------------------------------

  class PD final {

  public:
    // types

    /// The underlying detector description object (DetDesc or DD4HEP)
#ifdef USE_DD4HEP
    using DetElem = DeRichPD; // TODO: Change for equivalent DD4HEP class
#else
    using DetElem = DeRichPD;
#endif

    /// Tabulated function type
    using TabFunc = const Rich::TabulatedFunction1D;

  public:
    /// Constructor from DetDesc
    PD( const DetElem& pd )
        : m_pdSmartID( pd.pdSmartID() )
        , m_pdQuantumEff( pd.pdQuantumEff() )
        , m_effPixelArea( pd.effectivePixelArea() )
        , m_numPixels( pd.effectiveNumActivePixels() )
        , m_pd( &pd ) {}

    /// DD4HEP constructor
    template <typename DEPD>
    PD( const LHCb::RichSmartID id, //
        const DEPD&             pd, //
        const DetElem&          old_pd )
        : m_pdSmartID( id )
        , m_pdQuantumEff( std::make_shared<TabFunc>( pd.QE() ) )
        , m_effPixelArea( pd.effPixelArea() )
        , m_numPixels( pd.numPixels() )
        , m_pd( &old_pd ) {}

  public:
    // Accessors

    /// PD ID
    auto pdSmartID() const noexcept { return m_pdSmartID; }

    /// PD QE Curve
    auto pdQuantumEff() const noexcept { return m_pdQuantumEff.get(); }

    /// Effective pixel area
    auto effectivePixelArea() const noexcept { return m_effPixelArea; }

    /// effective number of pixels per PD
    auto effectiveNumActivePixels() const noexcept { return m_numPixels; }

    /// Chedck if this is an H-Type (large) PD
    auto isHType() const noexcept { return pdSmartID().isHTypePMT(); }

  public:
    // Accesssors that just forward on to underlying object

    FORWARD_TO_DET_OBJ( detectionPoint )

  public:
    /// Get access to the underlying object
    inline const DetElem* get() const noexcept { return m_pd; }

  public:
    // messaging

    /// Overload ostream operator
    friend inline std::ostream& operator<<( std::ostream& s, const PD& pd ) {
      s << "[ PD ";
      pd.pdSmartID().fillStream( s, false ); // Never include bit dump
      return s << " EffPixelArea=" << pd.effectivePixelArea() << " EffNumPixels=" << pd.effectiveNumActivePixels()
               << " QE=" << *pd.pdQuantumEff() << " ]";
    }

  private:
    // data

    /// PD Smart ID
    LHCb::RichSmartID m_pdSmartID;

    /// QE Curve
    std::shared_ptr<TabFunc> m_pdQuantumEff;

    /// The effective pixel area (in mm^2) including any demagnification factors
    float m_effPixelArea{0};

    /// (effective) Number of pixels
    float m_numPixels{0};

    /// DetDesc Rich1 object
    const DetElem* m_pd{nullptr};
  };

} // namespace Rich::Detector
