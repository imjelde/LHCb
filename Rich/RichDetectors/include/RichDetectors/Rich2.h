/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// Local
#include "RichDetectors/RichX.h"

// Detector description
#ifdef USE_DD4HEP
#  include "Detector/Rich2/DetElemAccess/DeRich2.h"
#endif
#include "RichDet/DeRich2.h"

// STL
#include <array>
#include <ostream>
#include <type_traits>

namespace Rich::Detector {

  namespace details {

    //-----------------------------------------------------------------------------
    /** @class Rich2 Rich2.h
     *
     *  Rich2 helper class
     *
     *  @author Chris Jones
     *  @date   2020-10-05
     */
    //-----------------------------------------------------------------------------

    template <typename DETELEM, typename BASEDETELEM>
    class Rich2 final : public RichX<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = RichX<BASEDETELEM>;

    public:
      /// Default constructor
      Rich2() = default;

      /// temporary constructor that takes both old and dd4hep detelem
      template <typename OLDRICH>
      Rich2( const DETELEM& deRich, const OLDRICH& oldRich ) : RichX<BASEDETELEM>( deRich, oldRich ) {}

      /// Constructor from detector element
      Rich2( const DETELEM& deRich ) : RichX<BASEDETELEM>( deRich ) {}

    public:
      // messaging

      /// My name
      inline static const std::string Name = "Rich::Detector::Rich2";

      /// Overload MsgStream operator
      friend inline MsgStream& operator<<( MsgStream& s, const Rich2<DETELEM, BASEDETELEM>& r ) {
        s << Name << " ";
        r.fillStream( s );
        return s;
      }

      /// Overload ostream operator
      friend inline std::ostream& operator<<( std::ostream& s, const Rich2<DETELEM, BASEDETELEM>& r ) {
        s << Name << " ";
        r.fillStream( s );
        return s;
      }

    public:
      // conditions handling

      /// Default conditions name
      inline static const std::string DefaultConditionKey = DeRichLocations::derivedCondition<DETELEM>( "DerivedDet" );

      /// static generator function
      static auto generate( const DETELEM& r2 ) {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        MsgStream log( msgSvc, Name );
        log << MSG::DEBUG << "Creating instance from " << (void*)&r2 << " name='" << r2.name() << "' loc='"
            << DeRichLocations::location<DETELEM>() << "' access()=" << (void*)r2.access() << endmsg;
        if constexpr ( std::is_base_of_v<DetectorElement, DETELEM> ) {
          return Rich2{r2};
        } else {
          // whilst comissioning dd4hep 'side' load old DetDesc class
          // pass to constructor as well as dd4hep object for comparison etc.
          log << MSG::WARNING << "Some functionality is still being accessed from DetDesc DeRich2" << endmsg;
          auto detSvc = Gaudi::svcLocator()->service<IDataProviderSvc>( "DetectorDataSvc" );
          assert( detSvc );
          const auto oldr2 = Gaudi::Utils::getFromTS<DeRich2>( detSvc, DeRichLocations::location<DeRich2>() );
          assert( oldr2 );
          return Rich2{r2, *oldr2};
        }
      }

      /// Creates a condition derivation for the given key
      template <typename PARENT>
      static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = DefaultConditionKey ) {
        assert( parent );
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << "Rich2::addConditionDerivation : " << DeRichLocations::location<DETELEM>() << " " << key
                          << endmsg;
        }
        return parent->addSharedConditionDerivation( //
            {DeRichLocations::location<DETELEM>()},  // input condition locations
            std::move( key ),                        // output derived condition location
            &generate );
      }
    };

  } // namespace details

#ifdef USE_DD4HEP
  using Rich2 = details::Rich2<LHCb::Detector::DeRich2, LHCb::Detector::DeRich>;
#else
  using Rich2 = details::Rich2<DeRich2, DeRich>;
#endif

} // namespace Rich::Detector
