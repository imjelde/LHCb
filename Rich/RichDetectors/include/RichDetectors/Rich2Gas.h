/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// Gaudi
#include "GaudiKernel/Bootstrap.h"
#include "GaudiKernel/IMessageSvc.h"
#include "GaudiKernel/ISvcLocator.h"
#include "GaudiKernel/MsgStream.h"

// local
#include "RichDetectors/Rich2.h"
#include "RichDetectors/RichRadiator.h"

// detector element
#ifdef USE_DD4HEP
#  include "Detector/Rich2/DetElemAccess/DeRich2.h"
#  include "Detector/Rich2/DetElemAccess/DeRich2RadiatorGas.h"
#endif
#include "RichDet/DeRichRadiator.h"

// STL
#include <array>

namespace Rich::Detector {

  namespace details {

    //-----------------------------------------------------------------------------
    /** @class Rich2Gas.h
     *
     *  RICH2 Radiator
     *
     *  @author Chris Jones
     *  @date   2021-12-07
     */
    //-----------------------------------------------------------------------------

    template <typename DETELEM, typename BASEDETELEM>
    class Rich2Gas : public Radiator<BASEDETELEM> {

    public:
      // types

      // expose base class types
      using DetElem     = DETELEM;
      using BaseDetElem = BASEDETELEM;
      using Base        = Radiator<BASEDETELEM>;
      using RICH        = Rich::Detector::Rich2::DetElem;

    public:
      // constructors

      /// Default Constructor
      Rich2Gas() = default;

      /// Constructor from Det Elem
      template <typename RAD>
      Rich2Gas( const RAD& rad ) : Radiator<DETELEM>( rad ) {}

      /// temporary constructor that takes both old and dd4hep detelem
      template <typename RAD, typename OLDRAD>
      Rich2Gas( const RAD& rad, const OLDRAD& oldrad ) : Radiator<BASEDETELEM>( rad, oldrad ) {}

    public:
      // messaging

      /// My name
      inline static const std::string Name = "Rich::Detector::Rich2Gas";

      /// Overload ostream operator
      friend inline std::ostream& operator<<( std::ostream& s, const Rich2Gas<DETELEM, BASEDETELEM>& r ) {
        s << Name << " ";
        r.fillStream( s );
        return s;
      }

    public:
      // conditions handling

      /// Default conditions name
      inline static const std::string DefaultConditionKey =
          DeRichLocations::derivedCondition<RICH>( "DerivedRadiator" );

      /// static generator function
      static auto generate( const DETELEM& g ) {
        auto msgSvc = Gaudi::svcLocator()->service<IMessageSvc>( "MessageSvc" );
        assert( msgSvc );
        MsgStream log( msgSvc, Name );
        log << MSG::DEBUG << "Creating instance from " << (void*)&g << " name='" << g.name() << "' loc='"
            << DeRichLocations::location<DETELEM>( Rich::Rich2Gas ) << "' access()=" << (void*)g.access() << endmsg;
        if constexpr ( std::is_base_of_v<DetectorElement, DETELEM> ) {
          return Rich2Gas{g};
        } else {
          // whilst comissioning dd4hep 'side' load old DetDesc class
          // pass to constructor as well as dd4hep object for comparison etc.
          log << MSG::WARNING << "Some functionality is still being accessed from DetDesc DeRich2Gas" << endmsg;
          auto detSvc = Gaudi::svcLocator()->service<IDataProviderSvc>( "DetectorDataSvc" );
          assert( detSvc );
          const auto oldg = Gaudi::Utils::getFromTS<DeRichRadiator>(
              detSvc, DeRichLocations::location<DeRichRadiator>( Rich::Rich2Gas ) );
          assert( oldg );
          return Rich2Gas{g, *oldg};
        }
      }

#ifdef USE_DD4HEP
      /// static generator from RICH object
      static auto generate_from_rich( const RICH& r ) {
        auto g = r.radiatorGas();
        return generate( g );
      }
#endif

      /// Creates a condition derivation for the given key
      template <typename PARENT>
      static auto addConditionDerivation( PARENT* parent, LHCb::DetDesc::ConditionKey key = DefaultConditionKey ) {
        assert( parent );
        if ( parent->msgLevel( MSG::DEBUG ) ) {
          parent->debug() << "Rich2Gas::addConditionDerivation : Key=" << key << endmsg;
        }
#ifdef USE_DD4HEP
        // Cannot yet directly instanciate dd4hep gas volumes, for some reason....
        // so for now access indirectly via the main RICH object
        return parent->addSharedConditionDerivation( //
            {DeRichLocations::location<RICH>()},     // input condition location
            std::move( key ),                        // output derived condition location
            &generate_from_rich );
#else
        return parent->addSharedConditionDerivation(                //
            {DeRichLocations::location<DETELEM>( Rich::Rich2Gas )}, // input condition location
            std::move( key ),                                       // output derived condition location
            &generate );
#endif
      }
    };

  } // namespace details

  using OldRich2Gas = details::Rich2Gas<DeRichRadiator, DeRichRadiator>;
#ifdef USE_DD4HEP
  using DD4Rich2Gas = details::Rich2Gas<LHCb::Detector::DeRich2Gas, DeRichRadiator>;
  using Rich2Gas    = DD4Rich2Gas;
#else
  using Rich2Gas = OldRich2Gas;
#endif

} // namespace Rich::Detector
