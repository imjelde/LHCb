/*****************************************************************************\
* (c) Copyright 2000-2020 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// local
#include "RichFutureDAQ/RichPDMDBDecodeMapping.h"

// RICH
#include "RichUtils/RichException.h"
#include "RichUtils/ToArray.h"
#include "RichUtils/ZipRange.h"

// Gaudi
#include "Gaudi/Algorithm.h"

// Messaging
#include "RichFutureUtils/RichMessaging.h"
#define debug( ... )                                                                                                   \
  if ( messenger() ) { ri_debug( __VA_ARGS__ ); }
#define verbo( ... )                                                                                                   \
  if ( messenger() ) { ri_verbo( __VA_ARGS__ ); }

// STL
#include <vector>

using namespace Rich::Future::DAQ;
using namespace Rich::DAQ;
using namespace Rich::Detector;

bool PDMDBDecodeMapping::fillRType( const DecodingConds& C ) {

  bool OK = true;

  // Loop over RICHes
  for ( const auto rich : Rich::detectors() ) {

    // load the condition
    const auto cond = C.rTypeConds[rich];

    // load the active PDBDBs
    const auto activePDMDBs = condition_param<std::vector<int>>( cond, "ActivePDMDBs" );
    debug( " -> Found ", activePDMDBs.size(), " active R-type PDMDBs for ", rich, endmsg );

    // Loop over active PDMDBs
    for ( const auto pdmdb : activePDMDBs ) {
      const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "R";

      // load the active Frames
      const auto activeFrames = condition_param<std::vector<int>>( cond, pdmdbS + "_ActiveFrames" );
      verbo( "  -> ", pdmdbS, " active frames ", activeFrames, endmsg );

      // loop over active frames
      for ( const auto frame : activeFrames ) {
        const auto frameS = "Frame" + std::to_string( frame );

        // load ECs, PMTs and Anodes data
        const std::string pA = pdmdbS + "_" + frameS;
        const auto        ECs =
            Rich::toArray<ElementaryCell::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_ECs" ) );
        const auto PMTs =
            Rich::toArray<PMTInEC::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_PMTs" ) );
        const auto Anodes =
            Rich::toArray<AnodeIndex::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_Anodes" ) );

        verbo( "   -> ", frameS, endmsg );

        // fill the data cache
        assert( (std::size_t)rich < m_pdmDataR.size() );
        auto& richD = m_pdmDataR[rich];
        assert( (std::size_t)pdmdb < richD.size() );
        auto& pdmdbD = richD[pdmdb];
        assert( (std::size_t)frame < pdmdbD.size() );
        auto& frameD = pdmdbD[frame];

        // loop over bit data and fill
        std::uint16_t bit{0};
        for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
          assert( (std::size_t)bit < frameD.size() ); // check bit index in range
          assert( !frameD[bit].isValid() );           // should not be initialised yet
          frameD[bit] = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
          verbo( "    -> Bit ", bit, frameD[bit], endmsg );
          ++bit;
        } // bit loop

      } // frame loop
    }   // PDMDB loop

  } // RICH loop

  return OK;
}

bool PDMDBDecodeMapping::fillHType( const DecodingConds& C ) {

  bool OK = true;

  // load condition
  const auto cond = C.hTypeCond;

  // load the active PDBDBs
  const auto activePDMDBs = condition_param<std::vector<int>>( cond, "ActivePDMDBs" );
  debug( " -> Found ", activePDMDBs.size(), " active H-type PDMDBs", endmsg );

  // Loop over active PDMDBs
  for ( const auto pdmdb : activePDMDBs ) {
    const auto pdmdbS = "PDMDB" + std::to_string( pdmdb ) + "H";

    // load the active Frames
    const auto activeFrames = condition_param<std::vector<int>>( cond, pdmdbS + "_ActiveFrames" );
    verbo( "  -> ", pdmdbS, " active frames ", activeFrames, endmsg );

    // loop over active frames
    for ( const auto frame : activeFrames ) {
      const auto frameS = "Frame" + std::to_string( frame );

      // load ECs, PMTs and Anodes data
      const std::string pA = pdmdbS + "_" + frameS;
      const auto        ECs =
          Rich::toArray<ElementaryCell::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_ECs" ) );
      const auto PMTs =
          Rich::toArray<PMTInEC::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_PMTs" ) );
      const auto Anodes =
          Rich::toArray<AnodeIndex::Type, BitsPerFrame>( condition_param<std::vector<int>>( cond, pA + "_Anodes" ) );

      verbo( "   -> ", frameS, endmsg );

      // fill the data cache
      assert( (std::size_t)pdmdb < m_pdmDataH.size() );
      auto& pdmdbD = m_pdmDataH[pdmdb];
      assert( (std::size_t)frame < pdmdbD.size() );
      auto& frameD = pdmdbD[frame];

      // loop over bit data and fill
      std::uint16_t bit{0};
      for ( const auto&& [ec, pmt, anode] : Ranges::ConstZip( ECs, PMTs, Anodes ) ) {
        assert( (std::size_t)bit < frameD.size() ); // check bit index in range
        assert( !frameD[bit].isValid() );           // should not be initialised yet
        frameD[bit] = BitData( ElementaryCell( ec ), PMTInEC( pmt ), AnodeIndex( anode ) );
        verbo( "    -> Bit ", bit, frameD[bit], endmsg );
        ++bit;
      } // bit loop
    }
  }

  return OK;
}
