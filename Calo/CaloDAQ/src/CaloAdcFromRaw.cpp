/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Include files

// local
#include "CaloAdcFromRaw.h"

//-----------------------------------------------------------------------------
// Implementation file for class : CaloAdcFromRaw
//
//  generic algorithm to put Calo(L0)Adc and L0PrsSpdBit on TES from rawEvent
//  Possibility to apply condDB calibration factor on ADC
//  ---> to be used to produce mis-calibrated data from calibrated ADC !!
//
// 2009-04-07 : Olivier Deschamps
//-----------------------------------------------------------------------------

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( CaloAdcFromRaw )

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
CaloAdcFromRaw::CaloAdcFromRaw( const std::string& name, ISvcLocator* pSvcLocator )
    : GaudiAlgorithm( name, pSvcLocator ), m_detectorName( details::alg_name_to_detector( name ) ) {
  switch ( m_detectorName ) {
  case details::DetectorName_t::Ecal:
    m_location = LHCb::CaloAdcLocation::Ecal;
    m_caloName = DeCalorimeterLocation::Ecal;
    m_offset   = +256;
    break;
  case details::DetectorName_t::Hcal:
    m_location = LHCb::CaloAdcLocation::Hcal;
    m_caloName = DeCalorimeterLocation::Hcal;
    m_offset   = +256;
    break;
  default:
    m_location = "";
    m_caloName = "";
    m_offset   = 0;
  }
}
//=============================================================================
// Initialization
//=============================================================================
StatusCode CaloAdcFromRaw::initialize() {
  StatusCode sc = GaudiAlgorithm::initialize(); // must be executed first
  if ( sc.isFailure() ) return sc;              // error printed already by GaudiAlgorithm

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Initialize" << endmsg;

  // get detector elements
  if ( m_caloName.empty() ) return Error( "Unknown calo detector name ", StatusCode::FAILURE );
  m_calo = getDet<DeCalorimeter>( m_caloName );
  // get data provider tools
  m_data = tool<ICaloDataProvider>( "CaloDataProvider", toString( m_detectorName ) + "DataProvider", this );

  return StatusCode::SUCCESS;
}

//=============================================================================
// Main execution
//=============================================================================
StatusCode CaloAdcFromRaw::execute() {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  // ADCs (ecal/hcal/prs)
  if ( !m_location.empty() && m_data->getBanks() ) {
    LHCb::CaloAdcs* outs = new LHCb::CaloAdcs();
    put( outs, m_location );
    const CaloVector<LHCb::CaloAdc>& adcs = m_data->adcs();

    if ( msgLevel( MSG::DEBUG ) ) debug() << " #ADCS " << adcs.size() << endmsg;
    for ( LHCb::CaloAdc adc : adcs ) {
      LHCb::Calo::CellID id    = adc.cellID();
      int                value = adc.adc();
      double             calib = 1.;
      if ( m_calib ) {
        calib = m_calo->cellParam( id ).calibration();
        value = ( calib > 0 ) ? int( (double)adc.adc() / calib ) : 0;
        if ( m_calo->isDead( id ) ) value = 0;
      }
      value = std::clamp( value, -m_offset, static_cast<int>( m_calo->adcMax() ) - m_offset );
      try {
        auto out = std::make_unique<LHCb::CaloAdc>( id, value );
        if ( msgLevel( MSG::DEBUG ) )
          debug() << "Inserting : " << id << " adc = " << value << "  =  " << adc.adc() << " / " << calib
                  << "  (dead channel ? " << m_calo->isDead( id ) << ")" << endmsg;
        outs->insert( out.get() );
        out.release();
      } catch ( GaudiException& exc ) {
        ++m_duplicateADC;
        std::ostringstream os( "" );
        os << "Duplicate CaloADC for channel " << id << std::endl;
        Warning( os.str(), StatusCode::SUCCESS ).ignore();
        int                         card   = m_data->deCalo()->cardNumber( id );
        int                         tell1  = m_data->deCalo()->cardToTell1( card );
        LHCb::RawBankReadoutStatus& status = m_data->status();
        status.addStatus( tell1, LHCb::RawBankReadoutStatus::Status::DuplicateEntry );
      }
    }
  }
  return StatusCode::SUCCESS;
}

//=============================================================================
