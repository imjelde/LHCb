/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
#pragma once
#include "CaloDet/DeCalorimeter.h"
#include "Event/CaloCluster.h"
#include "Event/CaloDataFunctor.h"

/** @namespace ClusterFunctors ClusterFunctors.h CaloUtils/ClusterFunctors.h
 *
 *  collection of useful functors for dealing with
 *  CaloCluster objects
 *
 *  @author Ivan Belyaev
 *  @date   04/07/2001
 */
namespace LHCb {
  namespace ClusterFunctors {

    /** @fn StatusCode throwException( const std::string& message )
     *  throw the exception
     *  @exception CaloException
     *  @param message exception message
     *  @return status code (fictive)
     */
    StatusCode throwException( const std::string& message );

    /** Calculate the "energy" of the cluster
     *  as a sum of energies of its digits, weighted with energy fractions
     *  @param   cl  pointer to cluster
     *  @return      "energy" of cluster
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   xx/xx/xxxx
     */
    double energy( const CaloCluster* cl );

    /** Useful function to determine, if clusters have at least one common cell.
     *
     *  For invalid arguments return "false"
     *
     *  @param   cl1   pointer to first  cluster
     *  @param   cl2   pointer to second cluster
     *  @return "true" if clusters have at least 1 common cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   xx/xx/xxxx
     */
    bool overlapped( const CaloCluster* cl1, const CaloCluster* cl2 );

    /** The simple class/function to get the index of area in Calo
     *  "calo-area" of cluster is defined as "calo-area" index of seed cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterArea final {
    public:
      /** the only one essential method
       *  @exception CaloException for invalid cluster
       *  @param cluster pointer to CaloCluster object
       *  @return index of calorimeter area for given cluster
       */
      unsigned int operator()( const CaloCluster* cluster ) const {
        if ( 0 == cluster ) { Exception( " CaloCluster* points to NULL! " ); }
        // find seed cell
        auto seed = Calo::Functor::clusterLocateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                       CaloDigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == seed ) { Exception( " 'SeedCell' is not found!" ); }
        const CaloDigit* digit = seed->digit();
        if ( 0 == digit ) { Exception( " CaloDigit* points to NULL for seed!" ); }
        // get the area
        return digit->cellID().area();
      };

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       */
      void Exception( const std::string& message ) const {
        throwException( "ClusterArea() " + message ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      };
    };

    /** The simple class/function to get the index of area in Calo
     *  "calo-area" of cluster is defined as "calo-area" index of seed cell
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline unsigned int clusterArea( const CaloCluster* cluster ) {
      ClusterArea evaluator;
      return evaluator( cluster );
    }

    /** @class ClusterCalo
     *
     *  The simple class/function to get the index of calorimeter.
     *  Index of calorimeter for clusers
     *  is defined as "calo" index of the seed cell
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterCalo final {
    public:
      /** the only one essential method
       *  @exception CaloException for invalid cluster
       *  @param cluster pointer to CaloCluster object
       *  @return index of calorimeter area for given cluster
       */
      unsigned int operator()( const CaloCluster* cluster ) const {
        if ( 0 == cluster ) { Exception( " CaloCluster* points to NULL! " ); }
        // find seed cell
        auto seed = Calo::Functor::clusterLocateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                       Calo::DigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == seed ) { Exception( " 'SeedCell' is not found!" ); }
        const CaloDigit* digit = seed->digit();
        if ( 0 == digit ) { Exception( " CaloDigit* points to NULL for seed!" ); }
        // get the area
        return digit->cellID().calo();
      };

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       */
      void Exception( const std::string& message ) const {
        throwException( "ClusterCalo() " + message ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      };
    };

    /** The simple class/function to get the index of calorimeter.
     *  Index of calorimeter for clusers
     *  is defined as "calo" index of the seed cell
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline unsigned int clusterCalo( const CaloCluster* cluster ) {
      ClusterCalo evaluator;
      return evaluator( cluster );
    }

    /** @class  ClusterFromCalo
     *
     *  simple predicate/functor to select cluster from given calorimeter
     *
     *  @see ClusterCalo
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterFromCalo final {
    public:
      /** constructor
       *  @see         CaloCellCode
       *  @exception   CaloException for invalid calorimeter name
       *  @param calo  name of calorimeter (full or abbreviated)
       */
      ClusterFromCalo( const std::string& calo ) : m_calo( CaloCellCode::CaloNumFromName( calo ) ), m_evaluator() {
        if ( 0 > m_calo ) { Exception( "Wrong Calo Name='" + calo + "'" ); }
      };
      /** the only one essential method
       *  @see ClusterCalo
       *  @exception CaloException fro ClusterCalo class
       *  @param cluster pointer to CaloCluster object
       *  @return true if cluster belongs to tehselected calorimter
       */
      bool operator()( const CaloCluster* cluster ) const { return (int)m_evaluator( cluster ) == m_calo; };
      /** set new calorimeter name
       *  @exception   CaloException for invalid calorimeter name
       *  @param calo  name of calorimeter (full or abbreviated)
       */
      void setCalo( const std::string& calo ) {
        m_calo = CaloCellCode::CaloNumFromName( calo );
        if ( 0 > m_calo ) { Exception( "Wrong Calo Name='" + calo + "'" ); }
      }

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       */
      void Exception( const std::string& message ) const {
        throwException( "ClusterFromCalo() " + message ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      };

    private:
      /// default constructor is private
      ClusterFromCalo();

    private:
      int         m_calo;
      ClusterCalo m_evaluator;
    };

    /** @class  ClusterFromArea
     *
     *  simple predicate/functor to select cluster from given
     *  area in calorimeter
     *
     *  @see ClusterArea
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ClusterFromArea final {
    public:
      /** constructor
       *  @param area area index
       */
      ClusterFromArea( const unsigned int area ) : m_area( area ), m_evaluator() {}
      /** the only one essential method
       *  @see ClusterArea
       *  @exception CaloException from ClusterArea class
       *  @param cluster pointer to CaloCluster object
       *  @return true if cluster belongs to the selected area in calorimter
       */
      bool operator()( const CaloCluster* cluster ) const { return m_evaluator( cluster ) == m_area; };

    private:
      unsigned int m_area;
      ClusterArea  m_evaluator;
    };

    /** @class OnTheBoundary
     *
     *  Simple utility to locate clusters, placed on the boundaries
     *  between different calorimeter zones
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class OnTheBoundary final {
    public:
      /** the only one essential method
       *  @param cluster pointer to CaloCluster object
       *  @return true if cluster is on the boundary,
       *               for empty clusters "false" is returned
       *  @exception CaloException for invalid clusters
       */
      bool operator()( const CaloCluster* cluster ) const {
        if ( !cluster ) { Exception( "CaloCluster* points to NULL!" ); }
        const CaloCluster::Entries& entries = cluster->entries();
        if ( entries.size() <= 1 ) { return false; } // RETURN !!!
        auto seed =
            Calo::Functor::clusterLocateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
        if ( entries.end() == seed ) { Exception( "'SeedCell' is not found!" ); }
        const CaloDigit* sd = seed->digit();
        if ( !sd ) { Exception( "CaloDigit* for 'SeedCell' is  NULL!" ); }
        const unsigned int seedArea = sd->cellID().area();
        for ( auto entry = entries.begin(); entries.end() != entry; ++entry ) {
          const CaloDigit* digit = entry->digit();
          if ( !digit ) { continue; }
          if ( seedArea != digit->cellID().area() ) { return true; }
        } // end of loop over all cluyster entries
        //
        return false;
      };

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       */
      void Exception( const std::string& message ) const {
        throwException( "OnTheBoundary() " + message ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      };
    };

    /** Simple utility to locate clusters, placed on the boundaries
     *  between different calorimeter zones
     *
     *  @see OnTheBoundary
     *  @param cluster pointer to CaloCluster object
     *  @return true if cluster is on the boundary,
     *               for empty clusters "false" is returned
     *  @exception CaloException for invalid clusters
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    inline bool onTheBoundary( const CaloCluster* cluster ) {
      OnTheBoundary evaluator;
      return evaluator( cluster );
    }

    /** @class ZPosition
     *
     *  The simple function to get the cluster z-posiiton as a z-position of
     *  "Seed Cell"
     *
     *  @author Vanya Belyaev Ivan.Belyaev@itep.ru
     *  @date   02/12/2001
     */
    class ZPosition final {
    public:
      /** the explicit constructor
       *  @param detector source of calorimeter detector information
       */
      ZPosition( const DeCalorimeter* detector ) : m_detector( detector ){};
      /** the only one essential method
       *  @exception CaloException if detector is not valid
       *  @exception CaloException if cluster is NULL
       *  @exception CaloException if cluster is Empty
       *  @exception CaloException if cluster has no SEED cell
       *  @exception CaloException if SEED digit is NULL
       *  @param cluster pointer to CaloCluster object
       *  @return z-position
       */
      double operator()( const CaloCluster* cluster ) const {
        if ( 0 == m_detector ) { Exception( " DeCalorimeter*     points to NULL! " ); }
        if ( 0 == cluster ) { Exception( " const CaloCluster* points to NULL! " ); }
        if ( cluster->entries().empty() ) { Exception( " CaloCluster is empty! " ); }
        auto iseed = Calo::Functor::clusterLocateDigit( cluster->entries().begin(), cluster->entries().end(),
                                                        CaloDigitStatus::Mask::SeedCell );
        if ( cluster->entries().end() == iseed ) { Exception( " The Seed Cell is not found! " ); }
        ///
        const CaloDigit* seed = iseed->digit();
        if ( 0 == seed ) { Exception( " The Seed Digit points to NULL! " ); }
        //
        return m_detector->cellCenter( seed->cellID() ).z();
      };

    protected:
      /** throw the exception
       *  @see CaloException
       *  @exception CaloException
       *  @param     message exception message
       */
      void Exception( const std::string& message ) const {
        throwException( "ZPosition() " + message ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
      };

    private:
      const DeCalorimeter* m_detector; ///< source of geometry information
    };

    /** Helpful function to tag the sub cluster according to the
     *  fractionc evaluated by "evaluator"
     *
     *  Error codes
     *
     *          - 225 : CaloCluster* points to NULL
     *          - 226 : Entry with status 'SeedCell' is not found
     *          - 227 : Entry with status 'SeedCell' is invalid
     *
     *
     *  @param cluster pointer to CaloCluster object
     *  @param evaluator  evaluator object
     *  @param modify  flag for modification of energy fractions
     *  @param tag     tag to be set for cells with modified fractions
     *
     *  @author Vanya Belyaev Ivan Belyaev
     *  @date   01/04/2002
     */

    template <class EVALUATOR>
    StatusCode tagTheSubCluster( CaloCluster* cluster, const EVALUATOR& evaluator, const bool modify,
                                 const CaloDigitStatus::Status& status, const CaloDigitStatus::Status& tag ) {
      // check the arguments
      if ( 0 == cluster ) { return StatusCode( 225 ); }
      // get all entries
      auto& entries = cluster->entries();
      // find seed digit
      auto seedEntry =
          Calo::Functor::clusterLocateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
      // check the seed
      if ( entries.end() == seedEntry ) { return StatusCode( 226 ); }
      const CaloDigit* seed = seedEntry->digit();
      if ( 0 == seed ) { return StatusCode( 227 ); }
      // loop over all entries

      for ( auto entry = entries.begin(); entries.end() != entry; ++entry ) {
        // reset existing statuses
        // std::cout << entry->digit()->cellID() << " initial status " << entry->status() << std::endl;
        entry->removeStatus( tag );
        entry->removeStatus( status );
        // std::cout << " removing status " << tag << " & " << status << " -> " << entry->status() << std::endl;

        // skip invalid digits
        const CaloDigit* digit = entry->digit();
        if ( 0 == digit ) { continue; }
        // evaluate the fraction
        const double fraction = evaluator( seed->cellID(), digit->cellID() );
        if ( 0 >= fraction ) { continue; }
        // update statuses
        entry->addStatus( status );
        // std::cout << " New status " << entry->status() << std::endl;

        if ( !modify ) { continue; }
        // modify the fractions
        entry->setFraction( entry->fraction() * fraction );
        entry->addStatus( tag );
      }
      ///
      return StatusCode::SUCCESS;
    }

    /** Helpful function to untag the sub cluster according to the
     *  fraction evaluated by "evaluator"
     *
     *  Error codes
     *
     *          - 225 : CaloCluster* points to NULL
     *          - 226 : Entry with status 'SeedCell' is not found
     *          - 227 : Entry with status 'SeedCell' is invalid
     *
     *
     *  @param cluster pointer to CaloCluster object
     *  @param evaluator  evaluator object
     *  @param tag     tag to be set for cells with modified fractions
     *
     *  @see CaloCuster
     *  @see tagTheSubCluster
     *
     *  @author Vanya Belyaev Ivan Belyaev
     *  @date   01/04/2002
     */

    template <class EVALUATOR>
    StatusCode untagTheSubCluster( CaloCluster* cluster, const EVALUATOR& evaluator, CaloDigitStatus::Status tag ) {
      // check the arguments
      if ( !cluster ) { return StatusCode( 225 ); }
      // get all entries
      auto& entries = cluster->entries();
      // find seed digit
      auto seedEntry =
          Calo::Functor::clusterLocateDigit( entries.begin(), entries.end(), CaloDigitStatus::Mask::SeedCell );
      // check the seed
      if ( entries.end() == seedEntry ) { return StatusCode( 226 ); }
      const CaloDigit* seed = seedEntry->digit();
      if ( !seed ) { return StatusCode( 227 ); }
      // loop over all entries
      for ( auto& entry : entries ) {
        // reset existing statuses
        entry.addStatus( {CaloDigitStatus::Mask::UseForEnergy, CaloDigitStatus::Mask::UseForPosition,
                          CaloDigitStatus::Mask::UseForCovariance} );
        // tagged ?
        if ( entry.status().noneOf( tag ) ) continue; // CONTINUE
        // skip invalid digits
        const CaloDigit* digit = entry.digit();
        if ( !digit ) continue; // CONTINUE
        // evaluate the fraction
        const double fraction = evaluator( seed->cellID(), digit->cellID() );
        if ( 0 >= fraction ) continue; // CONTINUE
        // modify the fractions
        entry.setFraction( entry.fraction() / fraction );
        entry.removeStatus( tag );
      }
      ///
      return StatusCode::SUCCESS;
    }

  } // namespace ClusterFunctors
} // namespace LHCb

// ============================================================================
