###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Packing configuration for HLT persisted reconstruction."""

import os
from PyConf.Algorithms import PackTrack, UnpackTrack
from PyConf.Algorithms import PackCaloHypo, UnpackCaloHypo
from PyConf.Algorithms import PackProtoParticle, UnpackProtoParticle
from PyConf.Algorithms import PackRecVertex, UnpackRecVertex
from PyConf.Algorithms import MuonPIDPacker
from PyConf.Algorithms import MuonPIDUnpacker as UnpackMuonPIDs
from PyConf.Algorithms import RichPIDPacker as PackRichPIDs
from PyConf.Algorithms import RichPIDUnpacker as UnpackRichPIDs

from PyConf.application import make_data_with_FetchDataFromFile
from PyConf.components import force_location
from PyConf.dataflow import dataflow_config

from Gaudi.Configuration import WARNING


class PackingDescriptor(object):
    """Holds information of how and where to (un)pack objects.

    Attributes:
        name: Descriptive name of the object
        location: Packed object location, stream will be prepended
        packer: Algorithm used for packing
        unpacker: Algorithm used for unpacking

    """

    def __init__(self, name, location, packer, unpacker):
        self.name = name
        self.location = location
        self.packer = packer
        self.unpacker = unpacker

    def copy(self, **kwargs):
        new = PackingDescriptor(self.name, self.location, self.packer,
                                self.unpacker)
        for k, v in kwargs.items():
            setattr(new, k, v)
        return new


standardDescriptors = {}

_standardDescriptors = [

    # Tracks
    PackingDescriptor(
        name="Tracks",
        location="pRec/Track/Best",
        packer=PackTrack,
        unpacker=UnpackTrack),

    # PVs
    PackingDescriptor(
        name="PVs",
        location="pRec/Vertex/Primary",
        packer=PackRecVertex,
        unpacker=UnpackRecVertex),

    #  ChargedProtos
    PackingDescriptor(
        name="ChargedProtos",
        location="pRec/ProtoP/Charged",
        packer=PackProtoParticle,
        unpacker=UnpackProtoParticle),

    #  NeutralProtos
    PackingDescriptor(
        name="NeutralProtos",
        location="pRec/ProtoP/Neutrals",
        packer=PackProtoParticle,
        unpacker=UnpackProtoParticle),

    #  CaloElectrons
    PackingDescriptor(
        name="CaloElectrons",
        location="pRec/Calo/Electrons",
        packer=PackCaloHypo,
        unpacker=UnpackCaloHypo),

    #  CaloPhotons
    PackingDescriptor(
        name="CaloPhotons",
        location="pRec/Calo/Photons",
        packer=PackCaloHypo,
        unpacker=UnpackCaloHypo),

    #  CaloMergedPi0s
    PackingDescriptor(
        name="CaloMergedPi0s",
        location="pRec/Calo/MergedPi0s",
        packer=PackCaloHypo,
        unpacker=UnpackCaloHypo),

    #  CaloSplitPhotons
    PackingDescriptor(
        name="CaloSplitPhotons",
        location="pRec/Calo/SplitPhotons",
        packer=PackCaloHypo,
        unpacker=UnpackCaloHypo),

    #  MuonPIDs
    PackingDescriptor(
        name="MuonPIDs",
        location="pRec/Muon/MuonPID",
        packer=MuonPIDPacker,
        unpacker=UnpackMuonPIDs),

    #  RichPIDs
    PackingDescriptor(
        name="RichPIDs",
        location="pRec/Rich/PIDs",
        packer=PackRichPIDs,
        unpacker=UnpackRichPIDs),
]

standardDescriptors['Upgrade'] = {
    i.name: i.copy(name=None)
    for i in _standardDescriptors
}

standardOutputs = {}

standardOutputs['Upgrade'] = {
    "Tracks": "Rec/Track/Best",
    "PVs": "Rec/Vertex/Primary",
    "ChargedProtos": "Rec/ProtoP/Charged",
    "NeutralProtos": "Rec/ProtoP/Neutrals",
    "CaloElectrons": "Rec/Calo/Electrons",
    "CaloPhotons": "Rec/Calo/Photons",
    "CaloMergedPi0s": "Rec/Calo/MergedPi0s",
    "CaloSplitPhotons": "Rec/Calo/SplitPhotons",
    "MuonPIDs": "Rec/Muon/MuonPID",
    "RichPIDs": "Rec/Rich/PIDs",
}


class PersistRecoPacking(object):
    """Collection of packed object descriptors.

    Provides useful methods for dealing with the PersistReco packed objects.

    """

    def __init__(self,
                 stream,
                 data_type,
                 descriptors=standardDescriptors,
                 inputs={},
                 outputs=standardOutputs):
        """Collection of packed object descriptors.

        Args:
            descriptors: Dict of the form
                {data_type: <OrderedDict of PackingDescriptor objects>}.
            inputs: Dict of the form {descriptor_name: input_location}.
                Used for configuration of packing.
            outputs: Dict of the form
                {data_type: {descriptor_name: output_location}}.
                Used for configuration of unpacking.
        """
        self._descriptors = descriptors[data_type]
        self.inputs = inputs
        self.outputs = outputs[data_type]
        self.prefix = stream

    def packedLocations(self):
        """Return a list with the packed object locations."""
        return [
            os.path.join(self.prefix, d.location)
            for d in self._descriptors.values()
        ]

    def unpackers_by_key(self, output_level=WARNING):
        """Return the dictionary of unpacking algorithms."""
        algs = {}
        for name, d in self._descriptors.items():
            alg = d.unpacker(
                name='Unpack' + name,
                InputName=make_data_with_FetchDataFromFile(
                    os.path.join(self.prefix, d.location)),
                OutputLevel=output_level,
                outputs={
                    'OutputName':
                    force_location(
                        os.path.join(self.prefix, self.outputs[name]))
                })
            algs[name] = alg
        return algs

    def unpackers(self, configurables=False, output_level=WARNING):
        """Return the list of unpacking algorithms."""
        algs = list(self.unpackers_by_key(output_level).values())
        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def packedToOutputLocationMap(self):
        """Return the dict {packed_location: output_location}."""
        m = {
            os.path.join(self.prefix, d.location): os.path.join(
                self.prefix, self.outputs[name])
            for name, d in self._descriptors.items()
        }

        return m

    def packers_by_key(self, output_level=WARNING):
        """Return the list of packing algorithms for the inputs."""
        algs = {}
        for name, d in self._descriptors.items():
            output = force_location(os.path.join(self.prefix, d.location))
            alg = d.packer(
                name='Pack' + name,
                InputName=force_location(self.inputs[name]),
                OutputLevel=output_level,
                outputs={'OutputName': output},
                ExtraOutputs=[output])
            algs[name] = alg
        return algs

    def packers(self, configurables=False, output_level=WARNING):
        """Return the list of packing algorithms."""
        algs = list(self.packers_by_key(output_level).values())
        if configurables:
            algs = self.as_configurables(algs)
        return algs

    def inputToPackedLocationMap(self):
        """Return the dict {input_location: packed_location}."""
        m = {
            self.inputs[name]: os.path.join(self.prefix, d.location)
            for name, d in self._descriptors.items()
        }
        return m

    @staticmethod
    def as_configurables(algs):
        """Convert a list of PyConf Algorithm objects to Configurables.

        The returned list is not guaranteed to be in an order which satisfies
        the data dependencies of the algorithms when run sequentially. Any
        tools the Algorithm objects reference will be ignored.

        This is required for the GaudiPython tests (PyConf uses HLT scheduler which has no support for GP),
        ie. we need Configurables not PyConf Algorithm objects
        """
        configuration = dataflow_config()
        for alg in algs:
            configuration.update(alg.configuration())
        configurables, _ = configuration.apply()
        return configurables
