###############################################################################
# (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Helpers for configuring an application to read Moore HLT2 output."""
from __future__ import absolute_import
import os, json, XRootD.client

from .PersistRecoConf import PersistRecoPacking
from PyConf.application import (
    default_raw_event, make_data_with_FetchDataFromFile, ComponentConfig)
from PyConf.components import force_location, setup_component

__all__ = [
    "mc_unpackers", "unpackers", "decoder", "hlt2_decisions",
    "spruce_decisions", "unpack_rawevent"
]


def mc_unpackers(process='Hlt2', filtered_mc=True, configurables=True):
    """Return a list of unpackers for reading Monte Carlo truth objects

    This must run BEFORE unpackers!!!.

    Args:
        process (str): 'Turbo', 'Spruce' or 'Hlt2'.
        filtered_mc (bool): If True, assume Moore saved only a filtered
                            subset of the input MC objects.
        configurables (bool): set to False to use PyConf Algorithm.

    """

    assert process == "Hlt2" or process == "Turbo" or process == "Spruce", 'MC unpacker helper only accepts Turbo, Spruce or Hlt2 process'
    mc_prefix = '/Event/HLT2' if filtered_mc else "/Event"
    if process == "Spruce":
        mc_prefix = '/Event/Spruce/HLT2'

    if configurables:
        from Configurables import UnpackMCParticle, UnpackMCVertex
        unpack_mcp = UnpackMCParticle(
            InputName=os.path.join(mc_prefix, "pSim/MCParticles"),
            OutputName=os.path.join(mc_prefix, "MC/Particles"))
        unpack_mcv = UnpackMCVertex(
            InputName=os.path.join(mc_prefix, "pSim/MCVertices"),
            OutputName=os.path.join(mc_prefix, "MC/Vertices"))
    else:
        from PyConf.application import make_data_with_FetchDataFromFile
        from PyConf.Algorithms import UnpackMCParticle, UnpackMCVertex
        unpack_mcp = UnpackMCParticle(
            InputName=make_data_with_FetchDataFromFile(
                os.path.join(mc_prefix, "pSim/MCParticles")),
            outputs={
                "OutputName":
                force_location(os.path.join(mc_prefix, "MC/Particles"))
            })

        unpack_mcv = UnpackMCVertex(
            InputName=make_data_with_FetchDataFromFile(
                os.path.join(mc_prefix, "pSim/MCVertices")),
            outputs={
                "OutputName":
                force_location(os.path.join(mc_prefix, "MC/Vertices"))
            })

    return [unpack_mcp, unpack_mcv]


def unpack_rawevent(bank_types=["DstData", "HltDecReports"],
                    process='Hlt2',
                    stream="default",
                    raw_event_format=0.3,
                    configurables=True,
                    output_level=4):
    """Return RawBank:Views of banks in RawEvent.

    Args:
        bank_types (list of str): RawBanks to create RawBank:Views of.
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'
    if process == "Spruce" or process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event(["DstData"],
                                          raw_event_format=raw_event_format)
    if configurables:
        from Configurables import LHCb__UnpackRawEvent
        bank_location = bank_location.location
    else:
        from PyConf.Algorithms import LHCb__UnpackRawEvent
    unpackrawevent = LHCb__UnpackRawEvent(
        BankTypes=bank_types,
        RawBankLocations=[
            '/Event/DAQ/RawBanks/%s' % (rb) for rb in bank_types
        ],
        RawEventLocation=bank_location,
        OutputLevel=output_level)
    return unpackrawevent


def unpackers(process='Hlt2',
              data_type='Upgrade',
              configurables=True,
              output_level=4):
    """Return a list of unpackers for reading reconstructed objects.

    This must run AFTER mc_unpackers if MC data!!!.

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2'.
        data_type (str): The data type to configure PersistRecoPacking
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'
    if configurables:
        from Configurables import UnpackParticlesAndVertices
    else:
        from PyConf.Algorithms import UnpackParticlesAndVertices
    if process == "Spruce":
        PANDV_ROOT = '/Event/Spruce'
        RECO_ROOT = '/Event/Spruce/HLT2'
    else:
        PANDV_ROOT = '/Event/HLT2'
        RECO_ROOT = PANDV_ROOT
    prpacking = PersistRecoPacking(stream=RECO_ROOT, data_type=data_type)
    unpack_persistreco = prpacking.unpackers(
        configurables=configurables, output_level=output_level)
    unpack_psandvs = UnpackParticlesAndVertices(
        InputStream=PANDV_ROOT, OutputLevel=output_level)
    return unpack_persistreco + [unpack_psandvs]


def decoder(process='Hlt2',
            stream="default",
            raw_event_format=0.3,
            data_type='Upgrade',
            annsvc_name='HltANNSvc',
            configurables=True,
            output_level=4):
    """Return a DstData raw bank decoder.

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        raw_event_format (float):
        data_type (str): The data type to configure PersistRecoPacking.
        annsvc_name (str): Name of ANNSvc to read data.
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'
    if process == "Spruce":
        bank_location = make_data_with_FetchDataFromFile(stream)
        RECO_ROOT = '/Event/Spruce/HLT2'
    elif process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
        RECO_ROOT = '/Event/HLT2'
    else:
        bank_location = default_raw_event(["DstData"],
                                          raw_event_format=raw_event_format)
        RECO_ROOT = '/Event/HLT2'

    if configurables:
        from Configurables import HltPackedDataDecoder
        bank_location = bank_location.location
    else:
        from PyConf.Algorithms import HltPackedDataDecoder

    prpacking = PersistRecoPacking(stream=RECO_ROOT, data_type=data_type)
    container_map = prpacking.packedToOutputLocationMap()
    decode_packeddata = HltPackedDataDecoder(
        RawEventLocations=[bank_location],
        ContainerMap=container_map,
        ANNSvc=annsvc_name,
        OutputLevel=output_level)
    return decode_packeddata


def hlt2_decisions(process='Hlt2',
                   stream="default",
                   output_loc="/Event/Hlt/DecReports",
                   raw_event_format=0.3,
                   configurables=True,
                   output_level=4):
    """Return a HltDecReportsDecoder instance for HLT2 decisions.

    Args:
        process (str): 'Turbo' or 'Spruce' or 'Hlt2' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        output_loc (str): TES location to put decoded DecReports.
        raw_event_format (float):
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo" or process == "Hlt2", 'Unpacking helper only accepts Turbo, Spruce or Hlt2 processes'

    if process == "Spruce" or process == "Turbo":
        bank_location = make_data_with_FetchDataFromFile(stream)
    else:
        bank_location = default_raw_event(["HltDecReports"],
                                          raw_event_format=raw_event_format)
    if configurables:
        from Configurables import HltDecReportsDecoder

        decode_hlt2 = HltDecReportsDecoder(
            name="HLT2",
            SourceID='Hlt2',
            RawEventLocations=bank_location.location,
            OutputHltDecReportsLocation=output_loc,
            OutputLevel=output_level)
    else:
        from PyConf.Algorithms import HltDecReportsDecoder

        decode_hlt2 = HltDecReportsDecoder(
            name="HLT2",
            SourceID='Hlt2',
            RawEventLocations=bank_location,
            outputs={
                'OutputHltDecReportsLocation': force_location(output_loc)
            },
            OutputLevel=output_level)

    return decode_hlt2


def spruce_decisions(process="Spruce",
                     stream="default",
                     configurables=True,
                     output_level=4):
    """Return a HltDecReportsDecoder instance for Sprucing decisions.

    Args:
        process (str): 'Turbo' or 'Spruce' - serves to determine `RawEventLocation` only.
        stream (str): needed post-sprucing as RawEvent is then dependent on stream name
            - drives `RawEventLocation` only.
        configurables (bool): set to False to use PyConf Algorithm.
        output_level (int): Level of verbosity 1-8.
    """
    assert process == "Spruce" or process == "Turbo", 'Can only ask for sprucing decisions if post - sprucing'
    bank_location = make_data_with_FetchDataFromFile(stream)
    output_loc = "/Event/Spruce/DecReports"
    if configurables:
        from Configurables import HltDecReportsDecoder

        decode_spruce = HltDecReportsDecoder(
            name="Spruce",
            SourceID='Spruce',
            RawEventLocations=bank_location.location,
            OutputHltDecReportsLocation=output_loc,
            OutputLevel=output_level)

    else:
        from PyConf.Algorithms import HltDecReportsDecoder

        decode_spruce = HltDecReportsDecoder(
            name="Spruce",
            SourceID='Spruce',
            RawEventLocations=bank_location,
            outputs={
                'OutputHltDecReportsLocation': force_location(output_loc)
            },
            OutputLevel=output_level)

    return decode_spruce


def get_hltAnn_dict(annsvc_json_config):
    """
    Extract the HLT ANN dictionary of HLT2/Spruce locations from a .json file.

    Args:
        annsvc_json_config (str): path to the .json file containing the HltAnnSvc configuration.
        Examples:
        1. local path:   path/to/json_file
        2. eos path:     root://eoslhcb.cern.ch//path/to/json_file

    Returns:
        Dict with all the HLT2/Spruce locations.
    """
    tck = {}
    if "root://eoslhcb.cern.ch//" in annsvc_json_config:
        with XRootD.client.File() as f:
            status, _ = f.open(annsvc_json_config)
            if not status.ok:
                raise RuntimeError(
                    f"could not open {annsvc_json_config}: {status.message}")
            status, data = f.read()
            if not status.ok:
                raise RuntimeError(
                    f"could not read {annsvc_json_config}: {status.message}")
        tck = json.loads(data.decode('utf-8'))
    elif annsvc_json_config:
        with open(os.path.expandvars(annsvc_json_config)) as f:
            tck = json.load(f)

    return tck


def set_hltAnn_svc(annsvc_json_config):
    """
    Configures the Hlt ANN service to read correctly the spruced locations using the HltAnnSvc
    configuration of the HLT2 application.

    Args:
       annsvc_json_config (str): path to the .json file containing the HltAnnSvc configuration.
       Examples:
        1. local path:   path/to/json_file
        2. eos path:     root://eoslhcb.cern.ch//path/to/json_file
    """
    config = ComponentConfig()
    tck = get_hltAnn_dict(annsvc_json_config)
    if tck:
        ann_config = tck["HltANNSvc/HltANNSvc"]

        hlt2_sel_ids = {
            str(k): v
            for k, v in ann_config["Hlt2SelectionID"].items()
        }
        spruce_sel_ids = {
            str(k): v
            for k, v in ann_config["SpruceSelectionID"].items()
        }
        packed_object_locs = {
            str(k): v
            for k, v in ann_config["PackedObjectLocations"].items()
        }

        config.add(
            setup_component(
                "HltANNSvc",
                Hlt2SelectionID=hlt2_sel_ids,
                SpruceSelectionID=spruce_sel_ids,
                PackedObjectLocations=packed_object_locs))
    else:
        config.add(setup_component("HltANNSvc"))

    return config
