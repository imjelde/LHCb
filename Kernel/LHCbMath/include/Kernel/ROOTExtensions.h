/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#pragma once

// This header extends ROOT functionality, which might become
// part of ROOT in the future, but are currently not available.

#include "Math/SMatrix.h"
#include "Math/SVector.h"

// SMatrix: auto with expression templates are cast to the concrete type.

namespace ROOT::Math {
  template <class A, class T, unsigned int D1, unsigned int D2, class R2>
  auto eval( const Expr<A, T, D1, D2, R2>& arg ) {
    return SMatrix{arg};
  }

  template <class A, class T, unsigned int D>
  auto eval( const VecExpr<A, T, D>& arg ) {
    return SVector{arg};
  }
} // namespace ROOT::Math
