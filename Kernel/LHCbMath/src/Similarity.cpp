/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include <type_traits>
#include <utility>

#include "GaudiKernel/System.h"
#include "LHCbMath/Similarity.h"
#include <Vc/Vc>

namespace LHCb::Math::detail {

  using gsl::span;

  namespace {

    //----------------------------------------------------------------------
    // AVX details
    //----------------------------------------------------------------------
#if defined( __AVX__ )
    inline double sum( Vc::double_v a ) {
      __m256d t1 = _mm256_hadd_pd( a.data(), a.data() );
      __m128d t2 = _mm256_extractf128_pd( t1, 1 );
      __m128d t3 = _mm_add_sd( _mm256_castpd256_pd128( t1 ), t2 );
      return _mm_cvtsd_f64( t3 );
    }
    inline double dot5( Vc::double_v f0, double f1, Vc::double_v r0, double r1 ) { return sum( r0 * f0 ) + r1 * f1; }
    inline double dot5( span<const double, 5> f, Vc::double_v r0, double r2 ) {
      return dot5( Vc::double_v( f.data() ), f[4], r0, r2 );
    }

    // missing in Vc. Should actually be put ther and dropped from here
    template <Vc::VecPos Dst0, Vc::VecPos Dst1, Vc::VecPos Dst2, Vc::VecPos Dst3>
    static Vc_ALWAYS_INLINE __m256d Vc_CONST blend( __m256d x, __m256d y ) {
      static_assert( Dst0 == Vc::X0 || Dst0 == Vc::Y0, "Incorrect_Range" );
      static_assert( Dst1 == Vc::X1 || Dst1 == Vc::Y1, "Incorrect_Range" );
      static_assert( Dst2 == Vc::X2 || Dst2 == Vc::Y2, "Incorrect_Range" );
      static_assert( Dst3 == Vc::X3 || Dst3 == Vc::Y3, "Incorrect_Range" );
      return _mm256_blend_pd(
          x, y, ( Dst0 / Vc::Y0 ) * 1 + ( Dst1 / Vc::Y1 ) * 2 + ( Dst2 / Vc::Y2 ) * 4 + ( Dst3 / Vc::Y3 ) * 8 );
    }

    inline Vc::double_v dots2_5( span<const double> f, Vc::double_v r0, double r2, Vc::double_v r0p, double r2p,
                                 int shift ) {
      const auto         x0  = r0 * Vc::double_v( f.data() );
      const auto         x1  = r0 * Vc::double_v( f.data() + 5 );
      const auto         x0p = r0p * Vc::double_v( f.data() + shift );
      const auto         x1p = r0p * Vc::double_v( f.data() + shift + 5 );
      const Vc::double_v temp( _mm256_hadd_pd( x0.data(), x1.data() ) );
      const Vc::double_v tempp( _mm256_hadd_pd( x0p.data(), x1p.data() ) );
      Vc::double_v       g{f.data(), Vc::double_v::IndexType{4, 9, 4 + shift, 9 + shift}};
      g *= blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( Vc::double_v( r2 ).data(), Vc::double_v( r2p ).data() );
      auto low  = Vc::AVX::lo128( temp.data() ) + Vc::AVX::hi128( temp.data() );
      auto high = Vc::AVX::lo128( tempp.data() ) + Vc::AVX::hi128( tempp.data() );
      return Vc::AVX::Casts::concat( low, high ) + g;
    }

    inline Vc::double_v madd4_5( Vc::double_v f0, Vc::double_v f1, Vc::double_v f2, Vc::double_v f3, Vc::double_v f4,
                                 const Vc::double_v r0, double r4 ) {
      return f0 * r0[0] + f1 * r0[1] + f2 * r0[2] + f3 * r0[3] + f4 * r4;
    }

    inline Vc::double_v dots4_5( span<const double, 20> f, Vc::double_v r0, double r4 ) {
      const auto         x0 = r0 * Vc::double_v( f.data() );
      const auto         x1 = r0 * Vc::double_v( f.data() + 5 );
      const auto         x2 = r0 * Vc::double_v( f.data() + 10 );
      const auto         x3 = r0 * Vc::double_v( f.data() + 15 );
      const Vc::double_v temp01{_mm256_hadd_pd( x0.data(), x1.data() )};
      const Vc::double_v temp23{_mm256_hadd_pd( x2.data(), x3.data() )};
      const Vc::double_v c( (Vc::double_v::VectorType)_mm256_i32gather_epi64( (const long long*)f.data(),
                                                                              _mm_setr_epi32( 4, 9, 14, 19 ), 8 ) );
      const auto         b = Vc::double_v{blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( temp01.data(), temp23.data() )};
      const Vc::double_v a( _mm256_permute2f128_pd( temp01.data(), temp23.data(), 33 ) ); // x0123, y0123 -> x2x3y0y1
      return b + a + c * r4;
    }

    // reshuffle a symmetric, lower triangle, row major matrix for SIMD use...
    struct alignas( 32 ) avx_5_t {
      Vc::double_v c0, c1, c2, c3, c4; // r4 == c4...
      double       c44;
      avx_5_t( span<const double, 15> d ) : c2{d.data() + 3}, c3{d.data() + 6}, c4{d.data() + 10}, c44{d[14]} {
        c0 = Vc::Mem::permute<Vc::X0, Vc::X1, Vc::X3, Vc::X3>( Vc::double_v( d.data() ).data() );
        c0 = blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>( c0.data(), c2.data() );
        c1 = Vc::Mem::permute<Vc::X0, Vc::X1, Vc::X3, Vc::X3>( Vc::double_v( d.data() + 1 ).data() );
        c1 = blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>( c1.data(), Vc::double_v( d.data() + 4 ).data() );
        c2 = blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>( c2.data(), Vc::double_v( d.data() + 5 ).data() );
      }
      // return a column of a rhs product with column-major f (aka. tranpose of row major f)
      template <int i = 0, int j = i + 1, int k = j + 1, int l = k + 1, int m = l + 1, auto extent>
      inline Vc::double_v c0i( span<const double, extent> f ) const {
        return c0 * f[i] + c1 * f[j] + c2 * f[k] + c3 * f[l] + c4 * f[m];
      }
      inline double c4i( span<const double, 5> f ) const { return dot5( f, c4, c44 ); }
    };

    //----------------------------------------------------------------------
    // SSE3 details
    //----------------------------------------------------------------------
#elif defined( __SSE3__ )

    inline double dot5( span<const double, 5> f, Vc::double_v r0, Vc::double_v r1, double r2 ) {
      auto r = r0 * Vc::double_v( f.data() ) + r1 * Vc::double_v( f.data() + 2 );
      return r.sum() + r2 * f[4];
    }

    inline Vc::double_v dots2_5( span<const double, 10> f, Vc::double_v r0, Vc::double_v r1, double r2 ) {
      auto x0 = r0 * Vc::double_v( f.data() );
      auto x1 = r1 * Vc::double_v( f.data() + 2 );
      auto y0 = r0 * Vc::double_v( f.data() + 5 );
      auto y1 = r1 * Vc::double_v( f.data() + 7 );
      return Vc::double_v( _mm_hadd_pd( x0.data(), y0.data() ) ) + Vc::double_v( _mm_hadd_pd( x1.data(), y1.data() ) ) +
             r2 * Vc::double_v( _mm_setr_pd( f[4], f[9] ) );
    }

    struct alignas( 16 ) sse_t {
      Vc::double_v r0, r10, r2, r12, r4, r14, r6, r16, r8, r18;
      double       r24;
      sse_t( span<const double, 15> d ) : r0{d.data()}, r24{d[14]} {
        r10    = Vc::double_v{d.data() + 3};
        r10[1] = d[6];
        r2     = Vc::double_v{d.data() + 1};
        r12    = Vc::double_v{d.data() + 4};
        r12[1] = d[7];
        r4     = Vc::double_v{d.data() + 3};
        r14    = Vc::double_v{d.data() + 5};
        r14[1] = d[8];
        r6     = Vc::double_v{d.data() + 6};
        r16    = Vc::double_v{d.data() + 8};
        r8     = Vc::double_v{d.data() + 10};
        r18    = Vc::double_v{d.data() + 12};
      }
      inline Vc::double_v g0( span<const double, 5> f ) const {
        return r0 * f[0] + r2 * f[1] + r4 * f[2] + r6 * f[3] + r8 * f[4];
      }
      inline Vc::double_v g2( span<const double, 5> f ) const {
        return r10 * f[0] + r12 * f[1] + r14 * f[2] + r16 * f[3] + r18 * f[4];
      }
      inline double g4( span<const double, 5> f ) const { return dot5( f, r8, r18, r24 ); }
    };

#endif
  } // namespace

  void similarity_5_1( span<const double, 15> Ci, span<const double, 5> Fi, span<double, 1> Ti ) {
#if defined( __AVX__ )

    const avx_5_t m{Ci};
    Ti[0] = dot5( Fi, m.c0i( Fi ), m.c4i( Fi ) );

#elif defined( __SSE3__ )

    sse_t m{Ci};
    Ti[0] = dot5( Fi, m.g0( Fi ), m.g2( Fi ), m.g4( Fi ) );

#else

    auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1] + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
    auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1] + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
    auto _2 = Ci[3] * Fi[0] + Ci[4] * Fi[1] + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
    auto _3 = Ci[6] * Fi[0] + Ci[7] * Fi[1] + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
    auto _4 = Ci[10] * Fi[0] + Ci[11] * Fi[1] + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];
    Ti[0]   = Fi[0] * _0 + Fi[1] * _1 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;

#endif
  }

  void similarity_5_2( span<const double, 15> Ci, span<const double, 10> Fi, span<double, 4> Ti ) {
    // Calculate FCF-1
    // Ci: only lower triangle: {{0,1,3,6,10}, {1,2,4,7,11}, {3,4,5,8,12}, {6,7,8,9,14}, {10,11,12,13,14}}
    // Fi: 2x5 so {{0,1,2,3,4},{5,6,7,8,9}}
    auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1] + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
    auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1] + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
    auto _2 = Ci[3] * Fi[0] + Ci[4] * Fi[1] + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
    auto _3 = Ci[6] * Fi[0] + Ci[7] * Fi[1] + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
    auto _4 = Ci[10] * Fi[0] + Ci[11] * Fi[1] + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];

    auto _5 = Ci[0] * Fi[5] + Ci[1] * Fi[6] + Ci[3] * Fi[7] + Ci[6] * Fi[8] + Ci[10] * Fi[9];
    auto _6 = Ci[1] * Fi[5] + Ci[2] * Fi[6] + Ci[4] * Fi[7] + Ci[7] * Fi[8] + Ci[11] * Fi[9];
    auto _7 = Ci[3] * Fi[5] + Ci[4] * Fi[6] + Ci[5] * Fi[7] + Ci[8] * Fi[8] + Ci[12] * Fi[9];
    auto _8 = Ci[6] * Fi[5] + Ci[7] * Fi[6] + Ci[8] * Fi[7] + Ci[9] * Fi[8] + Ci[13] * Fi[9];
    auto _9 = Ci[10] * Fi[5] + Ci[11] * Fi[6] + Ci[12] * Fi[7] + Ci[13] * Fi[8] + Ci[14] * Fi[9];

    Ti[0] = Fi[0] * _0 + Fi[1] * _1 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;
    Ti[1] = Fi[5] * _0 + Fi[6] * _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _9;
    Ti[2] = Fi[0] * _5 + Fi[1] * _6 + Fi[2] * _7 + Fi[3] * _8 + Fi[4] * _9;
    Ti[3] = Fi[5] * _5 + Fi[6] * _6 + Fi[7] * _7 + Fi[8] * _8 + Fi[9] * _9;
  }

  void similarity_5_5( span<const double, 15> Ci, span<const double, 25> Fi, span<double, 15> Ti ) {
#if defined( __AVX__ )

    const auto mask =
        _mm256_set_epi64x( 0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff );
    // reshuffle the symmetric, lower diagonal, row-major Ci matrix for SIMD use...
    const avx_5_t m{Ci};

    const auto _00 = m.c0i( Fi.subspan<0, 5>() );
    const auto _04 = m.c4i( Fi.subspan<0, 5>() );
    const auto _10 = m.c0i( Fi.subspan<5, 5>() );
    const auto _14 = m.c4i( Fi.subspan<5, 5>() );
    const auto _20 = m.c0i( Fi.subspan<10, 5>() );
    const auto _24 = m.c4i( Fi.subspan<10, 5>() );
    const auto _30 = m.c0i( Fi.subspan<15, 5>() );
    const auto _34 = m.c4i( Fi.subspan<15, 5>() );
    const auto _40 = m.c0i( Fi.subspan<20, 5>() );
    const auto _44 = m.c4i( Fi.subspan<20, 5>() );

    const auto r0 = dots4_5( Fi.subspan<0, 20>(), _00, _04 );
    const auto r1 = dots4_5( Fi.subspan<5, 20>(), _10, _14 );
    // is the layout of r2 & r3 optimal?
    const auto         r2 = dots2_5( Fi.subspan<10, 15>(), _20, _24, _30, _34, 5 );
    const Vc::double_v r3( _mm256_setr_pd( dot5( Fi.subspan<20, 5>(), _20, _24 ), 0., //
                                           dot5( Fi.subspan<20, 5>(), _40, _44 ),     //
                                           dot5( Fi.subspan<20, 5>(), _00, _04 ) ) );
    // shuffle the result so that we can store the output 'in order'
    auto         r0perm = Vc::Mem::permute<Vc::X0, Vc::X1, Vc::X2, Vc::X2>( r0.data() );
    Vc::double_v res    = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r0perm, r1.shifted( -2 ).data() );
    res.store( Ti.data() );
    auto r1perm = Vc::Mem::permute<Vc::X1, Vc::X0, Vc::X2, Vc::X2>( r1.data() );
    res         = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::X3>(
        blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r1perm, r0.shifted( 1 ).data() ), r2.shifted( -1 ).data() );
    res.store( Ti.data() + 4 );
    res = blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>(
        blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r2.shifted( 1 ).data(), r3.shifted( 1 ).data() ), r1.data() );
    res.store( Ti.data() + 8 );
    res = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::X3>( r3.data(), r2.shifted( 2 ).data() );
    _mm256_maskstore_pd( Ti.data() + 12, mask, res.data() );

#elif defined( __SSE3__ )

    // reshuffle the origin matrix for SIMD use...
    sse_t m{Ci};

    auto _0 = m.g0( Fi.subspan<0, 5>() );
    auto _2 = m.g2( Fi.subspan<0, 5>() );
    auto _4 = m.g4( Fi.subspan<0, 5>() );
    auto r  = dots2_5( Fi.subspan<0, 10>(), _0, _2, _4 );
    auto s  = dots2_5( Fi.subspan<10, 10>(), _0, _2, _4 );
    Ti[0]   = r[0];
    Ti[1]   = r[1];
    Ti[3]   = s[0];
    Ti[6]   = s[1];
    Ti[10]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<5, 5>() );
    _2      = m.g2( Fi.subspan<5, 5>() );
    _4      = m.g4( Fi.subspan<5, 5>() );
    r       = dots2_5( Fi.subspan<5, 10>(), _0, _2, _4 );
    s       = dots2_5( Fi.subspan<15, 10>(), _0, _2, _4 );
    Ti[2]   = r[0];
    Ti[4]   = r[1];
    Ti[7]   = s[0];
    Ti[11]  = s[1];
    _0      = m.g0( Fi.subspan<10, 5>() );
    _2      = m.g2( Fi.subspan<10, 5>() );
    _4      = m.g4( Fi.subspan<10, 5>() );
    r       = dots2_5( Fi.subspan<10, 10>(), _0, _2, _4 );
    Ti[5]   = r[0];
    Ti[8]   = r[1];
    Ti[12]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<15, 5>() );
    _2      = m.g2( Fi.subspan<15, 5>() );
    _4      = m.g4( Fi.subspan<15, 5>() );
    r       = dots2_5( Fi.subspan<15, 10>(), _0, _2, _4 );
    Ti[9]   = r[0];
    Ti[13]  = r[1];
    _0      = m.g0( Fi.subspan<20, 5>() );
    _2      = m.g2( Fi.subspan<20, 5>() );
    _4      = m.g4( Fi.subspan<20, 5>() );
    Ti[14]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );

#else

    if ( Ci.data() == Ti.data() ) { throw "target and source overlap -- do not do that"; }
    auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1] + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
    auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1] + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
    auto _2 = Ci[3] * Fi[0] + Ci[4] * Fi[1] + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
    auto _3 = Ci[6] * Fi[0] + Ci[7] * Fi[1] + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
    auto _4 = Ci[10] * Fi[0] + Ci[11] * Fi[1] + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];
    Ti[0]   = Fi[0] * _0 + Fi[1] * _1 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;
    Ti[1]   = Fi[5] * _0 + Fi[6] * _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[3]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[6]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[10]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    _0      = Ci[0] * Fi[5] + Ci[1] * Fi[6] + Ci[3] * Fi[7] + Ci[6] * Fi[8] + Ci[10] * Fi[9];
    _1      = Ci[1] * Fi[5] + Ci[2] * Fi[6] + Ci[4] * Fi[7] + Ci[7] * Fi[8] + Ci[11] * Fi[9];
    _2      = Ci[3] * Fi[5] + Ci[4] * Fi[6] + Ci[5] * Fi[7] + Ci[8] * Fi[8] + Ci[12] * Fi[9];
    _3      = Ci[6] * Fi[5] + Ci[7] * Fi[6] + Ci[8] * Fi[7] + Ci[9] * Fi[8] + Ci[13] * Fi[9];
    _4      = Ci[10] * Fi[5] + Ci[11] * Fi[6] + Ci[12] * Fi[7] + Ci[13] * Fi[8] + Ci[14] * Fi[9];
    Ti[2]   = Fi[5] * _0 + Fi[6] * _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[4]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[7]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[11]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    _0      = Ci[0] * Fi[10] + Ci[1] * Fi[11] + Ci[3] * Fi[12] + Ci[6] * Fi[13] + Ci[10] * Fi[14];
    _1      = Ci[1] * Fi[10] + Ci[2] * Fi[11] + Ci[4] * Fi[12] + Ci[7] * Fi[13] + Ci[11] * Fi[14];
    _2      = Ci[3] * Fi[10] + Ci[4] * Fi[11] + Ci[5] * Fi[12] + Ci[8] * Fi[13] + Ci[12] * Fi[14];
    _3      = Ci[6] * Fi[10] + Ci[7] * Fi[11] + Ci[8] * Fi[12] + Ci[9] * Fi[13] + Ci[13] * Fi[14];
    _4      = Ci[10] * Fi[10] + Ci[11] * Fi[11] + Ci[12] * Fi[12] + Ci[13] * Fi[13] + Ci[14] * Fi[14];
    Ti[5]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[8]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[12]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    _0      = Ci[0] * Fi[15] + Ci[1] * Fi[16] + Ci[3] * Fi[17] + Ci[6] * Fi[18] + Ci[10] * Fi[19];
    _1      = Ci[1] * Fi[15] + Ci[2] * Fi[16] + Ci[4] * Fi[17] + Ci[7] * Fi[18] + Ci[11] * Fi[19];
    _2      = Ci[3] * Fi[15] + Ci[4] * Fi[16] + Ci[5] * Fi[17] + Ci[8] * Fi[18] + Ci[12] * Fi[19];
    _3      = Ci[6] * Fi[15] + Ci[7] * Fi[16] + Ci[8] * Fi[17] + Ci[9] * Fi[18] + Ci[13] * Fi[19];
    _4      = Ci[10] * Fi[15] + Ci[11] * Fi[16] + Ci[12] * Fi[17] + Ci[13] * Fi[18] + Ci[14] * Fi[19];
    Ti[9]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[13]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    _0      = Ci[0] * Fi[20] + Ci[1] * Fi[21] + Ci[3] * Fi[22] + Ci[6] * Fi[23] + Ci[10] * Fi[24];
    _1      = Ci[1] * Fi[20] + Ci[2] * Fi[21] + Ci[4] * Fi[22] + Ci[7] * Fi[23] + Ci[11] * Fi[24];
    _2      = Ci[3] * Fi[20] + Ci[4] * Fi[21] + Ci[5] * Fi[22] + Ci[8] * Fi[23] + Ci[12] * Fi[24];
    _3      = Ci[6] * Fi[20] + Ci[7] * Fi[21] + Ci[8] * Fi[22] + Ci[9] * Fi[23] + Ci[13] * Fi[24];
    _4      = Ci[10] * Fi[20] + Ci[11] * Fi[21] + Ci[12] * Fi[22] + Ci[13] * Fi[23] + Ci[14] * Fi[24];
    Ti[14]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;

#endif
  }

  void similarity_5_5_transport_matrix( span<const double, 15> Ci, span<const double, 25> Fi, span<double, 15> Ti ) {
    /**
     * Exploits the simpler structure of track transport matrices
     * to perform a simpler similarity transform.
     *   ( 1  0 |  S00 S01 | U0 )
     *   ( 0  1 |  S10 S01 | U1 )
     * F=( 0  0 |  T00 T01 | V0 )
     *   ( 0  0 |  T10 T11 | V1 )
     *   ( 0  0 |   0   0  | 1  )
     **/

    if ( Ci.data() == Ti.data() ) { throw "target and source overlap -- do not do that"; }
    auto _0 = Ci[0] * 1. + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
    auto _1 = Ci[1] * 1. + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
    auto _2 = Ci[3] * 1. + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
    auto _3 = Ci[6] * 1. + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
    auto _4 = Ci[10] * 1. + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];
    Ti[0]   = _0 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;
    Ti[1]   = _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[3]   = Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[6]   = Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[10]  = _4;
    _0      = Ci[1] * 1.0 + Ci[3] * Fi[7] + Ci[6] * Fi[8] + Ci[10] * Fi[9];
    _1      = Ci[2] * 1.0 + Ci[4] * Fi[7] + Ci[7] * Fi[8] + Ci[11] * Fi[9];
    _2      = Ci[4] * 1.0 + Ci[5] * Fi[7] + Ci[8] * Fi[8] + Ci[12] * Fi[9];
    _3      = Ci[7] * 1.0 + Ci[8] * Fi[7] + Ci[9] * Fi[8] + Ci[13] * Fi[9];
    _4      = Ci[11] * 1.0 + Ci[12] * Fi[7] + Ci[13] * Fi[8] + Ci[14] * Fi[9];
    Ti[2]   = _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[4]   = Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[7]   = Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[11]  = _4;
    _0      = Ci[3] * Fi[12] + Ci[6] * Fi[13] + Ci[10] * Fi[14];
    _1      = Ci[4] * Fi[12] + Ci[7] * Fi[13] + Ci[11] * Fi[14];
    _2      = Ci[5] * Fi[12] + Ci[8] * Fi[13] + Ci[12] * Fi[14];
    _3      = Ci[8] * Fi[12] + Ci[9] * Fi[13] + Ci[13] * Fi[14];
    _4      = Ci[12] * Fi[12] + Ci[13] * Fi[13] + Ci[14] * Fi[14];
    Ti[5]   = Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[8]   = Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[12]  = _4;
    _0      = Ci[3] * Fi[17] + Ci[6] * Fi[18] + Ci[10] * Fi[19];
    _1      = Ci[4] * Fi[17] + Ci[7] * Fi[18] + Ci[11] * Fi[19];
    _2      = Ci[5] * Fi[17] + Ci[8] * Fi[18] + Ci[12] * Fi[19];
    _3      = Ci[8] * Fi[17] + Ci[9] * Fi[18] + Ci[13] * Fi[19];
    _4      = Ci[12] * Fi[17] + Ci[13] * Fi[18] + Ci[14] * Fi[19];
    Ti[9]   = Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[13]  = _4;
    Ti[14]  = Ci[14];
  }

  void similarity_5_7( span<const double, 15> Ci, span<const double, 35> Fi, span<double, 28> Ti ) {
#if defined( __AVX__ )

    // reshuffle the 5x5 symmetric Ci matrix for SIMD use...
    const avx_5_t m{Ci};

    const auto _00 = m.c0i( Fi.subspan<0, 5>() );
    const auto _04 = m.c4i( Fi.subspan<0, 5>() );
    const auto _10 = m.c0i( Fi.subspan<5, 5>() );
    const auto _14 = m.c4i( Fi.subspan<5, 5>() );
    const auto _20 = m.c0i( Fi.subspan<10, 5>() );
    const auto _24 = m.c4i( Fi.subspan<10, 5>() );
    const auto _30 = m.c0i( Fi.subspan<15, 5>() );
    const auto _34 = m.c4i( Fi.subspan<15, 5>() );
    const auto _40 = m.c0i( Fi.subspan<20, 5>() );
    const auto _44 = m.c4i( Fi.subspan<20, 5>() );
    const auto _50 = m.c0i( Fi.subspan<25, 5>() );
    const auto _54 = m.c4i( Fi.subspan<25, 5>() );
    const auto _60 = m.c0i( Fi.subspan<30, 5>() );
    const auto _64 = m.c4i( Fi.subspan<30, 5>() );

    const auto         r0 = dots4_5( Fi.subspan<0, 20>(), _00, _04 );
    const auto         r1 = dots4_5( Fi.subspan<5, 20>(), _10, _14 );
    const auto         r2 = dots4_5( Fi.subspan<10, 20>(), _20, _24 );
    const auto         r3 = dots4_5( Fi.subspan<15, 20>(), _30, _34 );
    const auto         r4 = dots2_5( Fi.subspan<20, 10>(), _40, _44, _00, _04, 0 );
    const auto         r5 = dots2_5( Fi.subspan<25, 10>(), _50, _54, _10, _14, 0 );
    const Vc::double_v r6( _mm256_setr_pd( dot5( Fi.subspan<30, 5>(), _00, _04 ), dot5( Fi.subspan<30, 5>(), _20, _24 ),
                                           dot5( Fi.subspan<30, 5>(), _40, _44 ),
                                           dot5( Fi.subspan<30, 5>(), _60, _64 ) ) );

    // shuffle the result so that we can store the output 'in order'
    auto         r0perm = Vc::Mem::permute<Vc::X0, Vc::X1, Vc::X2, Vc::X2>( r0.data() );
    Vc::double_v res    = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r0perm, r1.shifted( -2 ).data() );
    res.store( Ti.data() );
    auto r1perm = Vc::Mem::permute<Vc::X1, Vc::X0, Vc::X2, Vc::X2>( r1.data() );
    res         = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::X3>(
        blend<Vc::Y0, Vc::X1, Vc::X2, Vc::Y3>( r0.shifted( 1 ).data(), r1perm ), r2.shifted( -1 ).data() );
    res.store( Ti.data() + 4 );
    res = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>(
        blend<Vc::X0, Vc::Y1, Vc::X2, Vc::X3>( r2.shifted( 1 ).data(), r3.shifted( -1 ).data() ),
        blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r1.data(), r4.data() ) );
    res.store( Ti.data() + 8 );
    auto r4perm = _mm256_permute4x64_pd( r4.data(), 192 ); // abcd -> aaad
    res         = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>(
        blend<Vc::X0, Vc::Y1, Vc::X2, Vc::X3>( r2.shifted( 2 ).data(), r3.data() ), r4perm );
    res.store( Ti.data() + 12 );
    res = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>(
        Vc::double_v( blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( r2.data(), r5.data() ) ).shifted( 2 ).data(),
        blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>( r3.data(), r4.shifted( -2 ).data() ) );
    res.store( Ti.data() + 16 );
    auto r5perm = Vc::Mem::permute<Vc::X0, Vc::X1, Vc::X3, Vc::X3>( r5.data() );
    auto r6perm = _mm256_permute4x64_pd( r6.data(), 64 ); // abcd -> aaab
    res         = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( r5perm, r6perm );
    res.store( Ti.data() + 20 );
    r6perm = _mm256_permute4x64_pd( r6.data(), 200 ); // abcd -> acad
    res    = blend<Vc::Y0, Vc::X1, Vc::X2, Vc::X3>(
        blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( r5.shifted( -1 ).data(), r6perm ), r3.shifted( 3 ).data() );
    res.store( Ti.data() + 24 );

#elif defined( __SSE3__ )

    // reshuffle the 5x5 symmetric Ci matrix for SIMD use...
    sse_t m{Ci};

    auto _0 = m.g0( Fi.subspan<0, 5>() );
    auto _2 = m.g2( Fi.subspan<0, 5>() );
    auto _4 = m.g4( Fi.subspan<0, 5>() );
    Ti[0]   = dot5( Fi.subspan<0, 5>(), _0, _2, _4 );
    Ti[1]   = dot5( Fi.subspan<5, 5>(), _0, _2, _4 );
    Ti[3]   = dot5( Fi.subspan<10, 5>(), _0, _2, _4 );
    Ti[6]   = dot5( Fi.subspan<15, 5>(), _0, _2, _4 );
    Ti[10]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    Ti[15]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[21]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<5, 5>() );
    _2      = m.g2( Fi.subspan<5, 5>() );
    _4      = m.g4( Fi.subspan<5, 5>() );
    Ti[2]   = dot5( Fi.subspan<5, 5>(), _0, _2, _4 );
    Ti[4]   = dot5( Fi.subspan<10, 5>(), _0, _2, _4 );
    Ti[7]   = dot5( Fi.subspan<15, 5>(), _0, _2, _4 );
    Ti[11]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    Ti[16]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[22]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<10, 5>() );
    _2      = m.g2( Fi.subspan<10, 5>() );
    _4      = m.g4( Fi.subspan<10, 5>() );
    Ti[5]   = dot5( Fi.subspan<10, 5>(), _0, _2, _4 );
    Ti[8]   = dot5( Fi.subspan<15, 5>(), _0, _2, _4 );
    Ti[12]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    Ti[17]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[23]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<15, 5>() );
    _2      = m.g2( Fi.subspan<15, 5>() );
    _4      = m.g4( Fi.subspan<15, 5>() );
    Ti[9]   = dot5( Fi.subspan<15, 5>(), _0, _2, _4 );
    Ti[13]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    Ti[18]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[24]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<20, 5>() );
    _2      = m.g2( Fi.subspan<20, 5>() );
    _4      = m.g4( Fi.subspan<20, 5>() );
    Ti[14]  = dot5( Fi.subspan<20, 5>(), _0, _2, _4 );
    Ti[19]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[25]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<25, 5>() );
    _2      = m.g2( Fi.subspan<25, 5>() );
    _4      = m.g4( Fi.subspan<25, 5>() );
    Ti[20]  = dot5( Fi.subspan<25, 5>(), _0, _2, _4 );
    Ti[26]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );
    _0      = m.g0( Fi.subspan<30, 5>() );
    _2      = m.g2( Fi.subspan<30, 5>() );
    _4      = m.g4( Fi.subspan<30, 5>() );
    Ti[27]  = dot5( Fi.subspan<30, 5>(), _0, _2, _4 );

#else

    if ( Ci.data() == Ti.data() ) { throw "target and source overlap -- do not do that"; }
    auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1] + Ci[3] * Fi[2] + Ci[6] * Fi[3] + Ci[10] * Fi[4];
    auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1] + Ci[4] * Fi[2] + Ci[7] * Fi[3] + Ci[11] * Fi[4];
    auto _2 = Ci[3] * Fi[0] + Ci[4] * Fi[1] + Ci[5] * Fi[2] + Ci[8] * Fi[3] + Ci[12] * Fi[4];
    auto _3 = Ci[6] * Fi[0] + Ci[7] * Fi[1] + Ci[8] * Fi[2] + Ci[9] * Fi[3] + Ci[13] * Fi[4];
    auto _4 = Ci[10] * Fi[0] + Ci[11] * Fi[1] + Ci[12] * Fi[2] + Ci[13] * Fi[3] + Ci[14] * Fi[4];
    Ti[0]   = Fi[0] * _0 + Fi[1] * _1 + Fi[2] * _2 + Fi[3] * _3 + Fi[4] * _4;
    Ti[1]   = Fi[5] * _0 + Fi[6] * _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[3]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[6]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[10]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    Ti[15]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[21]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[5] + Ci[1] * Fi[6] + Ci[3] * Fi[7] + Ci[6] * Fi[8] + Ci[10] * Fi[9];
    _1      = Ci[1] * Fi[5] + Ci[2] * Fi[6] + Ci[4] * Fi[7] + Ci[7] * Fi[8] + Ci[11] * Fi[9];
    _2      = Ci[3] * Fi[5] + Ci[4] * Fi[6] + Ci[5] * Fi[7] + Ci[8] * Fi[8] + Ci[12] * Fi[9];
    _3      = Ci[6] * Fi[5] + Ci[7] * Fi[6] + Ci[8] * Fi[7] + Ci[9] * Fi[8] + Ci[13] * Fi[9];
    _4      = Ci[10] * Fi[5] + Ci[11] * Fi[6] + Ci[12] * Fi[7] + Ci[13] * Fi[8] + Ci[14] * Fi[9];
    Ti[2]   = Fi[5] * _0 + Fi[6] * _1 + Fi[7] * _2 + Fi[8] * _3 + Fi[9] * _4;
    Ti[4]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[7]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[11]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    Ti[16]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[22]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[10] + Ci[1] * Fi[11] + Ci[3] * Fi[12] + Ci[6] * Fi[13] + Ci[10] * Fi[14];
    _1      = Ci[1] * Fi[10] + Ci[2] * Fi[11] + Ci[4] * Fi[12] + Ci[7] * Fi[13] + Ci[11] * Fi[14];
    _2      = Ci[3] * Fi[10] + Ci[4] * Fi[11] + Ci[5] * Fi[12] + Ci[8] * Fi[13] + Ci[12] * Fi[14];
    _3      = Ci[6] * Fi[10] + Ci[7] * Fi[11] + Ci[8] * Fi[12] + Ci[9] * Fi[13] + Ci[13] * Fi[14];
    _4      = Ci[10] * Fi[10] + Ci[11] * Fi[11] + Ci[12] * Fi[12] + Ci[13] * Fi[13] + Ci[14] * Fi[14];
    Ti[5]   = Fi[10] * _0 + Fi[11] * _1 + Fi[12] * _2 + Fi[13] * _3 + Fi[14] * _4;
    Ti[8]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[12]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    Ti[17]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[23]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[15] + Ci[1] * Fi[16] + Ci[3] * Fi[17] + Ci[6] * Fi[18] + Ci[10] * Fi[19];
    _1      = Ci[1] * Fi[15] + Ci[2] * Fi[16] + Ci[4] * Fi[17] + Ci[7] * Fi[18] + Ci[11] * Fi[19];
    _2      = Ci[3] * Fi[15] + Ci[4] * Fi[16] + Ci[5] * Fi[17] + Ci[8] * Fi[18] + Ci[12] * Fi[19];
    _3      = Ci[6] * Fi[15] + Ci[7] * Fi[16] + Ci[8] * Fi[17] + Ci[9] * Fi[18] + Ci[13] * Fi[19];
    _4      = Ci[10] * Fi[15] + Ci[11] * Fi[16] + Ci[12] * Fi[17] + Ci[13] * Fi[18] + Ci[14] * Fi[19];
    Ti[9]   = Fi[15] * _0 + Fi[16] * _1 + Fi[17] * _2 + Fi[18] * _3 + Fi[19] * _4;
    Ti[13]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    Ti[18]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[24]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[20] + Ci[1] * Fi[21] + Ci[3] * Fi[22] + Ci[6] * Fi[23] + Ci[10] * Fi[24];
    _1      = Ci[1] * Fi[20] + Ci[2] * Fi[21] + Ci[4] * Fi[22] + Ci[7] * Fi[23] + Ci[11] * Fi[24];
    _2      = Ci[3] * Fi[20] + Ci[4] * Fi[21] + Ci[5] * Fi[22] + Ci[8] * Fi[23] + Ci[12] * Fi[24];
    _3      = Ci[6] * Fi[20] + Ci[7] * Fi[21] + Ci[8] * Fi[22] + Ci[9] * Fi[23] + Ci[13] * Fi[24];
    _4      = Ci[10] * Fi[20] + Ci[11] * Fi[21] + Ci[12] * Fi[22] + Ci[13] * Fi[23] + Ci[14] * Fi[24];
    Ti[14]  = Fi[20] * _0 + Fi[21] * _1 + Fi[22] * _2 + Fi[23] * _3 + Fi[24] * _4;
    Ti[19]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[25]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[25] + Ci[1] * Fi[26] + Ci[3] * Fi[27] + Ci[6] * Fi[28] + Ci[10] * Fi[29];
    _1      = Ci[1] * Fi[25] + Ci[2] * Fi[26] + Ci[4] * Fi[27] + Ci[7] * Fi[28] + Ci[11] * Fi[29];
    _2      = Ci[3] * Fi[25] + Ci[4] * Fi[26] + Ci[5] * Fi[27] + Ci[8] * Fi[28] + Ci[12] * Fi[29];
    _3      = Ci[6] * Fi[25] + Ci[7] * Fi[26] + Ci[8] * Fi[27] + Ci[9] * Fi[28] + Ci[13] * Fi[29];
    _4      = Ci[10] * Fi[25] + Ci[11] * Fi[26] + Ci[12] * Fi[27] + Ci[13] * Fi[28] + Ci[14] * Fi[29];
    Ti[20]  = Fi[25] * _0 + Fi[26] * _1 + Fi[27] * _2 + Fi[28] * _3 + Fi[29] * _4;
    Ti[26]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;
    _0      = Ci[0] * Fi[30] + Ci[1] * Fi[31] + Ci[3] * Fi[32] + Ci[6] * Fi[33] + Ci[10] * Fi[34];
    _1      = Ci[1] * Fi[30] + Ci[2] * Fi[31] + Ci[4] * Fi[32] + Ci[7] * Fi[33] + Ci[11] * Fi[34];
    _2      = Ci[3] * Fi[30] + Ci[4] * Fi[31] + Ci[5] * Fi[32] + Ci[8] * Fi[33] + Ci[12] * Fi[34];
    _3      = Ci[6] * Fi[30] + Ci[7] * Fi[31] + Ci[8] * Fi[32] + Ci[9] * Fi[33] + Ci[13] * Fi[34];
    _4      = Ci[10] * Fi[30] + Ci[11] * Fi[31] + Ci[12] * Fi[32] + Ci[13] * Fi[33] + Ci[14] * Fi[34];
    Ti[27]  = Fi[30] * _0 + Fi[31] * _1 + Fi[32] * _2 + Fi[33] * _3 + Fi[34] * _4;

#endif
  }

  void similarity_2_5( span<const double, 3> Ci, span<const double, 10> Fi, span<double, 15> Ti ) {
    if ( Ci.data() == Ti.data() ) { throw "target and source overlap -- do not do that"; }
    auto _0 = Ci[0] * Fi[0] + Ci[1] * Fi[1];
    auto _1 = Ci[1] * Fi[0] + Ci[2] * Fi[1];
    Ti[0]   = Fi[0] * _0 + Fi[1] * _1;
    Ti[1]   = Fi[2] * _0 + Fi[3] * _1;
    Ti[3]   = Fi[4] * _0 + Fi[5] * _1;
    Ti[6]   = Fi[6] * _0 + Fi[7] * _1;
    Ti[10]  = Fi[8] * _0 + Fi[9] * _1;
    _0      = Ci[0] * Fi[2] + Ci[1] * Fi[3];
    _1      = Ci[1] * Fi[2] + Ci[2] * Fi[3];
    Ti[2]   = Fi[2] * _0 + Fi[3] * _1;
    Ti[4]   = Fi[4] * _0 + Fi[5] * _1;
    Ti[7]   = Fi[6] * _0 + Fi[7] * _1;
    Ti[11]  = Fi[8] * _0 + Fi[9] * _1;
    _0      = Ci[0] * Fi[4] + Ci[1] * Fi[5];
    _1      = Ci[1] * Fi[4] + Ci[2] * Fi[5];
    Ti[5]   = Fi[4] * _0 + Fi[5] * _1;
    Ti[8]   = Fi[6] * _0 + Fi[7] * _1;
    Ti[12]  = Fi[8] * _0 + Fi[9] * _1;
    _0      = Ci[0] * Fi[6] + Ci[1] * Fi[7];
    _1      = Ci[1] * Fi[6] + Ci[2] * Fi[7];
    Ti[9]   = Fi[6] * _0 + Fi[7] * _1;
    Ti[13]  = Fi[8] * _0 + Fi[9] * _1;
    _0      = Ci[0] * Fi[8] + Ci[1] * Fi[9];
    _1      = Ci[1] * Fi[8] + Ci[2] * Fi[9];
    Ti[14]  = Fi[8] * _0 + Fi[9] * _1;
  }

  bool average( span<const double, 5> X1, span<const double, 15> C1, //
                span<const double, 5> X2, span<const double, 15> C2, //
                span<double, 5> X, span<double, 15> C ) {

#if defined( __AVX__ )

    // compute the inverse of the covariance (i.e. weight) of the difference: R=(C1+C2)
    Gaudi::SymMatrix5x5 invRM;
    auto                invR = to_span( invRM );
    const auto          y0   = Vc::double_v( C1.data() ) + Vc::double_v( C2.data() );
    const auto          y1   = Vc::double_v( C1.data() + 4 ) + Vc::double_v( C2.data() + 4 );
    const auto          y2   = Vc::double_v( C1.data() + 8 ) + Vc::double_v( C2.data() + 8 );
    const auto          mask =
        _mm256_set_epi64x( 0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff );
    const auto y3 = Vc::double_v( _mm256_maskload_pd( C1.data() + 12, mask ) ) +
                    Vc::double_v( _mm256_maskload_pd( C2.data() + 12, mask ) );
    y0.store( invR.data() );
    y1.store( invR.data() + 4 );
    y2.store( invR.data() + 8 );
    _mm256_maskstore_pd( invR.data() + 12, mask, y3.data() );

    bool success = invRM.InvertChol();
    // compute the gain matrix

    auto invRMs = to_span( std::as_const( invRM ) );
    // K <- C1*inverse(C1+C2) = C1*invR
    const avx_5_t _invR( invRMs );

    const auto kr0 = _invR.c0i<0, 1, 3, 6, 10>( C1 );
    const auto kr1 = _invR.c0i<1, 2, 4, 7, 11>( C1 );
    const auto kr2 = _invR.c0i<3, 4, 5, 8, 12>( C1 );
    const auto kr3 = _invR.c0i<6, 7, 8, 9, 13>( C1 );
    const auto kr4 = _invR.c0i<10, 11, 12, 13, 14>( C1 );

    // transpose (kr0,kr1,kr2,kr3) -> (kc0,kc1,kc2,kc3)
    const auto _0 = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( kr0.data(), kr2.shifted( -2 ).data() );
    const auto _1 = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( kr1.data(), kr3.shifted( -2 ).data() );
    const auto _2 = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( kr0.shifted( 2 ).data(), kr2.data() );
    const auto _3 = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( kr1.shifted( 2 ).data(), kr3.data() );

    auto       _0perm = Vc::Mem::permute<Vc::X0, Vc::X0, Vc::X2, Vc::X2>( _0 );
    auto       _1perm = Vc::Mem::permute<Vc::X0, Vc::X0, Vc::X2, Vc::X2>( _1 );
    const auto kc0    = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( _0perm, _1perm );
    _0perm            = Vc::Mem::permute<Vc::X1, Vc::X1, Vc::X3, Vc::X3>( _0 );
    _1perm            = Vc::Mem::permute<Vc::X1, Vc::X1, Vc::X3, Vc::X3>( _1 );
    const auto kc1    = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( _0perm, _1perm );
    auto       _2perm = Vc::Mem::permute<Vc::X0, Vc::X0, Vc::X2, Vc::X2>( _2 );
    auto       _3perm = Vc::Mem::permute<Vc::X0, Vc::X0, Vc::X2, Vc::X2>( _3 );
    const auto kc2    = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( _2perm, _3perm );
    _2perm            = Vc::Mem::permute<Vc::X1, Vc::X1, Vc::X3, Vc::X3>( _2 );
    _3perm            = Vc::Mem::permute<Vc::X1, Vc::X1, Vc::X3, Vc::X3>( _3 );
    const auto kc3    = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( _2perm, _3perm );

    const avx_5_t _C1( C1 );
    const auto    kc4 = _C1.c0i<10, 11, 12, 13, 14>( invRMs );
    const auto    k44 = _C1.c4i( invRMs.subspan<10, 5>() );

    //       kc0[0] kc1[0] kc2[0] kc3[0]  kc4[0]     kr0[0] kr0[1] kr0[2] kr0[3]  .         0  1  2  3  4
    //       kc0[1] kc1[1] kc2[1] kc3[1]  kc4[1]     kr1[0] kr1[1] kr1[2] kr1[3]  .         5  6  7  8  9
    //  K =  kc0[2] kc1[2] kc2[2] kc3[2]  kc4[2]  =  kr2[0] kr2[1] kr2[2] kr2[3]  .    =   10 11 12 13 14
    //       kc0[3] kc1[3] kc2[3] kc3[3]  kc4[3]     kr3[0] kr3[1] kr3[2] kr3[3]  .        15 16 17 18 19
    //        .      .      .      .      k44        kr4[0] kr4[1] kr4[2] kr4[3]  k44      20 21 22 23 24

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    const auto _x20 = Vc::double_v( X2.data() );
    const auto _x10 = Vc::double_v( X1.data() );
    const auto d0   = _x20 - _x10;
    double     d4   = X2[4] - X1[4];

    const auto _00 = _x10 + kc0 * d0[0] + kc1 * d0[1] + kc2 * d0[2] + kc3 * d0[3] + kc4 * d4;

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    const avx_5_t _C2( C2 );
    const auto    r1 = kc0 * _C2.c0 + kc1 * _C2.c1 + kc2 * _C2.c2 + kc3 * _C2.c3 + kc4 * _C2.c4;
    const auto    r2 = madd4_5( kc0, kc1, kc2, kc3, kc4, _C2.c0, _C2.c4[0] );
    const auto    r4 = madd4_5( kc0, kc1, kc2, kc3, kc4, _C2.c1, _C2.c4[1] );
    const auto    r3 = madd4_5( _C2.c0, _C2.c1, _C2.c2, _C2.c3, _C2.c4, kr4, k44 );
    // TODO: can we avoid the horizontal_add by transposing the following?? or use a variant of dots2 for the last
    // two???
    const Vc::double_v r5( _mm256_setr_pd( dot5( kr3, kc4[3], _C2.c2, _C2.c4[2] ), dot5( kr4, k44, d0, d4 ),
                                           dot5( kr4, k44, _C2.c0, _C2.c4[0] ), dot5( kr4, k44, _C2.c4, _C2.c44 ) ) );
    _00.store( X.data() );
    X[4]                = X1[4] + r5[1];
    auto         r1perm = _mm256_permute4x64_pd( r1.data(), 16 );  // abcd -> aaba
    auto         r2perm = _mm256_permute4x64_pd( r2.data(), 132 ); // abcd -> abac
    Vc::double_v res    = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>( r1perm, r2perm );
    res.store( C.data() );
    auto r4perm = _mm256_permute4x64_pd( r4.data(), 194 ); // abcd -> caad
    res         = blend<Vc::X0, Vc::Y1, Vc::Y2, Vc::X3>(
        r4perm, Vc::double_v( blend<Vc::X0, Vc::X1, Vc::X2, Vc::Y3>( r1.data(), r2.data() ) ).shifted( 1 ).data() );
    res.store( C.data() + 4 );
    res = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>(
        r5.data(), blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( r1.shifted( 2 ).data(), r3.shifted( -2 ).data() ) );
    res.store( C.data() + 8 );
    res = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( r3.shifted( 2 ).data(), r5.shifted( 1 ).data() );
    _mm256_maskstore_pd( C.data() + 12, mask, res.data() );

    return success;

#else

    // compute the inverse of the covariance (i.e. weight) of the difference: R=(C1+C2)
    Gaudi::SymMatrix5x5 invRM;
    auto                invR = invRM.Array();
    for ( int i = 0; i < 15; ++i ) invR[i] = C1[i] + C2[i];
    bool success = invRM.InvertChol();
    // compute the gain matrix

    // K <- C1*inverse(C1+C2) = C1*invR
    double K[25];
    K[0] = C1[0] * invR[0] + C1[1] * invR[1] + C1[3] * invR[3] + C1[6] * invR[6] + C1[10] * invR[10];
    K[1] = C1[0] * invR[1] + C1[1] * invR[2] + C1[3] * invR[4] + C1[6] * invR[7] + C1[10] * invR[11];
    K[2] = C1[0] * invR[3] + C1[1] * invR[4] + C1[3] * invR[5] + C1[6] * invR[8] + C1[10] * invR[12];
    K[3] = C1[0] * invR[6] + C1[1] * invR[7] + C1[3] * invR[8] + C1[6] * invR[9] + C1[10] * invR[13];
    K[4] = C1[0] * invR[10] + C1[1] * invR[11] + C1[3] * invR[12] + C1[6] * invR[13] + C1[10] * invR[14];

    K[5] = C1[1] * invR[0] + C1[2] * invR[1] + C1[4] * invR[3] + C1[7] * invR[6] + C1[11] * invR[10];
    K[6] = C1[1] * invR[1] + C1[2] * invR[2] + C1[4] * invR[4] + C1[7] * invR[7] + C1[11] * invR[11];
    K[7] = C1[1] * invR[3] + C1[2] * invR[4] + C1[4] * invR[5] + C1[7] * invR[8] + C1[11] * invR[12];
    K[8] = C1[1] * invR[6] + C1[2] * invR[7] + C1[4] * invR[8] + C1[7] * invR[9] + C1[11] * invR[13];
    K[9] = C1[1] * invR[10] + C1[2] * invR[11] + C1[4] * invR[12] + C1[7] * invR[13] + C1[11] * invR[14];

    K[10] = C1[3] * invR[0] + C1[4] * invR[1] + C1[5] * invR[3] + C1[8] * invR[6] + C1[12] * invR[10];
    K[11] = C1[3] * invR[1] + C1[4] * invR[2] + C1[5] * invR[4] + C1[8] * invR[7] + C1[12] * invR[11];
    K[12] = C1[3] * invR[3] + C1[4] * invR[4] + C1[5] * invR[5] + C1[8] * invR[8] + C1[12] * invR[12];
    K[13] = C1[3] * invR[6] + C1[4] * invR[7] + C1[5] * invR[8] + C1[8] * invR[9] + C1[12] * invR[13];
    K[14] = C1[3] * invR[10] + C1[4] * invR[11] + C1[5] * invR[12] + C1[8] * invR[13] + C1[12] * invR[14];

    K[15] = C1[6] * invR[0] + C1[7] * invR[1] + C1[8] * invR[3] + C1[9] * invR[6] + C1[13] * invR[10];
    K[16] = C1[6] * invR[1] + C1[7] * invR[2] + C1[8] * invR[4] + C1[9] * invR[7] + C1[13] * invR[11];
    K[17] = C1[6] * invR[3] + C1[7] * invR[4] + C1[8] * invR[5] + C1[9] * invR[8] + C1[13] * invR[12];
    K[18] = C1[6] * invR[6] + C1[7] * invR[7] + C1[8] * invR[8] + C1[9] * invR[9] + C1[13] * invR[13];
    K[19] = C1[6] * invR[10] + C1[7] * invR[11] + C1[8] * invR[12] + C1[9] * invR[13] + C1[13] * invR[14];

    K[20] = C1[10] * invR[0] + C1[11] * invR[1] + C1[12] * invR[3] + C1[13] * invR[6] + C1[14] * invR[10];
    K[21] = C1[10] * invR[1] + C1[11] * invR[2] + C1[12] * invR[4] + C1[13] * invR[7] + C1[14] * invR[11];
    K[22] = C1[10] * invR[3] + C1[11] * invR[4] + C1[12] * invR[5] + C1[13] * invR[8] + C1[14] * invR[12];
    K[23] = C1[10] * invR[6] + C1[11] * invR[7] + C1[12] * invR[8] + C1[13] * invR[9] + C1[14] * invR[13];
    K[24] = C1[10] * invR[10] + C1[11] * invR[11] + C1[12] * invR[12] + C1[13] * invR[13] + C1[14] * invR[14];

    // X <- X1 + C1*inverse(C1+C2)*(X2-X1) =  X1 + K*(X2-X1) = X1 + K*d
    double d[5]{X2[0] - X1[0], X2[1] - X1[1], X2[2] - X1[2], X2[3] - X1[3], X2[4] - X1[4]};
    X[0] = X1[0] + K[0] * d[0] + K[1] * d[1] + K[2] * d[2] + K[3] * d[3] + K[4] * d[4];
    X[1] = X1[1] + K[5] * d[0] + K[6] * d[1] + K[7] * d[2] + K[8] * d[3] + K[9] * d[4];
    X[2] = X1[2] + K[10] * d[0] + K[11] * d[1] + K[12] * d[2] + K[13] * d[3] + K[14] * d[4];
    X[3] = X1[3] + K[15] * d[0] + K[16] * d[1] + K[17] * d[2] + K[18] * d[3] + K[19] * d[4];
    X[4] = X1[4] + K[20] * d[0] + K[21] * d[1] + K[22] * d[2] + K[23] * d[3] + K[24] * d[4];

    // C <-  C1 * inverse(C1+C2)  * C2 =  K * C2
    C[0]  = K[0] * C2[0] + K[1] * C2[1] + K[2] * C2[3] + K[3] * C2[6] + K[4] * C2[10];
    C[1]  = K[5] * C2[0] + K[6] * C2[1] + K[7] * C2[3] + K[8] * C2[6] + K[9] * C2[10];
    C[3]  = K[10] * C2[0] + K[11] * C2[1] + K[12] * C2[3] + K[13] * C2[6] + K[14] * C2[10];
    C[6]  = K[15] * C2[0] + K[16] * C2[1] + K[17] * C2[3] + K[18] * C2[6] + K[19] * C2[10];
    C[10] = K[20] * C2[0] + K[21] * C2[1] + K[22] * C2[3] + K[23] * C2[6] + K[24] * C2[10];

    C[2]  = K[5] * C2[1] + K[6] * C2[2] + K[7] * C2[4] + K[8] * C2[7] + K[9] * C2[11];
    C[4]  = K[10] * C2[1] + K[11] * C2[2] + K[12] * C2[4] + K[13] * C2[7] + K[14] * C2[11];
    C[7]  = K[15] * C2[1] + K[16] * C2[2] + K[17] * C2[4] + K[18] * C2[7] + K[19] * C2[11];
    C[11] = K[20] * C2[1] + K[21] * C2[2] + K[22] * C2[4] + K[23] * C2[7] + K[24] * C2[11];

    C[5]  = K[10] * C2[3] + K[11] * C2[4] + K[12] * C2[5] + K[13] * C2[8] + K[14] * C2[12];
    C[8]  = K[15] * C2[3] + K[16] * C2[4] + K[17] * C2[5] + K[18] * C2[8] + K[19] * C2[12];
    C[12] = K[20] * C2[3] + K[21] * C2[4] + K[22] * C2[5] + K[23] * C2[8] + K[24] * C2[12];

    C[9]  = K[15] * C2[6] + K[16] * C2[7] + K[17] * C2[8] + K[18] * C2[9] + K[19] * C2[13];
    C[13] = K[20] * C2[6] + K[21] * C2[7] + K[22] * C2[8] + K[23] * C2[9] + K[24] * C2[13];

    C[14] = K[20] * C2[10] + K[21] * C2[11] + K[22] * C2[12] + K[23] * C2[13] + K[24] * C2[14];
    // the following used to be more stable, but isn't any longer, it seems:
    // ROOT::Math::AssignSym::Evaluate(C, -2 * K * C1) ;
    // C += C1 + ROOT::Math::Similarity(K,R) ;
    return success;

#endif
  }

  double filter( span<double, 5> X, span<double, 15> C,               //
                 span<const double, 5> Xref, span<const double, 5> H, //
                 span<const double, 1> refResidual_s, span<const double, 1> errorMeas2_s ) {

    auto refResidual = refResidual_s[0];
    auto errorMeas2  = errorMeas2_s[0];

#if defined( __AVX__ )

    const auto mask =
        _mm256_set_epi64x( 0x0000000000000000, 0xffffffffffffffff, 0xffffffffffffffff, 0xffffffffffffffff );
    const avx_5_t c( C );
    const auto    cht0 = c.c0i( H );
    const auto    cht4 = c.c4i( H );
    // TODO: combine
    const auto res = refResidual + dot5( H, Vc::double_v( Xref.data() ) - Vc::double_v( X.data() ), Xref[4] - X[4] );
    const auto errorRes2 = errorMeas2 + dot5( H, cht0, cht4 );

    const auto w     = 1. / errorRes2;
    const auto chtw0 = cht0 * w;
    const auto chtw4 = cht4 * w;
    // update the state vector and cov matrix

    // keep this version for now, as it doesn't change the results...
    const auto wres = res / errorRes2;
    // const auto wres = w*res; // even this already changes the results...
    const auto x0 = Vc::double_v( X.data() ) + cht0 * wres;
    const auto x4 = X[4] + cht4 * wres;

    const auto         _0     = c.c0 - cht0 * chtw0[0];
    auto               c4perm = _mm256_permute4x64_pd( c.c4.data(), 241 ); // abcd -> bacc
    const Vc::double_v g( _mm256_setr_pd( cht4, cht0[1], cht0[2], cht0[3] ) );
    const auto         _1 = blend<Vc::Y0, Vc::X1, Vc::X2, Vc::X3>( c.c1.data(), c4perm ) - g * chtw0[1];
    c4perm                = _mm256_permute4x64_pd( c.c4.data(), 32 );          // abcd -> aaca
    Vc::double_v       chtw0perm( _mm256_permute4x64_pd( chtw0.data(), 42 ) ); // abcd -> ccca
    const Vc::double_v g0 = _mm256_setr_pd( cht0[2], cht0[3], cht4, cht4 );
    const auto         _2 = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( c.c2.shifted( 2 ).data(), c4perm ) - g0 * chtw0perm;
    const Vc::double_v g1( _mm256_setr_pd( cht0[3], cht4, cht4, 0. ) );
    const Vc::double_v g2( _mm256_setr_pd( chtw0[3], chtw0[3], chtw4, 0. ) );
    Vc::double_v       _3( _mm256_setr_pd( c.c3[3], c.c4[3], c.c44, 0. ) );
    _3 -= g1 * g2;

    x0.store( X.data() );
    X[4] = x4;

    auto         _0perm = _mm256_permute4x64_pd( _0.data(), 132 ); // abcd -> abac
    Vc::double_v output = blend<Vc::X0, Vc::X1, Vc::Y2, Vc::X3>( _0perm, _1.shifted( -1 ).data() );
    output.store( C.data() );
    auto _1perm = _mm256_permute4x64_pd( _1.data(), 194 ); // abcd -> caad
    output      = blend<Vc::X0, Vc::Y1, Vc::Y2, Vc::X3>(
        _1perm, blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( _2.shifted( -1 ).data(), _0.shifted( 1 ).data() ) );
    output.store( C.data() + 4 );
    output = blend<Vc::X0, Vc::Y1, Vc::X2, Vc::Y3>(
        _2.shifted( 1 ).data(),
        blend<Vc::X0, Vc::X1, Vc::Y2, Vc::Y3>( _3.shifted( -1 ).data(), _1.shifted( -3 ).data() ) );
    output.store( C.data() + 8 );
    output = blend<Vc::Y0, Vc::X1, Vc::X2, Vc::X3>( _3.data(), _2.shifted( 2 ).data() );
    _mm256_maskstore_pd( C.data() + 12, mask, output.data() );

    return w * res * res;

#else

    // The ugly code below makes the filter step about 20% faster
    // than SMatrix would do it.
    auto res = refResidual + H[0] * ( Xref[0] - X[0] ) + H[1] * ( Xref[1] - X[1] ) + H[2] * ( Xref[2] - X[2] ) +
               H[3] * ( Xref[3] - X[3] ) + H[4] * ( Xref[4] - X[4] );
    auto CHT = std::array{C[0] * H[0] + C[1] * H[1] + C[3] * H[2] + C[6] * H[3] + C[10] * H[4],      //   C0 or -C1
                          C[1] * H[0] + C[2] * H[1] + C[4] * H[2] + C[7] * H[3] + C[11] * H[4],      //   C1 or -C2
                          C[3] * H[0] + C[4] * H[1] + C[5] * H[2] + C[8] * H[3] + C[12] * H[4],      //   C3 or -C4
                          C[6] * H[0] + C[7] * H[1] + C[8] * H[2] + C[9] * H[3] + C[13] * H[4],      //   C6 or -C7
                          C[10] * H[0] + C[11] * H[1] + C[12] * H[2] + C[13] * H[3] + C[14] * H[4]}; //  C10 or -C11
    auto errorRes2 =
        errorMeas2 + H[0] * CHT[0] + H[1] * CHT[1] + H[2] * CHT[2] + H[3] * CHT[3] + H[4] * CHT[4]; // + C0 or + C2

    // update the state vector and cov matrix
    auto w = res / errorRes2;
    X[0] += CHT[0] * w; // +=  C0 r/R  or  -C1 r/R
    X[1] += CHT[1] * w; // +=  C1 r/R  or  -C2 r/R
    X[2] += CHT[2] * w;
    X[3] += CHT[3] * w;
    X[4] += CHT[4] * w;

    w = 1. / errorRes2;
    C[0] -= w * CHT[0] * CHT[0]; // -=  C0*C0/R  or  C1*C1/R
    C[1] -= w * CHT[1] * CHT[0]; // -=  C1*C0/R  or  C2*C1/R
    C[3] -= w * CHT[2] * CHT[0];
    C[6] -= w * CHT[3] * CHT[0];
    C[10] -= w * CHT[4] * CHT[0];

    C[2] -= w * CHT[1] * CHT[1]; // -=  C1*C1/R  or  C2*C2/R
    C[4] -= w * CHT[2] * CHT[1];
    C[7] -= w * CHT[3] * CHT[1];
    C[11] -= w * CHT[4] * CHT[1];

    C[5] -= w * CHT[2] * CHT[2];
    C[8] -= w * CHT[3] * CHT[2];
    C[12] -= w * CHT[4] * CHT[2];

    C[9] -= w * CHT[3] * CHT[3];
    C[13] -= w * CHT[4] * CHT[3];

    C[14] -= w * CHT[4] * CHT[4];

    return res * res / errorRes2;

#endif
  }

  double filter( span<double, 5> X, span<double, 15> C,                //
                 span<const double, 5> Xref, span<const double, 10> H, //
                 span<const double, 2> refResidual, span<const double, 2> errorMeas2 ) {

    // 2D Kalman filter in line with thesis Jeroen van Tilburg [CERN-THESIS-2005-040]
    // WARNING: not tested beyond the VP2D case.

    // std::cout << "   > Covariance matrix (before): " << std::endl;
    // for(int i=0; i<5; i++) { std::cout << "      C[" << i << "] = " << C[i] << std::endl; }

    // 'predicted residual' res (r_k^k-1) = m - h(x_k^k-1) [eq. 6.10 of thesis JvT]
    // refResidual is given as (x,y) of refTraj.position(meas_z) - measurement (from minimize_point).
    // However, we want to linearize around the reference track, so we add H( Xref - Xpred ),
    //  to obtain res = m - h(xpred) = m - h(Xref) + h(Xref - Xpred).
    auto res = std::array{refResidual[0] + H[0] * ( Xref[0] - X[0] ) + H[1] * ( Xref[1] - X[1] ) +
                              H[2] * ( Xref[2] - X[2] ) + H[3] * ( Xref[3] - X[3] ) + H[4] * ( Xref[4] - X[4] ),
                          refResidual[1] + H[5] * ( Xref[0] - X[0] ) + H[6] * ( Xref[1] - X[1] ) +
                              H[7] * ( Xref[2] - X[2] ) + H[8] * ( Xref[3] - X[3] ) + H[9] * ( Xref[4] - X[4] )};

    // CHT = C.H^T, but note that C is delivered as 3x5=15 elements, not 5x5=25, as it is symmetric.
    // Only lower triangle: {{0,1,3,6,10}, {1,2,4,7,11}, {3,4,5,8,12}, {6,7,8,9,13}, {10,11,12,13,14}}
    // H = 2x5, so {{0,1,2,3,4},{5,6,7,8,9}} (5 columns, 2 rows). CHT = 5x2 so {{0,1},{2,3},...}.
    auto CHT = std::array{
        C[0] * H[0] + C[1] * H[1] + C[3] * H[2] + C[6] * H[3] + C[10] * H[4], //  C0
        C[0] * H[5] + C[1] * H[6] + C[3] * H[7] + C[6] * H[8] + C[10] * H[9], // -C1

        C[1] * H[0] + C[2] * H[1] + C[4] * H[2] + C[7] * H[3] + C[11] * H[4], //  C1
        C[1] * H[5] + C[2] * H[6] + C[4] * H[7] + C[7] * H[8] + C[11] * H[9], // -C2

        C[3] * H[0] + C[4] * H[1] + C[5] * H[2] + C[8] * H[3] + C[12] * H[4], //  C3
        C[3] * H[5] + C[4] * H[6] + C[5] * H[7] + C[8] * H[8] + C[12] * H[9], // -C4

        C[6] * H[0] + C[7] * H[1] + C[8] * H[2] + C[9] * H[3] + C[13] * H[4], //  C6
        C[6] * H[5] + C[7] * H[6] + C[8] * H[7] + C[9] * H[8] + C[13] * H[9], // -C7

        C[10] * H[0] + C[11] * H[1] + C[12] * H[2] + C[13] * H[3] + C[14] * H[4], //  C10
        C[10] * H[5] + C[11] * H[6] + C[12] * H[7] + C[13] * H[8] + C[14] * H[9]  // -C11
    };

    // 'expected variance' errorRes2 ('R_k^k-1') = errorMeas2 ('V') + H . CHT, where C = C_k^k-1. [eq. 6.11 of thesis
    // JvT]
    //  --> HCHT = (2x2), or ((C0, -C1),(-C1, C2)) for simple H, and V is diagonal (x & y meas errors independent)
    auto errorRes2 =
        std::array{H[0] * CHT[0] + H[1] * CHT[2] + H[2] * CHT[4] + H[3] * CHT[6] + H[4] * CHT[8] + errorMeas2[0],
                   H[0] * CHT[1] + H[1] * CHT[3] + H[2] * CHT[5] + H[3] * CHT[7] + H[4] * CHT[9],
                   H[5] * CHT[0] + H[6] * CHT[2] + H[7] * CHT[4] + H[8] * CHT[6] + H[9] * CHT[8],
                   H[5] * CHT[1] + H[6] * CHT[3] + H[7] * CHT[5] + H[8] * CHT[7] + H[9] * CHT[9] + errorMeas2[1]};
    auto errorRes2_det = errorRes2[0] * errorRes2[3] - errorRes2[1] * errorRes2[2]; // TODO test for =0?
    auto errorRes2inv  = std::array{errorRes2[3] / errorRes2_det, -errorRes2[1] / errorRes2_det,
                                   -errorRes2[2] / errorRes2_det, errorRes2[0] / errorRes2_det};
    // Simple H: det = C0C2 - C1C1 ~= C0C2; inv = 1/det ((C2 C1), (C1 C0)) ~= ((1/C0, C1/C0C2), C1/C0C2, 1/C2))

    // Gain matrix K = CHT(V + HCHT)-1 = CHT.R^{-1}, with R = R_k^k-1. [eq. 6.15 of thesis JvT].
    auto K = std::array{
        CHT[0] * errorRes2inv[0] + CHT[1] * errorRes2inv[2], CHT[0] * errorRes2inv[1] + CHT[1] * errorRes2inv[3],
        CHT[2] * errorRes2inv[0] + CHT[3] * errorRes2inv[2], CHT[2] * errorRes2inv[1] + CHT[3] * errorRes2inv[3],
        CHT[4] * errorRes2inv[0] + CHT[5] * errorRes2inv[2], CHT[4] * errorRes2inv[1] + CHT[5] * errorRes2inv[3],
        CHT[6] * errorRes2inv[0] + CHT[7] * errorRes2inv[2], CHT[6] * errorRes2inv[1] + CHT[7] * errorRes2inv[3],
        CHT[8] * errorRes2inv[0] + CHT[9] * errorRes2inv[2], CHT[8] * errorRes2inv[1] + CHT[9] * errorRes2inv[3],
    };

    // Update the state vector: X += K.r (x_k = x_k^k-1 + K.r_k^k-1) [eq. 6.13 of thesis JvT].
    X[0] += K[0] * res[0] + K[1] * res[1];
    X[1] += K[2] * res[0] + K[3] * res[1];
    X[2] += K[4] * res[0] + K[5] * res[1];
    X[3] += K[6] * res[0] + K[7] * res[1];
    X[4] += K[8] * res[0] + K[9] * res[1];

    // update cov matrix [eq.6.14 of thesis JvT]:
    // C -= K.H.C.  = (5x2).(2x5).(5x5) = (5x5). Also, H.C = (CHT)T.
    C[0] -= K[0] * CHT[0] + K[1] * CHT[1];
    C[1] -= K[0] * CHT[2] + K[1] * CHT[3];
    C[3] -= K[0] * CHT[4] + K[1] * CHT[5];
    C[6] -= K[0] * CHT[6] + K[1] * CHT[7];
    C[10] -= K[0] * CHT[8] + K[1] * CHT[9];

    C[2] -= K[2] * CHT[2] + K[3] * CHT[3];
    C[4] -= K[2] * CHT[4] + K[3] * CHT[5];
    C[7] -= K[2] * CHT[6] + K[3] * CHT[7];
    C[11] -= K[2] * CHT[8] + K[3] * CHT[9];

    C[5] -= K[4] * CHT[4] + K[5] * CHT[5];
    C[8] -= K[4] * CHT[6] + K[5] * CHT[7];
    C[12] -= K[4] * CHT[8] + K[5] * CHT[9];

    C[9] -= K[6] * CHT[6] + K[7] * CHT[7];
    C[13] -= K[6] * CHT[8] + K[7] * CHT[9];

    C[14] -= K[8] * CHT[8] + K[9] * CHT[9];

    // std::cout << "   > Covariance matrix (after): " << std::endl;
    // for(int i=0; i<5; i++) { std::cout << "      C[" << i << "] = " << C[i] << std::endl; }

    // Now, calculate filtered residual (r_k) and variation (R_k) from filtered state and cov [JvT eq. 6.16 and 6.17].
    // This was skipped for 1D, and instead the predicted chi2 contribution is returned (using r_k^k-1 and R_k^k-1).
    // <to implement?>

    // contribution to chi2 is r.R^{-1}.r, so sum of 2D components [eq.6.18 JvT]
    return ( res[0] * errorRes2inv[0] + res[1] * errorRes2inv[2] ) * res[0] +
           ( res[0] * errorRes2inv[1] + res[1] * errorRes2inv[3] ) * res[1];
  }

  double filter( span<double, 5> X, span<double, 15> C, //
                 span<const double, 5> Xref, H_VP2D,    //
                 span<const double, 2> refResidual, span<const double, 2> errorMeas2 ) {

    // Fast version of 2D filter for VP2D-type measurements.
    // Assumes H = [ [1 0 0 0 0], [0 -1 0 0 0] ].

    auto res = std::array{refResidual[0] + ( Xref[0] - X[0] ), refResidual[1] - ( Xref[1] - X[1] )};

    auto CHT = std::array{C[0], -C[1], C[1], -C[2], C[3], -C[4], C[6], -C[7], C[10], -C[11]};

    auto errorRes2     = std::array{CHT[0] + errorMeas2[0], CHT[1], -CHT[2], -CHT[3] + errorMeas2[1]};
    auto errorRes2_det = errorRes2[0] * errorRes2[3] - errorRes2[1] * errorRes2[2];
    auto errorRes2inv  = std::array{errorRes2[3] / errorRes2_det, -errorRes2[1] / errorRes2_det,
                                   -errorRes2[2] / errorRes2_det, errorRes2[0] / errorRes2_det};

    auto K = std::array{
        CHT[0] * errorRes2inv[0] + CHT[1] * errorRes2inv[2], CHT[0] * errorRes2inv[1] + CHT[1] * errorRes2inv[3],
        CHT[2] * errorRes2inv[0] + CHT[3] * errorRes2inv[2], CHT[2] * errorRes2inv[1] + CHT[3] * errorRes2inv[3],
        CHT[4] * errorRes2inv[0] + CHT[5] * errorRes2inv[2], CHT[4] * errorRes2inv[1] + CHT[5] * errorRes2inv[3],
        CHT[6] * errorRes2inv[0] + CHT[7] * errorRes2inv[2], CHT[6] * errorRes2inv[1] + CHT[7] * errorRes2inv[3],
        CHT[8] * errorRes2inv[0] + CHT[9] * errorRes2inv[2], CHT[8] * errorRes2inv[1] + CHT[9] * errorRes2inv[3],
    };

    X[0] += K[0] * res[0] + K[1] * res[1];
    X[1] += K[2] * res[0] + K[3] * res[1];
    X[2] += K[4] * res[0] + K[5] * res[1];
    X[3] += K[6] * res[0] + K[7] * res[1];
    X[4] += K[8] * res[0] + K[9] * res[1];

    C[0] -= K[0] * CHT[0] + K[1] * CHT[1];
    C[1] -= K[0] * CHT[2] + K[1] * CHT[3];
    C[3] -= K[0] * CHT[4] + K[1] * CHT[5];
    C[6] -= K[0] * CHT[6] + K[1] * CHT[7];
    C[10] -= K[0] * CHT[8] + K[1] * CHT[9];

    C[2] -= K[2] * CHT[2] + K[3] * CHT[3];
    C[4] -= K[2] * CHT[4] + K[3] * CHT[5];
    C[7] -= K[2] * CHT[6] + K[3] * CHT[7];
    C[11] -= K[2] * CHT[8] + K[3] * CHT[9];

    C[5] -= K[4] * CHT[4] + K[5] * CHT[5];
    C[8] -= K[4] * CHT[6] + K[5] * CHT[7];
    C[12] -= K[4] * CHT[8] + K[5] * CHT[9];

    C[9] -= K[6] * CHT[6] + K[7] * CHT[7];
    C[13] -= K[6] * CHT[8] + K[7] * CHT[9];

    C[14] -= K[8] * CHT[8] + K[9] * CHT[9];

    return ( res[0] * errorRes2inv[0] + res[1] * errorRes2inv[2] ) * res[0] +
           ( res[0] * errorRes2inv[1] + res[1] * errorRes2inv[3] ) * res[1];
  }

} // namespace LHCb::Math::detail
