/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#define BOOST_TEST_DYN_LINK
#define BOOST_TEST_MODULE test_PlatformInfo
#include <boost/test/unit_test.hpp>

#include "Kernel/CaloCellID.h"

BOOST_AUTO_TEST_CASE( test_calo_cellID_index ) {

  constexpr auto nCells = LHCb::Calo::Index::max();
  for ( unsigned int i = 0; i != nCells; ++i ) {
    LHCb::Calo::CellID id  = LHCb::Calo::DenseIndex::details::toCellID( i );
    LHCb::Calo::Index  idx = {id};
    LHCb::Calo::CellID id2 = idx;
    BOOST_CHECK( id2 == id );
    // std::cout << i << " " << id << " " << id2 << '\n';
  }
}
