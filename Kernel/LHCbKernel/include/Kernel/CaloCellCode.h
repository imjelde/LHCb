/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#ifdef USE_DD4HEP

#  include "Detector/Calo/CaloCellID.h"
namespace LHCb::Calo {
  namespace CellCode = LHCb::Detector::Calo::CellCode;
}

#else

#  include "GaudiKernel/StatusCode.h"
#  include <algorithm>
#  include <array>
#  include <iomanip>
#  include <string>
#  include <vector>

/**
 *  Namespace for all code/decode rules of CaloCellID
 *
 *  @todo CaloCellCode.h: switch from C-string to std::string
 *  @todo CaloCellCode.h: make code more robust & CPU-efficient
 *  @todo CaloCellCode.h: remove many redundant & confusing "static"
 *  @todo CaloCellCode.h: return by reference
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    19/02/2001
 */

/** @namespace Calo::CellCode
 *
 *  Namespace for all code/decode rules of CaloCellID
 *
 *  @author  Vanya Belyaev Ivan.Belyaev@itep.ru
 *  @date    19/02/2001
 */
namespace LHCb::Calo::CellCode {

  /** @var s_BadName
   *  representation of "bad-name"
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline const std::string s_BadName = "????";

  /** @typedef ContentType
   *  the actual type for 32 bits representation of internal data
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  using ContentType = unsigned int;
  // ==========================================================================
  // Coding of CaloCellID objects : Lengths, basic
  // ==========================================================================
  constexpr unsigned int BitsCol  = 6;
  constexpr unsigned int BitsRow  = 6;
  constexpr unsigned int BitsArea = 2;
  constexpr unsigned int BitsCalo = 2;
  // ==========================================================================
  // Coding of CaloCellID objects : Lengths, extra
  // ==========================================================================
  constexpr unsigned int BitsIndex = BitsCol + BitsRow + BitsArea;
  constexpr unsigned int BitsAll   = BitsCol + BitsRow + BitsArea + BitsCalo;
  constexpr unsigned int BitsTotal = 32;
  constexpr unsigned int BitsRest  = BitsTotal - BitsAll;
  // ==========================================================================
  // Coding of CaloCellID objects : Shifts, basic
  // ==========================================================================
  constexpr unsigned int ShiftCol  = 0;
  constexpr unsigned int ShiftRow  = ShiftCol + BitsCol;
  constexpr unsigned int ShiftArea = ShiftRow + BitsRow;
  constexpr unsigned int ShiftCalo = ShiftArea + BitsArea;
  // ==========================================================================
  // Coding of CaloCellID objects : Shifts, extra
  // ==========================================================================
  constexpr unsigned int ShiftIndex = ShiftCol;
  constexpr unsigned int ShiftAll   = ShiftCol;
  constexpr unsigned int ShiftRest  = ShiftCalo + BitsCalo;
  // ==========================================================================
  // Coding of CaloCellID objects : Masks, basic
  // ==========================================================================
  constexpr ContentType MaskCol  = ( ( ( (ContentType)1 ) << BitsCol ) - 1 ) << ShiftCol;
  constexpr ContentType MaskRow  = ( ( ( (ContentType)1 ) << BitsRow ) - 1 ) << ShiftRow;
  constexpr ContentType MaskArea = ( ( ( (ContentType)1 ) << BitsArea ) - 1 ) << ShiftArea;
  constexpr ContentType MaskCalo = ( ( ( (ContentType)1 ) << BitsCalo ) - 1 ) << ShiftCalo;
  // ==========================================================================
  // Coding of CaloCellID objects : Masks, extra
  // ==========================================================================
  constexpr ContentType MaskIndex = ( ( ( (ContentType)1 ) << BitsIndex ) - 1 ) << ShiftIndex;
  constexpr ContentType MaskAll   = ( ( ( (ContentType)1 ) << BitsAll ) - 1 ) << ShiftAll;
  constexpr ContentType MaskRest  = ( ( ( (ContentType)1 ) << BitsRest ) - 1 ) << ShiftRest;

  /** set the active fields
   *  @param Where (INPUT) the initial value
   *  @param Value (INPUT) value to be set
   *  @param Shift (INPUT) shift
   *  @param Mask  (INPUT) mask
   *  @return new Field
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline constexpr ContentType setField( const ContentType Field, const ContentType Value, const ContentType Shift,
                                         const ContentType Mask ) {
    ContentType tmp1 = ( Value << Shift ) & Mask;
    ContentType tmp2 = Field & ~Mask;
    return tmp1 | tmp2;
  }

  /** @enum Index
   *  the indices for  Calorimeter Detectors (to extract the coding of the "calo" )
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  enum Index {
    Undefined = -1,
    SpdCalo   = 0,
    PrsCalo,
    EcalCalo,
    HcalCalo,
    /// total number of NAMED Calorimeters
    CaloNums = 4 // total number of NAMED Calorimeters
  };
  std::string          toString( const Index& );
  StatusCode           parse( Index&, const std::string& );
  inline std::ostream& toStream( const Index& ci, std::ostream& os ) {
    return os << std::quoted( toString( ci ), '\'' );
  }

  /** @var CaloNames
   *  The actual list of Calorimter names
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline const std::array<std::string, CaloNums> CaloNames = {{"Spd", "Prs", "Ecal", "Hcal"}};

  /// the calorimeter names:
  inline const std::string_view SpdName  = {CaloNames[SpdCalo]};  //  Spd
  inline const std::string_view PrsName  = {CaloNames[PrsCalo]};  //  Prs
  inline const std::string_view EcalName = {CaloNames[EcalCalo]}; // Ecal
  inline const std::string_view HcalName = {CaloNames[HcalCalo]}; // Hcal

  /** simple function to get the calorimeter name from number
   *  @param num (INPUT) calorimeter index
   *  @return calorimeter name
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline const std::string& caloName( Index num ) {
    return num < 0 ? s_BadName : num < CaloNums ? CaloNames[num] : s_BadName;
  }

  /** get the calorimeter index from name, returns -1 for wrong name!
   *  @param name (INPUT) the calorimeter name (can be long string)
   *  @return calorimeter index from name, returns -1 for wrong name!
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline Index caloNum( std::string_view name ) {
    auto begin = CaloNames.begin();
    auto end   = CaloNames.end();
    auto m     = std::find_if( begin, end, [=]( const auto& i ) { return name.find( i ) != std::string_view::npos; } );
    return Index( m != end ? Index( m - begin ) : Index::Undefined );
  }

  /** get the calorimeter index from name, returns -1 for wrong name!
   *  @paran name (INPUT) the calorimeter name (can be long string)
   *  @return calorimeter index from name, returns -1 for wrong name!
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  inline Index CaloNumFromName( std::string_view name ) { return caloNum( name ); }

  /** get the area name from calorimeter index and number
   *  @attention function make heavy use of hadcoded structure of Calorimeter!
   *  @warning   the different convention for Hcal
   *  @param  calo (INPUT) calorimeter index
   *  @param  area (INPUT) area index
   *  @return name for the area
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  const std::string& caloArea( const int calo, const int area );

  /** get the area index from calorimeter index and name
   *  @attention function make heavy use of hadcoded structure of Calorimeter!
   *  @warning   the different convention for Hcal
   *  @param  calo (INPUT) calorimeter index
   *  @param  area (INPUT) area name
   *  @return indx for the area
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  int caloArea( const int calo, std::string_view area );

  /** Is the given area is Pin-diod area ?
   *  @attention It must be coherent with caloArea
   *  @see caloArea
   *  @param calo (INPUT) calorimeetr index
   *  @param area (INPUT) calorimeter index
   *  @reutrn true if the area is Pin-diod area
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  bool isPinArea( Index calo, const int area );

  /** @enum CaloArea
   *  The actual enumeration for the calorimeter areas
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-28
   */
  enum CaloArea {
    UndefinedArea = -1,
    Outer         = 0,
    Middle, // NB: Inner for Hcal
    Inner,
    PinArea,         // some code lines explicitly rely on this value
    CaloAreaNums = 4 // total number of Calorimeter areas
  };
} // namespace LHCb::Calo::CellCode

#endif
