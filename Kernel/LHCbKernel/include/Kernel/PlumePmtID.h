/*****************************************************************************\
* (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include <limits>
#include <ostream>

namespace LHCb {

  /** @class PlumePmtID PlumePmtID.h
   *
   * This class identifies a single PMT in Plume detector
   *
   * @author Vladyslav Orlov
   *
   */

  class PlumePmtID final {
  public:
    enum class ChannelType { LUMI, TIME, PIN, MON, ERR };

    /// Default Constructor
    constexpr PlumePmtID() = default;

    /// Constructor with PmtID
    constexpr explicit PlumePmtID( unsigned int id ) {
      m_pmtID = id;

      if ( id <= 47 ) { m_channelType = ChannelType::LUMI; }
      if ( ( 47 < id ) && ( id <= 127 ) ) { m_channelType = ChannelType::TIME; }
      if ( ( 127 < id ) && ( id <= 135 ) ) { m_channelType = ChannelType::PIN; }
      if ( ( 135 < id ) && ( id <= 139 ) ) { m_channelType = ChannelType::MON; }
    }

    /// Special serializer to ASCII stream
    std::ostream& fillStream( std::ostream& s ) const;

    /// Retrieve const  Plume Pmt ID
    [[nodiscard]] constexpr unsigned int PmtID() const { return m_pmtID; }

    /// Retrieve channel type
    [[nodiscard]] constexpr ChannelType channelType() const { return m_channelType; }

    /// Retrieve id by type
    [[nodiscard]] constexpr unsigned int ChannelID() const {
      switch ( m_channelType ) {
      case ChannelType::LUMI:
        return m_pmtID;
      case ChannelType::TIME:
        return m_pmtID - 96;
      case ChannelType::PIN:
        return m_pmtID - 128;
      case ChannelType::MON:
        return m_pmtID - 136;
      default:
        return std::numeric_limits<unsigned int>::max();
      };
    }

    /// Update  Plume Pmt ID
    constexpr PlumePmtID& setPmtID( unsigned int value ) {
      if ( value > 47 ) {
        throw std::out_of_range{"bad Plume::PmtID " + std::to_string( value )};
      } else {
        m_pmtID = value;
      }
      return *this;
    }

    friend std::ostream& operator<<( std::ostream& str, const PlumePmtID& obj ) { return obj.fillStream( str ); }

  private:
    unsigned int m_pmtID{1000}; ///< Plume PMT ID
    ChannelType  m_channelType{ChannelType::ERR};
  }; // class PlumePmtID

} // namespace LHCb
