/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichSide.h
 *
 *  Header file for RICH particle ID enumeration : Rich::Side
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   08/07/2004
 */
//-----------------------------------------------------------------------------

#pragma once

// Kernel
#include "Kernel/RichDetectorType.h"

// Gaudi
#include "GaudiKernel/SerializeSTL.h"

// Detector
#include "Kernel/DD4HEP/RichTypes.h"

// STL files
#include <iostream>
#include <string>

namespace Rich {

  /** Text conversion for Rich::Side enumeration
   *
   *  @param side Rich Side enumeration
   *  @return Rich Side as an std::string
   */
  std::string text( const Rich::Side side );

  /** Text conversion for Rich::DetectorType and Rich::Side enumeration
   *
   *  @param rich Rich Detector
   *  @param side Rich Side enumeration
   *  @return Rich Side as an std::string
   */
  std::string text( const Rich::DetectorType rich, const Rich::Side side );

  /// Implement textual ostream << method for Rich::Side enumeration
  inline std::ostream& operator<<( std::ostream& s, const Rich::Side& side ) { return s << Rich::text( side ); }

  /// Print a vector of Side IDs
  inline std::ostream& operator<<( std::ostream& str, const Sides& sides ) {
    return GaudiUtils::details::ostream_joiner( str << '[', sides, ", " ) << ']';
  }

} // namespace Rich
