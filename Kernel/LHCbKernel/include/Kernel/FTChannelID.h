/*****************************************************************************\
* (c) Copyright 2000-2019 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifdef USE_DD4HEP
#  include "Detector/FT/FTChannelID.h"
namespace LHCb {
  using FTChannelID = LHCb::Detector::FTChannelID;
}
#else
#  pragma once
#  include <cassert>
#  include <ostream>

namespace LHCb {

  /** @class FTChannelID FTChannelID.h
   *
   * Channel ID for the Fibre Tracker (LHCb Upgrade)
   *
   * @author FT software team
   *
   */

  class FTChannelID final {

    /// Bitmasks for bitfield channelID
    enum struct Mask : unsigned {
      channel         = 0x7f,
      sipm            = 0x180,
      mat             = 0x600,
      module          = 0x3800,
      quarter         = 0xc000,
      layer           = 0x30000,
      station         = 0xc0000,
      uniqueLayer     = layer | station,
      uniqueQuarter   = quarter | uniqueLayer,
      uniqueModule    = module | uniqueQuarter,
      uniqueMat       = mat | uniqueModule,
      uniqueSiPM      = sipm | uniqueMat,
      die             = 0x40,
      sipmInModule    = mat | sipm,
      channelInModule = channel | sipmInModule
    };

    template <Mask m>
    [[nodiscard]] static constexpr unsigned int extract( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      return ( i & static_cast<unsigned int>( m ) ) >> b;
    }

    template <Mask m>
    [[nodiscard]] static constexpr unsigned int shift( unsigned int i ) {
      constexpr auto b =
          __builtin_ctz( static_cast<unsigned int>( m ) ); // FIXME: C++20 replace __builtin_ctz with std::countr_zero
      auto v = ( i << static_cast<unsigned int>( b ) );
      assert( extract<m>( v ) == i );
      return v;
    }

    template <Mask m, typename T>
    [[nodiscard]] static constexpr unsigned int shift( T i ) {
      return shift<m>( to_unsigned( i ) );
    }

  public:
    enum struct StationID : unsigned int {};
    [[nodiscard]] friend constexpr unsigned int to_unsigned( StationID id ) { return static_cast<unsigned>( id ); }

    enum struct LayerID : unsigned int {};
    [[nodiscard]] friend constexpr unsigned int to_unsigned( LayerID id ) { return static_cast<unsigned>( id ); }
    [[nodiscard]] friend constexpr bool         is_X( LayerID id ) { return id == LayerID{0} || id == LayerID{3}; }

    enum struct QuarterID : unsigned int {};
    [[nodiscard]] friend constexpr unsigned int to_unsigned( QuarterID id ) { return static_cast<unsigned>( id ); }
    [[nodiscard]] friend constexpr bool is_bottom( QuarterID id ) { return id == QuarterID{0} || id == QuarterID{1}; }
    [[nodiscard]] friend constexpr bool is_top( QuarterID id ) { return id == QuarterID{2} || id == QuarterID{3}; }

    enum struct ModuleID : unsigned int {};
    [[nodiscard]] friend constexpr unsigned int to_unsigned( ModuleID id ) { return static_cast<unsigned>( id ); }

    enum struct MatID : unsigned int {};
    [[nodiscard]] friend constexpr unsigned int to_unsigned( MatID id ) { return static_cast<unsigned>( id ); }

    /// Default Constructor
    constexpr FTChannelID() = default;

    /// Constructor from int
    constexpr explicit FTChannelID( unsigned int id ) : m_channelID{id} {}

    /// Explicit constructor from the geometrical location
    constexpr FTChannelID( StationID station, LayerID layer, QuarterID quarter, ModuleID module, MatID mat,
                           unsigned int sipm, unsigned int channel )
        : FTChannelID{shift<Mask::station>( station ) | shift<Mask::layer>( layer ) | shift<Mask::quarter>( quarter ) |
                      shift<Mask::module>( module ) | shift<Mask::mat>( mat ) | shift<Mask::sipm>( sipm ) |
                      shift<Mask::channel>( channel )} {}

    /// Explicit constructor from the geometrical location
    constexpr FTChannelID( StationID station, LayerID layer, QuarterID quarter, ModuleID module,
                           unsigned int channelInModule )
        : FTChannelID{shift<Mask::station>( station ) | shift<Mask::layer>( layer ) | shift<Mask::quarter>( quarter ) |
                      shift<Mask::module>( module ) | shift<Mask::channelInModule>( channelInModule )} {}

    /// Operator overload, to cast channel ID to unsigned int. Used by linkers where the key (channel id) is an int
    constexpr operator unsigned int() const { return m_channelID; }

    /// Comparison equality
    constexpr friend bool operator==( FTChannelID lhs, FTChannelID rhs ) { return lhs.channelID() == rhs.channelID(); }

    /// Comparison <
    constexpr friend bool operator<( FTChannelID lhs, FTChannelID rhs ) { return lhs.channelID() < rhs.channelID(); }

    /// Comparison >
    constexpr friend bool operator>( FTChannelID lhs, FTChannelID rhs ) { return rhs < lhs; }

    /// Increment the channelID
    constexpr FTChannelID& advance() {
      ++m_channelID;
      return *this;
    }

    /// Return the SiPM number within the module (0-15)
    [[nodiscard]] constexpr unsigned int sipmInModule() const { return extract<Mask::sipmInModule>( m_channelID ); }

    /// Return the die number (0 or 1)
    [[nodiscard]] constexpr unsigned int die() const { return extract<Mask::die>( m_channelID ); }

    /// Return true if channelID is in x-layer
    [[nodiscard]] constexpr bool isX() const { return is_X( layer() ); }

    /// Return true if channelID is in bottom part of detector
    [[nodiscard]] constexpr bool isBottom() const { return is_bottom( quarter() ); }

    /// Return true if channelID is in top part of detector
    [[nodiscard]] constexpr bool isTop() const { return is_top( quarter() ); }

    /// Retrieve const  FT Channel ID
    [[nodiscard]] constexpr unsigned int channelID() const { return m_channelID; }

    /// Retrieve Channel in the 128 channel SiPM
    [[nodiscard]] constexpr unsigned int channel() const { return extract<Mask::channel>( m_channelID ); }

    /// Retrieve ID of the SiPM in the mat
    [[nodiscard]] constexpr unsigned int sipm() const { return extract<Mask::sipm>( m_channelID ); }

    /// Retrieve ID of the mat in the module
    [[nodiscard]] constexpr MatID mat() const { return MatID{extract<Mask::mat>( m_channelID )}; }

    /// Retrieve Module id (0 - 5 or 0 - 6)
    [[nodiscard]] constexpr ModuleID module() const { return ModuleID{extract<Mask::module>( m_channelID )}; }

    /// Retrieve Quarter ID (0 - 3)
    [[nodiscard]] constexpr QuarterID quarter() const { return QuarterID{extract<Mask::quarter>( m_channelID )}; }

    /// Retrieve Layer id
    [[nodiscard]] constexpr LayerID layer() const { return LayerID{extract<Mask::layer>( m_channelID )}; }

    /// Retrieve Station id
    [[nodiscard]] constexpr StationID station() const { return StationID{extract<Mask::station>( m_channelID )}; }

    /// Retrieve unique layer
    [[nodiscard]] constexpr unsigned int uniqueLayer() const { return extract<Mask::uniqueLayer>( m_channelID ); }

    /// Retrieve unique quarter
    [[nodiscard]] constexpr unsigned int uniqueQuarter() const { return extract<Mask::uniqueQuarter>( m_channelID ); }

    /// Retrieve unique module
    [[nodiscard]] constexpr unsigned int uniqueModule() const { return extract<Mask::uniqueModule>( m_channelID ); }

    /// Retrieve unique mat
    [[nodiscard]] constexpr unsigned int uniqueMat() const { return extract<Mask::uniqueMat>( m_channelID ); }

    /// Retrieve unique SiPM
    [[nodiscard]] constexpr unsigned int uniqueSiPM() const { return extract<Mask::uniqueSiPM>( m_channelID ); }

    /// Retrieve moduleID for monitoring (continuous)
    [[nodiscard]] constexpr unsigned int moniModuleID() const {
      /// Five modules per quarter in station 1 and 2 (IDs 0 to 159)
      if ( to_unsigned( station() ) < 3 ) {
        return 5 * ( 4 * ( 4 * ( to_unsigned( station() ) - 1 ) + to_unsigned( layer() ) ) +
                     to_unsigned( quarter() ) ) +
               to_unsigned( module() );
      }
      /// Six modules per quarter in station 3 (IDs 160 to 255)
      else {
        return 160 + 6 * ( 4 * ( to_unsigned( layer() ) ) + to_unsigned( quarter() ) ) + to_unsigned( module() );
      }
    }

    /// Retrieve moduleID unique per station for monitoring (continuous)
    [[nodiscard]] constexpr unsigned int moniModuleIDstation() const {
      if ( to_unsigned( station() ) < 3 ) {
        return 5 * ( 4 * to_unsigned( layer() ) + to_unsigned( quarter() ) ) + to_unsigned( module() );
      } else {
        return 6 * ( 4 * to_unsigned( layer() ) + to_unsigned( quarter() ) ) + to_unsigned( module() );
      }
    }

    /// Retrieve quarterID for monitoring
    [[nodiscard]] constexpr unsigned int moniQuarterID() const {
      return to_unsigned( quarter() ) + 4 * ( to_unsigned( layer() ) + 4 * ( to_unsigned( station() ) - 1 ) );
    }

    /// Retrieve SiPMID for monitoring (continuous)
    [[nodiscard]] constexpr unsigned int moniSiPMID() const {
      return sipm() + 4 * ( to_unsigned( mat() ) + 4 * to_unsigned( module() ) );
    }

    /// Retrieve channelID for monitoring
    [[nodiscard]] constexpr unsigned int moniChannelID() const {
      return channel() + 128 * ( sipm() + 4 * to_unsigned( mat() ) );
    }

    friend std::ostream& operator<<( std::ostream& s, const FTChannelID& obj ) {
      return s << "{ FTChannelID : "
               << " channel =" << obj.channel() << " sipm =" << obj.sipm() << " mat =" << to_unsigned( obj.mat() )
               << " module=" << to_unsigned( obj.module() ) << " quarter=" << to_unsigned( obj.quarter() )
               << " layer=" << to_unsigned( obj.layer() ) << " station=" << to_unsigned( obj.station() ) << " }";
    }

  private:
    unsigned int m_channelID{0}; ///< FT Channel ID

  }; // class FTChannelID

} // namespace LHCb

// -----------------------------------------------------------------------------
// end of class
// -----------------------------------------------------------------------------
#endif
