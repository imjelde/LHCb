/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Kernel/CaloCellID.h"
#include "Kernel/CaloCellCode.h"

#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/ToStream.h"

#include <iomanip>
#include <sstream>

/**
 *  Implementation file for non-inlined functions of class LHCb::Calo::CellID
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-09-29
 */

namespace LHCb::Calo {
  // ============================================================================
  // printout to output stream
  // ============================================================================
  std::ostream& CellID::fillStream( std::ostream& os ) const {
    os << "(";
    /// corrupted ?!
    if ( 0 != rest() ) { os << "?" << std::setw( 4 ) << rest() << "?,"; }
    return os << "'" << CellCode::caloName( calo() ) << "',"
              << "'" << CellCode::caloArea( calo(), area() ) << "'," << std::setw( 2 ) << row() << "," << std::setw( 2 )
              << col() << ")";
  }
  // ============================================================================
  // conversion to bit-string
  // ============================================================================
  std::string CellID::bits( char del ) const {
    std::string        str( "[" );
    const unsigned int one = 1;
    for ( int n = ( 0 == rest() ) ? CellCode::BitsAll - 1 : CellCode::BitsTotal - 1; n >= 0; --n ) {
      unsigned int pos    = n;
      bool         isNull = ( 0 == ( ( one << pos ) & m_all ) );
      str += isNull ? '0' : '1';
      if ( pos == 0 ) break;
      constexpr auto delimiters = std::array{CellCode::ShiftRest, CellCode::ShiftCalo, CellCode::ShiftArea,
                                             CellCode::ShiftRow,  CellCode::ShiftCol,  CellCode::ShiftAll};
      if ( del && std::any_of( delimiters.begin(), delimiters.end(), [&]( auto code ) { return pos == code; } ) ) {
        str += del;
      }
    }
    return str += "]";
  }
} // namespace LHCb::Calo

/** Implementation of streaming&parsing function for class LHCb::Calo::CellID
 *  @see LHCb::Calo::CellID
 *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
 *  @date 2009-09-29
 */
namespace Gaudi::Parsers {

  /** @class CCIDGrammar
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @author alexander.mazurov@gmail.com
   *  @date 2011-08-29
   */
  template <typename Iterator, typename Skipper>
  class CCIDGrammar : public qi::grammar<Iterator, LHCb::Calo::CellID(), Skipper> {
  public:
    /// the actual type of parsed result
    using ResultT  = LHCb::Calo::CellID; // the actual type of parsed result
    using ContentT = unsigned int;

    struct tag_calo {};
    struct tag_area {};
    struct tag_row {};
    struct tag_col {};
    struct Operations {
      void operator()( LHCb::Calo::CellID& val, const ContentT v ) const { val.setAll( v ); }
      void match( LHCb::Calo::CellID& val, const unsigned short Value, const unsigned int Shift,
                  const unsigned int Mask ) const {
        ContentT tmp = val.all();
                 operator()( val, LHCb::Calo::CellCode::setField( tmp, Value, Shift, Mask ) );
      }

      void operator()( LHCb::Calo::CellID& val, unsigned short v, tag_calo ) const {
        val.setCalo( static_cast<LHCb::Calo::CellCode::Index>( v ) );
      }
      void operator()( LHCb::Calo::CellID& val, const std::string& v, tag_calo ) const {
        val.setCalo( LHCb::Calo::CellCode::caloNum( v ) );
      }
      void operator()( LHCb::Calo::CellID& val, unsigned short v, tag_area ) const {
        match( val, v, LHCb::Calo::CellCode::ShiftArea, LHCb::Calo::CellCode::MaskArea );
      }
      void operator()( LHCb::Calo::CellID& val, const std::string& v, tag_area ) const {
        operator()( val, LHCb::Calo::CellCode::caloArea( val.calo(), v ), tag_area() );
      }

      void operator()( LHCb::Calo::CellID& val, unsigned short v, tag_row ) const {
        match( val, v, LHCb::Calo::CellCode::ShiftRow, LHCb::Calo::CellCode::MaskRow );
      }

      void operator()( LHCb::Calo::CellID& val, unsigned short v, tag_col ) const {
        match( val, v, LHCb::Calo::CellCode::ShiftCol, LHCb::Calo::CellCode::MaskCol );
      }
    };

    CCIDGrammar() : CCIDGrammar::base_type( result ) {
      max_limit %= qi::int_[qi::_a = qi::_1] >> qi::eps( qi::_a <= qi::_r1 && qi::_a >= 0 );
      result =
          ( -( qi::lit( "LHCb.CaloCellID" ) | qi::lit( "CaloCellID" ) ) >> qi::lit( '(' ) >>
            ( max_limit( 4 )[op( qi::_val, qi::_1, tag_calo() )] | calo[op( qi::_val, qi::_1, tag_calo() )] ) >> ',' >>
            ( max_limit( 4 )[op( qi::_val, qi::_1, tag_area() )] | area[op( qi::_val, qi::_1, tag_area() )] ) >> ',' >>
            max_limit( 64 )[op( qi::_val, qi::_1, tag_row() )] >> ',' >>
            max_limit( 64 )[op( qi::_val, qi::_1, tag_col() )] >> qi::lit( ')' ) ) |
          ( qi::lit( '(' ) >> qi::int_[op( qi::_val, qi::_1 )] >> qi::lit( ')' ) ) | qi::int_[op( qi::_val, qi::_1 )];
    }
    StringGrammar<Iterator, Skipper>                         area, calo;
    qi::rule<Iterator, LHCb::Calo::CellID(), Skipper>        result;
    qi::rule<Iterator, int( int ), qi::locals<int>, Skipper> max_limit;
    ph::function<Operations>                                 op;
  };
  REGISTER_GRAMMAR( LHCb::Calo::CellID, CCIDGrammar );

  /*  parse cellID from the string
   *  @param result (OUPUT) the parsed cellID
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( LHCb::Calo::CellID& result, const std::string& input ) { return parse_( result, input ); }

  /*  parse the vector of cellIDs from the string
   *  @param result (OUPUT) the parsed vector of cellIDs
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( std::vector<LHCb::Calo::CellID>& result, const std::string& input ) {
    return parse_( result, input );
  }

  /*  parse the vector of cellIDs from the string
   *  @param result (OUPUT) the parsed vector of cellIDs
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( std::set<LHCb::Calo::CellID>& result, const std::string& input ) {
    std::vector<LHCb::Calo::CellID> tmp;
    return parse( tmp, input ).andThen( [&] {
      result.clear();
      result.insert( tmp.begin(), tmp.end() );
    } );
  }

  /*  parse the map of  { cellID : double } from the string
   *  @param result (OUPUT) the parsed map { cellID : double }
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( std::map<LHCb::Calo::CellID, double>& result, const std::string& input ) {
    return parse_( result, input );
  }

  /* parse the map of  { cellID : vector<double> } from the string
   *  @param result (OUPUT) the parsed map { cellID : vetcor<double> }
   *  @param input  (INPUT) the input string
   *  @return status code
   *  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
   *  @date 2009-09-29
   */
  StatusCode parse( std::map<LHCb::Calo::CellID, std::vector<double>>& result, const std::string& input ) {
    return parse_( result, input );
  }
} // namespace Gaudi::Parsers
