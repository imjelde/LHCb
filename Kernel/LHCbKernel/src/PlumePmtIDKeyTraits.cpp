/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// ============================================================================
// Include files
#include "GaudiKernel/GaudiException.h"
// local
#include "Kernel/PlumePmtIDKeyTraits.h"

// ============================================================================
/** @file PlumePmtIDKeyTraits.cpp
 *
 *  Implementation file for methods from Containers namespace
 *
 *  @author Vladyslav Orlov based on CaloCellIDKeyTraits.cpp
 *  @date 19/06/2021
 */
// ============================================================================

// ============================================================================
/**
 *  function to be called at each attempt of automatic creation of PlumePmtID
 *  object as a KEY for KeyedObjects
 *
 *  @exception GaudiException
 *  @see GaudiException
 *  @see Containers
 *  @see PlumePmtID
 *  @see PlumePmtIDKeyTraits
 *  @author Vladyslav Orlov based on CaloCellIDKeyTraits.cpp
 *  @date 19/06/2021
 */
// ============================================================================
void Containers::errorMakePlumePmtIDKey() {
  throw GaudiException( "No automatic key creation for KEY=LHCb::PlumePmtID !", "KeyedObject<LHCb::PlumePmtID>",
                        StatusCode::FAILURE );
}

// ============================================================================
// The End
// ============================================================================
