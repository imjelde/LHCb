/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// local
#include "Kernel/VPChannelID.h"

//-----------------------------------------------------------------------------
// Implementation file for class : VPChannelID
//
// 2009-05-11 : Victor Coco
//-----------------------------------------------------------------------------

namespace LHCb {
#ifdef USE_DD4HEP
  namespace Detector {
#endif

    std::ostream& operator<<( std::ostream& s, const LHCb::VPChannelID& obj ) {
      return s << "{ "
               << " VPChannelID : " << obj.channelID() << " : row = " << to_unsigned( obj.row() )
               << " col = " << to_unsigned( obj.col() ) << " chip = " << to_unsigned( obj.chip() )
               << " sensor = " << to_unsigned( obj.sensor() ) << " }";
    }
#ifdef USE_DD4HEP
  }
#endif
} // namespace LHCb
