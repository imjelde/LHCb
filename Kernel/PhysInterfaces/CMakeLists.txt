###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/PhysInterfaces
---------------------
#]=======================================================================]

gaudi_add_library(PhysInterfacesLib
    SOURCES
        src/GetIDVAlgorithm.cpp
    LINK
        PUBLIC
            Gaudi::GaudiKernel
            LHCb::PhysEvent
            LHCb::RecEvent
        PRIVATE
            Gaudi::GaudiAlgLib
)

gaudi_add_dictionary(PhysInterfacesDict
    HEADERFILES dict/PhysInterfacesDict.h
    SELECTION dict/PhysInterfacesDict.xml
    LINK LHCb::PhysInterfacesLib
)
