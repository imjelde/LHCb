/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "EmptyEventNodeCleaner.h"

#include "GaudiKernel/IRegistry.h"
#include "GaudiKernel/SmartDataPtr.h"
#include "GaudiKernel/SmartIF.h"

//-----------------------------------------------------------------------------
// Implementation file for class : EmptyEventNodeCleaner
//
// 2012-01-31 : Chris Jones
//-----------------------------------------------------------------------------

StatusCode EmptyEventNodeCleaner::execute( const EventContext& ) const {
  // Try and load the root DataObject for the configured stream
  SmartDataPtr<DataObject> root( m_dataSvc.get(), m_inputStream );

  // if found, recursively clean
  if ( root ) cleanNodes( root, m_inputStream );

  // return
  return StatusCode::SUCCESS;
}

void EmptyEventNodeCleaner::cleanNodes( DataObject* obj, const std::string& location, unsigned int nRecCount ) const {
  // protect against infinite recursion
  if ( ++nRecCount > 99999 ) {
    ++m_recursed;
    return;
  }

  SmartIF<IDataManagerSvc> mgr( m_dataSvc.get() );
  std::vector<IRegistry*>  leaves;

  // Load the leaves
  StatusCode sc = mgr->objectLeaves( obj, leaves );
  if ( sc ) {

    if ( !leaves.empty() ) {
      for ( auto iL = leaves.begin(); leaves.end() != iL; ++iL ) {
        const std::string& id = ( *iL )->identifier();
        DataObject*        tmp( nullptr );
        sc = m_dataSvc->findObject( id, tmp );
        if ( sc && tmp ) {
          if ( CLID_DataObject == tmp->clID() ) { cleanNodes( tmp, id, nRecCount ); }
        }
      }
      // Load again, after cleaning, to see what remains
      sc = mgr->objectLeaves( obj, leaves );
    }

    if ( sc && leaves.empty() ) {
      if ( msgLevel( MSG::DEBUG ) ) debug() << "Removing node " << location << endmsg;
      sc = m_dataSvc->unlinkObject( location );
    }
  }
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( EmptyEventNodeCleaner )
