/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/Condition.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/IUpdateManagerSvc.h"
#include "GaudiKernel/ToolHandle.h"
#include "Kernel/IAccept.h" // IAccept
#include "Kernel/IDQFilter.h"
#include "Kernel/IDQScanner.h"

// ----------------------------------------------------------------------------
// Implementation file for class: DQAcceptTool
//
// 09/02/2012: Marco Clemencic
// ----------------------------------------------------------------------------

/** @class DQAcceptTool DQAcceptTool.h src/DQAcceptTool.h
 *
 *
 * @author Marco Clemencic
 * @date 09/02/2012
 */
class DQAcceptTool final : public extends<GaudiTool, IAccept> {

public:
  /// Standard constructor
  using extends::extends;

  /// Initialize the tool.
  StatusCode initialize() override;

  /// Tells if the current event has to be accepted or not, according to the
  bool accept() const override;

private:
  /// Call-back function passed to the UpdateManagerSvc to update the current
  /// filtering status (good or bad).
  StatusCode i_checkFlagsByRun();

  /// Call-back function passed to the UpdateManagerSvc to update the current
  /// filtering status (good or bad).
  StatusCode i_checkFlagsByEvent();

private:
  /// Path to the used condition object.
  /// Depending on the "ByRun" property it defined the condition to get
  /// the run boundaries or the one for the DQ Flags.
  Gaudi::Property<std::string> m_condPath{this, "ConditionPath", "Conditions/Online/LHCb/RunParameters",
                                          "Path in the Detector Transient Store where to find the run condition."};

  /// Tell if the DQ flags have to be used by run or by event.
  Gaudi::Property<bool> m_byRun{this, "ByRun", true, "If the DQ Flags have to be considered by run or by event."};

  /// @{
  /// Type/Name of the (private) tool to filter the DQ Flags (default: BasicDQFilter).
  ToolHandle<IDQFilter> m_filter{this, "Filter", "BasicDQFilter", "IDQFilter Tool defining the acceptance rules."};
  /// Type/Name of the (private) tool to scan the CondDB for the DQ Flags of a run (default: CondDBDQScanner).
  ToolHandle<IDQScanner> m_scanner{this, "Scanner", "CondDBDQScanner",
                                   "IDQScanner Tool to collect all the DQ Flags in a run."};
  /// @}

  /// Transient flag updated every time the run condition changes to state
  /// if the currently processed event is good or bad.
  bool m_accepted{true};

  /// Pointer to condition to use (filled by the UpdateManagerSvc).
  /// @see m_condPath
  Condition* m_cond = nullptr;
};
DECLARE_COMPONENT( DQAcceptTool )

#define ON_DEBUG if ( msgLevel( MSG::DEBUG ) )
#define DEBUG_MSG ON_DEBUG debug()

StatusCode DQAcceptTool::initialize() {
  StatusCode sc = GaudiTool::initialize();
  if ( sc.isFailure() ) return sc;

  if ( m_condPath.empty() ) {
    error() << "Cannot work with empty ConditionPath" << endmsg;
    return StatusCode::FAILURE;
  }

  registerCondition( m_condPath, m_cond,
                     m_byRun.value() ? &DQAcceptTool::i_checkFlagsByRun : &DQAcceptTool::i_checkFlagsByEvent );

  ON_DEBUG {
    debug() << "DQAcceptTool/" << name() << " initialized:" << endmsg;
    debug() << "  using condition at '" << m_condPath.value() << "'";
    if ( m_byRun.value() ) {
      debug() << " for run boundaries" << endmsg;
      debug() << "  scanning flags with " << m_scanner.typeAndName() << endmsg;
    } else {
      debug() << " DQ Flags" << endmsg;
    }
    debug() << "  analyze flags with " << m_filter.typeAndName() << endmsg;
  }

  // We do not want to run a scan during initialization, but we may be
  // instantiated during and event. So, if it is the case, we trigger an
  // immediate update.
  SmartIF<IStateful> stateMachine( serviceLocator() );
  if ( stateMachine->FSMState() >= Gaudi::StateMachine::RUNNING ) { sc = updMgrSvc()->update( this ); }

  return sc;
}

StatusCode DQAcceptTool::i_checkFlagsByRun() {
  if ( msgLevel( MSG::VERBOSE ) )
    verbose() << "Updating Data Quality flags for run " << m_cond->param<int>( "RunNumber" ) << endmsg;

  const IDQFilter::FlagsType& flags = m_scanner->scan( m_cond->validSince(), m_cond->validTill() );
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "-> " << flags << endmsg;

  m_accepted = m_filter->accept( flags );
  // we successfully updated the m_bad state
  return StatusCode::SUCCESS;
}

StatusCode DQAcceptTool::i_checkFlagsByEvent() {
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "Updating Data Quality flags" << endmsg;

  const auto& flags = m_cond->param<IDQFilter::FlagsType>( "map" );
  if ( msgLevel( MSG::VERBOSE ) ) verbose() << "-> " << flags << endmsg;

  m_accepted = m_filter->accept( flags );
  // we successfully updated the m_bad state
  return StatusCode::SUCCESS;
}

bool DQAcceptTool::accept() const { return m_accepted; }

// ============================================================================
