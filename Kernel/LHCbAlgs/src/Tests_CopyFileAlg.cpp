/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "LHCbAlgs/Consumer.h"

#define BOOST_FILESYSTEM_NO_DEPRECATED
#include <boost/filesystem.hpp>

namespace LHCbAlgsTests {

  /// Test algorithm that copies a file when executed.
  struct CopyFileAlg final : LHCb::Algorithm::Consumer<void()> {
    using Consumer::Consumer;

    void operator()() const override {
      boost::filesystem::copy_file( m_source.value(), m_destination.value(),
                                    boost::filesystem::copy_options::overwrite_existing );
    }

    Gaudi::Property<std::string> m_source{this, "Source", ""};
    Gaudi::Property<std::string> m_destination{this, "Destination", ""};
  };

  DECLARE_COMPONENT( CopyFileAlg )
} // namespace LHCbAlgsTests
