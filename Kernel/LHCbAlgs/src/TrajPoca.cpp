/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/ITrajPoca.h"
#include "Kernel/Trajectory.h"
#include "Math/CholeskyDecomp.h"
#include <algorithm>
#include <utility>

/** @class TrajPoca TrajPoca.h
 *
 *  Calculates points of closest approach
 *  between two trajectories
 *
 *  @author Steve Schaffner (babar implementation)
 *  @author Edwin Bos, Jeroen van Tilburg, Eduardo Rodrigues
 *  @date   2005-11-23
 **/

class TrajPoca : public extends<GaudiTool, ITrajPoca> {
public:
  /// Default constructor
  using extends::extends;

  /// Find points along trajectories at which the distance between the
  /// trajectories is at its minimum. The precision parameter is the desired
  /// numerical accuracy of mu1 and mu2. If the restrictrange flag is true, mu
  /// is restricted to the range of the trajectory.
  StatusCode minimize( const LHCb::Trajectory<double>& traj1, double& mu1, RestrictRange range1,
                       const LHCb::Trajectory<double>& traj2, double& mu2, RestrictRange range2,
                       ROOT::Math::XYZVector& distance, double precision ) const override;

  /// Find point along trajectory at which the distance to point 'p'
  /// is minimum. The precision parameter is the desired numerical accuracy of
  /// the expansion parameter mu. If the restrictrange flag is true, mu is
  /// restricted to the range of the trajectory.
  StatusCode minimize( const LHCb::Trajectory<double>& traj, double& mu, RestrictRange restrictRange,
                       const ROOT::Math::XYZPoint& pt, ROOT::Math::XYZVector& distance,
                       double precision ) const override;

private:
  // jobOptions
  Gaudi::Property<int>    m_maxnOscillStep{this, "MaxnOscillStep", 5};
  Gaudi::Property<int>    m_maxnDivergingStep{this, "MaxnDivergingStep", 5};
  Gaudi::Property<int>    m_maxnStuck{this, "MaxnStuck", 3};
  Gaudi::Property<int>    m_maxnTry{this, "MaxnTry", 100};
  Gaudi::Property<double> m_maxDist{this, "MaxDist", 100000000};
  Gaudi::Property<double> m_maxExtrapTolerance{this, "MaxExtrapTolerance", 1 * Gaudi::Units::cm};

  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_diverging{this, "Minimization was diverging"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_oscillating{this, "Minimization bailed out of oscillation."};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_notConverging{this, "Minimization did not converge"};
};

DECLARE_COMPONENT( TrajPoca )

namespace {

  bool restrictToRange( double& l, const LHCb::Trajectory<double>& t ) {
    const auto minmax = std::minmax( {t.beginRange(), t.endRange()} );
    const auto oldl   = std::exchange( l, std::clamp( l, minmax.first, minmax.second ) );
    return oldl != l;
  }

  struct cache_t {
    ROOT::Math::XYZPoint  p1, p2;
    ROOT::Math::XYZVector dp1dmu1, dp2dmu2;
    ROOT::Math::XYZVector d2p1dmu12, d2p2dmu22;
  };
} // namespace
namespace TP::details {
  enum class step_status_t : StatusCode::code_t { ok = 1, parallel = 10, nearly_parallel, beyond_maxdist };

  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "RungeKuttaExtrapolator"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<step_status_t>( code ) ) {
      case step_status_t::parallel:
        return "The Trajectories are parallel.";
      case step_status_t::nearly_parallel:
        return "The Trajectories are very nearly parallel.";
      case step_status_t::beyond_maxdist:
        return "Stepped further than MaxDist.";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace TP::details
STATUSCODE_ENUM_DECL( TP::details::step_status_t )
STATUSCODE_ENUM_IMPL( TP::details::step_status_t, TP::details::ErrorCategory )

//=============================================================================
//
//=============================================================================
TP::details::step_status_t stepTowardPoca( const LHCb::Trajectory<double>& traj1, double& mu1,
                                           ITrajPoca::RestrictRange        restrictRange1,
                                           const LHCb::Trajectory<double>& traj2, double& mu2,
                                           ITrajPoca::RestrictRange restrictRange2, double tolerance, cache_t& cache,
                                           double maxExtrapTolerance, double maxDist ) {
  // a bunch of ugly, unitialized member variables
  traj1.expansion( mu1, cache.p1, cache.dp1dmu1, cache.d2p1dmu12 );
  traj2.expansion( mu2, cache.p2, cache.dp2dmu2, cache.d2p2dmu22 );
  const ROOT::Math::XYZVector d( cache.p1 - cache.p2 );
  // if the distance between points is below 1e-4 mm, we consider the
  // minimisation to be converged
  if ( d.mag2() < std::pow( 1e-4 * Gaudi::Units::mm, 2 ) ) { return TP::details::step_status_t::ok; }
  std::array<double, 3> mat = {
      // keep all terms up to order mu1, mu2, mu1 * mu2
      cache.dp1dmu1.mag2() + d.Dot( cache.d2p1dmu12 ),
      -cache.dp2dmu2.Dot( cache.dp1dmu1 ),
      cache.dp2dmu2.mag2() - d.Dot( cache.d2p2dmu22 ),
  };
  ROOT::Math::CholeskyDecomp<double, 2> decomp( &mat[0] );
  if ( !decomp ) {
    // singular, or not pos. def; try again neglecting curvature
    mat[0] = cache.dp1dmu1.mag2(), mat[2] = cache.dp2dmu2.mag2();
    decomp = ROOT::Math::CholeskyDecomp<double, 2>( &mat[0] );
    if ( !decomp ) {
      // singular, or not pos. def; give up
      return TP::details::step_status_t::parallel;
    }
  }
  {
    // check product of eigenvalues of mat; if too small, trajectories are very
    // nearly parallel
    decltype( mat ) lmat;
    if ( !decomp.getL( &lmat[0] ) || lmat[0] * lmat[2] < 1e-8 ) return TP::details::step_status_t::nearly_parallel;
  }
  const std::array<double, 2> rhs = {-d.Dot( cache.dp1dmu1 ), d.Dot( cache.dp2dmu2 )};
  std::array<double, 2>       dmu = rhs;
  decomp.Solve( dmu );

  int pathDir1 = ( dmu[0] > 0 ) ? 1 : -1;
  int pathDir2 = ( dmu[1] > 0 ) ? 1 : -1;

  // Don't try going further than worst parabolic approximation will
  // allow. The tolerance is set by 'deltadoca', the expected
  // improvement in the doca. We bound this by tolerance from below
  // and maxExtrapTolerance from above.
  double                  deltadoca       = std::sqrt( std::abs( rhs[0] * dmu[0] ) +
                                std::abs( rhs[1] * dmu[1] ) ); // std::abs only because of machine precision issues.
  double                  extraptolerance = std::min( std::max( deltadoca, tolerance ), maxExtrapTolerance );
  static constexpr double smudge          = 1.01; // Factor to push just over border of piecewise traj (essential!)
  double                  distToErr1      = smudge * traj1.distTo2ndError( mu1, extraptolerance, pathDir1 );
  double                  distToErr2      = smudge * traj2.distTo2ndError( mu2, extraptolerance, pathDir2 );

  // Factor to push just over border of piecewise traj (essential!)
  if ( 0 < distToErr1 && distToErr1 < std::abs( dmu[0] ) ) {
    // choose solution for which dmu[0] steps just over border
    dmu[0] = distToErr1 * pathDir1;
    // now recalculate dmu[1], given dmu[0]:
    dmu[1] = ( rhs[1] - dmu[0] * mat[1] ) / mat[2];
  }

  if ( 0 < distToErr2 && distToErr2 < std::abs( dmu[0] ) ) {
    // choose solution for which dmu[1] steps just over border
    dmu[1] = distToErr2 * pathDir2;
    // now recalculate dmu[0], given dmu[1]:
    dmu[0] = ( rhs[0] - dmu[1] * mat[1] ) / mat[0];
    // if still not okay,
    if ( 0 < distToErr1 && distToErr1 < std::abs( dmu[0] ) ) { dmu[0] = distToErr1 * pathDir1; }
  }

  mu1 += dmu[0], mu2 += dmu[1];

  // these do not make any sense here. either we need to merge them with the lines above that restrict to the validity
  // of the expansion, or we need to move them out of here entirely.
  if ( bool( restrictRange1 ) ) restrictToRange( mu1, traj1 );
  if ( bool( restrictRange2 ) ) restrictToRange( mu2, traj2 );

  // another check for parallel trajectories
  if ( std::min( std::abs( mu1 ), std::abs( mu2 ) ) > maxDist ) return TP::details::step_status_t::beyond_maxdist;

  return TP::details::step_status_t::ok;
}

//=============================================================================
// Find mus along trajectories having a distance smaller than tolerance
//=============================================================================
StatusCode TrajPoca::minimize( const LHCb::Trajectory<double>& traj1, double& mu1,
                               ITrajPoca::RestrictRange restrictRange1, const LHCb::Trajectory<double>& traj2,
                               double& mu2, ITrajPoca::RestrictRange restrictRange2, ROOT::Math::XYZVector& distance,
                               double precision ) const {
  StatusCode status = StatusCode::SUCCESS;

  double  delta2( 0 ), prevdelta2( 0 );
  int     nOscillStep( 0 );
  int     nDivergingStep( 0 );
  int     nStuck( 0 );
  bool    finished = false;
  cache_t workspace;

  for ( int istep = 0; istep < m_maxnTry && !finished; ++istep ) {
    double prevflt1       = mu1;
    double prevflt2       = mu2;
    double prevprevdelta2 = std::exchange( prevdelta2, delta2 );
    auto   step_status = stepTowardPoca( traj1, mu1, restrictRange1, traj2, mu2, restrictRange2, precision, workspace,
                                       m_maxExtrapTolerance, m_maxDist );
    if ( step_status != TP::details::step_status_t::ok ) {
      status = step_status;
      if ( msgLevel( MSG::DEBUG ) ) { debug() << status.message() << endmsg; }
      break; // Parallel Trajectories in stepTowardPoca
    }

    distance        = traj1.position( mu1 ) - traj2.position( mu2 );
    delta2          = distance.Mag2();
    double step1    = mu1 - prevflt1;
    double step2    = mu2 - prevflt2;
    int    pathDir1 = ( step1 > 0. ) ? 1 : -1;
    int    pathDir2 = ( step2 > 0. ) ? 1 : -1;
    // Can we stop stepping?
    double distToErr1 = traj1.distTo1stError( prevflt1, precision, pathDir1 );
    double distToErr2 = traj2.distTo1stError( prevflt2, precision, pathDir2 );
    // converged if very small steps
    finished = std::abs( step1 ) < distToErr1 && std::abs( step2 ) < distToErr2;

    // we have to catch some problematic cases
    if ( !finished && istep > 2 && delta2 > prevdelta2 ) {
      // we can get stuck if a flt range is restricted
      if ( ( restrictRange1 && std::abs( step1 ) > 1.0e-10 ) || ( restrictRange2 && std::abs( step2 ) > 1e-10 ) ) {
        if ( ++nStuck > m_maxnStuck ) {
          // downgrade to a point poca
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "Minimization got stuck." << endmsg; }
          ROOT::Math::XYZVector dist( 0., 0., 0. );
          status = ( restrictRange2 ? minimize( traj1, mu1, restrictRange1, traj2.position( mu2 ), dist, precision )
                                    : minimize( traj2, mu2, restrictRange2, traj1.position( mu1 ), dist, precision ) );
          if ( status.isFailure() ) {
            if ( msgLevel( MSG::DEBUG ) ) { debug() << "Downgrade to point POCA FAILED" << endmsg; }
            break;
          }
          status   = StatusCode::SUCCESS; // "Stuck poca"
          finished = true;
        }
      } else if ( prevdelta2 > prevprevdelta2 ) {
        // diverging
        if ( ++nDivergingStep > m_maxnDivergingStep ) {
          status = StatusCode::SUCCESS; // "Failed to converge"
          ++m_diverging;
          finished = true;
        }
      } else {
        nDivergingStep = 0;
        // oscillating
        if ( ++nOscillStep > m_maxnOscillStep ) {
          // bail out of oscillation. since the previous step was
          // better, use that one.
          mu1 = prevflt1;
          mu2 = prevflt2;
          if ( msgLevel( MSG::DEBUG ) ) { debug() << "Minimization bailed out of oscillation." << endmsg; }
          status = StatusCode::SUCCESS; // "Oscillating poca"
          ++m_oscillating;
          finished = true;
        } else {
          // we might be oscillating, but we could also just have
          // stepped over the minimum. choose a solution `in
          // between'.
          mu1 = prevflt1 + 0.5 * step1;
          if ( restrictRange1 ) restrictToRange( mu1, traj1 );
          mu2 = prevflt2 + 0.5 * step2;
          if ( restrictRange2 ) restrictToRange( mu2, traj2 );
          distance = traj1.position( mu1 ) - traj2.position( mu2 );
          delta2   = distance.Mag2();
        }
      }
    }
  }

  if ( !finished ) {
    status = StatusCode::FAILURE;
    ++m_notConverging;
  }

  return status;
}

//=============================================================================
//
//=============================================================================
StatusCode TrajPoca::minimize( const LHCb::Trajectory<double>& traj, double& mu, RestrictRange restrictRange,
                               const ROOT::Math::XYZPoint& pt, ROOT::Math::XYZVector& distance,
                               double /*precision*/ ) const {
  // this does not work for non-linear Trajectories!
  mu = traj.muEstimate( pt );
  if ( restrictRange ) restrictToRange( mu, traj );
  distance = traj.position( mu ) - pt;
  return StatusCode::SUCCESS;
}
