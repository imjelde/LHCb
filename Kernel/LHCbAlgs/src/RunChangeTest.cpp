/*****************************************************************************\
* (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/ODIN.h"

#include "Gaudi/Accumulators.h"
#include "Gaudi/Algorithm.h"
#include "GaudiKernel/DataObjectHandle.h"
#include "GaudiKernel/IEventTimeDecoder.h"
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

#include <memory>

//-----------------------------------------------------------------------------
// Implementation file for class : RunChangeTest
//
// 2008-07-24 : Marco CLEMENCIC
//-----------------------------------------------------------------------------
using namespace LHCb;

namespace {
  std::vector<std::string> test_cases{
      "Define initial condition.",
      "Test: same run, flagging -> filtering",              // 1->2
      "Test: new run,  always filtering",                   // 2->3
      "Test: new run,  filtering -> flagging",              // 3->4
      "Test: new run,  always flagging",                    // 4->5
      "Test: new run,  flagging -> filtering",              // 5->6
      "Test: same run, filtering -> flagging (no trigger)", // 6->7 FIXME: correct?
      "Test: same run, filtering (stable, no trigger)",     // 7->
  };
}

namespace LHCbAlgsTests {

  /**
   *  Simple algorithm to test the RunChange incident.
   *  During the execute, it generates an ODIN object with incremented run number
   *  and calls the standard OdinTimeDecoder, which detects the change of run
   *  number and fires the incident.
   *
   *  Used in the test "lhcbalgs.runchange".
   *
   *  @author Marco CLEMENCIC
   *  @date   2008-07-24
   */
  struct RunChangeTest final : Gaudi::Algorithm {
    using Algorithm::Algorithm;

    StatusCode execute( const EventContext& ) const override;

    /// used to count the number of calls to decide which case to test
    mutable std::atomic<size_t>       m_counter{0};
    DataObjectWriteHandle<LHCb::ODIN> m_odinHandle{this, "ODIN", LHCb::ODINLocation::Default};
    ToolHandle<IEventTimeDecoder>     m_eventTimeDecoder{this, "TimeDecoder", "OdinTimeDecoder"};
  };

  /**
   * Empyt algorithm handling the incident created by RunChangeTest and printing a message
   */
  struct RunChangeIncidentHandler final : extends<Gaudi::Algorithm, IIncidentListener> {
    using extends::extends;

    StatusCode initialize() override {
      return extends::initialize().andThen( [&]() { m_incSvc->addListener( this, "RunChange" ); } );
    }

    StatusCode execute( const EventContext& ) const override { return StatusCode::SUCCESS; }

    StatusCode finalize() override {
      m_incSvc->removeListener( this, "RunChange" );
      return extends::finalize();
    }

    /// Handle the ChangeRun incident
    void handle( const Incident& incident ) override {
      info() << incident.type() << " incident received from " << incident.source() << endmsg;
      auto odin = m_odinHandle.get();
      info() << "Run " << odin->runNumber() << ", " << ( odin->isFlagging() ? "flagging" : "filtering" ) << endmsg;
    }

    DataObjectReadHandle<LHCb::ODIN> m_odinHandle{this, "ODIN", LHCb::ODINLocation::Default};
    ServiceHandle<IIncidentSvc>      m_incSvc{this, "IncidentSvc", "IncidentSvc"};
  };

  DECLARE_COMPONENT( RunChangeTest )
  DECLARE_COMPONENT( RunChangeIncidentHandler )

  StatusCode RunChangeTest::execute( const EventContext& ) const {
    auto odin  = std::make_unique<ODIN>();
    auto count = m_counter++;
    info() << test_cases[std::min( count, test_cases.size() - 1 )] << endmsg;
    switch ( count ) {
    case 0: // run 1, flagging
      odin->setRunNumber( 1 );
      odin->setEventType( ODIN::EventTypes::HltFlaggingMode );
      break;
    case 1: // run 1, filtering
      odin->setRunNumber( 1 );
      odin->setEventType( 0x0000 );
      break;
    case 2: // run 2, filtering
      odin->setRunNumber( 2 );
      odin->setEventType( 0x0000 );
      break;
    case 3: // run 3, flagging
      odin->setRunNumber( 3 );
      odin->setEventType( ODIN::EventTypes::HltFlaggingMode );
      break;
    case 4: // run 4, flagging
      odin->setRunNumber( 4 );
      odin->setEventType( ODIN::EventTypes::HltFlaggingMode );
      break;
    case 5: // run 5, filtering
      odin->setRunNumber( 5 );
      odin->setEventType( 0x0000 );
      break;
    case 6: // run 5, flagging
      odin->setRunNumber( 5 );
      odin->setEventType( ODIN::EventTypes::HltFlaggingMode );
      break;
    default: // run 5, filtering
      odin->setRunNumber( 5 );
      odin->setEventType( 0x0000 ); // FIXME
      break;
    }
    m_odinHandle.put( std::move( odin ) );
    // will not try to decode the ODIN bank, but issue a ChangeRun
    m_eventTimeDecoder->getTime();
    return StatusCode::SUCCESS;
  }

} // namespace LHCbAlgsTests
