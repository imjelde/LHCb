###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Test that LHCbApp can be used to write out ntuples (ROOT files)."""
from Configurables import ApplicationMgr, LHCbApp, TupleAlg
from Gaudi.Configuration import ERROR

LHCbApp().DataType = "Upgrade"
LHCbApp().EvtMax = 100
LHCbApp().Simulation = True
LHCbApp().TupleFile = "ntuple.root"

ApplicationMgr().EvtSel = "NONE"
ApplicationMgr().ExtSvc += ["RndmGenSvc"]
# Suppress warnings of the type "Tuple 'Types Test Column Wise' 'long' has
# different sizes on 32/64 bit systems."
ApplicationMgr().TopAlg = [TupleAlg("Tuple", OutputLevel=ERROR)]
