###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
Kernel/HltInterfaces
--------------------
#]=======================================================================]

gaudi_add_library(HltInterfaces
    SOURCES
        src/ConfigTreeNode.cpp
        src/ConfigTreeNodeAlias.cpp
        src/PropertyConfig.cpp
        src/ReadRoutingBits.cpp
        src/TCK.cpp
    LINK
        PUBLIC
            Boost::headers
            Gaudi::GaudiKernel
            LHCb::DAQEventLib
            LHCb::HltEvent
            LHCb::LHCbMathLib
)

gaudi_add_dictionary(HltInterfacesDict
    HEADERFILES dict/HltInterfacesDict.h
    SELECTION dict/HltInterfacesDict.xml
    LINK LHCb::HltInterfaces
)
