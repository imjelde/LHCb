/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawEvent.h"
#include "Event/VPLightCluster.h"
#include "GaudiKernel/Transform3DTypes.h"
#include "Kernel/VPChannelID.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Transformer.h"
#include "VPDet/DeVP.h"
#include "VPKernel/PixelUtils.h"
#include "VPKernel/VeloPixelInfo.h"
#include <array>
#include <iomanip>
#include <tuple>
#include <vector>

namespace VPRetina {
  enum class ErrorCode : StatusCode::code_t {
    BANK_VERSION_UNKNOWN,
    EMPTY_BANK,
    BANK_SIZE_INCONSISTENT,
    BAD_MAGIC,
    BAD_SOURCE_ID
  };
  struct ErrorCategory : StatusCode::Category {
    const char* name() const override { return "VPRetinaDecoder"; }
    bool        isRecoverable( StatusCode::code_t ) const override { return false; }
    std::string message( StatusCode::code_t code ) const override {
      switch ( static_cast<VPRetina::ErrorCode>( code ) ) {
      case ErrorCode::BANK_VERSION_UNKNOWN:
        return "Bank version unknown";
      case ErrorCode::EMPTY_BANK:
        return "Empty bank";
      case ErrorCode::BANK_SIZE_INCONSISTENT:
        return "Bank size and declared number of clusters inconsistent";
      case ErrorCode::BAD_MAGIC:
        return "Wrong magic number for bank";
      case ErrorCode::BAD_SOURCE_ID:
        return "Source ID not in the expected range";
      default:
        return StatusCode::default_category().message( code );
      }
    }
  };
} // namespace VPRetina
STATUSCODE_ENUM_DECL( VPRetina::ErrorCode )
STATUSCODE_ENUM_IMPL( VPRetina::ErrorCode, VPRetina::ErrorCategory )

[[gnu::noreturn]] void throw_exception( VPRetina::ErrorCode ec, const char* tag ) {
  auto sc = StatusCode( ec );
  throw GaudiException{sc.message(), tag, std::move( sc )};
}
#define OOPS( x ) throw_exception( x, __PRETTY_FUNCTION__ )

// Namespace for locations in TES
namespace LHCb {
  namespace VPClusterLocation {
    inline const std::string Offsets = "Raw/VP/LightClustersOffsets";
  }
} // namespace LHCb

class VPRetinaClusterDecoder
    : public LHCb::Algorithm::MultiTransformer<
          std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>(
              const LHCb::RawEvent&, const DeVP& ),
          LHCb::Algorithm::Traits::usesConditions<DeVP>> {

public:
  /// Standard constructor
  VPRetinaClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator );

  /// Algorithm initialization
  // StatusCode initialize() override;

  /// Algorithm execution
  std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
  operator()( const LHCb::RawEvent&, const DeVP& ) const override;

private:
  std::bitset<VP::NModules>                       m_modulesToSkipMask;
  Gaudi::Property<std::vector<unsigned int>>      m_modulesToSkip{this,
                                                             "ModulesToSkip",
                                                             {},
                                                             [=]( auto& ) {
                                                               m_modulesToSkipMask.reset();
                                                               for ( auto i : m_modulesToSkip )
                                                                 m_modulesToSkipMask.set( i );
                                                             },
                                                             Gaudi::Details::Property::ImmediatelyInvokeHandler{true},
                                                             "List of modules that should be skipped in decoding"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nBanks{this, "Number of banks"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClusters{this, "Number of clusters"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod14{this, "Number of clusters - Mod14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod15{this, "Number of clusters - Mod15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersMod16{this, "Number of clusters - Mod16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersIsolated{this, "Number of clusters from isolated SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflow{this, "Number of clusters from overflowing SPs"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod14{
      this, "Number of clusters from overflowing SPs - Mod 14"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod15{
      this, "Number of clusters from overflowing SPs - Mod 15"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersOverflowMod16{
      this, "Number of clusters from overflowing SPs - Mod 16"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNeighbor{this, "Number of clusters from SPs w/ neighbors"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersEdge{this, "Number of clusters at matrix edge"};
  mutable Gaudi::Accumulators::AveragingCounter<> m_nClustersNotCont{this, "Number of clusters not self contained"};
};

DECLARE_COMPONENT( VPRetinaClusterDecoder )

using namespace Pixel;

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaClusterDecoder::VPRetinaClusterDecoder( const std::string& name, ISvcLocator* pSvcLocator )
    : MultiTransformer( name, pSvcLocator,
                        {KeyValue{"RawEventLocation", LHCb::RawEventLocation::VeloCluster},
                         KeyValue{"DEVP", LHCb::Det::VP::det_path}},
                        {KeyValue{"ClusterLocation", LHCb::VPClusterLocation::Light},
                         KeyValue{"ClusterOffsets", LHCb::VPClusterLocation::Offsets}} ) {}

//=============================================================================
// Main execution
//=============================================================================
std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>
VPRetinaClusterDecoder::operator()( const LHCb::RawEvent& rawEvent, const DeVP& devp ) const {
  auto result = std::tuple<std::vector<LHCb::VPLightCluster>, std::array<unsigned, VeloInfo::Numbers::NOffsets>>{};
  auto& [clusters, offsets] = result;

  const auto& banks = rawEvent.banks( LHCb::RawBank::VPRetinaCluster );
  m_nBanks += banks.size();
  if ( banks.empty() ) return result;

  const unsigned int version = banks[0]->version();
  if ( version != 2 ) OOPS( VPRetina::ErrorCode::BANK_VERSION_UNKNOWN );

  // Since 'clusters` is local, to first preallocate, then count hits per module,
  // and then preallocate per module and move hits might not be faster than adding
  // directly to the PixelModuleHits (which would require more allocations, but
  // not many if we start with a sensible default)
  constexpr unsigned int startSize = 10000U;
  clusters.reserve( startSize );

  uint32_t nclusterMod14         = 0;
  uint32_t nclusterMod15         = 0;
  uint32_t nclusterMod16         = 0;
  uint32_t nclusterIsolated      = 0;
  uint32_t nclusterNeighbor      = 0;
  uint32_t nclusterOverflow      = 0;
  uint32_t nclusterOverflowMod14 = 0;
  uint32_t nclusterOverflowMod15 = 0;
  uint32_t nclusterOverflowMod16 = 0;
  uint32_t nclusterEdge          = 0;
  uint32_t nclusterNotCont       = 0;

  // Loop over VP RawBanks
  for ( const auto* bank : banks ) {
    if ( LHCb::RawBank::MagicPattern != bank->magic() ) OOPS( VPRetina::ErrorCode::BAD_MAGIC );

    if ( static_cast<unsigned int>( bank->sourceID() ) >= VP::NSensors ) OOPS( VPRetina::ErrorCode::BAD_SOURCE_ID );
    const auto         sensor = LHCb::VPChannelID::SensorID( bank->sourceID() );
    const unsigned int module = to_unsigned( sensor ) % VP::NSensorsPerModule + 1;
    if ( m_modulesToSkipMask[module - 1] ) continue;

    auto data = bank->range<uint32_t>();
    if ( data.empty() ) OOPS( VPRetina::ErrorCode::EMPTY_BANK );

    const uint32_t ncluster = data[0];
    if ( data.size() != ncluster + 1 ) OOPS( VPRetina::ErrorCode::BANK_SIZE_INCONSISTENT );
    data = data.subspan( 1 );

    const auto& ltg = devp.ltg( sensor );

    // Read clusters
    std::transform( data.begin(), data.end(), std::back_inserter( clusters ),
                    [&]( const uint32_t cluster_word ) -> LHCb::VPLightCluster {
                      const uint32_t cx   = ( cluster_word >> 14 ) & 0x3FF;
                      const float    fx   = ( ( cluster_word >> 11 ) & 0x7 ) / 8.f;
                      const auto     cy   = LHCb::VPChannelID::RowID{( cluster_word >> 3 ) & 0xFF};
                      const float    fy   = ( cluster_word & 0x7 ) / 8.f;
                      const auto     chip = LHCb::VPChannelID::ChipID{cx / CHIP_COLUMNS};
                      const auto     ccol = LHCb::VPChannelID::ColumnID{cx % CHIP_COLUMNS};

                      const float local_x = devp.local_x( cx ) + fx * devp.x_pitch( cx );
                      const float local_y = ( to_unsigned( cy ) + 0.5 + fy ) * devp.pixel_size();

                      const float gx = ( ltg[0] * local_x + ltg[1] * local_y + ltg[9] );
                      const float gy = ( ltg[3] * local_x + ltg[4] * local_y + ltg[10] );
                      const float gz = ( ltg[6] * local_x + ltg[7] * local_y + ltg[11] );

                      const auto mod = to_unsigned( sensor ) / VP::NSensorsPerModule;
                      switch ( mod ) {
                      case 14:
                        nclusterMod14 += 1;
                        break;
                      case 15:
                        nclusterMod15 += 1;
                        break;
                      case 16:
                        nclusterMod16 += 1;
                        break;
                      default: /* nothing */;
                      }

                      if ( ( cluster_word >> 30 ) && ( ( cluster_word << 3 ) >> 31 ) ) { nclusterIsolated += 1; }

                      if ( ( cluster_word >> 30 ) && !( ( cluster_word << 3 ) >> 31 ) ) {
                        nclusterOverflow += 1;
                        switch ( mod ) {
                        case 14:
                          nclusterOverflowMod14 += 1;
                          break;
                        case 15:
                          nclusterOverflowMod15 += 1;
                          break;
                        case 16:
                          nclusterOverflowMod16 += 1;
                          break;
                        default: /* nothing */;
                        }
                      }

                      if ( !( cluster_word >> 30 ) ) { nclusterNeighbor += 1; }

                      if ( !( cluster_word >> 30 ) && ( ( cluster_word << 3 ) >> 31 ) ) { nclusterEdge += 1; }

                      if ( !( cluster_word >> 30 ) && !( ( cluster_word << 2 ) >> 31 ) ) { nclusterNotCont += 1; }

                      return {1, 1, gx, gy, gz, LHCb::VPChannelID{sensor, chip, ccol, cy}};
                    } );
    offsets[module] += ncluster;
  } // loop over all banks

  std::partial_sum( offsets.begin(), offsets.end(), offsets.begin() );

  // sorting in phi for even modules
  auto cmp_phi_for_odd_modules = []( const LHCb::VPLightCluster& a, const LHCb::VPLightCluster& b ) {
    return ( a.y() < 0.f && b.y() > 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  // sorting in phi for odd modules
  auto cmp_phi_for_even_modules = []( const LHCb::VPLightCluster& a, const LHCb::VPLightCluster& b ) {
    return ( a.y() > 0.f && b.y() < 0.f ) ||
           // same y side even and odd modules, check y1/x1 < y2/x2
           ( ( a.y() * b.y() ) > 0.f && ( a.y() * b.x() < b.y() * a.x() ) );
  };

  auto sort_module = [clusters = std::ref( clusters ), offsets = std::ref( offsets )]( auto id, auto cmp ) {
    std::sort( clusters.get().begin() + offsets.get()[id], clusters.get().begin() + offsets.get()[id + 1], cmp );
  };

  for ( size_t moduleID = 0; moduleID < VeloInfo::Numbers::NModules; ++moduleID ) {
    // if( msgLevel(MSG::DEBUG)){
    //   debug()<<"Sorting hits in moduleID by phi, usign x,y information "<<moduleID<<endmsg;
    // }
    // In even modules you fall in the branching at -180, 180 degrees, you want to do that continuos
    if ( moduleID % 2 == 1 ) {
      sort_module( moduleID, cmp_phi_for_odd_modules );
    } else {
      sort_module( moduleID, cmp_phi_for_even_modules );
    }
  }

  m_nClusters += offsets.back();
  m_nClustersMod14 += nclusterMod14;
  m_nClustersMod15 += nclusterMod15;
  m_nClustersMod16 += nclusterMod16;
  m_nClustersIsolated += nclusterIsolated;
  m_nClustersOverflow += nclusterOverflow;
  m_nClustersOverflowMod14 += nclusterOverflowMod14;
  m_nClustersOverflowMod15 += nclusterOverflowMod15;
  m_nClustersOverflowMod16 += nclusterOverflowMod16;
  m_nClustersNeighbor += nclusterNeighbor;
  m_nClustersEdge += nclusterEdge;
  m_nClustersNotCont += nclusterNotCont;

  return result;
}
