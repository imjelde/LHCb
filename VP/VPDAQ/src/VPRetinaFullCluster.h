/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef VPRETINAFULLCLUSTER_H
#define VPRETINAFULLCLUSTER_H 1

#include "Kernel/VPChannelID.h"
#include <cstdint>
#include <vector>

/*
 * @author Federico Lazzari
 * @date   2018-06-22
 */

namespace LHCb {

  class VPRetinaFullCluster final {
  public:
    /// Constructor
    VPRetinaFullCluster( const uint32_t word, std::vector<LHCb::VPChannelID> channelIDs )
        : m_word( word ), m_channelIDs( std::move( channelIDs ) ) {}

    ///
    uint32_t word() const noexcept { return m_word; }

    ///
    const std::vector<LHCb::VPChannelID>& channelIDs() const noexcept { return m_channelIDs; }

  private:
    uint32_t                       m_word;       /// RetinaCluster value
    std::vector<LHCb::VPChannelID> m_channelIDs; /// ID of pixel used in RetinaCluster
  };                                             // class VPRetinaFullCluster

  /// lightweight container for VPRetinaFullCluster
  using VPRetinaFullClusters = std::vector<VPRetinaFullCluster>;

} // namespace LHCb

#endif // VPRETINAFULLCLUSTER_H
