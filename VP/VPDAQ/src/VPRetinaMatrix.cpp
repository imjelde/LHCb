/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// LHCb
#include "Event/State.h"
#include "VPKernel/PixelUtils.h"

// Local
#include "VPRetinaMatrix.h"

namespace {
  constexpr auto to_chip( uint32_t id_X ) { return LHCb::VPChannelID::ChipID{id_X / Pixel::CHIP_COLUMNS}; }
  constexpr auto to_column( uint32_t id_X ) { return LHCb::VPChannelID::ColumnID{id_X % Pixel::CHIP_COLUMNS}; }

  // if sensor number % 4 is 0 or 3 apply a direct cluster search pattern
  constexpr bool useDirectSearch( LHCb::VPChannelID::SensorID id ) {
    return ( to_unsigned( id ) % 4 == 0 ) || ( to_unsigned( id ) % 4 == 3 );
  }
} // namespace

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
VPRetinaMatrix::VPRetinaMatrix( uint32_t SP_row, uint32_t SP_col, uint8_t SP_pixel, LHCb::VPChannelID::SensorID sensor )
    : Coordinate_Retina_row( SP_row - 1 ), Coordinate_Retina_col( SP_col - 1 ), Sensor( sensor ) {
  // record SP pixels.
  for ( uint32_t shift = 0; shift < 8; ++shift ) {
    const uint8_t pixel = SP_pixel & 1;
    if ( pixel ) {
      const uint32_t row             = 4 + shift % 4;
      const uint32_t col             = 2 + shift / 4;
      Pixel_Matrix[row + 1][col + 2] = pixel;
    }
    SP_pixel = SP_pixel >> 1;
    if ( 0 == SP_pixel ) break;
  }

  // record which SP is added
  SPixel_Matrix[SP_row - Coordinate_Retina_row][SP_col - Coordinate_Retina_col] = 1;
}

//=============================================================================
// Check if a SP coordinate are inside the Retina
//=============================================================================
bool VPRetinaMatrix::IsInRetina( uint32_t SP_row, uint32_t SP_col ) const {
  return Coordinate_Retina_row <= (int32_t)SP_row && (int32_t)SP_row < Coordinate_Retina_row + 3 &&
         Coordinate_Retina_col <= (int32_t)SP_col && (int32_t)SP_col < Coordinate_Retina_col + 3;
}

//=============================================================================
// Add a SP to the Retina
//=============================================================================
VPRetinaMatrix& VPRetinaMatrix::AddSP( uint32_t SP_row, uint32_t SP_col, uint8_t SP_pixel ) {
  // record which SP is added
  SPixel_Matrix[SP_row - Coordinate_Retina_row][SP_col - Coordinate_Retina_col] = 1;

  // record SP pixels.
  for ( uint32_t shift = 0; shift < 8; ++shift ) {
    const uint8_t pixel = SP_pixel & 1;
    if ( pixel ) {
      const int32_t row              = ( SP_row - Coordinate_Retina_row ) * 4 + shift % 4;
      const int32_t col              = ( SP_col - Coordinate_Retina_col ) * 2 + shift / 4;
      Pixel_Matrix[row + 1][col + 2] = pixel;
    }
    SP_pixel = SP_pixel >> 1;
    if ( 0 == SP_pixel ) break;
  }
  return *this;
}

//=============================================================================
// Search cluster - Accounting for mirror geometry in sensors 1 and 2
//=============================================================================

std::vector<uint32_t> VPRetinaMatrix::SearchCluster() const {
  std::vector<uint32_t> retinaCluster;
  retinaCluster.reserve( 63 );

  // if sensor number % 4 is 0 or 3 apply a direct cluster search pattern
  if ( useDirectSearch( Sensor ) ) {
    for ( unsigned int iX = 0 + 2; iX < 8; ++iX )
      for ( unsigned int iY = 0 + 1; iY < 12 + 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY - 1][iX] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          // compute flags
          if ( iX >= 5 || iY >= 10 || iX == 1 || iY == 1 ) {

            edge = 1;

          } else {
            if ( Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                 Pixel_Matrix[iY + 2][iX + 3] == 0 && Pixel_Matrix[iY + 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY][iX + 3] == 0 && Pixel_Matrix[iY - 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY - 1][iX + 2] == 0 ) {

              self_cont = 1;
            }
          }
          // find cluster center
          if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
               ( Pixel_Matrix[iY + 1][iX] == 0 ) ) {
            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
            for ( unsigned int iiY = 0; iiY < 3; ++iiY )
              if ( Pixel_Matrix[iY + iiY][iX] == 1 ) {
                shift_row += iiY;
                n++;
              }
            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                shift_col += iiX;
                n++;
              }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          }
        }
      }
    // if sensor number % 4 is 1 or 2 apply a reverse cluster search pattern
  } else {
    for ( unsigned int iX = 0; iX < 6; ++iX )
      for ( unsigned int iY = 1; iY < 12 + 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX + 2] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX + 2] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 2 - 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX + 1 + 2] == 0 && Pixel_Matrix[iY + 1][iX + 1 + 2] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1 + 2] == 0 && Pixel_Matrix[iY - 1][iX + 2] == 0 &&
             Pixel_Matrix[iY - 1][iX - 1 + 2] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          // compute flags
          if ( iX <= 2 || iY == 1 || iX == 5 || iY >= 10 ) {

            edge = 1;

          } else {
            if ( Pixel_Matrix[iY - 1][iX] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 && Pixel_Matrix[iY][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY + 2][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 1] == 0 && Pixel_Matrix[iY + 3][iX + 2] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 3] == 0 && Pixel_Matrix[iY + 2][iX + 3] == 0 ) {

              self_cont = 1;
            }
          }
          // find cluster center
          if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
               ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 - 2 ) << 3 );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
            for ( unsigned int iiY = 0; iiY < 3; ++iiY )
              if ( Pixel_Matrix[iY + iiY][iX + 2] == 1 ) {
                shift_row += iiY;
                n++;
              }
            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 - 2 ) << 3 );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                shift_col += iiX;
                n++;
              }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          } else {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY );
          }
        }
      }
  }
  return retinaCluster;
}
// same operations as before but reconstructing FullClusters instead of LightClusters
std::vector<LHCb::VPRetinaFullCluster> VPRetinaMatrix::SearchFullCluster() const {
  std::vector<LHCb::VPRetinaFullCluster> retinaCluster;
  retinaCluster.reserve( 63 );

  std::vector<LHCb::VPChannelID> channelID;
  channelID.reserve( 9 );
  if ( useDirectSearch( Sensor ) ) {
    for ( unsigned int iX = 0 + 2; iX < 8; ++iX )
      for ( unsigned int iY = 0 + 1; iY < 12 + 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX - 1] == 0 && Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY - 1][iX] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          if ( iX >= 5 || iY >= 10 || iX == 1 || iY == 1 ) {
            edge = 1;
          } else {
            if ( Pixel_Matrix[iY + 2][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX] == 0 && Pixel_Matrix[iY + 3][iX + 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 2] == 0 && Pixel_Matrix[iY + 3][iX + 3] == 0 &&
                 Pixel_Matrix[iY + 2][iX + 3] == 0 && Pixel_Matrix[iY + 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY][iX + 3] == 0 && Pixel_Matrix[iY - 1][iX + 3] == 0 &&
                 Pixel_Matrix[iY - 1][iX + 2] == 0 ) {

              self_cont = 1;
            }
          }

          if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
               ( Pixel_Matrix[iY + 1][iX] == 0 ) ) {
            const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2;
            const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1};
            channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );

            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
            for ( unsigned int iiY = 0; iiY < 3; ++iiY )
              if ( Pixel_Matrix[iY + iiY][iX] == 1 ) {
                const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2;
                const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1 + iiY};
                channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                shift_row += iiY;
                n++;
              }
            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2 + iiX;
                const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1};
                channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                shift_col += iiX;
                n++;
              }

            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2 + iiX;
                  const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1 + iiY};
                  channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }
            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();
          }
        }
      }
  } else {
    for ( unsigned int iX = 0; iX < 6; ++iX )
      for ( unsigned int iY = 1; iY < 12 + 1; ++iY ) {
        if ( ( ( Pixel_Matrix[iY][iX + 2] == 1 ) ||
               ( ( ( Pixel_Matrix[iY + 1][iX + 2] == 1 ) ) && ( ( Pixel_Matrix[iY][iX + 2 - 1] == 1 ) ) ) ) &&
             Pixel_Matrix[iY][iX + 1 + 2] == 0 && Pixel_Matrix[iY + 1][iX + 1 + 2] == 0 &&
             Pixel_Matrix[iY - 1][iX + 1 + 2] == 0 && Pixel_Matrix[iY - 1][iX + 2] == 0 &&
             Pixel_Matrix[iY - 1][iX - 1 + 2] == 0 ) {

          uint32_t shift_col = 0;
          uint32_t shift_row = 0;
          uint32_t n         = 0;
          uint8_t  edge      = 0;
          uint8_t  self_cont = 0;

          if ( iX <= 2 || iY == 1 || iX == 5 || iY >= 10 ) {
            edge = 1;
          } else {
            if ( Pixel_Matrix[iY - 1][iX] == 0 && Pixel_Matrix[iY - 1][iX - 1] == 0 && Pixel_Matrix[iY][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 1][iX - 1] == 0 && Pixel_Matrix[iY + 2][iX - 1] == 0 &&
                 Pixel_Matrix[iY + 3][iX - 1] == 0 && Pixel_Matrix[iY + 3][iX] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 1] == 0 && Pixel_Matrix[iY + 3][iX + 2] == 0 &&
                 Pixel_Matrix[iY + 3][iX + 3] == 0 && Pixel_Matrix[iY + 2][iX + 3] == 0 ) {

              self_cont = 1;
            }
          }

          if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
               ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            const uint32_t id_X = Coordinate_Retina_col * 2 + iX + 2 - 2;
            const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1};
            channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );

            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 - 2 ) << 3 );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );

            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else if ( ( Pixel_Matrix[iY][iX + 1] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 2][iX + 1] == 0 ) ) {
            for ( unsigned int iiY = 0; iiY < 3; ++iiY )
              if ( Pixel_Matrix[iY + iiY][iX + 2] == 1 ) {
                const uint32_t id_X = Coordinate_Retina_col * 2 + iX + 2 - 2;
                const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1 + iiY};
                channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                shift_row += iiY;
                n++;
              }

            const uint64_t cX = ( ( Coordinate_Retina_col * 2 + iX + 2 - 2 ) << 3 );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else if ( ( Pixel_Matrix[iY + 1][iX] == 0 ) && ( Pixel_Matrix[iY + 1][iX + 1] == 0 ) &&
                      ( Pixel_Matrix[iY + 1][iX + 2] == 0 ) ) {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              if ( Pixel_Matrix[iY][iX + iiX] == 1 ) {
                const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2 + iiX;
                const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1};
                channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                shift_col += iiX;
                n++;
              }

            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY = ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 );
            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();

          } else {
            for ( unsigned int iiX = 0; iiX < 3; ++iiX )
              for ( unsigned int iiY = 0; iiY < 3; ++iiY )
                if ( Pixel_Matrix[iY + iiY][iX + iiX] == 1 ) {
                  const uint32_t id_X = Coordinate_Retina_col * 2 + iX - 2 + iiX;
                  const auto     id_Y = LHCb::VPChannelID::RowID{Coordinate_Retina_row * 4 + iY - 1 + iiY};
                  channelID.emplace_back( Sensor, to_chip( id_X ), to_column( id_X ), id_Y );
                  shift_col += iiX;
                  shift_row += iiY;
                  n++;
                }

            const uint64_t cX =
                ( ( Coordinate_Retina_col * 2 + iX - 2 ) << 3 ) + ( uint64_t )( ( ( shift_col << 3 ) + n / 2 ) / n );
            const uint64_t cY =
                ( ( Coordinate_Retina_row * 4 + iY - 1 ) << 3 ) + ( uint64_t )( ( ( shift_row << 3 ) + n / 2 ) / n );

            retinaCluster.emplace_back( self_cont << 29 | edge << 28 | cX << 11 | cY, channelID );
            channelID.clear();
          }
        }
      }
  }
  return retinaCluster;
}
