###############################################################################
# (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[========================================================================[.rst:
FindDataPackage
===============

Provide a function to locate LHCb data packages.
#]========================================================================]

include_guard(GLOBAL) # Protect from multiple include (global scope, because
                      # everything defined in this file is globally visible)

# === helper functions and modules ===
include(FindPackageHandleStandardArgs)
# convert a version string from CMake to LHCb style
function(cmake_to_lhcb_version version variable)
    string(REGEX MATCHALL "[0-9]+" parts "${version}")
    set(out)
    set(letters v r p t)
    while(parts AND letters)
        list(POP_FRONT parts number)
        list(POP_FRONT letters letter)
        set(out "${out}${letter}${number}")
    endwhile()
    set(${variable} "${out}" PARENT_SCOPE)
endfunction()
# convert a version string from LHCb to CMake style
macro(lhcb_to_cmake_version version variable)
    string(REGEX REPLACE "^v" "" ${variable} "${version}")
    string(REGEX REPLACE "[a-z]+" "." ${variable} "${${variable}}")
endmacro()


#[========================================================================[.rst:
.. cmake:command:: find_data_package

    .. code-block:: cmake
    
      find_data_package(<PackageName> [version] [REQUIRED])
    
    Look for a data package called ``<PackageName>``, basically a directory
    named::
    
      <prefix>/<PackageName>/<version>
    
    containing a file called ``<name>.xenv`` (or ``<name>Environment.xml`` for
    backward compatibility) where ``<name>`` is the same as ``<PackageName>``
    with ``/`` replaced by ``_``.
    
    The optional ``[version]`` argument should have the standard CMake version
    format (``major[.minor[.patch[.tweak]]]``) and it will be used to match the
    conventional LHCb version strings ``v<major>r<minor>[p<patch]``.
    
    If a the package is found the variable ``<PackageName>_FOUND`` is set to
    TRUE (otherwise is set to FALSE) and then we set the variables:
    
    ``<PackageName>_VERSION``
      the version of the data package that was selected
    
    ``<PackageName>_ROOT_DIR``
      the base directory of the package
    
    We also add the package to the global property ``DATA_PACKAGES_FOUND``.
#]========================================================================]
function(find_data_package name)
    message(DEBUG "find_data_package(${ARGV})")
    # this is to please find_package_handle_standard_args
    set(CMAKE_FIND_PACKAGE_NAME ${name})

    # parse optional arguments
    set(required NO)
    set(${name}_FIND_VERSION)
    if(ARGC EQUAL 2)
        if(ARGV1 STREQUAL "REQUIRED")
            set(required YES)
        else()
            set(${name}_FIND_VERSION ${ARGV1})
        endif()
    elseif(ARGC EQUAL 3)
        set(${name}_FIND_VERSION ${ARGV1})
        if(ARGV2 STREQUAL "REQUIRED")
            set(required YES)
        else()
            message(FATAL_ERROR "invalid argumet ${ARGV2}")
        endif()
    elseif(NOT ARGC EQUAL 1)
        message(FATAL_ERROR "invalid argumets")
    endif()

    if(NOT ${name}_FOUND)
        string(REPLACE "/" "_" envname ${name})

        # FIXME: honor https://cmake.org/cmake/help/latest/policy/CMP0074.html

        if(${name}_FIND_VERSION)
            if(NOT ${name}_FIND_VERSION MATCHES "^[0-9.]+\$")
                message(FATAL_ERROR "invalid version string ${${name}_FIND_VERSION}")
            endif()
            string(REGEX MATCH "^[0-9]+" version_upper_bound "${${name}_FIND_VERSION}")
            math(EXPR version_upper_bound "${version_upper_bound} + 1")
        else()
            set(version_upper_bound 99999)
        endif()

        # Note: it works even if the env. var. is not set.
        file(TO_CMAKE_PATH "$ENV{CMTPROJECTPATH}" projects_search_path)
        file(TO_CMAKE_PATH "$ENV{CMAKE_PREFIX_PATH}" env_prefix_path)

        set(candidates)
        foreach(prefix IN LISTS CMAKE_PREFIX_PATH env_prefix_path)
            foreach(suffix IN ITEMS "" "DBASE" "PARAM" "EXTRAPACKAGES")
                message(DEBUG "find_data_package: check ${prefix}/${suffix}/${name}")
                if(IS_DIRECTORY ${prefix}/${suffix}/${name})
                    message(DEBUG "find_data_package: scanning ${prefix}/${suffix}/${name}")
                    # Look for env files with the matching version.
                    file(GLOB envfiles RELATIVE ${prefix}/${suffix}/${name}
                        ${prefix}/${suffix}/${name}/*/${envname}.xenv
                        ${prefix}/${suffix}/${name}/*/${envname}Environment.xml)
                    # Translate the list of env files into the list of available versions
                    # (directories)
                    foreach(f IN LISTS envfiles)
                        get_filename_component(f ${f} DIRECTORY)
                        lhcb_to_cmake_version(${f} v)
                        if(v VERSION_GREATER_EQUAL ${name}_FIND_VERSION AND v VERSION_LESS version_upper_bound)
                            message(DEBUG "find_data_package: new candidate ${v} -> ${prefix}/${suffix}/${name}/${f}")
                            list(APPEND candidates ${v} ${prefix}/${suffix}/${name}/${f})
                        endif()
                    endforeach()
                endif()
            endforeach()
        endforeach()

        set(${name}_VERSION)
        set(${name}_ROOT_DIR)
        while(candidates)
            list(POP_FRONT candidates v p)
            if(v VERSION_GREATER ${name}_VERSION)
                set(${name}_VERSION ${v})
                set(${name}_ROOT_DIR ${p})
            endif()
        endwhile()

        find_package_handle_standard_args(${name}
            REQUIRED_VARS ${name}_ROOT_DIR
            VERSION_VAR ${name}_VERSION
        )
        if(${name}_FOUND)
            set(${name}_FOUND "${${name}_FOUND}" CACHE INTERNAL "")
            set(${name}_VERSION "${${name}_VERSION}" CACHE STRING "Version of ${name} found")
            set(${name}_ROOT_DIR "${${name}_ROOT_DIR}" CACHE PATH "Base directory of ${name}")
        endif()
    endif()
    if(${name}_FOUND AND ${name}_FIND_VERSION)
        set_property(GLOBAL APPEND PROPERTY DATA_PACKAGES_FOUND ${name}:${${name}_FIND_VERSION})
    else()
        set_property(GLOBAL APPEND PROPERTY DATA_PACKAGES_FOUND ${name})
    endif()
endfunction()
