#!/usr/bin/env python
###############################################################################
# (c) Copyright 2022 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""Script to convert data to MDF format.

Example usage:

    mdfwriter.py -h
    mdfwriter.py -n 10 -o output.mdf /path/to/file.xdigi

"""
import argparse
import os
import tempfile

from Gaudi.Configuration import WARNING, INFO
from PRConfig.TestFileDB import test_file_db
from Configurables import LHCbApp, CondDB
from Configurables import (EventSelector, IODataManager, ApplicationMgr,
                           Gaudi__MultiFileCatalog)
from Configurables import LHCb__MDFWriter as MDFWriter
from GaudiConf import IOExtension

parser = argparse.ArgumentParser(description='Convert data to MDF format')
parser.add_argument(
    "files",
    nargs="+",
    help="Input filenames or single TestFileDB entry (requires --db-entry)")
parser.add_argument("-o", "--output", required=True, help="Output filename")
parser.add_argument(
    "-f",
    "--force",
    action="store_true",
    help="Overwrite output file if it exists")
parser.add_argument(
    "--db-entry",
    dest="db_entry",
    action="store_true",
    help="Interpret the file as a TestFileDB entry")
parser.add_argument(
    "--start", type=int, default=0, help="First event to write")
group = parser.add_mutually_exclusive_group()
group.add_argument(
    "-n", "--nevents", type=int, help="Number of events to write")
group.add_argument("--stop", type=int, help="Last event to write (exclusive)")
args = parser.parse_args()
if os.path.exists(args.output) and not args.force:
    parser.error(
        "output already exists (use --force to overwrite existing file): " +
        args.output)
if args.stop is not None:
    args.nevents = args.stop - args.start
if args.nevents is None:
    args.nevents = -1

lhcb_app = LHCbApp(
    SkipEvents=args.start,
    DataType='Upgrade',  # doesn't really matter, but just have something
)
EventSelector().PrintFreq = 10000
# Deal with our software's quirks
CondDB().EnableRunStampCheck = False
IODataManager().DisablePFNWarning = True
with tempfile.NamedTemporaryFile(prefix="catalog-", suffix=".xml") as f:
    Gaudi__MultiFileCatalog("FileCatalog").Catalogs = [
        "xmlcatalog_file:" + f.name
    ]

if args.db_entry:
    if len(args.files) > 1:
        parser.error("provide only one input with --db-entry")
    test_file_db[args.files[0]].run(configurable=lhcb_app)
else:
    IOExtension().inputFiles(args.files)

mdf_writer = MDFWriter(
    'MDFWriter',
    OutputLevel=INFO,
    Compress=0,
    ChecksumType=1,
    GenerateMD5=True,
    Connection='file://' + args.output)
ApplicationMgr().TopAlg = [mdf_writer]
ApplicationMgr().OutputLevel = WARNING

import GaudiPython  # noqa : this import is slow...
appmgr = GaudiPython.AppMgr()
appmgr.run(args.nevents)
