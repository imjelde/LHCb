/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/RawBank.h"
#include "Event/RawEvent.h"

#include "LHCbAlgs/Transformer.h"

#include <algorithm>
#include <string>
#include <vector>

//-----------------------------------------------------------------------------
// Implementation file for class : RawEventSelectiveCopy
//
// 2009-06-22 : Tomasz Skwarnicki
//-----------------------------------------------------------------------------

/** @class RawEventSelectiveCopy RawEventSelectiveCopy.h
 *  Copies selected RawBanks to RawEvent at a new TES location.
 *
 *  @author Tomasz Skwarnicki
 *  @date   2009-06-22
 */
class RawEventSelectiveCopy : public LHCb::Algorithm::Transformer<LHCb::RawEvent( const LHCb::RawEvent& )> {
public:
  /// Standard constructor
  RawEventSelectiveCopy( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer{name,
                    pSvcLocator,
                    {"InputRawEventLocation", LHCb::RawEventLocation::Default},
                    {"OutputRawEventLocation", LHCb::RawEventLocation::Copied}} {}

  StatusCode     initialize() override;                              ///< Algorithm initialization
  LHCb::RawEvent operator()( const LHCb::RawEvent& ) const override; ///< Algorithm execution

private:
  Gaudi::Property<std::vector<std::string>> m_banksToCopy{
      this, "RawBanksToCopy", {}, "Create a new RawEvent copying only these banks"};
  Gaudi::Property<std::vector<std::string>> m_banksToRemove{
      this, "RawBanksToRemove", {}, "Create a RawEvent copy, with these banks removed"};

  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_inconsistentProperty{
      this, "Inconsistent properties, set only one of RawBanksToCopy and RawBanksToRemove"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_invalidBankName{this, "Invalid bank name requested"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_removeAllBanks{
      this, "Requested to remove ALL banks from copied event!"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_noEffect{
      this, "Neither RawBanksToCopy nor RawBanksToRemove are set, algorithm has no effect"};

  std::vector<LHCb::RawBank::BankType> m_bankTypes;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawEventSelectiveCopy )

using namespace LHCb;

StatusCode RawEventSelectiveCopy::initialize() {
  return Transformer::initialize().andThen( [&]() {
    const std::vector<std::string>& copyVector   = m_banksToCopy.value();
    const std::vector<std::string>& removeVector = m_banksToRemove.value();

    if ( copyVector.size() > 0 && removeVector.size() > 0 ) {
      ++m_inconsistentProperty;
      return StatusCode::FAILURE;
    }

    m_bankTypes.clear();

    if ( copyVector.size() > 0 ) {
      // all banks?
      if ( copyVector.size() == 1 ) {
        if ( copyVector[0] == "all" || copyVector[0] == "All" || copyVector[0] == "ALL" ) {
          for ( auto i : RawBank::types() ) { m_bankTypes.push_back( i ); }
          if ( msgLevel( MSG::VERBOSE ) )
            verbose() << " All RawBank types will be copied from input to output RawEvent " << endmsg;
          return StatusCode::SUCCESS;
        }
      }
      // convert bankNames to bankTypes
      for ( auto const& bankName : copyVector ) {
        const auto tr = RawBank::types();
        auto i = std::find_if( tr.begin(), tr.end(), [&bankName]( auto t ) { return toString( t ) == bankName; } );
        if ( i != tr.end() ) {
          m_bankTypes.push_back( *i );
        } else {
          error() << "Requested bank '" << bankName << "' is not a valid name" << endmsg;
          ++m_invalidBankName;
          return StatusCode::FAILURE;
        }
      }
    }

    else if ( removeVector.size() > 0 ) {
      // all banks?
      if ( removeVector.size() == 1 ) {
        if ( removeVector[0] == "all" || removeVector[0] == "All" || removeVector[0] == "ALL" ) {
          ++m_removeAllBanks;
          return StatusCode::FAILURE;
        }
      }

      // Check inputs
      for ( auto const& bankName : removeVector ) {
        auto tr = RawBank::types();
        auto i  = std::find_if( tr.begin(), tr.end(), [&bankName]( auto bt ) { return toString( bt ) == bankName; } );
        if ( i == tr.end() ) {
          error() << "Requested bank '" << bankName << "' is not a valid name" << endmsg;
          ++m_invalidBankName;
          return StatusCode::FAILURE;
        }
      }

      // convert bankNames to bankTypes
      for ( auto i : RawBank::types() ) {
        bool found = std::any_of( removeVector.begin(), removeVector.end(),
                                  [name = toString( i )]( const std::string& s ) { return s == name; } );
        if ( !found ) m_bankTypes.push_back( i );
      }
    }

    else {
      ++m_noEffect;
      return StatusCode::SUCCESS;
    }

    if ( msgLevel( MSG::DEBUG ) ) {
      debug() << " RawBank types to be copied = ";
      for ( const auto& b : m_bankTypes ) { debug() << toString( b ) << " "; }
      debug() << endmsg;
    }
    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
LHCb::RawEvent RawEventSelectiveCopy::operator()( const LHCb::RawEvent& rawEvent ) const {
  // create empty output RawEvent
  RawEvent rawEventCopy;
  // copy selected banks
  for ( const auto& ib : m_bankTypes ) {
    const auto& banks = rawEvent.banks( ib );
    for ( const RawBank* b : banks ) {
      rawEventCopy.addBank( b->sourceID(), ib, b->version(), b->range<std::byte>() );
      if ( msgLevel( MSG::VERBOSE ) ) {
        verbose() << " Copied RawBank type= " << toString( ib ) << " version= " << b->version()
                  << " sourceID= " << b->sourceID() << " size (bytes) = " << b->size() << endmsg;
      }
    }
    if ( banks.empty() && msgLevel( MSG::VERBOSE ) ) {
      verbose() << " No banks found of type= " << toString( ib ) << endmsg;
    }
  }
  return rawEventCopy;
}

//=============================================================================
